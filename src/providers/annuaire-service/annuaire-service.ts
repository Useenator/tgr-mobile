//Ahmed

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the AnnuaireServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class AnnuaireServiceProvider {
  public data: any;
  public Url: any;
  public UrlLogin: any;
  public foundResponsibles;
  public foundResponsiblesRegionaux = [
    {
      "id": "af22fba4-7c49-1036-9b1d-81f483ff64fc",
      "firstName": "BOUTAQBOUT",
      "lastName": "ABDELMJID",
      "phone": "0537578815",
      "email": "abdelmjid.boutaqbout@tgr.gov.ma",
      "fax": "0537572217",
      "adresse": "Ilôt 31 (près de l'avenue Al Araar) Hay Riad, Rabat",
      "poste": "DIRECTEUR",
      "direction": "DIVISION DE L AUDIT ET DE L INSPECTION",
      "division": "",
      "poste direct": "8815",
      "secretaire standard": "0537578816",
      "photo": "../assets/img/tgr_photos_responsables/BOUTAQBOUT ABDELMJID.jpeg",
      "lat": 34.0132500,
      "lng": -6.8325500

    },
    {
      "id": "dd7f9afe-8016-1036-9b1e-81f483ff64fc",
      "firstName": "LAGHLAM",
      "lastName": "SAID",
      "phone": "0537572227",
      "email": "said.laghlam@tgr.gov.ma",
      "fax": "0537572217",
      "adresse": "Ilôt 31 (près de l'avenue Al Araar) Hay Riad, Rabat",
      "poste": "CHEF DE DIVISION",
      "direction": "DIVISION DE L AUDIT ET DE L INSPECTION",
      "division": "",
      "poste direct": "8825",
      "secretaire standard": "0537578826",
      "photo": "../assets/img/tgr_photos_responsables/LAGHLAM SAID.jpeg",
      "lat": 34.0132500,
      "lng": -6.8325500
    }
  ];

  constructor(private http: Http) { }

  //Fetch central responsebles
  getCentralDirectionsList() {
    // let directions_centrale_list = this.http.get(`./assets/annuaire_centrale_new.json`);

    let directions_centrale_list = this.http.get(`https://api.tgr.gov.ma/api/TypesPoste/directionsCentrales`);
    directions_centrale_list.subscribe(
      data => {
        this.foundResponsibles = data.json();
      },
      err => console.error(err),
      () => console.log('getCentralDirectionsList completed ' + directions_centrale_list)
    );
    return directions_centrale_list;
  } // END Fetch central responsebles


  //Fetch regionale responsebles
  getRegionalDirectionsList() {
    // let directions_regional_list = this.http.get(`./assets/annuaire_regionale_new.json`);
    // return directions_regional_list;

    // let directions_regional_list = this.http.get(`./assets/annuaire_regionale_new.json`);
    let directions_regional_list = this.http.get(`https://api.tgr.gov.ma/api/TypesPoste/directionsRegionales`);

    directions_regional_list.subscribe(
      data => {
        this.foundResponsibles = data.json();
      },
      err => console.error(err),
      () => console.log('getRegionalDirectionsList completed ' + directions_regional_list)
    );
    return directions_regional_list;

  } // END Fetch regionale responsebles

  /**
   * 
   * @param responsibleName 
   */
  getReponsableByResponsibleName(responsibleName) {
    //https://api.tgr.gov.ma/api/Postes/chercherResponsables?nom=ahmed
    let responsables = this.http.get(`https://api.tgr.gov.ma/api/Postes/chercherResponsables?nom=` + responsibleName);
    return responsables;
  } // END getReponsableByDirection 

  /**
   * 
   * @param posteName 
   */
  getReponsableByPosteName(posteName) {
    //* ex: https://api.tgr.gov.ma/api/Postes/chercherPostes?poste=taza
    let postes = this.http.get(`https://api.tgr.gov.ma/api/Postes/chercherPostes?poste=${posteName}`);
    return postes;
  } // END getReponsableByDirection 

  getReponsableByDirection(directionCode) {
    let responsables = this.http.get(`https://api.tgr.gov.ma/api/Postes/listResponsables?direction=` + directionCode);
    return responsables;
  } // END getReponsableByDirection

}
