import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the LocalisationAndThemesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocalisationAndThemesProvider {

  constructor(public http: Http) {
    console.log('Hello LocalisationAndThemesProvider Provider');
  }

  /**
   * 
   * @param language 
  */
  translationAPI(language) {
    let item = this.http.get(
      'https://api.tgr.gov.ma/api/Traductions?filter=%7B%22where%22%3A%7B%22language%22%3A%22' + language + '%22%20%7D%20%7D'
    );
    return item;
  }

    /**
   * 
   * @param language 
  */
  themeAPI(theme) {
    let item = this.http.get(
      'https://api.tgr.gov.ma/api/Traductions?filter=%7B%22where%22%3A%7B%22theme%22%3A%22' + theme + '%22%20%7D%20%7D'
    );
    return item;
  }

}
