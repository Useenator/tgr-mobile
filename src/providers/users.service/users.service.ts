import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { NavController } from 'ionic-angular';
import { Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { URLSearchParams } from '@angular/http';

@Injectable()
export class UsersService {
    public data: any;
    public Url: any;
    public UrlLogin: any;

    private UrlAdherentBase: string;
    private UrlAdherentValidation: string;

    constructor(private http: Http) {
        this.data = {};
        this.data.username = '';
        this.data.response = '';
        this.UrlLogin = ' https://api.tgr.gov.ma/api/AppUsers/login';

        this.UrlAdherentBase = "https://api.tgr.gov.ma/api/Adherents/";
        this.UrlAdherentValidation = "https://api.tgr.gov.ma/api/EtatAdherents/enregistrerAdherent";
    }


    // Vérification de l’existence du login :
    // ex:https://api.tgr.gov.ma/Adherents/verifierLogin?login=157814


    /////////////////////////////////// Adherent Module //////////////////////////////////////

    /**
     * Check fonctionnaire data.
     * if true the data are correct => fonctionnaire exist.
     * ex: https://api.tgr.gov.ma/api/Adherents/verifierFonctionnaire?ppr=157814&amp;dateEntree=1981-01-02&amp;netMensuel=3468
     *
     */
    checkFonctionnaireData(ppr, dateEntree, netMensuel) {
        // let item = this.http.get(
        //     `./assets/verifierFonctionnaire_.json`
        //     //this.UrlAdherentBase + '/verifierFonctionnaire?ppr=' + ppr + '&amp;dateEntree=' + dateEntree + '&amp;netMensuel=' + netMensuel
        // );
        // return item;

        let fonctionnaire = {
            "ppr": ppr,
            "dateEntree": dateEntree,
            "netMensuel": Number(netMensuel)
            // "netMensuel":parseFloat(netMensuel)
        }
        console.log("Fonctionnaire ", fonctionnaire);
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        let bodyString = JSON.stringify(fonctionnaire); // Stringify payload { email: 'a@hotmail.com', password: 'root'}

        return this.http.post(this.UrlAdherentBase + 'verifierFonctionnaire', bodyString, options) // ...using post request
            .map((res: Response) => res.json()); // ...and calling .json() on the response to return data
        //.catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any

    }


    /**
      * https://api.tgr.gov.ma/Adherents/verifierLogin?login=157814
  
      * si existant   ce login existe déjà retenter  
      * sinon : continuer la saisie du formulaire
     */
    checkLoginAvailability(login) {
        let item = this.http.get(
            // `./assets/verifierFonctionnaire.json`
            this.UrlAdherentBase + 'verifierLogin?log=' + login
        );
        return item;
    }

    /**
    * Adhérent :
    *    Api ajoutée vérifier Email :
    *    https://api.tgr.gov.ma/api/Adherents/verifierEmail?mail={testingMail}  
    *    https://api.tgr.gov.ma/api/Adherents/verifierEmail?mail=useeeeeeeee@gmail.com  
    * @param email 
    */
    checkEmailAvailibility(email) {
        let item = this.http.get(
            // this.UrlAdherentBase + 'verifierEmail?mail=' + email
            'https://api.tgr.gov.ma/api/Adherents/verifierEmail?mail=' + email
        );
        return item;
    }


    /**
     * Calculer le code contribuable {codeContribuables} en appelant l’api : 
     * https://api.tgr.gov.ma/api/Contribuables/getIdentifiant/referenceAvis/montantAvis
     * ex: https://api.tgr.gov.ma/api/Contribuables/getIdentifiant/60565406725/306.9
     */
    calculateCodeContribuable(referenceAvis, montantAvis) {
        let item = this.http.get(
            // `./assets/verifierFonctionnaire.json`
            // this.UrlAdherentBase + 
            'https://api.tgr.gov.ma/api/Contribuables/getIdentifiant/' + referenceAvis + '/' + montantAvis
        );
        return item;
    }



    /**
     * EX: {"nomPrenom": "",
        "raisonSociale": "",
        "cin": "",
        "rc": "",
        "mail": "",
        "mobile": "",
        "activite": "",
        "adresse": "",
        "login": "",
        "password": "",
        "ppr": "157814",
        "codeContribuables": [],
        "ribs": [],
        "_etatAdherent": { "dateEtat": "2017-05-25T14:28:05.862Z", "typeEtatId": 0 },
        "reference_avis": "85595613",
        "montant_avis": "162.9",
        "net_mensuel": "",
        "derniere_entree": ""}
    
     * https://api.tgr.gov.ma/api/Adherents
     */
    sendAdherentData(adherent) {
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        let bodyString = JSON.stringify(adherent); // Stringify payload { email: 'a@hotmail.com', password: 'root'}

        return this.http.post(this.UrlAdherentBase, bodyString, options) // ...using post request
            .map((res: Response) => res.json()); // ...and calling .json() on the response to return data
        //.catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any

    }

    /**
     * Reset password
     * @param login 
     * @param email 
     */
    resetPassword(login, email) {
        let item = this.http.get(
            `https://api.tgr.gov.ma/api/AppUsers/${login}/${email}/initialiserMotDePasseOublie`
        );
        return item;

    }


    ////////////////////////////////////////////////////////////////////
    // import { URLSearchParams } from 'angular2/http';

    urlEncode(obj: Object): string {
        let urlSearchParams = new URLSearchParams();
        for (let key in obj) {
            urlSearchParams.append(key, obj[key]);
        }
        return urlSearchParams.toString();
    }
    ////////////////////////////////////////////////////////////////////
    validateAccount(adherentValidation) {
        let headers = new Headers({ 'Content-Type': 'application/X-www-form-urlencoded' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        // let bodyString = JSON.stringify(adherentValidation); // Stringify payload { email: 'a@hotmail.com', password: 'root'}

        /*
        {
            "login": this.login.trim(),
            "passwd ": this.password.trim(),
            "codeActivation": this.codeValidation.trim()
            }
        */
        // let __body = "login=" + adherentValidation.login + "&passwd=" + adherentValidation.passwd + "&codeActivation=" + adherentValidation.codeActivation;
        let _body = `login=${adherentValidation.login}&passwd=${adherentValidation.passwd}&codeActivation=${adherentValidation.codeActivation}`;
        // let body = `login=${username}&password=${password}`;
        console.log("BODY", _body);

        var body = new URLSearchParams();
        body.set('login', adherentValidation.login);
        body.set('passwd', adherentValidation.passwd);
        body.set('codeActivation', adherentValidation.codeActivation);


        return this.http.post(this.UrlAdherentValidation, _body.toString(), options) // ...using post request
            .map((res: Response) => res.json()); // ...and calling .json() on the response to return data
        //.catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
        /////////////////////////////////////////////////////////////////////////////////////



    }


    /**
      * - Une fois l’adhérent clique sur le lien : envoyer le fichier json pour la validation de l’adhésion
      * json : {"login":"string","passwd":"string"}
      *
      * Envoyer un mail avec un lien de validation :
      *
      *Api: https://api.tgr.gov.ma/api/EtatAdherents/validerEtatAdherent
     */
    sendMailWithValidationLink(adherent) {
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        let bodyString = JSON.stringify(adherent); // Stringify payload { email: 'a@hotmail.com', password: 'root'}

        return this.http.post('https://api.tgr.gov.ma/api/EtatAdherents/validerEtatAdherent', bodyString, options) // ...using post request
            .map((res: Response) => res.json()); // ...and calling .json() on the response to return data
        //.catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any

    }
    /**
      *
      *
      *
      *Api: https://api.tgr.gov.ma/api/Adherents
     */
    modifyAdherentData(adherent) {
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        let bodyString = JSON.stringify(adherent); // Stringify payload { email: 'a@hotmail.com', password: 'root'}

        return this.http.put('https://api.tgr.gov.ma/api/Adherents', bodyString, options) // ...using post request
            .map((res: Response) => res.json()); // ...and calling .json() on the response to return data
        //.catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any

    }



















    ///////////////////////////////////////// OLD ////////////////////////////////////////////////
    // getAllUsers() {
    //     // this.http.get('/assets/path/to/file').success(function(response){ //do something });
    //     let users = this.http.get(this.Url);

    //     //let users = this.http.get(`/assets/annuaire.json`);
    //     return users;
    // }

    // Add a new comment
    createUser(body) {//: Object: Observable<Comment[]>
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        let bodyString = JSON.stringify(body);//body); // Stringify payload

        return this.http.post(this.Url, bodyString, options) // ...using post request
            .map((res: Response) => res.json()); // ...and calling .json() on the response to return data
        //.catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
    }

    backendLogin(user) {
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        let bodyString = JSON.stringify(user); // Stringify payload { email: 'a@hotmail.com', password: 'root'}

        return this.http.post(this.UrlLogin, bodyString, options) // ...using post request
            .map((res: Response) => res.json()); // ...and calling .json() on the response to return data
        //.catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
    }
}
    // getDetails(repo) {  
    // let headers = new Headers();
    // headers.append('Accept','application/vnd.github.VERSION.html');

    // return this.http.get(`${repo.url}/readme`, { headers: headers });
// }
