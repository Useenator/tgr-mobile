//Ahmed

import { Injectable } from '@angular/core';
// import {Http} from '@angular/http';
import { Http, Headers } from '@angular/http';

import { NavController } from 'ionic-angular';
// import { DetailsPage } from '../details/details';

// Imports
//import { Injectable }     from '@angular/core';
import { Response, RequestOptions } from '@angular/http';
//import { Comment }           from '../model/comment';
//import {Observable} from 'rxjs/Rx';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


//import {Observable} from 'rxjs/Rx';

@Injectable()
export class BankServiceProvider {
    public data: any;
    public UrlBase: any;
    public UrlBaseSensOperation: any;
    public UrlAccount;
    public UrlAgency;
    public UrlLastFifteenOperation;
    public ribStatic;
    public foundItems;

    constructor(private http: Http) {
        this.ribStatic = '310810100011200804310192';
        this.UrlBase = 'https://api.tgr.gov.ma/api/Comptes/';
        this.UrlBaseSensOperation = 'https://api.tgr.gov.ma/api/SensOperations/';
        this.UrlAccount = this.UrlBase + this.ribStatic + '/solde';
        this.UrlAgency = this.UrlBase + this.ribStatic + '/agence';
        this.UrlLastFifteenOperation = this.UrlBase + this.ribStatic + '/operations?filter={"limit":"15"}';
        this;

    }

    getGraphSolde(username, rib, token){//
        let url = `https://api.tgr.gov.ma/api/AppUsers/${username}/${rib}/grapheSolde?access_token=${token}`
        console.log(url);
        
        let items = this.http.get(url);
        return items;
    }

    /**
     * Operations between two dates for a given abank account.
     */
    getOperationBetweenTwoDatesByRib(rib, user_id, token, date1, date2) {
        //operations?filter={"where": { "date": {"lte":"2012-06-01", "gte":"2012-01-02"} } }
        //https://api.tgr.gov.ma/api/AppUsers/CompteDemo/310810100011200804360147/entreDatesOperations?date1=2012-01-01&date2=2010-04-30&access_token=tVqaVyYmzfgK0XTQZh8NamSvwDzrl7vEmah3zzMsUsRVDMCpaSm4W0GrOARt9DtC
        let url = 'https://api.tgr.gov.ma/api/AppUsers/' + user_id + '/' + rib + '/entreDatesOperations?date1=' + date2 + '&date2=' + date1 + '&access_token=' + token;
        // let items = this.http.get(this.UrlBase + rib + '/operations?filter={"where": { "date": {"lte":"' + date1 + '", "gte":"' + date2 + '"} } }');
        console.log('\n date '+date1+'date2 '+date2);
        let items = this.http.get(url);
        return items;
    }

    /**
     * Last fifteen bank operation for a given abank account.
     */
    getLastFifteenOperationByRib(rib, user_id, token) {
        let url = 'https://api.tgr.gov.ma/api/AppUsers/' + user_id + '/' + rib + '/dernieresOperations?access_token=' + token;
        // let items = this.http.get(this.UrlBase + rib + '/operations?filter={"limit":"15"}');
        let items = this.http.get(url);
        return items;
    }

    /**
     https://api.tgr.gov.ma/api/AppUsers/{id}/comptes?access_token={access_tocken} => liste des comptes d'un utilisateur {id}
     https://api.tgr.gov.ma/api/AppUsers/{id}/{rib}/soldeBancaire?access_token={access_tocken} => solde bancaire d'un utilisateur {id} et son rib {rib}
     */
    /**
     * Get an an account by rib code.
     */
    getAccountsRibs(user_id, token) {
        let url = 'https://api.tgr.gov.ma/api/AppUsers/' + user_id + '/comptes?access_token=' + token;
        //let items = this.http.get(this.UrlBase + rib + '/solde');
        let items = this.http.get(url);
        return items;
    }

    /**
     * Get the agency by a rib code.
     */
    getAgencyByRib(rib, user_id, token) {
        //
        let url = 'https://api.tgr.gov.ma/api/AppUsers/' + user_id + '/' + rib + '/soldeBancaire?access_token=' + token;
        //let items = this.http.get(this.UrlBase + rib + '/agence');
        let items = this.http.get(url);
        return items;
    }

    /**
     * Get sensoperation DEBIT ou CREDIT  by a id code.
     * Ex: https://api.tgr.gov.ma/api/SensOperations/4
     */
    getSensOperationDeditCredit(id) {
        let items = this.http.get(this.UrlBaseSensOperation + id);
        return items;
    }


    filterItems(searchTerm, segment) {
        return this.foundItems.filter((item) => {
            return item.firstLastName.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        });
    }

}
