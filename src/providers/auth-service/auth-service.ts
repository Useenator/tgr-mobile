//Najim

import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { Config } from '../../MyUtils/Config';
import { MyUtilsProvider } from '../my-utils/my-utils';

import { PushServiceProvider } from '../push-service/push-service';
import { Storage } from '@ionic/storage';
/*
Generated class for the AuthServiceProvider provider.

See https://angular.io/docs/ts/latest/guide/dependency-injection.html
for more info on providers and Angular DI.
*/
@Injectable()
export class AuthServiceProvider {

  constructor(private myUtils: MyUtilsProvider,
    private storage: Storage,
    public pushServiceProvider: PushServiceProvider,
    public events: Events) {

    console.log("AuthServiceProvider constructor");


    /*
    this.getUser()
    .then(userData=>{
      if(this.myUtils.checker(userData['tokenData']) && this.myUtils.checker(userData['tokenData']['id'])){
        this.currentUser = userData;
      }else{
        this.currentUser = null;
      }
    })
    */

  }

  public currentUser = null;

  public login(credentials, sendToActualitesPage = true) {
    let context = this;
    console.log("credentials>>", credentials);
    if (credentials.userid === null || credentials.password === null) {
      return Observable.throw("Please insert credentials");
    } else {
      return Observable.create(observer => {
        // At this point make a request to your backend to make a real check!
        var data = { username: credentials.userid, 
          password: credentials.password, 
          saveSess: credentials.saveSess, //just for client app!
          ttl: '86400'  }; // ttl by seconds ttl=1200
        console.log('===>LOGIN API POST DATA<===',data)
        this.myUtils.doPost(Config.getLoginApi(), data)
          .then((responce) => {
            console.log("retun::", responce)
            //ex: _body : {"id":"0DsDnBlr40ioSocPI3LQgATn0sPVuy2MYqU7RJLxK3Dm6L6j7AT6misbuV6MOGfV","ttl":1299,"created":"2017-06-28T21:57:23.285Z","userId":"CompteDemo"}
            if (this.myUtils.checker(responce["_body"]) && this.myUtils.checker(JSON.parse(responce["_body"]).id)) {

              let tokenData = JSON.parse(responce["_body"]);
              this.myUtils.doGet(Config.getUserLDAP(credentials.userid, tokenData.id))
                .then((responce) => {
                  /*
                  ex: _body :
                  {"id":"CompteDemo","username":"CompteDemo","title":"No","cin":"1111111",
                  "norib":["310810100011200804360147"],"userOU":"N513","userRAT":"1000","lastName":"CompteDemo",
                  "firstName":"CompteDemo","codeadministration":"29","codecontribuable":["602728021059|7350145"],
                  "codeservice":"00000374","sexe":"M","sn":"CompteDemo","titlecode":"34","titlehierarchy":"2",
                  "employeenumber":"CompteDemo","preferredlanguage":"FR","fixe":"No",
                  "dn":"uid=CompteDemo,ou=adherents,ou=users,DC=TGR,DC=MA","email":"CompteDemo@demo.net"}
                  */

                  if (context.myUtils.checker(responce["_body"]) && context.myUtils.checker(JSON.parse(responce["_body"]).id)) {

                    let ldapData = JSON.parse(responce["_body"]);
                    context.myUtils.doGet(Config.getHabilitationsApi(credentials.userid, tokenData.id)).
                      then(res => {
                        if (context.myUtils.checker(res["_body"])) {//&& this.myUtils.checker(JSON.parse(res["_body"]).id)){

                          let prefsData = { userAccess: credentials, tokenData: tokenData, LDAPData: ldapData, roleData: JSON.parse(res["_body"]) };


                          context.myUtils.doSetPrefs("userAccess", credentials);
                          context.myUtils.doSetPrefs("tokenData", tokenData);
                          context.myUtils.doSetPrefs("LDAPData", ldapData);
                          context.myUtils.doSetPrefs("roleData", JSON.parse(res["_body"]));


                          context.currentUser = prefsData;

                          //credentials.userid
                          this.storage.get('push-token').then((token) => {
                            console.log('Your age is', token);

                            let registrationTokenObj = {
                              "registrationToken": token,
                              "appUserId": credentials.userid
                            }

                            this.postRegistrationToken(registrationTokenObj)
                          });


                          setAccess(true, sendToActualitesPage);
                        }
                      })
                  }
                });
            }
          })
          .catch((err) => {
            console.log("retun err::", err)
            setAccess(false);
          });

        function setAccess(isValid: Boolean, goToActualites = true) {
          console.log("setAccess(" + isValid + ")");
          if (!isValid) {
            context.currentUser = null;

            context.myUtils.doSetPrefs("userAccess", null);
            context.myUtils.doSetPrefs("tokenData", null);
            context.myUtils.doSetPrefs("LDAPData", null);
            context.myUtils.doSetPrefs("roleData", null);
            context.myUtils.doSetPrefs("saveSess", null);
          } else {
            if (typeof context.currentUser.userAccess.saveSess != 'undefined' && context.currentUser.userAccess.saveSess == true) {
              context.myUtils.doSetPrefs("saveSess", context.currentUser.userAccess.userid);
            } else {
              context.myUtils.doSetPrefs("saveSess", null);
            }
            if (goToActualites) {
              console.warn("GO TO ACTUALITES...")
              context.events.publish('user:login', context.currentUser);
            } else {
              console.warn("GO TO NO PAGE");
            }
          }
          observer.next(isValid);
          observer.complete();
        }


      });
    }
  }

  postRegistrationToken(registrationToken) {
    this.pushServiceProvider.postPushRegistrationToken(registrationToken).subscribe(
      data => {
        // this.showAlert("", "Adhérent ajouté avec succes");
        console.error("Server response DATA! ", data);
        //redirect to login page.
        // this.nav.setRoot(LoginPage);

        let validationResponseObj = data;
        if (validationResponseObj.status == 200) {
          // set a key/value pair
          // this.storage.set(this.VALIDATION_RESPONSE_OBJ, this.validationResponseObj);
          console.log("registraction token sended successfully");

        }

        return true;
      },
      error => {
        console.error("Error saving token! " + error);
        //show a nice error message to the user
        if (error.status == 422) {
          console.error('422',error);
          // this.showAlert("Nous sommes désolés!", "Login indisponible, veuillez choisir un autre !");
          // this.showAlert("Nous sommes désolés!", "422 - Erreur d'enregistrement de l'adherent - Essayez un autre email !! ");
        } else
          if (error.status == 500) {
            console.error('500',error);
            // this.showAlert("Nous sommes désolés!", "500 - Service  d'enregistrement des adherents indisponible veuillez réessayer ultérieurement");
          } else {
            // this.showAlert("Nous sommes désolés!", "Service  d'enregistrement des device indisponible veuillez réessayer ultérieurement");
            console.log("Service  d'enregistrement des device indisponible veuillez réessayer ultérieurement");
          }

        // this.dismissLoading();
        return Observable.throw(error);
      },
      () => {
        // this.dismissLoading();
        console.log("postRegistrationToken Complete");
      }
    );
  }
  public register(credentials) {
    if (credentials.userid === null || credentials.password === null) {
      return Observable.throw("Please insert credentials");
    } else {
      // At this point store the credentials to your backend!
      return Observable.create(observer => {
        observer.next(true);
        observer.complete();
      });
    }
  }

  public getUserAccess() {
    return this.myUtils.doGetPrefs("userAccess");
  }

  public getUserSaveSess() {
    return this.myUtils.doGetPrefs("saveSess");
  }

  public getTokenData() {
    return this.myUtils.doGetPrefs("tokenData");
  }

  public getLDAPData() {
    return this.myUtils.doGetPrefs("LDAPData");
  }

  public getRoleData() {
    return this.myUtils.doGetPrefs("roleData");
  }

  public getUser() {
    return new Promise((resolve, reject) => {
      this.getUserAccess()
        .then(userdata => {
          this.getTokenData()
            .then(tokenData => {
              this.getLDAPData()
                .then(ldapData => {
                  this.getRoleData()
                    .then(roleData => {
                      let prefsData = { userAccess: userdata, tokenData: tokenData, LDAPData: ldapData, roleData: roleData };
                      resolve(prefsData);
                    })
                })
            })
        });
    });

  }

  refreshData() {
    return new Promise((resolve, reject) => {
      this.getUserAccess()
        .then(userAccess => {
          console.warn("START REFRESH")
          this.login(userAccess, false).subscribe(allowed => {
            console.log("subscribe allowed=" + allowed);
            if (allowed) {
              resolve(true);

            } else {
              resolve(false);
            }

            //  this.dismissLoading();
          },
            error => {
              console.log(error);
              reject(error);
            });

        },
        err => {
          reject(err);
        })
    });

  }

  public logout() {

    console.log("logout...");

    //this.myUtils.doSetPrefs("userAccess", null);
    this.myUtils.doSetPrefs("tokenData", null);
    this.myUtils.doSetPrefs("LDAPData", null);
    this.myUtils.doSetPrefs("roleData", null);
    console.log("publish logout..");
    if (this.myUtils.checker(this.currentUser) && this.myUtils.checker(this.currentUser.tokenData)) {
      this.myUtils.doGet(Config.getLogoutApi(this.currentUser.tokenData.id))
        .then(res => {
          this.currentUser = null;
          this.events.publish("showDemo", false);
          this.events.publish('user:logout', Date.now());
        },
        err => {
          this.events.publish('user:logout', Date.now());
          this.events.publish("showDemo", false);
          this.currentUser = null;
        });
    } else {
      this.events.publish('user:logout', Date.now());
      this.events.publish("showDemo", false);
      this.currentUser = null;
    }


    return Observable.create(observer => {
      console.log("Observable.create");

      this.currentUser = null;
      observer.next(true);
      observer.complete();

    });

  }
}
