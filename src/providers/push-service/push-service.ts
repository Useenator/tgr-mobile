import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { NavController } from 'ionic-angular';
import { Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

/*
 
*/
@Injectable()
export class PushServiceProvider {


    private UrlBase;
    constructor(private http: Http) {
        console.log("Hello PushServiceProvider");

        this.UrlBase = 'https://api.tgr.gov.ma/api/';

    }

    /**
     *  
        {
            "registrationToken" : "xxxxx",
            "appUserId":"xxxxx"
        }
    
     * https://api.tgr.gov.ma/api/Devices 
     */
    postPushRegistrationToken(registrationTokenObject) {
        let url = "https://api.tgr.gov.ma/api/devices";
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        let bodyString = JSON.stringify(registrationTokenObject); // Stringify payload { email: 'a@hotmail.com', password: 'root'}

        return this.http.post(url, bodyString, options) // ...using post request
            .map((res: Response) => res.json()); // ...and calling .json() on the response to return data
        //.catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any

    }

}
