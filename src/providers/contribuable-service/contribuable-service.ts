import { Injectable } from '@angular/core';
import { Http } from '@angular/http';


import { Response, RequestOptions } from '@angular/http';
import { Headers } from '@angular/http';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
/*
  Generated class for the Contribuable provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ContribuableService {

  private UrlBase = '';
  private UrlArticleSolde = '';
  private UrlArticleNonSolde = '';
  private UrlContribuableBAse = '';

  constructor(public http: Http) {
    console.log('Hello Contribuable Provider');

    // this.UrlBase = 'https://api.tgr.gov.ma/api/Contribuables';///getResultS/9758663 //https://api.tgr.gov.ma/api/AppUsers/
    // this.UrlBase = 'https://api.tgr.gov.ma/api/AppUsers/';///getResultS/9758663 //
    // this.UrlArticleSolde = this.UrlBase + '/getResultS/';
    // this.UrlArticleNonSolde = this.UrlBase + '/getResultNS/';
    this.UrlContribuableBAse = 'https://api.tgr.gov.ma/api/AppUsers/';

  }

  /**
   * Liste des articles non payés.
   * ex : https://api.tgr.gov.ma/api/Contribuables/getResultPEL/605654067251/306.9
   */
  getResultPEL(reference, montant) {
    let item = this.http.get('https://api.tgr.gov.ma/api/Contribuables/getResultPEL/' + reference + '/' + montant);
    return item;
  }

  /**
   * liste des codecontribuable d'un utilisateur {id}]
   * ex: https://api.tgr.gov.ma/api/AppUsers/CompteDemo/contribuables?access_token=
   */
  getCodeContribuableByUserId(userId, token) {
    let item = this.http.get(this.UrlContribuableBAse + userId + '/contribuables?access_token=' + token);
    return item;
  }

  /**
   * liste des articles soldés
   * ex: https://api.tgr.gov.ma/api/AppUsers/CompteDemo/7350145/consulterArticlesSoldes?access_token=
   */
  getArticlesSolder(userId, token, code_contribuable) {
    // let item = this.http.get(this.UrlArticleSolde + code_contribuable);
    let item = this.http.get(this.UrlContribuableBAse + userId + '/' + code_contribuable + '/consulterArticlesSoldes?access_token=' + token);
    return item;
  }

  /**
   * liste des articles non soldés
   * ex: https://api.tgr.gov.ma/api/AppUsers/CompteDemo/7350145/consulterArticlesNonSoldes?access_token=
   */
  getArticlesNonSolder(userId, token, code_contribuable) {
    // let item = this.http.get(this.UrlArticleNonSolde + code_contribuable);
    let item = this.http.get(this.UrlContribuableBAse + userId + '/' + code_contribuable + '/consulterArticlesNonSoldes?access_token=' + token);
    return item;
  }

  /**
 * liste des nature creance
 * ex: https://api.tgr.gov.ma/api/NatureCreances
 */
  getNatureCreance() {
    // let item = this.http.get(this.UrlArticleNonSolde + code_contribuable);
    let item = this.http.get("https://api.tgr.gov.ma/api/NatureCreances");
    return item;
  }
  /**
    * Open Paiement URL 
    */
  openPaiementURL(url) {
    let item = this.http.get(url);
    return item;
  }

  /**
   * consulter situation fiscale par avis d'imposition
   * http://api.tgr.gov.ma/api/Contribuables/consulterArticlesARegler?nature={codenature}&reference={reference}&montant={montant]&annee={annee}
   */
  getSituationFiscaleAvisImposition(item) {

    let annee = item.annee;
    let reference = item.reference;
    let montant = item.montant;
    let codenature = item.codenature;

    let _url = 'http://api.tgr.gov.ma/api/Contribuables/consulterArticlesARegler?nature=' + codenature + '&reference=' + reference + '&montant=' + montant + '&annee=' + annee;
    let itemReturn = this.http.get(_url);
    return itemReturn;
    //     let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    // let options = new RequestOptions({ headers: headers }); // Create a request option
    // let bodyString = JSON.stringify(item); // Stringify payload { email: 'a@hotmail.com', password: 'root'}

    // return this.http.post(_url, bodyString, options) // ...using post request
    //     .map((res: Response) => res.json()); // ...and calling .json() on the response to return data
    //     //.catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
  }//

  /**
   * http:///api.tgr.gov.ma/api/Contribuables/getURL/{code}/{cc}/{re}/{mon}/{email}/{total}/{id} 
   * => paiement en ligne articles fiscaux
   */
  get_PaiementEnLigneArticlesFiscaux() {
    let _url = 'http://api.tgr.gov.ma/api/Contribuables/getURL/{code}/{cc}/{re}/{mon}/{email}/{total}/{id} ';
    let item = this.http.get(_url);
    return item;
  }

}
