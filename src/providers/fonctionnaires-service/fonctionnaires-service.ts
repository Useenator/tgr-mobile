//Ahmed

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the FonctionnairesServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class FonctionnairesServiceProvider {
    public data: any;
    public UrlBase: any;
    public UrlSituationAdministrativeBase: any;
    public UrlSituationPrelevementBase: any;
    public UrlAttestationSalaireBase: any;
    public UrlLogin: any;
    public foundItems;

    //   private token= 'IotYAuVXRlIZLhqQCFvrJtJGAUQRggXndTYHklaETN3wwffuvVl2QibLPuVevbL5';
    //   private ppr=157814;

    constructor(private http: Http) {
        this.UrlSituationAdministrativeBase = 'https://api.tgr.gov.ma/api/AppUsers/';
        this.UrlSituationPrelevementBase = 'https://api.tgr.gov.ma/api/AppUsers/';
        this.UrlAttestationSalaireBase = 'https://api.tgr.gov.ma/api/AppUsers/';
    }


    getSituationAdministrativeByPPR(ppr, token) {
        let item = this.http.get(this.UrlSituationAdministrativeBase + ppr + '/editionSituationAdministrative?access_token=' + token);
        return item;
    }

    getAttestationSalaireByPPR(ppr, token) {
        let item = this.http.get(this.UrlAttestationSalaireBase + ppr + '/attestationSalaire?access_token=' + token);
        return item;
    }

    getSituationPrelevementByPPR(ppr, token) {
        let item = this.http.get(this.UrlSituationPrelevementBase + ppr + '/situationPrelevements?access_token=' + token);
        return item;
    }
}
