import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the GidServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class GidServiceProvider {

  private UrlBase;

  constructor(public http: Http) {
    console.log('Hello GidServiceProvider Provider');

    this.UrlBase = 'https://api.tgr.gov.ma/api/';
  }

  /**
   *
   *https://api.tgr.gov.ma/api/Tiers/126/mDs 
  */
  getMds() {
    let item = this.http.get(this.UrlBase + 'Tiers/126/mDs ');
    //let item=this.http.get(`./assets/gid_mds.json`)
    return item;
  }

  /**
   *
   *https://api.tgr.gov.ma/api/Tiers/126/mPs
  */
  getMps() {
    let item = this.http.get(this.UrlBase + 'Tiers/126/mPs');
    //let item=this.http.get(`./assets/gid_mps.json`)
    return item;
  }

/**
 * https://api.tgr.gov.ma/api/AppUsers/165361/tiersActesFournisseurs?access_token=ycBB1YfsI30qyS4SQ2TuBedh7BSX6Gdg7stmyDwFcP1jhHW4SFaEUPfxTzbuLkql
 */
  getGid(tierId,token){
    let item = this.http.get(this.UrlBase + 'AppUsers/'+tierId+'/tiersActesFournisseurs?access_token='+token);
    //let item=this.http.get(`./assets/gid_mps.json`)
    return item;
  }
}
