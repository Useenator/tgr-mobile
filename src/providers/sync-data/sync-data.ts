import { Injectable } from '@angular/core';

import { MyUtilsProvider } from "../my-utils/my-utils";

@Injectable()
export class SyncDataProvider {

  constructor(private myUtils: MyUtilsProvider) {

  }

  /*
  USAGE: 

  var foundItems = [];

  this.syncService.syncData("annuaire", (res=>{
    this.foundItems = res;
  }), 
  ()=>{
    //Online http ...
    //this.annuaire.getListFromHttp()
    ///... rest of code !!!
  }
  )

  */

  //NUMBER_OF_DAYS:number=1;
  syncData(storageKey: string, resultCB, execCallBack,numberOfDaysOffLine=1) {
    //numberOfDaysOffLine=this.NUMBER_OF_DAYS;

    this.myUtils.doGetPrefs(storageKey + "_###date###")
      .then(lastDate => {
        console.log("lastDate", lastDate);
        let date = undefined;
        if (lastDate) date = new Date(lastDate)
        let nbrDays = this.getNbrDaysBetween(date, new Date());
        console.log('-- Number of days : '+nbrDays);
        console.log('-- Number of OffLine days : '+numberOfDaysOffLine);

        if (!lastDate || nbrDays > numberOfDaysOffLine) {
          console.log("---> doGet from NET..");

          execCallBack()
            .then(data => {
              if (data) {

                this.myUtils.doSetPrefs(storageKey, data);
                this.myUtils.doSetPrefs(storageKey + "_###date###", new Date());
                resultCB(data);
                
              }
            })
            .catch((ex) => {
              console.error(ex);
              //toFill = undefined;
              this.myUtils.doGetPrefs(storageKey)
                .then(data => {
                  resultCB(data);
                })

            });
          

        } else {
          console.log("---> doGet from Storage.. key="+storageKey);
          this.myUtils.doGetPrefs(storageKey)
            .then(data => {
              //-- Added to prevent a bug in annuaire .. first directions list load  -> without internet. 
              if (!data) {
                console.log("data not found!!");
                
                execCallBack()
                .then(data => {
                  if (data) {
    
                    this.myUtils.doSetPrefs(storageKey, data);
                    resultCB(data);
                  }
                })
                .catch((ex) => {
                  console.error(ex);
                });
              }else {
                console.log("data found!!!");
                
                resultCB(data);
              }
              //END Added to prevent a bug in annuaire .. first directions list load  -> without internet.

            })

        }
      })
  }

  private getNbrDaysBetween(firstDate: Date, secondDate: Date) {
    var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    //var firstDate = new Date(2008, 01, 12);
    //var secondDate = new Date(2008, 01, 22);
    console.log(typeof firstDate, firstDate, typeof secondDate, secondDate);

    if (!firstDate || !secondDate) return 10;
    else
      return Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
  }

}
