import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Storage } from '@ionic/storage';
import { OnInit } from '@angular/core';



@Injectable()
export class TranslationServiceProvider{
     


     private defaultLanguage:string;

     private translationResource: string;

     private currentLanguage : string;
     private availableLanguages : any[];
     private availableTranslations : any[];
     private cachedTranslations :any;


      constructor(private http: Http, private localStorage: Storage) {
        console.warn("init translationservice");
        this.defaultLanguage="Français";
        this.translationResource ="https://api.tgr.gov.ma/api/Traductions/";
        this.loadData();
      }
     


    loadData(){
        localStorage.removeItem("availableLanguages");
        localStorage.removeItem("availableTranslations");
        
        this.loadAvailableLanguages();
        this.loadCurrentLanguage();
        this.loadAvailableTranslations();
    }

     loadCurrentLanguage(){
        // this.localStorage.get('currentLanguage').then((val)=>{
        //      if(val){
        //          this.currentLanguage = val;
        //      }else{
        //          this.currentLanguage = "Français";
        //          this.localStorage.set("currentLanguage",this.currentLanguage);
        //          localStorage.setItem("availableTranslations",this.currentLanguage);
                 
                 
        //      }
        // })
        this.getCurrentLanguage();


     }

     loadAvailableTranslations(){
        let langs = []; 
      
              this.http.get(this.translationResource).subscribe(
                  data => {
                      localStorage.setItem("availableTranslations",data["_body"]);
                  },
                  error =>{
                      localStorage.setItem("availableTranslations",JSON.stringify([]));
                      
                  }
              )

     }

     
     loadAvailableLanguages(){
        let langs = []; 
              this.http.get(this.translationResource).subscribe(
                  data => {
                      let response = JSON.parse(data["_body"]);
                      response.forEach(element => {
                          langs.push(element['language']);
                      });
                      localStorage.setItem("availableLanguages",JSON.stringify(langs));
                      
                  },
                  error =>{
                      console.warn("Could not retrieve available language");
                      localStorage.setItem("availableLanguages",JSON.stringify([]));
                      
                  }
              )
          

    }      

    setCurrentLanguage(language){
        localStorage.setItem("currentLanguage",language);
    }

    getCurrentLanguage(){
        var currentLang = localStorage.getItem("currentLanguage");
        if(!currentLang){
            localStorage.setItem("currentLanguage",this.defaultLanguage);
            return this.defaultLanguage;
        }
        return  currentLang;
    }

    getAvailableLanguages(){        
        let availableTrans = this.localStorage.get("availableLanguages");
        return availableTrans;
    }

    getAvailableTranslations(){
        let availableTranslations = this.localStorage.get("availableTranslations");
        return availableTranslations;
    }
     

}
