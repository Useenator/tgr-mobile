import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';

/*
  Generated class for the ContribuableObserverProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ContribuableObserverProvider {

  constructor(public http: Http) {
    console.log('Hello ContribuableObserverProvider Provider');

    this.observer = new Subject();
  }

  observer;

  

}
