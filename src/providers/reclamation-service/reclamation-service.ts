import { Injectable } from '@angular/core';
// import {Http} from '@angular/http';
import { Http, Headers } from '@angular/http';

import { NavController } from 'ionic-angular';
// import { DetailsPage } from '../details/details';

// Imports
//import { Injectable }     from '@angular/core';
import { Response, RequestOptions } from '@angular/http';
//import { Comment }           from '../model/comment';
import { Observable } from 'rxjs/Rx';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { stringify } from '@angular/core/src/util';

/*
  Generated class for the ReclamationServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ReclamationServiceProvider {

      // public data: any;
      // public Url: any;
      // public UrlLogin: any;
      private UrlReclamationBAse; 

      constructor(private http: Http) {
        console.log("Hello ReclamationServiceProvider");

          this.UrlReclamationBAse = 'https://api.tgr.gov.ma/api/';

      }

      /**
       * liste des codecontribuable d'un utilisateur {id}]
       * ex: https://api.tgr.gov.ma/api/TypeReclamants
       */
      getTypeReclamants() {
          let item = this.http.get(this.UrlReclamationBAse + 'TypeReclamants');
          return item;
      }

      /**
       * liste des codecontribuable d'un utilisateur {id}]
       * ex: http://api.tgr.gov.ma/api/TypeReclamants/{idTypeReclamant}/natureReclamations
       */
      getNatureReclamations(codeReclamant) {
          let item = this.http.get("https://api.tgr.gov.ma/api/TypeReclamants/natureReclamations?code="+codeReclamant);
          return item;
      }
    /**
       * liste des posts
       * ex: https://api.tgr.gov.ma/api/TypeReclamants/postesParVille?codereclamant={codereclamant}&codeville={codeville}
       */
      getPosts(codereclamant, codeville){
          let strUrl = 'https://api.tgr.gov.ma/api/TypeReclamants/postes?codetypereclamant='+codereclamant+'&codeville='+codeville
          console.log("getPosts strUrl", strUrl);
          
          return this.http.get(strUrl);
      }

      /**
       * liste des codecontribuable d'un utilisateur {id}]
       * ex: https://api.tgr.gov.ma/api/Villes
       */
      getVilles() { 
          let item = 
          this.http.get('https://api.tgr.gov.ma/api/TypesVille/villesParTypeEtNature?codetype=NATIONAL&codenature=CENTRAL');
          return item;
      }



      /**
       *
       * ex: http://api.tgr.gov.ma/api/Villes/{idVille}/postes
       */
      getVillesPostesByIdVille(idVille, codetypeacteur) { 
          //let strUrl = "https://api.tgr.gov.ma/api/Villes/postes?code="+idVille;
          let strUrl = "https://api.tgr.gov.ma/api/Villes/acteurs?codeville="+idVille+"&codetypeacteur="+codetypeacteur
          //let strUrl = this.UrlReclamationBAse + 'Villes/' + idVille + '/postes';
          let item = this.http.get(strUrl);
          return item;
      }

      /**
       *
       * ex: https://api.tgr.gov.ma/api/TypeReclamants/{typeReclamntID}/typeActeur
       */
      getTypeReclamantsBytypeReclamntID(typeReclamntID) {
          let item = this.http.get(this.UrlReclamationBAse + 'TypeReclamants/' + typeReclamntID + '/typeActeur');
          return item;
      }
      /**
       *
       * ex: http://api.tgr.gov.ma/api/TypeActeurs/{typeActeurID}/acteurs
       */
      getTypeActeursBytypeActeurID() {
        let urlStr = "https://api.tgr.gov.ma/api/TypeActeurs/acteurs?code=DEPARTEMENT";
        //let urlStr = this.UrlReclamationBAse + 'TypeActeurs/' + typeActeurID + '/acteurs';
          console.info("getTypeActeursBytypeActeurID-->", urlStr)
          return this.http.get(urlStr);
      }

      //********** */
      
      getActeursCollectiviteLocale(codeVille, reclamantParam){
          let urlStr = "https://api.tgr.gov.ma/api/Villes/acteurs?"+codeVille+"=1&codetypeacteur=COLLECTIVITE-LOCALE";
          return this.http.get(urlStr);
      } 
      /**
       * liste départements pour typeReclamant Fonctionnaire & Ordonnateurs/ssOrdonnateur
       * @param reclamantParam = "FONCTIONNAIRE" or "ORDONNATEUR_SOUS_ORDONNATEUR"
       */
      getActeurs(reclamantParam){
          let urlStr = "https://api.tgr.gov.ma/api/TypeReclamants/acteurs?code="+reclamantParam;
          return this.http.get(urlStr);
      }
      //********** */
 
      /**
       * Add a new Reclamation
       * ex: http://api.tgr.gov.ma/api/Reclamations http://api.tgr.gov.ma/api/Reclamations
       */
      postReclamation(body) {
          console.log(' --> ReclamationOBJ: ',body);
          let UrlReclamationPost = this.UrlReclamationBAse + 'Reclamations';
          let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
          let options = new RequestOptions({ headers: headers }); // Create a request option
          let bodyString = JSON.stringify(body);//body); // Stringify payload

          return this.http.post(this.UrlReclamationBAse+'Reclamations', bodyString, options) // ...using post request
              .map((res: Response) => res.json()); // ...and calling .json() on the response to return data
          //.catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
      }



      /**
       *  Add a new Reclamation
       *
      */
      createReclamation(body) {
          let headers = new Headers({ 'Content-Type': 'application/json' });
          let options = new RequestOptions({ headers: headers });
          let bodyString = JSON.stringify(body);

          return this.http.post(this.UrlReclamationBAse, bodyString, options)
              .map((res: Response) => res.json());
      }


}
