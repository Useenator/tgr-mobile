//Najim

import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';


/*
  Generated class for the MyUtilsProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/

@Injectable()
export class MyUtilsProvider {

  constructor(public http: Http, private storage: Storage) {
    console.log('Hello MyUtilsProvider Provider');
  }

  doGet(url) {
    return new Promise((resolve, reject) => {
      this.http.get(url)
        .subscribe(data => {
          resolve(data);
        }, error => {
          reject(error);
        });
    });

  }

  doPost(url, jsondata) {
    return new Promise((resolve, reject) => {
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      console.log("json to post: ", jsondata);
      this.http.post(url, jsondata, options)
        .subscribe(data => {
          resolve(data);
        }, error => {
          reject(error);
        });
    });

  }

  doPostForm(url, params: Array<{ key: string, value: string }>) {
    return new Promise((resolve, reject) => {
      let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
      var body = ''
      params.forEach(param => {
        body += param.key + "=" + param.value + "&"
      });

      if (body.length > 0) body = body.substr(0, body.length - 1)

      let options = new RequestOptions({ headers: headers });

      console.log("body to post: ", body);
      this.http.post(url, body.toString(), options)
        .subscribe(data => {
          resolve(data);
        }, error => {
          reject(error);
        });
    });

  }

  doSetPrefs(key: string, valueBody) {
    return this.storage.set(key, valueBody);
  }

  doGetPrefs(key: string) {
    return this.storage.get(key)
    /*
    .then((val) => {
      console.log('value', val);
    });
    //*/
  }

  public checker(arg) {
    let res = false;
    if (arg && arg != null && typeof arg != "undefined" && arg != "") res = true;

    console.log("arg toCheck=", arg, "res=", res)
    return res;
  }

}
