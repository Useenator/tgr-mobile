import { Component, NgModule } from '@angular/core';
import {
  IonicPage, Events,
  NavController, NavParams, LoadingController, AlertController
} from 'ionic-angular';

import { MyUtilsProvider } from '../../providers/my-utils/my-utils';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { Config } from '../../MyUtils/Config';

import { SyncDataProvider } from "../../providers/sync-data/sync-data";

import { InAppBrowser } from '@ionic-native/in-app-browser';

import { InAppBrowserOptions } from 'ionic-native';

import { IonicImageLoader } from 'ionic-image-loader';
/**
* Generated class for the ActualitesPage page.
*
* See http://ionicframework.com/docs/components/#navigation for more info
* on Ionic pages and navigation.
*/
//@IonicPage()

@Component({
  selector: 'page-actualites',
  templateUrl: 'actualites.html',
  providers: [InAppBrowser]
})
export class ActualitesPage {

  actualites: any;
  data = {news : null, stats: null};

  newsTabs = "bulletin";

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private myUtils: MyUtilsProvider,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private auth: AuthServiceProvider,
    private syncService: SyncDataProvider,
    private iab: InAppBrowser,
    private events: Events
  ) {

    this.connectedUser = this.auth.currentUser;
  }

  connectedUser;
  logout() {
    let loading = this.loadingCtrl.create({
      content: 'Traitement en cours...',
      dismissOnPageChange: true
    });
    loading.present();

    this.events.publish('user:doLogout', Date.now());
  }

  ionViewDidLoad() {
    let ctx = this;
    console.log('ionViewDidLoad HomePage');

    this.getActualites();

  }

  getActualites() {
    this.presentLoading();
    this.syncService.syncData("Actualites",
      (res => {
        this.dismissLoading();
        console.log("res>>>>", res);
        if (res && ((res.news && res.news.constructor === [].constructor) || (res.stats && res.stats.constructor === [].constructor))) {
          console.log("set actualites");
          this.actualites = this.data.stats;

        } else {
          this.showAlert("Service indisponible");
        }
      }),
      () => {
        //Online http ...
        //this.annuaire.getListFromHttp()
        ///... rest of code !!!
        let ctx = this;
        return new Promise((resolve, reject) => {

          this.myUtils.doGet("https://api.tgr.gov.ma/api/Actualites/getStats")
            .then(res => {
              try {
                let stats = JSON.parse(res['_body']).stats;

                this.myUtils.doGet(Config.getNewsApi())
                  .then(res => {
                    //console.warn("res['_body']", res['_body']);
                    try {
                      let news : Array<any> = JSON.parse(res['_body']).news;
                      
                      if(stats && stats.length>0){
                        ctx.data.stats = stats;
                      }

                      if(news && news.length>0){
                        ctx.data.news = news;
                      }
                      resolve(ctx.data);
                    } catch (e) {
                      reject(e);
                    }

                  },
                  err => {
                    reject(err);
                    //ctx.showAlert("Service indesponible");
                    //ctx.dismissLoading();
                  });

              } catch (err) {
                reject(err);
              }
            },
            err => {
              reject(err);
              //ctx.showAlert("Service indesponible");
              //ctx.dismissLoading();
            })


        });
      }
    );//*/
  }

  segmentChanged(evt){
    console.log(this.newsTabs, this.data);
    if(this.newsTabs == "bulletin"){
      this.actualites = this.data.stats;
    }else{
      this.actualites = this.data.news;
    }
    
  }

  public showAlert(message) {
    let alert = this.alertCtrl.create({
      title: 'TGR Mobile',
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }//END Alert


  private loader;
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Chargement des données...",
      showBackdrop: true,
      enableBackdropDismiss: true
      //,dismissOnPageChange: true
    });

    this.loader.onDidDismiss(() => {
      console.log('Dismissed loading');
      //duplicated : app event
      //if(this.isValid) this.navCtrl.setRoot(AccountDetailPage);
    });

    this.loader.present();
  }

  dismissLoading() {
    console.log('--Dismissed loading--');
    this.loader.dismiss();

  }

  getImageFromUrl(url) {
    return "";
  }

  adaptImg(imgUrl: string) {
    return imgUrl.replace('http://10.96.8.79:10039', 'https://www.tgr.gov.ma');
  }

  _openUrl(url: string) {
    console.log("origin url", url);

    let toOpen = url.replace('http://10.96.8.79:10039/', 'https://www.tgr.gov.ma/');

    console.log("to open", toOpen);

    window.open(toOpen);
  }
  /**
 * Confirmation Alert box for paiement url.
 */
  openUrl(url: string) {

    let options: InAppBrowserOptions = {

      location: 'yes',//Or 'no' 
      hidden: 'no', //Or  'yes'
      clearcache: 'yes',
      clearsessioncache: 'yes',
      zoom: 'yes',//Android only ,shows browser zoom controls 
      hardwareback: 'yes',
      mediaPlaybackRequiresUserAction: 'no',
      shouldPauseOnSuspend: 'no', //Android only 
      closebuttoncaption: 'Close', //iOS only
      disallowoverscroll: 'no', //iOS only 
      toolbar: 'yes', //iOS only 
      enableViewportScale: 'no', //iOS only 
      allowInlineMediaPlayback: 'no',//iOS only 
      presentationstyle: 'pagesheet',//iOS only 
      fullscreen: 'yes',//Windows only  
    };

    const browser = this.iab.create(url, '_blank', options);//inap browser
    browser.show();
  }//END Alert

  decodeAscii(text: string) {
    var txt = document.createElement("textarea");
    txt.innerHTML = text;
    return txt.value;
  }

}
