//Ahmed

import { Component } from '@angular/core';
import { NavController, ViewController, NavParams, IonicPage, Events } from 'ionic-angular';
import { FonctionnairesServiceProvider } from '../../providers/fonctionnaires-service/fonctionnaires-service';
//import { SituationFonctionnairesDetailsPage } from '../situation-fonctionnaires-details/situation-fonctionnaires-details';

import { AlertController } from 'ionic-angular';
//loader
import { LoadingController } from 'ionic-angular';

///// For the modal presenter /////
import { ModalController, ModalOptions } from 'ionic-angular';
//import { Profile } from '../profile/profile';

import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

/**
 * Generated class for the FonctionnairesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
//@IonicPage()
@Component({
    selector: 'page-fonctionnaires',
    templateUrl: 'fonctionnaires.html',
})
export class FonctionnairesPage {
    private foundFonctionnaires;
    relationship = "a0";
    modalPresented = false;

    private itemSituationAdmin = {
        // "service": {
        //     "code": "",
        //     "libelle": "...",
        //     "id": 0,
        //     "administrationId": 0
        // },
        // "administration": {
        //     "code": "",
        //     "libelle": "...",
        //     "id": 0
        // }
    }
    /*
    {

        "editionSituationAdministrative": {
            "dateDebut": "2017-04-01T00:00:00.000Z",
            "dateFin": "2017-04-24T00:00:00.000Z",
            "dateEntree": "1981-01-02T00:00:00.000Z",
            "echelle": "6 ",
            "departement": null,
            "statut": "TITULAIRE",
            "position": "ACTIVITÉ NORMALE",
            "echelon": "9",
            "indice": "249",
            "zone": "B",
            "ppr": "157814",
            "nom": "",
            "prenom": "",
            "cin": "F 777713",
            "nationalite": "Marocaine",
            "dateNaissance": "1960-12-31T00:00:00.000Z",
            "situationFamiliale": "Marié(e)",
            "sexe": "M",
            "villeId": 2389,
            "fonctionId": null,
            "gradeId": 1716,
            "administrationId": 690,
            "serviceId": 49471,
            "ville": {
                "code": "250",
                "libelle": "OUJDA",
                "id": 2389
            },
            "service": {
                "code": "00000282",
                "libelle": "TRESORERIE PREFECTORALE D'OUJDA",
                "id": 49471,
                "administrationId": 690
            },
            "administration": {
                "code": "29",
                "libelle": "ECONOMIE ET FINANCES",
                "id": 690
            },
            "grade": {
                "code": "99150103",
                "libelle": "ADJOINT TECHNIQUE 3EME GRADE",
                "id": 1716
            },
            "enfants": [
                {
                    "rang": "1",
                    "dateDeces": null,
                    "paye": "NON",
                    "lit": "1",
                    "ppr": null,
                    "nom": null,
                    "prenom": "SAIDA",
                    "cin": null,
                    "nationalite": "Marocaine",
                    "dateNaissance": "1991-09-26T00:00:00.000Z",
                    "situationFamiliale": null,
                    "sexe": null,
                    "situationAdministrativeId": "157814"
                },
                {
                    "rang": "2",
                    "dateDeces": null,
                    "paye": "NON",
                    "lit": "2",
                    "ppr": null,
                    "nom": null,
                    "prenom": "ABDENNACER",
                    "cin": null,
                    "nationalite": "Marocaine",
                    "dateNaissance": "1994-06-12T00:00:00.000Z",
                    "situationFamiliale": null,
                    "sexe": null,
                    "situationAdministrativeId": "157814"
                },
                {
                    "rang": "3",
                    "dateDeces": null,
                    "paye": "NON",
                    "lit": "2",
                    "ppr": null,
                    "nom": null,
                    "prenom": "ABDELILAH",
                    "cin": null,
                    "nationalite": "Marocaine",
                    "dateNaissance": "1996-02-28T00:00:00.000Z",
                    "situationFamiliale": null,
                    "sexe": null,
                    "situationAdministrativeId": "157814"
                },
                {
                    "rang": "4",
                    "dateDeces": null,
                    "paye": "OUI",
                    "lit": "2",
                    "ppr": null,
                    "nom": null,
                    "prenom": "BRAHIM",
                    "cin": null,
                    "nationalite": "Marocaine",
                    "dateNaissance": "1999-04-10T00:00:00.000Z",
                    "situationFamiliale": null,
                    "sexe": null,
                    "situationAdministrativeId": "157814"
                }
            ],
            "conjoints": [
                {
                    "rang": "1",
                    "dateEffet": "2000-02-10T00:00:00.000Z",
                    "profession": "NEANT",
                    "ppr": null,
                    "nom": "SOUAD",
                    "prenom": "MOUKHTARI",
                    "cin": "A1111254",
                    "nationalite": "MAROCAINE",
                    "dateNaissance": "1970-03-25T00:00:00.000Z",
                    "situationFamiliale": "MARIE",
                    "sexe": "F",
                    "situationAdministrativeId": "157814"
                },
                {
                    "rang": "2",
                    "dateEffet": "2001-02-10T00:00:00.000Z",
                    "profession": "NEANT",
                    "ppr": null,
                    "nom": "CHAFIA",
                    "prenom": "MOULAY",
                    "cin": "C221299",
                    "nationalite": "MAROCAINE",
                    "dateNaissance": "1980-03-25T00:00:00.000Z",
                    "situationFamiliale": "DIVORVE",
                    "sexe": "F",
                    "situationAdministrativeId": "157814"
                }
            ],
            "reglements": [
                {
                    "mode": "N",
                    "agence": null,
                    "compte": null,
                    "dateDebut": "1985-05-01T00:00:00.000Z",
                    "dateFin": "1991-03-01T00:00:00.000Z",
                    "id": 1,
                    "villeId": null,
                    "situationAdministrativeId": "157814"
                },
                {
                    "mode": "V",
                    "agence": "BANQUE POPULAIRE OUJDA IDRISS EL_AKBAR B. - BANQUE POPULAIRE",
                    "compte": "1623746",
                    "dateDebut": "1991-03-01T00:00:00.000Z",
                    "dateFin": null,
                    "id": 2,
                    "villeId": 2389,
                    "situationAdministrativeId": "157814"
                }
            ],
            "igrs": [],
            "allocationsfamiliales": [
                {
                    "situation": "Déduction d'impot auto",
                    "dateEffet": "2015-07-01T00:00:00.000Z",
                    "dateDebut": "1994-06-12T00:00:00.000Z",
                    "observation": null,
                    "rang": "1",
                    "id": 3,
                    "situationAdministrativeId": "157814"
                },
                {
                    "situation": "Déduction d'impot auto",
                    "dateEffet": "2012-10-01T00:00:00.000Z",
                    "dateDebut": "1991-09-26T00:00:00.000Z",
                    "observation": null,
                    "rang": "2",
                    "id": 4,
                    "situationAdministrativeId": "157814"
                },
                {
                    "situation": "Déduction d'impot auto",
                    "dateEffet": "2017-03-01T00:00:00.000Z",
                    "dateDebut": "1996-02-28T00:00:00.000Z",
                    "observation": null,
                    "rang": "3",
                    "id": 5,
                    "situationAdministrativeId": "157814"
                },
                {
                    "situation": "Enfant Scolarisé auto",
                    "dateEffet": "2015-05-01T00:00:00.000Z",
                    "dateDebut": "1999-04-10T00:00:00.000Z",
                    "observation": null,
                    "rang": "4",
                    "id": 6,
                    "situationAdministrativeId": "157814"
                }
            ],
            "mutuelles": [
                {
                    "dateDebut": "2001-09-01T00:00:00.000Z",
                    "dateFin": null,
                    "taux": 1,
                    "id": 1,
                    "typeMutuelleId": 9,
                    "situationAdministrativeId": "157814"
                },
                {
                    "dateDebut": "2001-09-01T00:00:00.000Z",
                    "dateFin": null,
                    "taux": 1,
                    "id": 2,
                    "typeMutuelleId": 3,
                    "situationAdministrativeId": "157814"
                },
                {
                    "dateDebut": "2001-09-01T00:00:00.000Z",
                    "dateFin": null,
                    "taux": 1,
                    "id": 3,
                    "typeMutuelleId": 2,
                    "situationAdministrativeId": "157814"
                },
                {
                    "dateDebut": "2001-09-01T00:00:00.000Z",
                    "dateFin": null,
                    "taux": 1,
                    "id": 4,
                    "typeMutuelleId": 12,
                    "situationAdministrativeId": "157814"
                }
            ],
            "prelevements": [
                {
                    "numeroDossier": "59665B6",
                    "libelle": "SOCIETE DE CREDIT EQDOM",
                    "montantMensuel": 217,
                    "montantGlobal": 13037,
                    "dateDebut": "2016-11-01T00:00:00.000Z",
                    "dateFin": "2021-11-01T00:00:00.000Z",
                    "id": 15,
                    "rubriqueId": 689,
                    "situationAdministrativeId": "157814"
                },
                {
                    "numeroDossier": "66601B6",
                    "libelle": "SOCIETE DE CREDIT EQDOM",
                    "montantMensuel": 217,
                    "montantGlobal": 13037,
                    "dateDebut": "2016-12-01T00:00:00.000Z",
                    "dateFin": "2021-12-01T00:00:00.000Z",
                    "id": 16,
                    "rubriqueId": 689,
                    "situationAdministrativeId": "157814"
                },
                {
                    "numeroDossier": "01402B7",
                    "libelle": "SOCIETE DE CREDIT EQDOM",
                    "montantMensuel": 436,
                    "montantGlobal": 28758,
                    "dateDebut": "2017-02-01T00:00:00.000Z",
                    "dateFin": "2022-08-01T00:00:00.000Z",
                    "id": 17,
                    "rubriqueId": 689,
                    "situationAdministrativeId": "157814"
                },
                {
                    "numeroDossier": "08639B7",
                    "libelle": "SOCIETE DE CREDIT EQDOM",
                    "montantMensuel": 217,
                    "montantGlobal": 13037,
                    "dateDebut": "2017-03-01T00:00:00.000Z",
                    "dateFin": "2022-03-01T00:00:00.000Z",
                    "id": 18,
                    "rubriqueId": 689,
                    "situationAdministrativeId": "157814"
                },
                {
                    "numeroDossier": "70459B6",
                    "libelle": "SOCIETE DE CREDIT EQDOM",
                    "montantMensuel": 247,
                    "montantGlobal": 16272,
                    "dateDebut": "2017-03-01T00:00:00.000Z",
                    "dateFin": "2022-09-01T00:00:00.000Z",
                    "id": 19,
                    "rubriqueId": 689,
                    "situationAdministrativeId": "157814"
                }
            ]
        }

    };//item
    */

    private itemASalaire = {}
    /*
    {
        "attestationSalaire": {
            "dateDebut": "2017-04-01T00:00:00.000Z",
            "dateFin": "2017-04-24T00:00:00.000Z",
            "dateEntree": "1981-01-02T00:00:00.000Z",
            "echelle": "6 ",
            "departement": null,
            "statut": "TITULAIRE",
            "position": "ACTIVITÉ NORMALE",
            "echelon": "9",
            "indice": "249",
            "zone": "B",
            "ppr": "157814",
            "nom": "",
            "prenom": "",
            "cin": "F 777713",
            "nationalite": "Marocaine",
            "dateNaissance": "1960-12-31T00:00:00.000Z",
            "situationFamiliale": "Marié(e)",
            "sexe": "M",
            "villeId": 2389,
            "fonctionId": null,
            "gradeId": 1716,
            "administrationId": 690,
            "serviceId": 49471,
            "situationPecuniere": {
                "imputation": "ECONOMIE ET FINANCES",
                "totalBrutBrutAnnuel": 0,
                "netAnnuel": 41615,
                "netMensuel": 0,
                "totalRetenuesAnnuelles": 0,
                "brutImposable": 0,
                "id": 5,
                "situationAdministrativeId": "157814",
                "situationPecuniereDetails": [
                    {
                        "montantTotal": 18907,
                        "id": 46,
                        "rubriqueId": 2,
                        "typeRubriqueId": 3,
                        "situationPecuniereId": 5,
                        "rubrique": {
                            "code": "1200",
                            "libelle": "TRAITEMENT DE BASE",
                            "id": 2
                        },
                        "typeRubrique": {
                            "code": "EMOLUMENT",
                            "libelle": "Emolument",
                            "id": 3
                        }
                    },
                    {
                        "montantTotal": 22512,
                        "id": 47,
                        "rubriqueId": 73,
                        "typeRubriqueId": 3,
                        "situationPecuniereId": 5,
                        "rubrique": {
                            "code": "1291",
                            "libelle": "INDEMNITE DE TECHNICITE",
                            "id": 73
                        },
                        "typeRubrique": {
                            "code": "EMOLUMENT",
                            "libelle": "Emolument",
                            "id": 3
                        }
                    },
                    {
                        "montantTotal": 3660,
                        "id": 48,
                        "rubriqueId": 103,
                        "typeRubriqueId": 3,
                        "situationPecuniereId": 5,
                        "rubrique": {
                            "code": "1328",
                            "libelle": "INDEMNITE DE SUJETION",
                            "id": 103
                        },
                        "typeRubrique": {
                            "code": "EMOLUMENT",
                            "libelle": "Emolument",
                            "id": 3
                        }
                    },
                    {
                        "montantTotal": 1891,
                        "id": 49,
                        "rubriqueId": 128,
                        "typeRubriqueId": 3,
                        "situationPecuniereId": 5,
                        "rubrique": {
                            "code": "1358",
                            "libelle": "INDEMNITE DE RESIDENCE DE BASE",
                            "id": 128
                        },
                        "typeRubrique": {
                            "code": "EMOLUMENT",
                            "libelle": "Emolument",
                            "id": 3
                        }
                    },
                    {
                        "montantTotal": 2400,
                        "id": 50,
                        "rubriqueId": 172,
                        "typeRubriqueId": 3,
                        "situationPecuniereId": 5,
                        "rubrique": {
                            "code": "1402",
                            "libelle": "INDEMNITE FAMILIALE MAROCAINE",
                            "id": 172
                        },
                        "typeRubrique": {
                            "code": "EMOLUMENT",
                            "libelle": "Emolument",
                            "id": 3
                        }
                    },
                    {
                        "montantTotal": 60,
                        "id": 51,
                        "rubriqueId": 316,
                        "typeRubriqueId": 1,
                        "situationPecuniereId": 5,
                        "rubrique": {
                            "code": "4117",
                            "libelle": "COTISATION ADHESION FOSMEF",
                            "id": 316
                        },
                        "typeRubrique": {
                            "code": "RETENUE",
                            "libelle": "Retenue",
                            "id": 1
                        }
                    },
                    {
                        "montantTotal": 180,
                        "id": 52,
                        "rubriqueId": 562,
                        "typeRubriqueId": 1,
                        "situationPecuniereId": 5,
                        "rubrique": {
                            "code": "4802",
                            "libelle": "MUTUELLE GENER. CAISSE COMPL.DE DECES",
                            "id": 562
                        },
                        "typeRubrique": {
                            "code": "RETENUE",
                            "libelle": "Retenue",
                            "id": 1
                        }
                    },
                    {
                        "montantTotal": 5636,
                        "id": 53,
                        "rubriqueId": 574,
                        "typeRubriqueId": 1,
                        "situationPecuniereId": 5,
                        "rubrique": {
                            "code": "4814",
                            "libelle": "CAISSE MAROCAINE DE RETRAITE (CIVIL)",
                            "id": 574
                        },
                        "typeRubrique": {
                            "code": "RETENUE",
                            "libelle": "Retenue",
                            "id": 1
                        }
                    },
                    {
                        "montantTotal": 705,
                        "id": 54,
                        "rubriqueId": 603,
                        "typeRubriqueId": 1,
                        "situationPecuniereId": 5,
                        "rubrique": {
                            "code": "4842",
                            "libelle": "MUTUELLE GENER. SECTEUR MUTUALISTE",
                            "id": 603
                        },
                        "typeRubrique": {
                            "code": "RETENUE",
                            "libelle": "Retenue",
                            "id": 1
                        }
                    },
                    {
                        "montantTotal": 1174,
                        "id": 55,
                        "rubriqueId": 634,
                        "typeRubriqueId": 1,
                        "situationPecuniereId": 5,
                        "rubrique": {
                            "code": "4873",
                            "libelle": "ASSURANCE MALADIE OBLIGATOIRE (BASE CMR)",
                            "id": 634
                        },
                        "typeRubrique": {
                            "code": "RETENUE",
                            "libelle": "Retenue",
                            "id": 1
                        }
                    }
                ]
            },
            "ville": {
                "code": "250",
                "libelle": "OUJDA",
                "id": 2389
            },
            "service": {
                "code": "00000282",
                "libelle": "TRESORERIE PREFECTORALE D'OUJDA",
                "id": 49471,
                "administrationId": 690
            },
            "administration": {
                "code": "29",
                "libelle": "ECONOMIE ET FINANCES",
                "id": 690
            },
            "grade": {
                "code": "99150103",
                "libelle": "ADJOINT TECHNIQUE 3EME GRADE",
                "id": 1716
            },
            "enfants": [
                {
                    "rang": "1",
                    "dateDeces": null,
                    "paye": "NON",
                    "ppr": null,
                    "nom": null,
                    "prenom": "SAIDA",
                    "cin": null,
                    "nationalite": "Marocaine",
                    "dateNaissance": "1991-09-26T00:00:00.000Z",
                    "situationFamiliale": null,
                    "sexe": null,
                    "situationAdministrativeId": "157814"
                },
                {
                    "rang": "2",
                    "dateDeces": null,
                    "paye": "NON",
                    "ppr": null,
                    "nom": null,
                    "prenom": "ABDENNACER",
                    "cin": null,
                    "nationalite": "Marocaine",
                    "dateNaissance": "1994-06-12T00:00:00.000Z",
                    "situationFamiliale": null,
                    "sexe": null,
                    "situationAdministrativeId": "157814"
                },
                {
                    "rang": "3",
                    "dateDeces": null,
                    "paye": "NON",
                    "ppr": null,
                    "nom": null,
                    "prenom": "ABDELILAH",
                    "cin": null,
                    "nationalite": "Marocaine",
                    "dateNaissance": "1996-02-28T00:00:00.000Z",
                    "situationFamiliale": null,
                    "sexe": null,
                    "situationAdministrativeId": "157814"
                },
                {
                    "rang": "4",
                    "dateDeces": null,
                    "paye": "OUI",
                    "ppr": null,
                    "nom": null,
                    "prenom": "BRAHIM",
                    "cin": null,
                    "nationalite": "Marocaine",
                    "dateNaissance": "1999-04-10T00:00:00.000Z",
                    "situationFamiliale": null,
                    "sexe": null,
                    "situationAdministrativeId": "157814"
                }
            ]
        }
    };//item
    */

    public queryText = "1168305";// ppr 157814, ppr 1168305
    public segment = 'all';

    private token = '5367svO8wPzuCwMPwAN9LlGAF8HXrzQckimF1SHRjM1jvNTZM72ZKIHzSkOJsoNq';
    private PPR = 157814;

    constructor(
        private fonctionnairesService: FonctionnairesServiceProvider,
        public alertCtrl: AlertController,
        public navCtrl: NavController,
        private nav: NavController,
        public navParams: NavParams,
        public loadingCtrl: LoadingController,
        public modalCtrl: ModalController,
        private auth: AuthServiceProvider, public events: Events) {
        //Static PPR

        this.auth.getUserAccess().then((val) => {
            if (val) {
                this.PPR = val.userid;
                console.log('\n userId storage', this.PPR);

                this.auth.getTokenData().then((val) => {
                    if (typeof val != 'undefined' && val != null) {
                        this.token = val.id;
                        console.log('\n token storage', this.token);
                        // //Init
                        this.getAttestationSalaireByPPR(this.PPR, this.token);
                        // this.getSituationPrelevementByPPR(this.PPR, this.token);
                    }
                })
            }
        })


    }


    private modal;
    isSituAdminInfoDispo: boolean = false;
    presentModal() {
        this.modal = this.modalCtrl.create(OptionsList, { selected: this.relationship });
        this.modal.present();
        this.modalPresented = true;
        this.modal.onDidDismiss(data => {
            this.modalPresented = false;
            console.log(data);
            if (data)
                if (data.selected) {
                    this.relationship = data.selected;
                    if (!this.isSituAdminInfoDispo) {
                        if (this.relationship != "a0" && this.relationship != "a1") {
                            this.auth.getUserAccess().then((val) => {
                                if (val) {
                                    this.PPR = val.userid;
                                    console.log('\n user ppr storage', this.PPR);

                                    this.auth.getTokenData().then((val) => {
                                        if (val) {
                                            this.token = val.id;
                                            console.log('\n token storage', this.token);
                                            this.getSituationAdministrativeByPPR(this.PPR, this.token);
                                        }
                                    })
                                }
                            })
                        }
                    }

                }
        });
    }

    ionViewDidLeave() {
        if (this.modalPresented) this.modal.dismiss();
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad SituationFonctionnairesPage');
        this.presentModal();
    }
    //specific to angular 2
    ngOnInit() {
    }

    getAttestationSalaireByPPR(ppr, token) {

        this.presentLoading();

        this.fonctionnairesService.getAttestationSalaireByPPR(ppr, token).subscribe(
            data => {
                this.itemASalaire = data.json();
                console.log('items A salaire !!', this.itemASalaire);

                this.dismissLoading()
            },
            err => {
                this.dismissLoading()

                if (err.status == 404) {
                    this.showAlert('', 'Erreur 404 !!');
                } else if (err.status == 401) {
                    this.logout(() => {
                        this.showAlert('', (JSON.parse(err["_body"]).error && JSON.parse(err["_body"]).error.message) ? JSON.parse(err["_body"]).error.message : 'Erreur 401!');
                    });
                }
                // else if (err.status == 0) {
                //   this.showAlert('Ooops', "Oooops !! PPR Doesn't exist !!");
                // }
                else {
                    console.error(err);
                    this.showAlert("Nous sommes désolés!", "Service fonctionnaire indisponible veuillez réessayer ultérieurement");
                }
                console.error("getAttestationSalaireByPPR Error!! " + err);

            }
        );//End service

        ///////////////////////////////////////////End loader
    }//End getAttestationSalaireByPPR

    getSituationAdministrativeByPPR(ppr, token) {
        ////////////////////////// Loader //////////////////////////////
        this.presentLoading();

        this.fonctionnairesService.getSituationAdministrativeByPPR(ppr, token).subscribe(
            data => {
                this.itemSituationAdmin = data.json();
                this.isSituAdminInfoDispo = true;
                console.log('itemSituationAdmin', this.itemSituationAdmin);

                this.dismissLoading()
            },
            err => {
                this.dismissLoading()

                console.error(err);

                if (err.status == 404) {
                    this.showAlert('', 'Erreur 404 !!');
                } else if (err.status == 401) {

                    this.logout(() => {
                        this.showAlert('', (JSON.parse(err["_body"]).error && JSON.parse(err["_body"]).error.message) ? JSON.parse(err["_body"]).error.message : 'Erreur 401!');
                    });
                }

                // else if (err.status == 0) {
                //   this.showAlert('Ooops', "Oooops !! PPR Doesn't exist !!");
                // }
                else {
                    console.error(err);
                    this.showAlert("Nous sommes désolés!", "Service fonctionnaire indisponible veuillez réessayer ultérieurement");
                }
                console.error("getEmployeeByPPR Error!! " + err);


            }
        );//End service
        ///////////////////////////////////////////End loader
    }//End getSituationAdministrativeByPPR


    private loader;
    presentLoading() {
        this.loader = this.loadingCtrl.create({
            content: "Chargement des données...",
            showBackdrop: true,
            enableBackdropDismiss: true
        });
        /*
        this.loader.onDidDismiss(() => {
        console.log('Dismissed loading');
        });*/

        this.dismissed = false;
        this.loader.present();
    }

    dismissed = false;
    dismissLoading() {
        console.log('--Dismissed loading--');
        if (!this.dismissed) this.loader.dismiss();
        this.dismissed = true;

    }


    /////////////////////// present modal //////////////////////////////////////////


    // constructor(public modalCtrl: ModalController)
    /*
    presentProfileModal() {
        let profileModal = this.modalCtrl.create(Profile, { userId: 8675309 });
        profileModal.present();
    }
    */
    /////////////////////////// END  present modal /////////////////////////////////

    ///////////////////////// present modal //////////////////////////////////////////
    //
    //import { ModalController, NavParams } from 'ionic-angular';
    //
    // constructor(public modalCtrl: ModalController)
    //
    // presentProfileModal() {
    // let profileModal = this.modalCtrl.create(Profile, { userId: 8675309 });
    // profileModal.present();
    // }
    ///////////////////////////// END  present modal /////////////////////////////////




    /*
    goToDetails(item) {
        this.nav.push(SituationFonctionnairesDetailsPage, { item: item });
    }*/
    /**
    * reuseable Alert box.
    */
    showAlert(title, message) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }

    logout(cbFinish?) {
        if (this.auth.currentUser) {
            let loading = this.loadingCtrl.create({
                content: 'Traitement en cours...',
                dismissOnPageChange: true,
                showBackdrop: true,
                enableBackdropDismiss: true
            });
            loading.present();

            this.events.publish('user:doLogout', Date.now());
            var isCbExec = false;
            if (cbFinish) this.events.subscribe('user:logout', (xdate) => {
                if (!isCbExec) {
                    isCbExec = true;
                    cbFinish()
                };
                if (loading.isOverlay) loading.dismiss();
            });
        }
    }

    formatedFLoat(str) {
        let res = String(parseFloat(str).toFixed(2)).replace('.', ',');
        if (res == 'NaN') res = "";
        return res;
    }

}

import { Renderer } from '@angular/core';
@Component({
    selector: 'options-list',
    template: `
    <ion-header class="iOptions">
      <ion-toolbar color="fonctionnaire">
        <ion-buttons end>
          <button ion-button (click)='dismiss()'  icon-only>
            <ion-icon name="close"></ion-icon>
          </button>
        </ion-buttons>
        <ion-title>Veuillez choisir</ion-title>
      </ion-toolbar>

    </ion-header>

    <ion-content style="background">
        <ion-list>
            <ion-item (click)="setSelectedItem('a1')" [ngClass]="(selectedItem=='a1') ? 'selectedOne' : ''">
                <ion-avatar item-left style="position: relative;">
                    <ion-icon class="sIcon" name="radio-button-on" [color]="(selectedItem=='a1') ? 'secondary': 'fonctionnaire'"></ion-icon>
                </ion-avatar>
                Attestation de salaire
            </ion-item>
            <ion-item [ngClass]="(selectedItem=='a2') ? 'selectedOne' : ''" (click)="setSelectedItem('a2')">
                <ion-avatar item-left style="position: relative;">
                    <ion-icon class="sIcon" name="radio-button-on" [color]="(selectedItem=='a2') ? 'secondary': 'fonctionnaire'"></ion-icon>
                </ion-avatar>
                Situation Administrative
            </ion-item>
            <ion-item [ngClass]="(selectedItem=='a3') ? 'selectedOne' : ''" (click)="setSelectedItem('a3')">  
                <ion-avatar item-left style="position: relative;">
                    <ion-icon class="sIcon" name="radio-button-on" [color]="(selectedItem=='a3') ? 'secondary': 'fonctionnaire'"></ion-icon>
                </ion-avatar>  
                Situation Prélèvements
            </ion-item>
            <ion-item [ngClass]="(selectedItem=='a4') ? 'selectedOne' : ''" (click)="setSelectedItem('a4')">
                <ion-avatar item-left style="position: relative;">
                    <ion-icon class="sIcon" name="radio-button-on" [color]="(selectedItem=='a4') ? 'secondary': 'fonctionnaire'"></ion-icon>
                </ion-avatar>
                Situation enfants
            </ion-item>
            <ion-item [ngClass]="(selectedItem=='a5') ? 'selectedOne' : ''" (click)="setSelectedItem('a5')">
                <ion-avatar item-left style="position: relative;">
                    <ion-icon class="sIcon" name="radio-button-on" [color]="(selectedItem=='a5') ? 'secondary': 'fonctionnaire'"></ion-icon>
                </ion-avatar>
                Situation conjoint
            </ion-item>
            <ion-item [ngClass]="(selectedItem=='a6') ? 'selectedOne' : ''" (click)="setSelectedItem('a6')">
                <ion-avatar item-left style="position: relative;">
                    <ion-icon class="sIcon" name="radio-button-on" [color]="(selectedItem=='a6') ? 'secondary': 'fonctionnaire'"></ion-icon>
                </ion-avatar>
                Situation Règlements
            </ion-item>
            <ion-item [ngClass]="(selectedItem=='a7') ? 'selectedOne' : ''" (click)="setSelectedItem('a7')">   
                <ion-avatar item-left style="position: relative;">
                    <ion-icon class="sIcon" name="radio-button-on" [color]="(selectedItem=='a7') ? 'secondary': 'fonctionnaire'"></ion-icon>
                </ion-avatar>
                Allocations familiales
            </ion-item>
            <ion-item [ngClass]="(selectedItem=='a8') ? 'selectedOne' : ''" (click)="setSelectedItem('a8')"> 
                <ion-avatar item-left style="position: relative;">
                    <ion-icon class="sIcon" name="radio-button-on" [color]="(selectedItem=='a8') ? 'secondary': 'fonctionnaire'"></ion-icon>
                </ion-avatar>
                Mutuelles
            </ion-item>
        </ion-list>
    </ion-content>
    `
    , styles: [
        `
    ion-content {
        background: rgba(255, 255, 255, 0.72) !important;
    }
    ion-item {
        background-color: rgba(255, 255, 255, 0.66) !important;
        margin-bottom: 0.4em;
    }

    .sIcon{
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }

    .selectedOne{
        background-color: rgba(210, 178, 138, 0.66) !important;
    }

    `
    ]
})
export class OptionsList {

    constructor(public renderer: Renderer, public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams) {
        this.selectedItem = navParams.get("selected");
        this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'options-popup', true);

    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad OptionsList');
    }

    selectedItem;
    setSelectedItem(data) {
        this.selectedItem = data;
        this.dismiss();
    }

    dismiss() {
        this.viewCtrl.dismiss({ selected: this.selectedItem });
    }

}
