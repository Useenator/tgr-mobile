import { Component, Directive } from '@angular/core';
import { NavController, Events } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { Response, RequestOptions } from '@angular/http';
import { Http, Headers } from '@angular/http';

import { Observable } from 'rxjs/Rx';

import { AlertController } from 'ionic-angular';

import { UsersService } from '../../providers/users.service/users.service';

import { RegisterStep2Page } from '../register-step2/register-step2';

import { LoginPage } from "../login/login";

import { Utils } from '../../MyUtils/Utils';

import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

import * as EmailValidator from 'email-validator';

/*
 Adhésion :

 

Garder l’Astérisque pour les champs obligatoires

(*) champs obligatoires

 

Ajouter ces descriptions au dessous des champs concernés

-          login entre 5 et 15

-          Login=PPR si l’utilisateur est un fonctionnaire de l’état

-          mot de passe min 8

 

 

-          après la création de l’adhérent, afficher un message création avec succès : votre code d’activation est envoyé par mail

 

-          se rediriger vers l’écran d’activation

 

-          dans l’écran d’activation :  indiquer que le code est envoyé par mail

 

ð  l’adhésion ne passe pas, aucun message n’est affiché , l’API est opérationnelle

 
 
 */

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
  //providers: [UsersService]
})

export class RegisterPage {
  private foundUser;
  private data;
  private _http;


  private contribuableFlag = false;
  private fonctionnaireFlag = false;
  // ngSwitch
  private SendObject = {
    "nomPrenom": "",
    "raisonSociale": "",
    "cin": "",
    "rc": "",
    "mail": "",
    "mobile": "",
    "activite": "",
    "adresse": "",
    "login": "",
    "password": "",
    "ppr": "157814",
    "codeContribuables": [],
    "ribs": [],
    "_etatAdherent": { "dateEtat": "2017-05-25T14:28:05.862Z", "typeEtatId": 1 },
    "reference_avis": "85595613",
    "montant_avis": "162.9",
    "net_mensuel": "",
    "derniere_entree": ""
  };

  constructor(
    private userService: UsersService,
    private nav: NavController,
    _http: Http,

    public loading: LoadingController,
    public alertCtrl: AlertController,
    private auth: AuthServiceProvider,
    private events: Events
  ) {
    this.connectedUser = this.auth.currentUser;

    this.data = {};
    this.data.nom = '';
    this.data.prenom = '';
    this.data.raisonSociale = '';
    this.data.cin = '';
    this.data.rc = '';
    this.data.mail = '';
    this.data.mobile = '';
    this.data.fixe = '';
    this.data.activite = '';
    this.data.adresse = '';
    this.data.login = '';
    this.data.password = '';
    this.data.ppr = '';
    this.data.codeContribuables = [];
    this.data.ribs = [];
    this.data._etatAdherent = {
      "dateEtat": "2017-05-25T14:28:05.862Z",
      "typeEtatId": 1
    };
    this.data.cin = '';

    this.data.password = '';
    //Contribuable
    this.data.reference_avis = '85595613';
    this.data.montant_avis = '162.9';

    //Fonctionnaire
    // this.data.ppr = '157814';
    this.data.net_mensuel = '';
    this.data.derniere_entree = '';

    this.data.response = '';
    this._http = _http;

    console.log(" JSON DATA ", JSON.stringify(this.data));
  }

  connectedUser;
  logout() {
    let loading = this.loading.create({
      content: 'Traitement en cours...',
      dismissOnPageChange: true
    });
    loading.present();

    this.events.publish('user:doLogout', Date.now());
  }

  cancelClick() {
    console.log("go to login page");

    this.nav.setRoot(LoginPage);
  }

  /**
  *
  * @param e
  */
  fonctionnaireFlagShow(e) {
    if (e.checked) {
      this.fonctionnaireFlag = true;
    } else {
      this.fonctionnaireFlag = false;
    }
  }

  /**
  *
  * @param e
  */
  contribuableFlagShow(e) {
    if (e.checked) {
      this.contribuableFlag = true;
    } else {
      this.contribuableFlag = false;
    }

  }



  /**
  * https://api.tgr.gov.ma/Adherents/verifierLogin?login=157814

  * si existant   ce login existe déjà retenter  
  * sinon : continuer la saisie du formulaire
 */
  isLoginAvailable(login, email, userModel) {
    console.log('checkLoginAvailability begin')
//    Utils.presentLoading(this.loading);
    this.presentLoading();
    this.userService.checkLoginAvailability(login).subscribe(
      data => {
        var foundLogin;
        try {
          foundLogin = data.json();
          console.log('\n foundItems .. checkLoginAvailability!!! ', JSON.stringify(data.json()));
        } catch (err) {
          console.log('\n ERROR .. checkLoginAvailability!!! ', err);
        }



        //TODO: need to check the returned object to know if the login is free or no

        //if login is used
        // console.log('\n isLoginAvalibale !!! ', );
        //return true;

        //if (foundLogin.result as boolean == true) {
        if (foundLogin.result as boolean == false) {
          console.log('\n isLoginAvalibale !!! !!! ', foundLogin.result);
          // callback();
          /////////////////////////////////////
          // this.goToStep2(userModel);
          // Utils.dismissLoading();
          this.dismissLoading();
          this.isEmailAvailable(email, userModel);

          /////////////////////////////////////
        } else {
          Utils.showAlert(this.alertCtrl,"", "Login déjà utilisé..!");
        }

        // return false;
        //let's assume the login is dispo
        // callback();
        // this.dismissLoading();
      },
      err => {
        console.error(err);

        //show a nice error message to the user
        Utils.showAlert(this.alertCtrl,"Nous sommes désolés!", "Service de verification de login indisponible veuillez réessayer ultérieurement");
        this.dismissLoading();

      },
      () => {
        console.log('checkLoginAvailability completed')
        this.dismissLoading();;
        //return true;
      }

    );//subscrib
  }//END isLoginAvailable

  /**
    * Adhérent :
    *    Api ajoutée vérifier Email :
    *    https://api.tgr.gov.ma/api/Adherents/verifierEmail?mail={testingMail}  
 */
  isEmailAvailable(email, userModel) {

//import * as EmailValidator from 'email-validator';
    //Verify email
    if(!EmailValidator.validate(email)){
       Utils.showAlert(this.alertCtrl,"","Merci de vérifier votre e-mail")
       console.log("==>Merci de vérifier votre Email",email);
       
       return;
       //result = false;
     }

    console.log('checkEmailAvailability begin')
    this.presentLoading();
    this.userService.checkEmailAvailibility(email).subscribe(
      data => {
        var foundEmail;
        try {
          foundEmail = data.json();
          console.log('\n foundItems .. checkEmailAvailability!!! '+ JSON.stringify(data.json()));
        } catch (err) {
          console.log('\n ERROR .. checkEmailAvailability ', err);
        }

        console.log('\n EMAIL ', email);
        console.log('\n isEmailnAvalibale !!! before ', foundEmail.result);
        //TODO: need to check the returned object to know if the login is free or no
        // if (foundEmail.result as boolean == true) {
        // if (foundEmail.result as boolean == false) {
          if (!foundEmail.result) {
          console.log('\n isEmailnAvalibale !!! !!! ', foundEmail.result);
          // callback();
          /////////////////////////////////////
          this.goToStep2(userModel);
          /////////////////////////////////////
        } else {
          Utils.showAlert(this.alertCtrl,"", "Email déjà utilisé..!");
        }


      },
      err => {
        console.error(err);

        //show a nice error message to the user
        Utils.showAlert(this.alertCtrl,"Nous sommes désolés!", "Service de verification de Email indisponible veuillez réessayer ultérieurement");
        // this.dismissLoading();
        this.dismissLoading();

      },
      () => {
        console.log('checkEmailAvailability completed')
        this.dismissLoading();
        //return true;
      }

    );//subscrib
  }//END isEmailAvailable


  /**
   * 
   */
  goToStep2(userModel) {
    this.nav.push(RegisterStep2Page, { user: userModel });
  }



  comfirmPassword = "";
  codeContribuable = "";


  
  isFormValid = false;
  checkLogin(){
    
    if(this.data.login.trim().length < 5 || this.data.login.trim().length > 15){
      this.isFormValid = false
    }else{
      var alphanumeric = this.data.login;
      var myRegEx  = /^[A-Za-z0-9]+$/i;
      var isLoginValid = (myRegEx.test(alphanumeric));
      this.isFormValid = isLoginValid
      console.log("checkForm", isLoginValid);
    }
   
  }

  /**
   * 
   */
  newUser() {
    let dateNow = new Date().toISOString();
    console.log('DATE_NOW', dateNow);
    //prepare userObject
    var userModel;
    var codeContribuable;
    userModel = {
      "nom": this.data.nom,
      "prenom": this.data.prenom,
      "raisonSociale": this.data.raisonSociale,
      "cin": this.data.cin,
      "rc": this.data.rc,
      "mail": this.data.mail,
      "fixe": this.data.fixe,
      "mobile": this.data.mobile,
      "activite": this.data.activite,
      "adresse": this.data.adresse,
      "login": this.data.login,
      "password": this.data.password,
      // "ppr": this.data.ppr,
      "codeContribuables": [],
      // "ribs": [],
      "_etatAdherent":
      {
        "dateEtat": dateNow,//"2017-05-25T14:28:05.862Z",//TODO: need to add the date of registraction
        "typeEtatId": 1  //  id etat adhérent =1 CREE
      }
    }//userModel

    if (this.comfirmPassword != this.data.password || this.comfirmPassword.trim() == "") {
      Utils.showAlert(this.alertCtrl,"Error", "Erreur de confirmation du mot de passe");
      return;
    } else {
      // this.presentLoading();
      //Check if the login is free or no
      this.isLoginAvailable(this.data.login, this.data.mail, userModel);// END isLogin available

    }//END if check password

    



  }//END newUser























  ////////////////////////////////////// Alert ///////////////////////////////////////
  // showAlert(title, message) {
  //   let alert = this.alertCtrl.create({
  //     title: title,
  //     subTitle: message,
  //     buttons: ['OK']
  //   });
  //   alert.present();
  // }


  ///////////////////////////////////// loader; ////////////////////////////////
  private loader;
  // /**
  //  * Show loading
  //  */
  presentLoading() {
    this.loader = this.loading.create({
      content: "Patienter svp ...",
      showBackdrop: true,
      enableBackdropDismiss: true,
      dismissOnPageChange: true
    });

    this.loader.onDidDismiss(() => {
      console.log('Dismissed loading');
    });

    this.loader.present();

    setTimeout(() => {
      this.loader.dismiss();
    }, 15000);
  }

  // /**
  //  * Dismiss loading
  //  */
  dismissLoading() {
    console.log('--Dismissed loading--');
    if (!this.loader.Dismissed) this.loader.dismissAll();
  }
}
