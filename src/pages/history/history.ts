//Ahmed

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, Events } from 'ionic-angular';

import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { BankServiceProvider } from '../../providers/bank-service/bank-service';

import { Storage } from '@ionic/storage';

/**
* Generated class for the HistoryPage page.
*
* See http://ionicframework.com/docs/components/#navigation for more info
* on Ionic pages and navigation.
*/
//@IonicPage()
@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {
  //constructor(public navCtrl: NavController, public navParams: NavParams) {}
  public token;
  public userId;
  private _storage;
  // public foundItems = { dernieresOperations: {} };

  foundItemsOffline = {
    dernieresOperations: {
      rib: "..",
      identite: "..",
      agenceId: null,
      typePersonneId: null,
      appUserId: null,
      operations: [
        {
          montant: 500,
          date: "2012-02-29T00:00:00.000Z",
          numero: "010512417001",
          libelle: "RETRAIT GAB",
          numeroPiece: "800",
          dateValeur: "2012-02-29T00:00:00.000Z",
          id: 402,
          sensOperationId: 4,
          natureOperationId: null,
          compteId: "310810100011200804360147",
          sensOperation: {
            code: "DEBIT",
            libelle: "Debit",
            id: 4
          }
        },
        {
          montant: 800,
          date: "2012-02-29T00:00:00.000Z",
          numero: "010512417001",
          libelle: "RETRAIT GAB",
          numeroPiece: "800",
          dateValeur: "2012-02-29T00:00:00.000Z",
          id: 402,
          sensOperationId: 3,
          natureOperationId: null,
          compteId: "310810100011200804360147",
          sensOperation: {
            code: "DEBIT",
            libelle: "Debit",
            id: 4
          }
        },
        {
          montant: 1200,
          date: "2012-02-29T00:00:00.000Z",
          numero: "010512417001",
          libelle: "RETRAIT GAB",
          numeroPiece: "800",
          dateValeur: "2012-02-29T00:00:00.000Z",
          id: 402,
          sensOperationId: 3,
          natureOperationId: null,
          compteId: "310810100011200804360147",
          sensOperation: {
            code: "DEBIT",
            libelle: "Debit",
            id: 4
          }
        },
        {
          montant: 300,
          date: "2012-02-29T00:00:00.000Z",
          numero: "010512417001",
          libelle: "RETRAIT GAB",
          numeroPiece: "800",
          dateValeur: "2012-02-29T00:00:00.000Z",
          id: 402,
          sensOperationId: 4,
          natureOperationId: null,
          compteId: "310810100011200804360147",
          sensOperation: {
            code: "DEBIT",
            libelle: "Debit",
            id: 4
          }
        }
      ],
      agence: {
        code: "..",
        libelle: "..",
        id: 3
      },
      typePersonne: {
        code: "..",
        libelle: "..",
        id: null
      }
    }
  };

  items : Array<any>;


  public foundItems = {
    dernieresOperations: {
      rib: "..",
      identite: "..",
      agenceId: null,
      typePersonneId: null,
      appUserId: null,
      operations: [
      ],
      agence: {
        code: "..",
        libelle: "..",
        id: 3
      },
      typePersonne: {
        code: "..",
        libelle: "..",
        id: null
      }
    }
  };
  /*
  {
  montant: 200,
  date: "2012-02-29T00:00:00.000Z",
  numero: "010512417001",
  libelle: "RETRAIT GAB",
  numeroPiece: "800",
  dateValeur: "2012-02-29T00:00:00.000Z",
  id: 402,
  sensOperationId: 4,
  natureOperationId: null,
  compteId: "310810100011200804360147",
  sensOperation: {
  code: "DEBIT",
  libelle: "Debit",
  id: 4
}
}
*/
  private rib;

  selectedAccount = { rib: "", balance: "" };

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    storage: Storage,
    private bankService: BankServiceProvider,
    private nav: NavController,
    public loadingCtrl: LoadingController,
    // private datePicker: DatePicker,
    public alertCtrl: AlertController,
    private auth: AuthServiceProvider, public events: Events) {


    if (typeof navParams.get('selectedAccount') != 'undefined' && navParams.get('selectedAccount') != null) {
      this.selectedAccount = this.navParams.get('selectedAccount');
      this.rib = navParams.get('selectedAccount').rib;
      console.log("RIB=" + this.rib);
    } else this.selectedAccount = { rib: "", balance: "" };

    // this.getLastFifteenOperationByRib(this.item);
    this._storage = storage;

    this.token = '';
    this.userId = '';

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad Bank15OperationPage');


    //get credentials for storage
    //and get opertaions !!!
    this.getCredantials(this._storage);



    console.log(this.foundItemsOffline);

  }

  // fetch credancials from storage
  getCredantials(storage) {
    // storage.get('username').then((val) => {
    //   console.log('\n userneme storage', val);
    //   this.username = val;
    // })
    // // Or to get a key/value pair
    // storage.get('password').then((val) => {
    //   console.log('\n password storage', val);
    //   this.password = val;
    // })
    // Or to get a key/value pair
    this.auth.getTokenData().then((val) => {
      if (typeof val != 'undefined' && val != null) {
        this.token = val.id;
        console.log('\n token storage', this.token);
      }

    })
    // Or to get a key/value pair
    this.auth.getUserAccess().then((val) => {
      if (typeof val != 'undefined' && val != null) {
        this.userId = val.userid;
        console.log('\n userId storage', this.userId);

        // get the credantials then call the endpoints
        //this.testEndPoints();
        this.getLastFifteenOperationByRib(this.rib, this.userId, this.token);
      }
    })
  }

  /**
  * Last fifteen bank operation for a given abank account.
  */
  getLastFifteenOperationByRib(rib, userId, token) {
    try {
      ////////////////////////// Loader //////////////////////////////
      //
      // let resps;
      this.presentLoading();
      this.bankService.getLastFifteenOperationByRib(rib, userId, token).subscribe(data => {
        try {
          //TODO --
          this.foundItems = data.json();
          
          for (var idx = 0; idx < this.foundItems.dernieresOperations.operations.length; idx++) {
            this.foundItems.dernieresOperations.operations[idx].orderIdx = idx;
            this.foundItems.dernieresOperations.operations[idx].type = "operation";
          }
          this.items = this.foundItems.dernieresOperations.operations;
          console.log("this.items", this.items);
          
          this.sortDate = 2;
          this.doSortDate();

        } catch (e) {
          this.showAlert('', 'Service indisponible!!');
          // this.dismissLoading();
        }

        console.log('foundItems ..!!!', typeof data, data);
        //loader.dismiss();
      },
        err => {
          console.error(err);
          if (err.status == 401) {

            this.logout(() => {
              this.showAlert('', 
              (JSON.parse(err["_body"]).error && JSON.parse(err["_body"]).error.message) ? JSON.parse(err["_body"]).error.message : 'Erreur 401!');
            });
          } else {
          this.showAlert('', 'Service indisponible!!');
          this.dismissLoading();}
        },
        () => {
          console.log('getLastFifteenOperationByRib completed');
          console.log(this.foundItems);
          this.dismissLoading();
        }
      );//End service

      ///////////////////////////////////////////End loader
      return this.foundItems;
    }
    catch (e) {
      // if (e instanceof RangeError) {
      console.log(e);
      // }
    }
  }//END getLastFifteenOperationByRib

  private loader;
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Chargement des données",
      dismissOnPageChange: true
    });

    this.loader.onDidDismiss(() => {
      console.log('Dismissed loading');
    });

    this.loader.present();
  }

  dismissLoading() {
    console.log('--Dismissed loading--');
    if (!this.loader.Dismissed) this.loader.dismiss();

  }

  /**
  * reuseable Alert box.
  */
  showAlert(title, message) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

  toFloat(str: string) {
    return parseFloat(str);
  }

  logout(cbFinish?) {
    if (this.auth.currentUser) {
        let loading = this.loadingCtrl.create({
            content: 'Traitement en cours...',
            dismissOnPageChange: true,
            showBackdrop: true,
            enableBackdropDismiss: true
        });
        loading.present();

        this.events.publish('user:doLogout', Date.now());
        var isCbExec = false;
        if (cbFinish) this.events.subscribe('user:logout', (xdate) => {
            if(!isCbExec){
                isCbExec = true;
                cbFinish()
            };
            if (loading.isOverlay) loading.dismiss();
        });
    }
}

  formatedFLoat(str) {
    let res = String(parseFloat(str).toFixed(2)).replace('.', ',');
    if (res == 'NaN') res = "";
    return res;
  }

  formatingDate(dateData) {
    let date = new Date(dateData);
    var options = { month: 'short', day: 'numeric', year: "numeric" };
    return date.toLocaleString('fr-FR', options);
  }

  getDay(dateData: string) {
    let date = new Date(dateData);
    var options = { day: 'numeric' };
    return date.toLocaleString('fr-FR', options);
  }

  getMounthAndYear(dateData: string) {
    let date = new Date(dateData);
    var options = { month: 'short', year: "numeric" };
    return date.toLocaleString('fr-FR', options);
  }

  getDayAndMounth(dateData: string) {
    let date = new Date(dateData);
    var options = { month: 'numeric', day: "numeric" };
    return date.toLocaleString('fr-FR', options);
  }

  getYear(dateData: string) {
    let date = new Date(dateData);
    var options = { year: "numeric" };
    return date.toLocaleString('fr-FR', options);
  }

  getMonth(dateData: string) {
    let date = new Date(dateData);
    var options = { month: "long" };
    return date.toLocaleString('fr-FR', options);
  }

  lastMonthGrp;
  doGroup(dateData: string): boolean {
    let date = new Date(dateData);
    var options = { month: 'numeric' };
    let month = date.toLocaleString('fr-FR', options);

    
    if (!this.lastMonthGrp || this.lastMonthGrp != month) {
      this.lastMonthGrp = month;
      return true;
    }else {
      this.lastMonthGrp = month;
      return false;
    }
  }

  queryText = "";
  doSearch() {
    console.log("queryText: ", this.queryText, "foundItems", this.foundItems);
    if (this.queryText.trim() != "") {
      this.items = [];
      this.foundItems.dernieresOperations.operations.forEach((op, idx) => {
        console.warn("idx: ", idx, op);
        if (this.formatingDate(op.date).toLowerCase().indexOf(this.queryText.toLowerCase()) !== -1
          || op.libelle.toLowerCase().indexOf(this.queryText.toLowerCase()) !== -1
          || this.formatedFLoat(op.montant).toLowerCase().indexOf(this.queryText.toLowerCase()) !== -1
        ) {
          this.items.push(op);
        }
      });
    } else {
      this.items = this.foundItems.dernieresOperations.operations;
    }

    if(this.sortDate == 0){
      this.sortDate = 2;
      this.doSortDate();
    }else if(this.sortDate == 2){
      this.sortDate = 1;
      this.doSortDate();
    }

  }

  sortCash = 1; //next click>> IF 0: no sort, 1: lower to higher, 2: higher to lower !
  sortDate = 1; //next click>> IF 0: no sort, 1: lower to higher, 2: higher to lower !

  doSortCash() {
    //console.log(this.sortCash);
    console.log(this.foundItems.dernieresOperations.operations);
    console.warn(this.items);

    if (this.sortCash == 0) {
      this.items = this.items.filter(item =>
        item.type != "group"
      );
      this.items.sort((a, b) => {
        let res = 0;
        if (a.orderIdx > b.orderIdx) res = 1;
        else if (a.orderIdx < b.orderIdx) res = -1;
        return res;
      });
      this.sortCash = 1;
      this.lastMonthGrp = undefined;
    } else if (this.sortCash == 1) {

      this.items = this.items.filter(item =>
        item.type != "group"
      );
      this.items.sort((a, b) => {
        //console.log(a, b);
        let res = 0;
        let nbr1 = (a.sensOperation.code == "CREDIT") ? a.montant : -1 * a.montant;
        let nbr2 = (b.sensOperation.code == "CREDIT") ? b.montant : -1 * b.montant;
        if (nbr1 > nbr2) res = 1;
        else if (nbr1 < nbr2) res = -1;
        return res;
      });

      this.sortCash = 2;
    } else if (this.sortCash == 2) {

      this.items = this.items.filter(item =>
        item.type != "group"
      );
      this.items.sort((a, b) => {
        //console.log(a, b);
        let res = 0;
        let nbr1 = (a.sensOperation.code == "CREDIT") ? a.montant : -1 * a.montant;
        let nbr2 = (b.sensOperation.code == "CREDIT") ? b.montant : -1 * b.montant;
        if (nbr1 < nbr2) res = 1;
        else if (nbr1 > nbr2) res = -1;
        return res;
      });

      this.sortCash = 0;
    }

    this.sortDate = 1;
  }

  doSortDate() {
    //console.log(this.sortCash);
    console.log(this.foundItems.dernieresOperations.operations);
    console.warn(this.items);

    if (this.sortDate == 0) {
      this.lastMonthGrp = null;
      this.items = this.foundItems.dernieresOperations.operations;
      this.items.sort((a, b) => {
        let res = 0;
        if (a.orderIdx > b.orderIdx) res = 1;
        else if (a.orderIdx < b.orderIdx) res = -1;
        return res;
      });
      this.sortDate = 1;
    } else if (this.sortDate == 1) {

      this.lastMonthGrp = null;
      this.items = this.items.filter(item =>
        item.type != "group"
      );
      this.items.sort((a, b) => {
        let res = new Date(a.date).getTime() - new Date(b.date).getTime();
        //console.log(res);
        return res;
      });
      this.sortDate = 2;

      let idx = 0;
      while (this.items.length>idx) {
        let item = this.items[idx];
        let date = new Date(Date.parse(item.date));
        var options = { month: 'numeric' };
        let month = date.toLocaleString('fr-FR', options);
    
        console.log("idx>>>", idx, "current>>>", month, date, "lastMonth>>>", this.lastMonthGrp );
        
        if (!this.lastMonthGrp || this.lastMonthGrp != month) {
          console.log("push at: ", idx);
          
          this.lastMonthGrp = month;
          for(var i = this.items.length; i>idx; i--){
            this.items[i]=this.items[i-1];
          }
          this.items[idx]= {type : "group", date: item.date};
        }
        idx++;
      }

    } else if (this.sortDate == 2) {

      this.lastMonthGrp = null;
      this.items = this.items.filter(item =>
        item.type != "group"
      );
      this.items.sort((a, b) => {
        //console.log(a, b);
        let res = -(new Date(a.date).getTime() - new Date(b.date).getTime());
        //console.log(res);
        return res;
      });
      this.sortDate = 0;

      let idx = 0;
      while (this.items.length>idx) {
        let item = this.items[idx];
        let date = new Date(Date.parse(item.date));
        var options = { month: 'numeric' };
        let month = date.toLocaleString('fr-FR', options);
    
        console.log("idx>>>", idx, "current>>>", month, date, "lastMonth>>>", this.lastMonthGrp );
        
        if (!this.lastMonthGrp || this.lastMonthGrp != month) {
          console.log("push at: ", idx);
          
          this.lastMonthGrp = month;
          for(var i = this.items.length; i>idx; i--){
            this.items[i]=this.items[i-1];
          }
          this.items[idx]= {type : "group", date: item.date};
        }
        idx++;
      }

      
    }

    this.sortCash = 1;
  }

}
