//Ahmed

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
//loader
import { LoadingController } from 'ionic-angular';
//storage
import { Storage } from '@ionic/storage';

import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { BankServiceProvider } from '../../providers/bank-service/bank-service';

/**
* Generated class for the HistoryByDatePage page.
*
* See http://ionicframework.com/docs/components/#navigation for more info
* on Ionic pages and navigation.
*/
//@IonicPage()
@Component({
  selector: 'page-history-by-date',
  templateUrl: 'history-by-date.html',
})
export class HistoryByDatePage {
  // public foundItems;
  private rib;
  public today;
  public someDay;

  public token;
  public userId;
  private _storage;

  public dateMin = new Date();

  private isDisabled = true;
  dateSelected() {

    console.log("Changed...")
    this.isDisabled = false;
  }
  // public foundItems = { dernieresOperations: {} };
  public foundItems = {
    entreDatesOperations: {
      rib: "..",
      identite: "..",
      agenceId: null,
      typePersonneId: null,
      appUserId: null,
      operations: [
      ],
      agence: {
        code: "..",
        libelle: "..",
        id: 3
      },
      typePersonne: {
        code: "..",
        libelle: "..",
        id: null
      }
    }
  };
  /*
  {
  montant: 200,
  date: "2012-02-29T00:00:00.000Z",
  numero: "010512417001",
  libelle: "RETRAIT GAB",
  numeroPiece: "800",
  dateValeur: "2012-02-29T00:00:00.000Z",
  id: 402,
  sensOperationId: 4,
  natureOperationId: null,
  compteId: "310810100011200804360147",
  sensOperation: {
  code: "DEBIT",
  libelle: "Debit",
  id: 4
}
}

*/
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private bankService: BankServiceProvider,
    public loadingCtrl: LoadingController,
    private nav: NavController,
    storage: Storage,
    // private datePicker: DatePicker,
    public alertCtrl: AlertController,
    private auth: AuthServiceProvider, public events: Events) {
    //For the date picker
    this.someDay = new Date().toISOString();
    this.today = new Date().toISOString();
    this.rib = navParams.get('selectedAccount').rib;


    this._storage = storage;

    this.token = '';
    this.userId = '';

    this.dateMin.setMonth(this.dateMin.getMonth() - 7);
    //this.dateMin.setDate(1);
    console.log("min date: ", this.dateMin.getFullYear() + '-' + (this.dateMin.getMonth() + 1) + '-' + this.dateMin.getDate())

  }

  getMinDate() {
    let mounth: string = String(this.dateMin.getMonth() + 1);
    if (mounth.length == 1) mounth = '0' + mounth;
    let day: string = String(this.dateMin.getDate());
    if (day.length == 1) day = '0' + day;
    return this.dateMin.getFullYear() + '-' + mounth + '-' + day;
  }
  getOptBetweenTwoDates() {

    //get credentials for storage
    this.getCredantials(this._storage);
    //this.getOperationBetweenTwoDatesByRib(this.rib, this.userId,this.token,this.today, this.someDay);
  }

  // fetch credancials from storage
  getCredantials(storage) {
    // storage.get('username').then((val) => {
    //   console.log('\n userneme storage', val);
    //   this.username = val;
    // })
    // // Or to get a key/value pair
    // storage.get('password').then((val) => {
    //   console.log('\n password storage', val);
    //   this.password = val;
    // })
    // Or to get a key/value pair
    this.auth.getTokenData().then((val) => {
      this.token = val.id;
      console.log('\n token storage', this.token);
    })
    // Or to get a key/value pair
    this.auth.getUserAccess().then((val) => {
      this.userId = val.userid;
      console.log('\n userId storage', this.userId);

      console.warn("SELECTED DATES");
      console.warn(this.today);
      console.warn(this.someDay);

      var todayDate = new Date(this.today)
      var someDayDate = new Date(this.someDay)
      // 16-05-2015 09:50
      var todayStr: string = todayDate.getFullYear()+("0"+(todayDate.getMonth()+1)).slice(-2)+("0" + todayDate.getDate()).slice(-2)
      var someDayStr: string = someDayDate.getFullYear()+("0"+(someDayDate.getMonth()+1)).slice(-2)+("0" + someDayDate.getDate()).slice(-2)
      
  


      // get the credantials then call the endpoints
      //this.testEndPoints();
      this.getOperationBetweenTwoDatesByRib(this.rib, this.userId,
        this.token, todayStr, someDayStr);
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BankOperationBetweenToDates');
    //    this.getLastFifteenOperationByRib(this.item);
  }

  public showAlert(message) {
    let alert = this.alertCtrl.create({
      title: 'TGR Mobile',
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }//END Alert

  /**
  * Operations between two dates for a given abank account.
  */
  // /operations?filter=/operations?filter={"where": { "date": {"lte":"2012-06-01", "gte":"2012-01-02"} } }
  getOperationBetweenTwoDatesByRib(rib, user_id, token, date1, date2) {
    try {
      ////////////////////////// Loader //////////////////////////////
      //
      // let resps;
      this.presentLoading();
      this.bankService.getOperationBetweenTwoDatesByRib(rib, user_id, token, date1, date2)
      .subscribe(data => {
        this.foundItems = data.json();
        if(this.foundItems.entreDatesOperations.operations){
          for (var idx = 0; idx < this.foundItems.entreDatesOperations.operations.length; idx++) {
            this.foundItems.entreDatesOperations.operations[idx].orderIdx = idx;
            this.foundItems.entreDatesOperations.operations[idx].type = "operation";
          }
          this.items = this.foundItems.entreDatesOperations.operations;
          
          console.log('foundItems ..!!!' + JSON.stringify(data) + '\n\n');
  
          this.sortDate = 2;
          this.doSortDate();
        }else{
          this.showAlert(this.foundItems.entreDatesOperations)
          
        }

        this.dismissLoading();

      },
        err => {
          console.error(err);
          if (err.status == 401) {

            this.logout(() => {
              this.showAlert((JSON.parse(err["_body"]).error && JSON.parse(err["_body"]).error.message) ? JSON.parse(err["_body"]).error.message : 'Erreur 401!');
            });
          } else {
          this.showAlert(this.foundItems.entreDatesOperations)
          //this.showAlert('Ooops', 'Oooops !!, We can not connect to the server "OperationBetweenTwoDatesByRib" !!');
          this.dismissLoading();}
        }
      );//End service
      //loader.dismiss();
      ///////////////////////////////////////////End loader
      //return this.foundItems;
    }
    catch (e) {
      console.log(e);
    }
  }/////

  private loader;
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Chargement des données...",
      dismissOnPageChange: true
    });

    this.loader.onDidDismiss(() => {
      console.log('Dismissed loading');
    });

    this.loader.present();
  }

  dismissLoading() {
    console.log('--Dismissed loading--');
    if (!this.loader.Dismissed) this.loader.dismiss();

  }

  logout(cbFinish?) {
    if (this.auth.currentUser) {
        let loading = this.loadingCtrl.create({
            content: 'Traitement en cours...',
            dismissOnPageChange: true,
            showBackdrop: true,
            enableBackdropDismiss: true
        });
        loading.present();

        this.events.publish('user:doLogout', Date.now());
        var isCbExec = false;
        if (cbFinish) this.events.subscribe('user:logout', (xdate) => {
            if(!isCbExec){
                isCbExec = true;
                cbFinish()
            };
            if (loading.isOverlay) loading.dismiss();
        });
    }
}

  formatedFLoat(str) {
    let res = String(parseFloat(str).toFixed(2)).replace('.', ',');
    if (res == 'NaN') res = "";
    return res;
  }

  formatingDate(dateData) {
    let date = new Date(dateData);
    var options = { month: 'short', day: 'numeric', year: "numeric" };
    return date.toLocaleString('fr-FR', options);
  }

  getDay(dateData: string) {
    let date = new Date(dateData);
    var options = { day: 'numeric' };
    return date.toLocaleString('fr-FR', options);
  }

  getMounthAndYear(dateData: string) {
    let date = new Date(dateData);
    var options = { month: 'short', year: "numeric" };
    return date.toLocaleString('fr-FR', options);
  }

  getDayAndMounth(dateData: string) {
    let date = new Date(dateData);
    var options = { month: 'numeric', day: "numeric" };
    return date.toLocaleString('fr-FR', options);
  }

  getYear(dateData: string) {
    let date = new Date(dateData);
    var options = { year: "numeric" };
    return date.toLocaleString('fr-FR', options);
  }

  getMonth(dateData: string) {
    let date = new Date(dateData);
    var options = { month: "long" };
    return date.toLocaleString('fr-FR', options);
  }

  lastMonthGrp;
  doGroup(dateData: string): boolean {
    let date = new Date(dateData);
    var options = { month: 'numeric' };
    let month = date.toLocaleString('fr-FR', options);

    if (!this.lastMonthGrp || this.lastMonthGrp != month) {
      this.lastMonthGrp = month;
      return true;
    } {
      this.lastMonthGrp = month;
      return false;
    }
  }


  items;
  queryText = "";
/*
  doSearch() { 

    console.log("queryText: ", this.queryText, "foundItems", this.foundItems);
    if (this.queryText.trim() != "") {
      this.items = [];
      this.foundItems.entreDatesOperations.operations.forEach((op, idx) => {
       // console.warn("idx: ", idx, 
        //  this.formatingDate(op.date), 
         // op.libelle.toLowerCase(), 
         // this.formatedFLoat(op.montant).toLowerCase()
       // );
        

        let condition = this.formatingDate(op.date).toLowerCase().indexOf(this.queryText.toLowerCase()) !== -1
          || op.libelle.toLowerCase().indexOf(this.queryText.toLowerCase()) !== -1
          || this.formatedFLoat(op.montant).toLowerCase().indexOf(this.queryText.toLowerCase()) !== -1;

         // console.warn(condition)
        
        if (condition) {
          this.items.push(op);
        }
      })
    } else {
      this.items = this.foundItems.entreDatesOperations.operations;
    }

  }

  sortCash = 1; //next click>> IF 0: no sort, 1: lower to higher, 2: higher to lower !
  sortDate = 1; //next click>> IF 0: no sort, 1: lower to higher, 2: higher to lower !

  doSortCash() {
    //test 1 desactivate date filter ...  this.lastMonthGrp = null;
    //this.lastMonthGrp = null;
    //console.log(this.sortCash);
    console.log(this.foundItems.entreDatesOperations.operations);
    console.warn(this.items);

    if (this.sortCash == 0) {
      this.items.sort((a, b) => {
        let res = 0;
        if (a.orderIdx > b.orderIdx) res = 1;
        else if (a.orderIdx < b.orderIdx) res = -1;
        return res;
      });
      this.sortCash = 1;
      this.lastMonthGrp = undefined;
    } else if (this.sortCash == 1) {

      this.items.sort((a, b) => {
        //console.log(a, b);
        let res = 0;
        let nbr1 = (a.sensOperationId == 3) ? a.montant : -1 * a.montant;
        let nbr2 = (b.sensOperationId == 3) ? b.montant : -1 * b.montant;
        if (nbr1 > nbr2) res = 1;
        else if (nbr1 < nbr2) res = -1;
        return res;
      });

      this.sortCash = 2;
    } else if (this.sortCash == 2) {

      this.items.sort((a, b) => {
        //console.log(a, b);
        let res = 0;
        let nbr1 = (a.sensOperationId == 3) ? a.montant : -1 * a.montant;
        let nbr2 = (b.sensOperationId == 3) ? b.montant : -1 * b.montant;
        if (nbr1 < nbr2) res = 1;
        else if (nbr1 > nbr2) res = -1;
        return res;
      });

      this.sortCash = 0;
    }
  }

  doSortDate() {
    //console.log(this.sortCash);
    console.log(this.foundItems.entreDatesOperations.operations);
    console.warn(this.items);

    if (this.sortDate == 0) {
      this.lastMonthGrp = null;
      this.items = this.foundItems.entreDatesOperations.operations;
      this.items.sort((a, b) => {
        let res = 0;
        if (a.orderIdx > b.orderIdx) res = 1;
        else if (a.orderIdx < b.orderIdx) res = -1;
        return res;
      });
      this.sortDate = 1;
    } else if (this.sortDate == 1) {

      this.lastMonthGrp = null;
      this.items = this.items.filter(item =>
        item.type != "group"
      );
      this.items.sort((a, b) => {
        let res = new Date(a.date).getTime() - new Date(b.date).getTime();
        //console.log(res);
        return res;
      });
      this.sortDate = 2;

      let idx = 0;
      while (this.items.length>idx) {
        let item = this.items[idx];
        let date = new Date(Date.parse(item.date));
        var options = { month: 'numeric' };
        let month = date.toLocaleString('fr-FR', options);
    
        console.log("idx>>>", idx, "current>>>", month, date, "lastMonth>>>", this.lastMonthGrp );
        
        if (!this.lastMonthGrp || this.lastMonthGrp != month) {
          console.log("push at: ", idx);
          
          this.lastMonthGrp = month;
          for(var i = this.items.length; i>idx; i--){
            this.items[i]=this.items[i-1];
          }
          this.items[idx]= {type : "group", date: item.date};
        }
        idx++;
      }

    } else if (this.sortDate == 2) {

      this.lastMonthGrp = null;
      this.items = this.items.filter(item =>
        item.type != "group"
      );
      this.items.sort((a, b) => {
        //console.log(a, b);
        let res = -(new Date(a.date).getTime() - new Date(b.date).getTime());
        //console.log(res);
        return res;
      });
      this.sortDate = 0;

      let idx = 0;
      while (this.items.length>idx) {
        let item = this.items[idx];
        let date = new Date(Date.parse(item.date));
        var options = { month: 'numeric' };
        let month = date.toLocaleString('fr-FR', options);
    
        console.log("idx>>>", idx, "current>>>", month, date, "lastMonth>>>", this.lastMonthGrp );
        
        if (!this.lastMonthGrp || this.lastMonthGrp != month) {
          console.log("push at: ", idx);
          
          this.lastMonthGrp = month;
          for(var i = this.items.length; i>idx; i--){
            this.items[i]=this.items[i-1];
          }
          this.items[idx]= {type : "group", date: item.date};
        }
        idx++;
      }

      
    }
  }
*/

doSearch() {
  console.log("queryText: ", this.queryText, "foundItems", this.foundItems);
  if (this.queryText.trim() != "") {
    this.items = [];
    this.foundItems.entreDatesOperations.operations.forEach((op, idx) => {
      console.warn("idx: ", idx, op);
      if (this.formatingDate(op.date).toLowerCase().indexOf(this.queryText.toLowerCase()) !== -1
        || op.libelle.toLowerCase().indexOf(this.queryText.toLowerCase()) !== -1
        || this.formatedFLoat(op.montant).toLowerCase().indexOf(this.queryText.toLowerCase()) !== -1
      ) {
        this.items.push(op);
      }
    });
  } else {
    this.items = this.foundItems.entreDatesOperations.operations;
  }

  if(this.sortDate == 0){
    this.sortDate = 2;
    this.doSortDate();
  }else if(this.sortDate == 2){
    this.sortDate = 1;
    this.doSortDate();
  }

}

sortCash = 1; //next click>> IF 0: no sort, 1: lower to higher, 2: higher to lower !
sortDate = 1; //next click>> IF 0: no sort, 1: lower to higher, 2: higher to lower !

doSortCash() {
  //console.log(this.sortCash);
  console.log(this.foundItems.entreDatesOperations.operations);
  console.warn(this.items);

  if (this.sortCash == 0) {
    this.items = this.items.filter(item =>
      item.type != "group"
    );
    this.items.sort((a, b) => {
      let res = 0;
      if (a.orderIdx > b.orderIdx) res = 1;
      else if (a.orderIdx < b.orderIdx) res = -1;
      return res;
    });
    this.sortCash = 1;
    this.lastMonthGrp = undefined;
  } else if (this.sortCash == 1) {

    this.items = this.items.filter(item =>
      item.type != "group"
    );
    this.items.sort((a, b) => {
      //console.log(a, b);
      let res = 0;
      let nbr1 = (a.sensOperation.code == "CREDIT") ? a.montant : -1 * a.montant;
      let nbr2 = (b.sensOperation.code == "CREDIT") ? b.montant : -1 * b.montant;
      if (nbr1 > nbr2) res = 1;
      else if (nbr1 < nbr2) res = -1;
      return res;
    });

    this.sortCash = 2;
  } else if (this.sortCash == 2) {

    this.items = this.items.filter(item =>
      item.type != "group"
    );
    this.items.sort((a, b) => {
      //console.log(a, b);
      let res = 0;
      let nbr1 = (a.sensOperation.code == "CREDIT") ? a.montant : -1 * a.montant;
      let nbr2 = (b.sensOperation.code == "CREDIT") ? b.montant : -1 * b.montant;
      if (nbr1 < nbr2) res = 1;
      else if (nbr1 > nbr2) res = -1;
      return res;
    });

    this.sortCash = 0;
  }

  this.sortDate = 1;
}

doSortDate() {
  //console.log(this.sortCash);
  console.log(this.foundItems.entreDatesOperations.operations);
  console.warn(this.items);

  if (this.sortDate == 0) {
    this.lastMonthGrp = null;
    this.items = this.foundItems.entreDatesOperations.operations;
    this.items.sort((a, b) => {
      let res = 0;
      if (a.orderIdx > b.orderIdx) res = 1;
      else if (a.orderIdx < b.orderIdx) res = -1;
      return res;
    });
    this.sortDate = 1;
  } else if (this.sortDate == 1) {

    this.lastMonthGrp = null;
    this.items = this.items.filter(item =>
      item.type != "group"
    );
    this.items.sort((a, b) => {
      let res = new Date(a.date).getTime() - new Date(b.date).getTime();
      //console.log(res);
      return res;
    });
    this.sortDate = 2;

    let idx = 0;
    while (this.items.length>idx) {
      let item = this.items[idx];
      let date = new Date(Date.parse(item.date));
      var options = { month: 'numeric' };
      let month = date.toLocaleString('fr-FR', options);
  
      console.log("idx>>>", idx, "current>>>", month, date, "lastMonth>>>", this.lastMonthGrp );
      
      if (!this.lastMonthGrp || this.lastMonthGrp != month) {
        console.log("push at: ", idx);
        
        this.lastMonthGrp = month;
        for(var i = this.items.length; i>idx; i--){
          this.items[i]=this.items[i-1];
        }
        this.items[idx]= {type : "group", date: item.date};
      }
      idx++;
    }

  } else if (this.sortDate == 2) {

    this.lastMonthGrp = null;
    this.items = this.items.filter(item =>
      item.type != "group"
    );
    this.items.sort((a, b) => {
      //console.log(a, b);
      let res = -(new Date(a.date).getTime() - new Date(b.date).getTime());
      //console.log(res);
      return res;
    });
    this.sortDate = 0;

    let idx = 0;
    while (this.items.length>idx) {
      let item = this.items[idx];
      let date = new Date(Date.parse(item.date));
      var options = { month: 'numeric' };
      let month = date.toLocaleString('fr-FR', options);
  
      console.log("idx>>>", idx, "current>>>", month, date, "lastMonth>>>", this.lastMonthGrp );
      
      if (!this.lastMonthGrp || this.lastMonthGrp != month) {
        console.log("push at: ", idx);
        
        this.lastMonthGrp = month;
        for(var i = this.items.length; i>idx; i--){
          this.items[i]=this.items[i-1];
        }
        this.items[idx]= {type : "group", date: item.date};
      }
      idx++;
    }

    
  }

  this.sortCash = 1;
}
}
