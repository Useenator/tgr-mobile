import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ContribuableService } from '../../providers/contribuable-service/contribuable-service';
import { AlertController, Events } from 'ionic-angular';

import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { MyUtilsProvider } from '../../providers/my-utils/my-utils';
import { Config } from '../../MyUtils/Config';
import { Utils } from '../../MyUtils/Utils';
import { Http } from '@angular/http';

import { SituationFiscaleDetailPage } from '../situation-fiscale-detail/situation-fiscale-detail';
//loader
import { LoadingController } from 'ionic-angular';

import { ContribuableObserverProvider } from '../../providers/contribuable-observer/contribuable-observer';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { InAppBrowserOptions } from 'ionic-native';

// import { NativeStorage } from '@ionic-native/native-storage';
/*
   class for the Contribuable page.
*/
@Component({
  selector: 'page-contribuable',
  templateUrl: 'contribuable.html',
  providers: [ContribuableService, AuthServiceProvider, InAppBrowser]
})
export class ContribuablePage {

  /////////////////////////// these parametres will from the session /////////////////////////////
  //private token = '0rFRTPrdTMoyrPpqyFxE2BzX8FdEFLEy8nTAzZJsKbAiJ7bFsnG2c08w6mBX1DQX';
  //private userId = 'CompteDemo';
  //private codeContribuables = ["7350145", "4000"];// : Array<string> ;
  ///////////////////////////////////////////////////////////////////////////////////////////////

  ///////////////////////////END these parametres will from the session //////////////////////////

  private foundItems = {
    code: "",
    message: "",
    adresse: "",
    annees: [],
    articles: [],
    codepostes: [],
    ids: [],
    libelle: "",
    libellepostes: [],
    libelles: [],
    montantsfraisrecouvrement: [],
    montantsmajoration: [],
    montantsprincipal: [],
    montantstotalligne: [],
    natures: [],
    nom: "",
    secteurs: []
  };
  private foundItems_offligne = {
    code: "500",
    message: "Entrée incorrecte - off",
    adresse: "BD MED V B MELLAL",
    annees: [
      "1997/1997",
      "1998/1998",
      "1998/1998",
      "1997/1997",
      "1997/1997",
      "2015/2015",
      "2017/2017",
      "2017/2017",
      "2014/2014",
      "2000/2000",
      "2000/2000",
      "1999/1999",
      "1998/1998",
      "1999/1999",
      "1999/1999",
      "2001/2001",
      "2001/2001",
      "2000/2000",
      "2013/2013",
      "2013/2014",
      "2014/2014",
      "2015/2015",
      "2016/2016",
      "2014/2014",
      "2015/2015",
      "2016/2016",
      "2012/2015",
      "2013/2013",
      "2013/2013",
      "2010/2011",
      "2011/2011",
      "2012/2012",
      "2014/2014",
      "2014/2014",
      "2015/2015",
      "2016/2016",
      "2003/2003",
      "2002/2002",
      "2002/2002",
      "2001/2001",
      "2002/2002",
      "2003/2003",
      "2003/2003",
      "2003/2005",
      "2003/2005",
      "2005/2005",
      "2004/2004",
      "2005/2008",
      "2004/2004",
      "2004/2004",
      "2006/2006",
      "2005/2005",
      "2005/2005",
      "2006/2006",
      "2008/2008",
      "2007/2007",
      "2006/2006",
      "2007/2007",
      "2007/2007",
      "2009/2009",
      "2010/2011",
      "2009/2009",
      "2008/2008",
      "2011/2013",
      "2011/2013",
      "2010/2010",
      "2011/2011",
      "2010/2010",
      "2012/2012",
      "2012/2012",
      "2011/2011",
      "2012/2012"
    ],
    articles: [
      "50560",
      "50560",
      "13090",
      "50560",
      "13090",
      "74388",
      "50560",
      "74388",
      "74388",
      "50560",
      "50560",
      "13090",
      "50560",
      "50560",
      "50560",
      "50560",
      "50560",
      "13090",
      "13090",
      "18304",
      "15948",
      "15948",
      "15948",
      "13090",
      "13090",
      "13090",
      "18304",
      "50560",
      "74388",
      "15948",
      "15948",
      "15948",
      "50560",
      "74388",
      "50560",
      "50560",
      "13090",
      "50560",
      "50560",
      "13090",
      "13090",
      "50560",
      "50560",
      "50560",
      "50560",
      "13090",
      "50560",
      "50560",
      "13090",
      "50560",
      "13090",
      "50560",
      "50560",
      "50560",
      "50560",
      "13090",
      "50560",
      "50560",
      "50560",
      "13090",
      "18304",
      "50560",
      "13090",
      "18304",
      "74388",
      "13090",
      "13090",
      "50560",
      "74388",
      "13090",
      "50560",
      "50560"
    ],
    codepostes: [
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111",
      "1111"
    ],
    ids: [
      "12818781",
      "12648801",
      "12387726",
      "12531086",
      "12263641",
      "94429159",
      "112342837",
      "112347439",
      "70751251",
      "12766905",
      "12441304",
      "12347318",
      "12365129",
      "12718699",
      "12593303",
      "12407862",
      "12600081",
      "12690020",
      "16907710",
      "56862485",
      "74913364",
      "96823926",
      "106088440",
      "74907871",
      "96817562",
      "106082210",
      "98408104",
      "16557873",
      "16565380",
      "12838001",
      "12625421",
      "12625151",
      "70743822",
      "70751252",
      "94425085",
      "102568138",
      "12391001",
      "12285820",
      "12247212",
      "12737655",
      "12740322",
      "12695820",
      "12517754",
      "12531550",
      "12599490",
      "12742167",
      "12248027",
      "12412828",
      "12791731",
      "12308156",
      "12683960",
      "12775628",
      "12828380",
      "12460995",
      "12542725",
      "12347916",
      "12829159",
      "12409714",
      "12556344",
      "12730839",
      "12786336",
      "12672639",
      "12794049",
      "12570909",
      "12576356",
      "12783604",
      "12495262",
      "12380288",
      "12501054",
      "12682560",
      "12501616",
      "12642219"
    ],
    libelle: "PERCEPTION BNI MELLAL - AM",
    libellepostes: [
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM",
      "PERCEPTION BNI MELLAL - AM"
    ],
    libelles: [
      "TAXE EDILITE AVANT 10/2000 ",
      "TAXE URBAINE AVANT 10/2000 ",
      "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
      "TAXE URBAINE AVANT 10/2000 ",
      "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
      "TAXE DE SERVICES COMMUNAUX ",
      "TAXE DE SERVICES COMMUNAUX ",
      "TAXE DE SERVICES COMMUNAUX ",
      "TAXE HABITATION ",
      "TAXE EDILITE AVANT 10/2000 ",
      "TAXE URBAINE AVANT 10/2000 ",
      "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
      "TAXE EDILITE AVANT 10/2000 ",
      "TAXE EDILITE AVANT 10/2000 ",
      "TAXE URBAINE AVANT 10/2000 ",
      "TAXE URBAINE APRES 10/2000 ",
      "TAXE EDILITE APRES 10/2000 ",
      "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
      "TAXE PROFESSIONNELLE ",
      "TAXE PROFESSIONNELLE ",
      "TAXE PROFESSIONNELLE ",
      "TAXE PROFESSIONNELLE ",
      "TAXE PROFESSIONNELLE ",
      "TAXE PROFESSIONNELLE ",
      "TAXE PROFESSIONNELLE ",
      "TAXE PROFESSIONNELLE ",
      "TAXE PROFESSIONNELLE ",
      "TAXE DE SERVICES COMMUNAUX ",
      "TAXE DE SERVICES COMMUNAUX ",
      "TAXE PROFESSIONNELLE ",
      "TAXE PROFESSIONNELLE ",
      "TAXE PROFESSIONNELLE ",
      "TAXE DE SERVICES COMMUNAUX ",
      "TAXE DE SERVICES COMMUNAUX ",
      "TAXE DE SERVICES COMMUNAUX ",
      "TAXE DE SERVICES COMMUNAUX ",
      "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
      "TAXE EDILITE APRES 10/2000 ",
      "TAXE URBAINE APRES 10/2000 ",
      "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
      "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
      "TAXE URBAINE APRES 10/2000 ",
      "TAXE EDILITE APRES 10/2000 ",
      "TAXE URBAINE APRES 10/2000 ",
      "TAXE EDILITE APRES 10/2000 ",
      "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
      "TAXE URBAINE APRES 10/2000 ",
      "TAXE URBAINE APRES 10/2000 ",
      "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
      "TAXE EDILITE APRES 10/2000 ",
      "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
      "TAXE EDILITE APRES 10/2000 ",
      "TAXE URBAINE APRES 10/2000 ",
      "TAXE URBAINE APRES 10/2000 ",
      "TAXE DE SERVICES COMMUNAUX ",
      "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
      "TAXE EDILITE APRES 10/2000 ",
      "TAXE URBAINE APRES 10/2000 ",
      "TAXE EDILITE APRES 10/2000 ",
      "TAXE PROFESSIONNELLE ",
      "TAXE PROFESSIONNELLE ",
      "TAXE DE SERVICES COMMUNAUX ",
      "TAXE PROFESSIONNELLE ",
      "TAXE PROFESSIONNELLE ",
      "TAXE DE SERVICES COMMUNAUX ",
      "TAXE PROFESSIONNELLE ",
      "TAXE PROFESSIONNELLE ",
      "TAXE DE SERVICES COMMUNAUX ",
      "TAXE DE SERVICES COMMUNAUX ",
      "TAXE PROFESSIONNELLE ",
      "TAXE DE SERVICES COMMUNAUX ",
      "TAXE DE SERVICES COMMUNAUX "
    ],
    montantsfraisrecouvrement: [
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0"
    ],
    montantsmajoration: [
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "6615.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "6615.0",
      "0.0",
      "0.0",
      "1842.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0"
    ],
    montantsprincipal: [
      "4230.0",
      "5076.0",
      "7905.6",
      "4230.0",
      "6588.0",
      "42226.6",
      "10449.3",
      "888.7",
      "888.79",
      "5184.0",
      "5184.0",
      "7905.6",
      "5076.0",
      "5184.0",
      "5184.0",
      "5184.0",
      "5184.0",
      "7905.6",
      "38754.0",
      "41337.9",
      "11880.0",
      "11880.0",
      "11880.0",
      "38754.0",
      "38754.0",
      "38754.0",
      "42226.6",
      "10449.3",
      "888.7",
      "11880.0",
      "11880.0",
      "11880.0",
      "10449.3",
      "888.79",
      "10449.3",
      "10449.3",
      "21960.0",
      "5184.0",
      "5184.0",
      "7905.6",
      "7905.6",
      "5184.0",
      "5184.0",
      "12906.0",
      "12906.0",
      "21960.0",
      "5184.0",
      "12906.0",
      "21960.0",
      "5184.0",
      "21960.0",
      "5184.0",
      "5184.0",
      "18090.0",
      "10449.3",
      "21960.0",
      "18090.0",
      "18090.0",
      "18090.0",
      "38754.0",
      "41337.9",
      "10449.3",
      "38754.0",
      "41337.9",
      "888.7",
      "38754.0",
      "38754.0",
      "10449.3",
      "888.7",
      "38754.0",
      "10449.3",
      "10449.3"
    ],
    montantstotalligne: [
      "4230.0",
      "5076.0",
      "7905.6",
      "4230.0",
      "6588.0",
      "48841.6",
      "10449.3",
      "888.7",
      "888.79",
      "5184.0",
      "5184.0",
      "7905.6",
      "5076.0",
      "5184.0",
      "5184.0",
      "5184.0",
      "5184.0",
      "7905.6",
      "38754.0",
      "41337.9",
      "11880.0",
      "11880.0",
      "11880.0",
      "38754.0",
      "38754.0",
      "38754.0",
      "48841.6",
      "10449.3",
      "888.7",
      "13722.0",
      "11880.0",
      "11880.0",
      "10449.3",
      "888.79",
      "10449.3",
      "10449.3",
      "21960.0",
      "5184.0",
      "5184.0",
      "7905.6",
      "7905.6",
      "5184.0",
      "5184.0",
      "12906.0",
      "12906.0",
      "21960.0",
      "5184.0",
      "12906.0",
      "21960.0",
      "5184.0",
      "21960.0",
      "5184.0",
      "5184.0",
      "18090.0",
      "10449.3",
      "21960.0",
      "18090.0",
      "18090.0",
      "18090.0",
      "38754.0",
      "41337.9",
      "10449.3",
      "38754.0",
      "41337.9",
      "888.7",
      "38754.0",
      "38754.0",
      "10449.3",
      "888.7",
      "38754.0",
      "10449.3",
      "10449.3"
    ],
    natures: [
      "TE av2000",
      "TU av2000",
      "PAT",
      "TU av2000",
      "PAT",
      "TSC",
      "TSC",
      "TSC",
      "TH",
      "TE av2000",
      "TU av2000",
      "PAT",
      "TE av2000",
      "TE av2000",
      "TU av2000",
      "TU",
      "TE ap2000",
      "PAT",
      "TP",
      "TP",
      "TP",
      "TP",
      "TP",
      "TP",
      "TP",
      "TP",
      "TP",
      "TSC",
      "TSC",
      "TP",
      "TP",
      "TP",
      "TSC",
      "TSC",
      "TSC",
      "TSC",
      "PAT",
      "TE ap2000",
      "TU",
      "PAT",
      "PAT",
      "TU",
      "TE ap2000",
      "TU",
      "TE ap2000",
      "PAT",
      "TU",
      "TU",
      "PAT",
      "TE ap2000",
      "PAT",
      "TE ap2000",
      "TU",
      "TU",
      "TSC",
      "PAT",
      "TE ap2000",
      "TU",
      "TE ap2000",
      "TP",
      "TP",
      "TSC",
      "TP",
      "TP",
      "TSC",
      "TP",
      "TP",
      "TSC",
      "TSC",
      "TP",
      "TSC",
      "TSC"
    ],
    nom: "CREDIT DU MAROC",
    secteurs: [
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "419",
      "419",
      "419",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "419",
      "419",
      "419",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413",
      "413"
    ]
  };
  private _foundItemsArticleNonSolder: ArticleNonSolderRoot = {
    consulterArticlesNonSoldes: {
      adresse: "BD MED V B MELLAL",
      annees: [
        "1997/1997",
        "1998/1998",
        "1998/1998",
        "1997/1997",
        "1997/1997",
        "2015/2015",
        "2017/2017",
        "2017/2017",
        "2014/2014",
        "2000/2000",
        "2000/2000",
        "1999/1999",
        "1998/1998",
        "1999/1999",
        "1999/1999",
        "2001/2001",
        "2001/2001",
        "2000/2000",
        "2013/2013",
        "2013/2014",
        "2014/2014",
        "2015/2015",
        "2016/2016",
        "2014/2014",
        "2015/2015",
        "2016/2016",
        "2012/2015",
        "2013/2013",
        "2013/2013",
        "2010/2011",
        "2011/2011",
        "2012/2012",
        "2014/2014",
        "2014/2014",
        "2015/2015",
        "2016/2016",
        "2003/2003",
        "2002/2002",
        "2002/2002",
        "2001/2001",
        "2002/2002",
        "2003/2003",
        "2003/2003",
        "2003/2005",
        "2003/2005",
        "2005/2005",
        "2004/2004",
        "2005/2008",
        "2004/2004",
        "2004/2004",
        "2006/2006",
        "2005/2005",
        "2005/2005",
        "2006/2006",
        "2008/2008",
        "2007/2007",
        "2006/2006",
        "2007/2007",
        "2007/2007",
        "2009/2009",
        "2010/2011",
        "2009/2009",
        "2008/2008",
        "2011/2013",
        "2011/2013",
        "2010/2010",
        "2011/2011",
        "2010/2010",
        "2012/2012",
        "2012/2012",
        "2011/2011",
        "2012/2012"
      ],
      articles: [
        "50560",
        "50560",
        "13090",
        "50560",
        "13090",
        "74388",
        "50560",
        "74388",
        "74388",
        "50560",
        "50560",
        "13090",
        "50560",
        "50560",
        "50560",
        "50560",
        "50560",
        "13090",
        "13090",
        "18304",
        "15948",
        "15948",
        "15948",
        "13090",
        "13090",
        "13090",
        "18304",
        "50560",
        "74388",
        "15948",
        "15948",
        "15948",
        "50560",
        "74388",
        "50560",
        "50560",
        "13090",
        "50560",
        "50560",
        "13090",
        "13090",
        "50560",
        "50560",
        "50560",
        "50560",
        "13090",
        "50560",
        "50560",
        "13090",
        "50560",
        "13090",
        "50560",
        "50560",
        "50560",
        "50560",
        "13090",
        "50560",
        "50560",
        "50560",
        "13090",
        "18304",
        "50560",
        "13090",
        "18304",
        "74388",
        "13090",
        "13090",
        "50560",
        "74388",
        "13090",
        "50560",
        "50560"
      ],
      codepostes: [
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111"
      ],
      ids: [
        "12818781",
        "12648801",
        "12387726",
        "12531086",
        "12263641",
        "94429159",
        "112342837",
        "112347439",
        "70751251",
        "12766905",
        "12441304",
        "12347318",
        "12365129",
        "12718699",
        "12593303",
        "12407862",
        "12600081",
        "12690020",
        "16907710",
        "56862485",
        "74913364",
        "96823926",
        "106088440",
        "74907871",
        "96817562",
        "106082210",
        "98408104",
        "16557873",
        "16565380",
        "12838001",
        "12625421",
        "12625151",
        "70743822",
        "70751252",
        "94425085",
        "102568138",
        "12391001",
        "12285820",
        "12247212",
        "12737655",
        "12740322",
        "12695820",
        "12517754",
        "12531550",
        "12599490",
        "12742167",
        "12248027",
        "12412828",
        "12791731",
        "12308156",
        "12683960",
        "12775628",
        "12828380",
        "12460995",
        "12542725",
        "12347916",
        "12829159",
        "12409714",
        "12556344",
        "12730839",
        "12786336",
        "12672639",
        "12794049",
        "12570909",
        "12576356",
        "12783604",
        "12495262",
        "12380288",
        "12501054",
        "12682560",
        "12501616",
        "12642219"
      ],
      libelle: "PERCEPTION BNI MELLAL - AM",
      libellepostes: [
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM"
      ],
      libelles: [
        "TAXE EDILITE AVANT 10/2000 ",
        "TAXE URBAINE AVANT 10/2000 ",
        "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
        "TAXE URBAINE AVANT 10/2000 ",
        "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
        "TAXE DE SERVICES COMMUNAUX ",
        "TAXE DE SERVICES COMMUNAUX ",
        "TAXE DE SERVICES COMMUNAUX ",
        "TAXE HABITATION ",
        "TAXE EDILITE AVANT 10/2000 ",
        "TAXE URBAINE AVANT 10/2000 ",
        "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
        "TAXE EDILITE AVANT 10/2000 ",
        "TAXE EDILITE AVANT 10/2000 ",
        "TAXE URBAINE AVANT 10/2000 ",
        "TAXE URBAINE APRES 10/2000 ",
        "TAXE EDILITE APRES 10/2000 ",
        "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
        "TAXE PROFESSIONNELLE ",
        "TAXE PROFESSIONNELLE ",
        "TAXE PROFESSIONNELLE ",
        "TAXE PROFESSIONNELLE ",
        "TAXE PROFESSIONNELLE ",
        "TAXE PROFESSIONNELLE ",
        "TAXE PROFESSIONNELLE ",
        "TAXE PROFESSIONNELLE ",
        "TAXE PROFESSIONNELLE ",
        "TAXE DE SERVICES COMMUNAUX ",
        "TAXE DE SERVICES COMMUNAUX ",
        "TAXE PROFESSIONNELLE ",
        "TAXE PROFESSIONNELLE ",
        "TAXE PROFESSIONNELLE ",
        "TAXE DE SERVICES COMMUNAUX ",
        "TAXE DE SERVICES COMMUNAUX ",
        "TAXE DE SERVICES COMMUNAUX ",
        "TAXE DE SERVICES COMMUNAUX ",
        "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
        "TAXE EDILITE APRES 10/2000 ",
        "TAXE URBAINE APRES 10/2000 ",
        "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
        "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
        "TAXE URBAINE APRES 10/2000 ",
        "TAXE EDILITE APRES 10/2000 ",
        "TAXE URBAINE APRES 10/2000 ",
        "TAXE EDILITE APRES 10/2000 ",
        "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
        "TAXE URBAINE APRES 10/2000 ",
        "TAXE URBAINE APRES 10/2000 ",
        "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
        "TAXE EDILITE APRES 10/2000 ",
        "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
        "TAXE EDILITE APRES 10/2000 ",
        "TAXE URBAINE APRES 10/2000 ",
        "TAXE URBAINE APRES 10/2000 ",
        "TAXE DE SERVICES COMMUNAUX ",
        "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
        "TAXE EDILITE APRES 10/2000 ",
        "TAXE URBAINE APRES 10/2000 ",
        "TAXE EDILITE APRES 10/2000 ",
        "TAXE PROFESSIONNELLE ",
        "TAXE PROFESSIONNELLE ",
        "TAXE DE SERVICES COMMUNAUX ",
        "TAXE PROFESSIONNELLE ",
        "TAXE PROFESSIONNELLE ",
        "TAXE DE SERVICES COMMUNAUX ",
        "TAXE PROFESSIONNELLE ",
        "TAXE PROFESSIONNELLE ",
        "TAXE DE SERVICES COMMUNAUX ",
        "TAXE DE SERVICES COMMUNAUX ",
        "TAXE PROFESSIONNELLE ",
        "TAXE DE SERVICES COMMUNAUX ",
        "TAXE DE SERVICES COMMUNAUX "
      ],
      montantsfraisrecouvrement: [
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0"
      ],
      montantsmajoration: [
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "6615.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "6615.0",
        "0.0",
        "0.0",
        "1842.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0"
      ],
      montantsprincipal: [
        "4230.0",
        "5076.0",
        "7905.6",
        "4230.0",
        "6588.0",
        "42226.6",
        "10449.3",
        "888.7",
        "888.79",
        "5184.0",
        "5184.0",
        "7905.6",
        "5076.0",
        "5184.0",
        "5184.0",
        "5184.0",
        "5184.0",
        "7905.6",
        "38754.0",
        "41337.9",
        "11880.0",
        "11880.0",
        "11880.0",
        "38754.0",
        "38754.0",
        "38754.0",
        "42226.6",
        "10449.3",
        "888.7",
        "11880.0",
        "11880.0",
        "11880.0",
        "10449.3",
        "888.79",
        "10449.3",
        "10449.3",
        "21960.0",
        "5184.0",
        "5184.0",
        "7905.6",
        "7905.6",
        "5184.0",
        "5184.0",
        "12906.0",
        "12906.0",
        "21960.0",
        "5184.0",
        "12906.0",
        "21960.0",
        "5184.0",
        "21960.0",
        "5184.0",
        "5184.0",
        "18090.0",
        "10449.3",
        "21960.0",
        "18090.0",
        "18090.0",
        "18090.0",
        "38754.0",
        "41337.9",
        "10449.3",
        "38754.0",
        "41337.9",
        "888.7",
        "38754.0",
        "38754.0",
        "10449.3",
        "888.7",
        "38754.0",
        "10449.3",
        "10449.3"
      ],
      montantstotalligne: [
        "4230.0",
        "5076.0",
        "7905.6",
        "4230.0",
        "6588.0",
        "48841.6",
        "10449.3",
        "888.7",
        "888.79",
        "5184.0",
        "5184.0",
        "7905.6",
        "5076.0",
        "5184.0",
        "5184.0",
        "5184.0",
        "5184.0",
        "7905.6",
        "38754.0",
        "41337.9",
        "11880.0",
        "11880.0",
        "11880.0",
        "38754.0",
        "38754.0",
        "38754.0",
        "48841.6",
        "10449.3",
        "888.7",
        "13722.0",
        "11880.0",
        "11880.0",
        "10449.3",
        "888.79",
        "10449.3",
        "10449.3",
        "21960.0",
        "5184.0",
        "5184.0",
        "7905.6",
        "7905.6",
        "5184.0",
        "5184.0",
        "12906.0",
        "12906.0",
        "21960.0",
        "5184.0",
        "12906.0",
        "21960.0",
        "5184.0",
        "21960.0",
        "5184.0",
        "5184.0",
        "18090.0",
        "10449.3",
        "21960.0",
        "18090.0",
        "18090.0",
        "18090.0",
        "38754.0",
        "41337.9",
        "10449.3",
        "38754.0",
        "41337.9",
        "888.7",
        "38754.0",
        "38754.0",
        "10449.3",
        "888.7",
        "38754.0",
        "10449.3",
        "10449.3"
      ],
      natures: [
        "TE av2000",
        "TU av2000",
        "PAT",
        "TU av2000",
        "PAT",
        "TSC",
        "TSC",
        "TSC",
        "TH",
        "TE av2000",
        "TU av2000",
        "PAT",
        "TE av2000",
        "TE av2000",
        "TU av2000",
        "TU",
        "TE ap2000",
        "PAT",
        "TP",
        "TP",
        "TP",
        "TP",
        "TP",
        "TP",
        "TP",
        "TP",
        "TP",
        "TSC",
        "TSC",
        "TP",
        "TP",
        "TP",
        "TSC",
        "TSC",
        "TSC",
        "TSC",
        "PAT",
        "TE ap2000",
        "TU",
        "PAT",
        "PAT",
        "TU",
        "TE ap2000",
        "TU",
        "TE ap2000",
        "PAT",
        "TU",
        "TU",
        "PAT",
        "TE ap2000",
        "PAT",
        "TE ap2000",
        "TU",
        "TU",
        "TSC",
        "PAT",
        "TE ap2000",
        "TU",
        "TE ap2000",
        "TP",
        "TP",
        "TSC",
        "TP",
        "TP",
        "TSC",
        "TP",
        "TP",
        "TSC",
        "TSC",
        "TP",
        "TSC",
        "TSC"
      ],
      nom: "CREDIT DU MAROC",
      secteurs: [
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "419",
        "419",
        "419",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "419",
        "419",
        "419",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413"
      ]
    }
  };//ArticlesNonSoldes
  private _foundItemsArticleSolder = {
    consulterArticlesSoldes: {
      adresse: "ND 20 AOUT LOT KHALIFA O HAMDA",
      annees: [
        "2014/2014",
        "2016/2016"
      ],
      articles: [
        "50560",
        "74388"
      ],
      codepostes: [
        "1111",
        "1111"
      ],
      ids: [
        "70743821",
        "102572516"
      ],
      libelle: "PERCEPTION BNI MELLAL - AM",
      libellepostes: [
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM"
      ],
      libelles: [
        "TAXE HABITATION ",
        "TAXE DE SERVICES COMMUNAUX "
      ],
      montantsfraisrecouvrement: [
        "0.0",
        "0.0"
      ],
      montantsmajoration: [
        "0.0",
        "0.0"
      ],
      montantsprincipal: [
        "0.02",
        "888.7"
      ],
      montantstotalligne: [
        "0.02",
        "888.7"
      ],
      natures: [
        "TH",
        "TSC"
      ],
      nom: "CREDIT DU MAROC",
      secteurs: [
        "413",
        "413"
      ]
    }
  };
  private selectedCodeContribuable = '';
  private selectedItems: string[] = [];
  private listviewIndexesChecker = [];
  private selectedItemsToPay2: number[] = [];
  private myUtils;
  public loader;
  private checkedItems: boolean[];
  private isChecked: boolean = false;
  //TODO delete situation fiscal detail
  private itemDetails = {};
  private index = null;
  private nom;
  private adresse;
  private title;
  private email;
  private totalMontant: number = 0;
  private selectedIDS: Array<string> = [""];

  formatedFLoat(str) {
    let res = String(parseFloat(str).toFixed(2)).replace('.', ',');
    if (res == 'NaN') res = "";
    return res;
  }

  fromSFiscal = false;

  userId;
  token;
  code_contribuable;
  private parentPage;
  private reference;
  private montant;

  constructor(private inAppBrowser: InAppBrowser, public navCtrl: NavController,
    public contribuableService: ContribuableService,
    public authServiceProvider: AuthServiceProvider,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loading: LoadingController,
    private nav: NavController,
    public http: Http,
    public contribuableobserver: ContribuableObserverProvider,
    // ,nativeStorage: NativeStorage
    private loadingCtrl: LoadingController,
    private auth: AuthServiceProvider,
    private events: Events
  ) {
    this.connectedUser = this.auth.currentUser;


    // let parentPage = navParams.get('page');
    this.email = navParams.get('email');
    console.log("email>>>>>> ", this.email);
    
    this.parentPage = navParams.get('page');
    this.reference = navParams.get('reference');
    this.montant = navParams.get('montant');
    let codenature = navParams.get('codenature');
    let annee = navParams.get('annee');
    this.userId = navParams.get('userId');
    this.token = navParams.get('token');
    this.code_contribuable = navParams.get('code_contribuable');
    let title = navParams.get('title');
    // this.foundItems = navParams.get('items');
    this.title = title;
    // get infos from the root page
    if (this.parentPage === 'articles_non_payes') {

      this.foundItems = this.getResultPEL(this.reference, this.montant);
      // this.foundItems = this.getResultPEL(reference, montant);
      console.log("articles_non_payes " + JSON.stringify(this.foundItems));
    }
    if (this.parentPage === 'avi-imposition-non-recu') {

      this.foundItems = this.getSituationFiscaleAvisImposition(codenature, this.reference, this.montant, annee);
      console.log("avi-imposition-non-recu " + JSON.stringify(this.foundItems));
    }//
    if (this.parentPage === 'article-non-soldes') {
      this.fromSFiscal = true;
      this.refreshNonSoldes()
    }//article-non-soldes
    if (this.parentPage === 'article-soldes') {
      this.fromSFiscal = true;
      this.refreshSoldes();
    }//article-soldes

    try {
      //this.selectedItemsToPay2 = [this.foundItems.ids.length];
    } catch (err) {
      console.log(" TRY to init selected Items to pay ", err);
    }

  }//END Constractor

  /**
   * Generate a string of Concateneted IDS by "," from an afray of ids
   * @param selectedIDS 
   */
  // getConcatenetedIDS(selectedIDS) {
  getConcatenatedIDS(selectedIDS: Array<string>): string {
    // selectedIDS = "";
    console.log("THIS SELECTED IDS ", this.selectedIDS);
    console.log("SELECTED IDS ", selectedIDS);

    // if (typeof selectedIDS == "undefined") {
    //   selectedIDS = "";
    // }

    var idsConcatenated = "";//by ,
    // for (var id; id > selectedIDS.length; ++id) {
    for (let id of selectedIDS) {
      // if (selectedIDS.length < selectedIDS.length + 1 && typeof id != "undefined" && id != null && id != "") {
      // if (selectedIDS.length  selectedIDS.length - 1) {
      if (typeof id != "undefined") {

        idsConcatenated += id + ",";//selectedIDS
      }
      // } else {
      // idsConcatenated += id;
      // }//END if
      // idsConcateneted += selectedIDS. [id] + ",";//

    }//END for
    //To delete the last ","
    idsConcatenated = idsConcatenated.substr(0, idsConcatenated.length - 1);

    console.log("Concateneted IDS ", idsConcatenated);
    return idsConcatenated;
  }
  /**
   * 
   */
  goToPay() {

    if (this.selectedIDS.length < 1) {
      console.log("nothing to pay", this.selectedIDS.length);
      return;
    }
    var url = "";
    var articlesIDSConcatenated;
    if (this.parentPage === 'articles_non_payes') {
      // let ref = 602728021067;
      // let montant = 451.70;//
      let ref = this.reference;
      let montant = this.montant;//
      let email = this.email;
      let totalMontant = this.totalMontant;// 451.70;
      articlesIDSConcatenated = this.getConcatenatedIDS(this.selectedIDS);//102901299;
      // url = "https://api.tgr.gov.ma/api/Contribuables/getURL/c1/%20/" + ref + "/" + montant + "/test%40test.com/" + totalMontant + "/" + articlesIDSConcatenated;
      url = "https://api.tgr.gov.ma/api/Contribuables/getURL/c1/%20/" + encodeURI(ref) + "/" + encodeURI(montant) + "/" + encodeURI(email) + "/" + encodeURI(String(totalMontant)) + "/" + +encodeURI(articlesIDSConcatenated);

      // if(articlesIDSConcatenated.trim().length>0){

      //   this.openPaiementURL(url);
      // }else{
      //   Utils.showAlert(this.alertCtrl,'','Veuillez sélectionner un article a payer .');
      // }
      // this.showAlertConfirmToPay('Confirmation pour le paiement onligne', "https://www.google.com");
      // this.showAlertConfirmToPayExternalBrowser('Confirmation pour le paiement onligne', "https://expired.badssl.com/");
      //


      console.log("GET_URL paiement en ligne ", url);
    }

    if (this.parentPage === 'avi-imposition-non-recu') {
      // let ref = 602728021067;
      // let montant = 451.70;//
      let ref = this.reference;
      let montant = this.montant;//
      let email = this.email;
      let totalMontant = this.totalMontant;// 451.70;
      articlesIDSConcatenated = this.getConcatenatedIDS(this.selectedIDS);//102901299;
      // url = "https://api.tgr.gov.ma/api/Contribuables/getURL/c1/%20/" + ref + "/" + montant + "/test%40test.com/" + totalMontant + "/" + articlesIDS;
      url = "https://api.tgr.gov.ma/api/Contribuables/getURL/c1/%20/" + encodeURI(ref) + "/" + encodeURI(montant) + "/" + encodeURI(email) + "/" + encodeURI(String(totalMontant)) + "/" + articlesIDSConcatenated;
      //this.openPaiementURL(url);
      console.log("GET_URL avi-imposition-non-recu ", url);
    }//
    if (this.parentPage === 'article-non-soldes') {
      // let url;

      let codecontribuable = this.code_contribuable;//
      let totalMontant = this.totalMontant;// 451.70;
      let email = this.email
      articlesIDSConcatenated = this.getConcatenatedIDS(this.selectedIDS);//102901299;
      // url = "https://api.tgr.gov.ma/api/Contribuables/getURL/c2/" + codecontribuable + "/%20/%20/test%40test.com/" + totalMontant + "/" + articlesIDSConcatenated;
      url = "https://api.tgr.gov.ma/api/Contribuables/getURL/c2/" + codecontribuable + "/%20/%20/" + encodeURI(email) + "/" + encodeURI(String(totalMontant)) + "/" + encodeURI(articlesIDSConcatenated);

      //this.openPaiementURL(url);
      console.log("GET_URL article-non-soldes ", url);
    }//article-non-soldes
    if (this.parentPage === 'article-soldes') {
      let codecontribuable = this.code_contribuable;//
      let totalMontant = this.totalMontant;// 451.70;
      articlesIDSConcatenated = this.getConcatenatedIDS(this.selectedIDS);//102901299;
      let email = this.email
      url = "https://api.tgr.gov.ma/api/Contribuables/getURL/c2/" + codecontribuable + "/%20/%20/" + encodeURI(email) + "/" + encodeURI(String(totalMontant)) + "/" + encodeURI(articlesIDSConcatenated);
      //this.openPaiementURL(url);
      //console.log("GET_URL article-soldes ", url);
    }//article-soldes
    // then open the link
    // this.openPaiementURL(url);
    ////////////////////////////////////// test if concatenatedIDS is not empty /////////////////////////////////////////
    console.log('===>before .. articlesIDSConcatenated' + articlesIDSConcatenated);
    console.log('===>before .. articlesIDSConcatenated length' + articlesIDSConcatenated.length);
    if (typeof articlesIDSConcatenated != 'undefined' && articlesIDSConcatenated.trim().length > 0) {
      console.log('===>articlesIDSConcatenated' + articlesIDSConcatenated);

      this.openPaiementURL(url);
    } else {
      Utils.showAlert(this.alertCtrl, '', 'Veuillez sélectionner un article à payer .');
    }
  }  

  /**
   * 
   * @param url 
   */
  openBrowser(url: string) {
    /*
      window.open(‘http://example.com’, ‘_system’);   Loads in the system browser 
      window.open(‘http://example.com’, ‘_blank’);    Loads in the InAppBrowser
      window.open(‘http://example.com’, ‘_blank’, ‘location=no’); Loads in the InAppBrowser with no location bar
      window.open(‘http://example.com’, ‘_self’); Loads in the Cordova web view 
    */
    window.open(url, '_system');
    console.log('TYPE_OF URL>>>', typeof url);
  }
  /**
   * Open Paiement URL 
   */
  openPaiementURL(url) {
    let foundItem;
    this.presentLoading();
    this.contribuableService.openPaiementURL(url).subscribe(
      data => {
        // this.dismissLoading();
        foundItem = data.json();
        console.log('FOUND_ITEMS>>>', foundItem);
        console.log('\n ..foundItems.message= ', data.json().code);
        if (foundItem.code == "500") {
          Utils.showAlert(this.alertCtrl, 'Paiement en ligne', foundItem.message);
          console.log('\n ..foundItems.message ', foundItem.message);
        } else {
          // this.openBrowser(String(foundItem.url));
          this.showAlertConfirmToPay('Confirmation pour le paiement en ligne', foundItem.url);
          //this.showAlertConfirmToPayExternalBrowser('Confirmation pour le paiement onligne', foundItem.url);
          console.log('URL for paiement>>>', foundItem.url);
        }
        this.dismissLoading();
      },
      err => {
        this.dismissLoading();
        console.error(err);
        Utils.showAlert(this.alertCtrl, 'Confirmation pour le paiement en ligne ', "Service indisponible !");

      }
    );
    // return this.foundItems;
  }

  connectedUser;
  logout(cbFinish?) {
    console.log("START LOGOUT.", this.connectedUser, this.auth.getUser);
    this.auth.getUser()
      .then(user => {
        if (user && user['tokenData']) {
          console.log("START LOGOUT..");

          let loading = this.loadingCtrl.create({
            content: 'Traitement en cours...',
            showBackdrop: true,
            enableBackdropDismiss: true
          });
          loading.present();

          this.events.publish('user:doLogout', Date.now());
          var isCbExec = false;
          if (cbFinish) this.events.subscribe('user:logout', (xdate) => {
            console.log("logout done !!!");

            if (!isCbExec) {
              isCbExec = true;
              cbFinish()
            };
            if (loading.isOverlay) loading.dismiss();
          });
        }
      })
  }

  refreshNonSoldes() {
    this.title = 'Articles non soldés';
    this.foundItems = this.getArticlesNonSolderByCodeContribuable(this.userId, this.token, this.code_contribuable);
    console.log("article-soldes " + JSON.stringify(this.foundItems));
  }

  refreshSoldes() {
    this.title = 'Articles soldés';
    this.foundItems = this.getArticleSoldeByCodeContribuable(this.userId, this.token, this.code_contribuable);
    console.log("article-non-soldes " + JSON.stringify(this.foundItems));
  }

  /**
   *
   * @param index
   */
  goToDetails(index) {
    // if(thi){
    // let itemDetails = this.foundItemsArticleNonSolder.consulterArticlesNonSoldes
    let itemDetails = this.foundItems;//ArticleNonSolder.consulterArticlesNonSoldes
    // }
    this.nav.push(SituationFiscaleDetailPage, { item: itemDetails, index: index, FromSFiscal: this.fromSFiscal });
    ///////////////////////////////// TEST /////////////////////////////////////////

  }



  ionViewDidLoad() {
    //to test expired/bad ssl..
    //this.showAlertConfirmToPay('Confirmation pour le paiement onligne', "https://expired.badssl.com/");

    ///////////// important! ////////////////
    // this.selectedItemsToPay = [];
    // this.selectedItems = [];
    /////////// END important! ////////////////

    console.log('ionViewDidLoad ContribuablePage');

    // this.authServiceProvider.getUserInfo()
    //   .then(
    //   data => {
    //     console.log(data);
    //     console.log('\n authServiceProvider !!! ' + JSON.stringify(data) + '\n\n');
    //     // let codecontribuables = data.LDAP.codecontribuable;   this.staticCodeContribuables=codecontribuables;
    //     // let userid = data.access.userid;                      this.staticUserId=userid;
    //     // let token = data.data.id;                             this.staticToken=token;
    //     ////////////////////////// Loader //////////////////////////////
    //     this.presentLoading();
    //     //TODO : split the ContribualeCode Array
    //     this.codeContribuables = data.LDAP.codecontribuable;//["7350145", "602728021059"] ;//= data.LDAP.codecontribuable;
    //     this.codeContribuables = this.codeContribuables[0].split("|");
    //     this.userId = data.access.userid;
    //     this.token = data.data.id;

    //     //   this.staticCodeContribuables.forEach((codecontribuable) => {

    //     //     // this.myUtils.doGet(Config.getSoldeBancaireApi('RIB'))//userid, ref, token))
    //     //     //   .then((responce) => {
    //     //     //     if (this.myUtils.checker(responce["_body"]) && this.myUtils.checker(JSON.parse(responce["_body"]).soldeBancaire)) {

    //     //     //       //this.accounts.push({ ref: ref, total: JSON.parse(responce["_body"]).soldeBancaire.solde.disponibleNet });

    //     //     //     }
    //     //     //     //this.dismissLoading();
    //     //     //   })
    //     //     //   .catch((err) => {
    //     //     //     console.log("retun err::", err);
    //     //     //     //this.dismissLoading();
    //     //     //   });
    //     // console.error('\n CodeContrib \n'+codecontribuable);
    //     //  });
    //     console.error('\n MyUtils complete !!! \n');
    //   },
    //   error => {
    //     console.error(error)
    //     //this.dismissLoading();
    //   });
    // this.dismissLoading();

    //this.showHeader = this.navCtrl.parent.childsShowHeader;

  }//END ionViewDidLoad /////////////////////////////////////////////////////////////////////////////////////////////////

  /**
   * 
   */
  private selectAllFlag = false;
  selectedAll() {

    this.totalMontant = 0;
    this.selectAllFlag = true;

    console.info("selectALL -- this.totalMontant", this.totalMontant);
    for (let i = 0; i < this.foundItems.montantstotalligne.length; i++) {
      // this.foundItems.m[i] = true;
      this.listviewIndexesChecker[i] = true;
      this.addItemToArticlesToPay(this.foundItems.montantstotalligne[i], i)
    }


  }

  /**
   * 
   */
  deselectedAll() {
    // if (e.checked) {

    // this.totalMontant = 0;
    this.selectAllFlag = false;
    console.info("DeselectALL -- this.totalMontant", this.totalMontant);

    // else {
    for (let i = 0; i < this.foundItems.montantstotalligne.length; i++) {
      this.listviewIndexesChecker[i] = false;
      this.deleteItemFromArticlesToPay(this.foundItems.montantstotalligne[i], i);
    }
    this.totalMontant = 0;
    // }
  }

  /**
   * 
   * @param montant 
   * @param index 
   * @param id 
   */
  addItemToArticlesToPay(montant, index) {
    console.log("/n INDEX !! ", index);
    console.log("/n ITEM ADDED !! ", JSON.stringify(montant));

    this.selectedItemsToPay2[index] = Number(montant);
    this.selectedIDS[index] = this.foundItems.ids[index];


    console.log("selectedIDS", this.selectedIDS);
    var countIds = 0;
    this.selectedIDS.forEach((selectedId)=>{
      if(selectedId){
        countIds++;
      }
    })
    console.log("countIds", countIds, this.foundItems.articles.length);
    
    if(countIds < this.foundItems.articles.length){
      this.selectAllFlag = false
    }else{
      this.selectAllFlag = true
    }
    
    console.log("/n ID ADDED !! ", this.foundItems.ids[index]);
    this.listviewIndexesChecker[index] = true
    // Calculate the total of a given array of numbers.
    this.totalMontant = this.calculateTotalMontant(this.selectedItemsToPay2);

    console.log("/n TOTAL MONTANT ", JSON.stringify(this.totalMontant));
  }

  /**
   * 
   * @param montant 
   * @param index 
   * @param id 
   */
  deleteItemFromArticlesToPay(montant, index) {
    console.log("/n INDEX !! ", index);
    console.log("/n ITEM DELETED !! ", JSON.stringify(montant));
    delete this.selectedItemsToPay2[index];// [index]=
    // this.selectedItemsToPay2[index];
    delete this.selectedIDS[index];// [index]=
    console.warn("/n ID DELETED !! ", this.foundItems.ids[index]);

    console.log("selectedIDS", this.selectedIDS);
    var countIds = 0;
    this.selectedIDS.forEach((selectedId)=>{
      if(selectedId){
        countIds++;
      }
    })
    console.log("countIds", countIds, this.foundItems.articles.length);
    
    if(countIds < this.foundItems.articles.length){
      this.selectAllFlag = false
    }else{
      this.selectAllFlag = true
    }

    this.listviewIndexesChecker[index] = false;
    this.totalMontant = this.calculateTotalMontant(this.selectedItemsToPay2);
    // this.totalMontant = this.selectedItemsToPay2.reduce((a, b) => a + b, 0);//this.calculateTotalMontant(this.selectedItemsToPay2);
    console.log("/n TOTAL MONTANT ", JSON.stringify(this.totalMontant));
  }

  /**
   * Calculate the total of a given array of numbers.
   * @param selectedItemsToPay
   */
  calculateTotalMontant(selectedItemsToPay: Array<number>): number { 

    var totalMontant: number = 0;
    totalMontant = selectedItemsToPay.reduce((a, b) => a + b, 0);
    return totalMontant;
  }

  /**
  * Liste des articles non payés.
  * ex : https://api.tgr.gov.ma/api/Contribuables/getResultPEL/605654067251/306.9
  */
  getResultPEL(reference, montant) {
    // var foundItems = { "code": "500", "message": "Entrée incorrecte" };
    try {
      // Utils.presentLoading(this.loading);
      this.presentLoading();

      this.contribuableService.getResultPEL(reference, montant).subscribe(
        data => {
          this.foundItems = data.json();//foundItems
          // foundItems = data.json();//foundItems

          if (this.foundItems.code == "500") {

            Utils.showAlert(this.alertCtrl, '', this.foundItems.message);
            console.log('\n ..foundItems.message ', this.foundItems.message);
          }

          // console.log('\n errJson OBJ !!! ', errJson);
          console.log('\n foundItems OBJ !!! ', this.foundItems);
          // var o = (errJson == foundItems);
          // console.log('\n var OBJ !!! ', o);



          // if (errJson === foundItems) {
          //   console.log('\n ERROR 500 !!! ', data.json());
          // } else {
          //   this.foundItems = data.json();//foundItems
          //   console.log('\n foundItems .. getResultPEL!!! ', data.json());
          // }
          ////////////////////// or whereever `items` is initialized ///////////////////////
          // this.listviewIndexesChecker = [];
          // if (typeof this.foundItems.ids.length != 'undefined') {
          //   for (var i; i < this.foundItems.ids.length; i++) {
          //     this.listviewIndexesChecker.push(false);
          //   }
          // }
          //////////////////////////////////////////////////////////////////////////////////
          this.dismissLoading();
        },
        err => {
          console.error(err);
          if (err.status == 504) {
            Utils.showAlert(this.alertCtrl, '', "504 Gateway Timeout .");
          } else {

            Utils.showAlert(this.alertCtrl, '', "vérifier les entrées ou la connexion internet.");
          }
          this.dismissLoading();
          // return;
          // this.showAlert('Ooops', 'Oooops !!, We can not connect to the server "AgencyByRib" !!');
        }
      );
    } catch (err) {
      // this.dismissLoading();
      Utils.showAlert(this.alertCtrl, '', 'Entrée incorrecte !!');
    }
    // this.dismissLoading();
    // return this.foundItems;
    return this.foundItems;
  }

  /**
   * consulter situation fiscale par avis d'imposition
   * http://api.tgr.gov.ma/api/Contribuables/consulterArticlesARegler?nature={codenature}&reference={reference}&montant={montant]&annee={annee}
   */
  getSituationFiscaleAvisImposition(codenature, reference, montant, annee) {
    this.presentLoading();
    try {

      this.contribuableService.getSituationFiscaleAvisImposition({
        'annee': annee,
        'reference': reference,
        'montant': montant,
        'codenature': codenature
      }).subscribe(
        data => {

          try {

            console.log('\n foundItems .. getSituationFiscaleAvisImposition!!! ' + JSON.stringify(data.json()) + '\n\n');


            console.log('\n ..foundItems.message= ', data.json().code);
            if (data.json().code == "500") {
              Utils.showAlert(this.alertCtrl, '', this.foundItems.message);
              console.log('\n ..foundItems.message ', this.foundItems.message);
            }

            if (typeof data.json().consulterArticlesARegler == 'string') {
              Utils.showAlert(this.alertCtrl, '', data.json().consulterArticlesARegler);
              console.log('\n ..foundItems.message ', data.json().consulterArticlesARegler);
            }
            this.foundItems = (data.json()).consulterArticlesARegler;
            // for (let i = 0; i < this.foundItems.montantstotalligne.length; i++) {
            //   this.listviewIndexesChecker[i] = true;
            // }

            console.log('\n foundItems .. getSituationFiscaleAvisImposition!!! ' + JSON.stringify(data.json()) + '\n\n');
            ////////////////////// or whereever `items` is initialized ///////////////////////
            // this.selectedItems = data.json().consulterArticlesARegler.ids;
            // this.selectedItemsToPay = [];
          } catch (error) {
            Utils.showAlert(this.alertCtrl, '', "Service indisponible!");
          }
          this.dismissLoading();
        },
        err => {

          console.error(err);
          //Utils.showAlert(this.alertCtrl, '', 'Entrée incorrecte !!');
          // Utils.showAlert(this.alertCtrl, '', "vérifier les entrées ou la connexion internet.");
          //in the case of expired TOKEN
          if (err.status === 401) {
            // this.refresh().then(() => {
            //   //recall the service again.
            // });
          } if (err.status == 504) {
            Utils.showAlert(this.alertCtrl, '', "504 Gateway Timeout .");
          } else {

            Utils.showAlert(this.alertCtrl, '', "vérifier les entrées ou la connexion internet.");
          }
          this.dismissLoading();
          // return;
          // this.showAlert('Ooops', 'Oooops !!, We can not connect to the server "AgencyByRib" !!');

        });
    } catch (err) {
      this.dismissLoading();
      Utils.showAlert(this.alertCtrl, '', "vérifier les entrées ou la connexion internet.");
    }
    return this.foundItems;
  }

  /*
   * liste des articles non soldés
   * ex: https://api.tgr.gov.ma/api/AppUsers/CompteDemo/7350145/consulterArticlesNonSoldes?access_token=
   */
  getArticlesNonSolderByCodeContribuable(userId, token, code_contribuable) {
    this.presentLoading();
    this.contribuableService.getArticlesNonSolder(userId, token, code_contribuable).subscribe(
      data => {
        this.dismissLoading();
        try {
          this.foundItems = (data.json()).consulterArticlesNonSoldes;//consulterArticleSoldes
          // this.nom=this.foundItems.nom;
          // this.adresse=this.foundItems.adresse;

        } catch (err) {

          Utils.showAlert(this.alertCtrl, '', "Service indisponible !");

        }
        console.log('\n foundItems .. ArticleNonSolde!!! \n' + JSON.stringify(this.foundItems) + '\n\n');
      },
      err => {

        console.error(err);
        if (err.status == 504) {
          this.dismissLoading();
          Utils.showAlert(this.alertCtrl, '', "504 Gateway Timeout .");
        } else if (err.status == 401) {
          console.log("trying logout..");

          this.logout(() => {
            Utils.showAlert(this.alertCtrl, '',
              (JSON.parse(err["_body"]).error && JSON.parse(err["_body"]).error.message) ? JSON.parse(err["_body"]).error.message : 'Erreur 401!');
          });
        } else {
          this.dismissLoading();
          // Utils.showAlert(this.alertCtrl, '', "vérifier les entrées ou la connexion internet.");
          Utils.showAlert(this.alertCtrl, '', "Service indisponible !");
        }
      }
    );
    return this.foundItems;
  }//getArticleNonSoldeByCodeContribuable



  /**
    * liste des articles soldés
    * ex: https://api.tgr.gov.ma/api/Contribuables/getResultS/9758663
    */
  getArticleSoldeByCodeContribuable(userId, token, code_contribuable) {
    this.presentLoading();
    this.contribuableService.getArticlesSolder(userId, token, code_contribuable).subscribe(
      data => {
        // this.dismissLoading();

        // console.log('\n foundItems .. data.json()!!! \n' + JSON.stringify(data.json()) + '\n\n');
        try {

          this.foundItems = (data.json()).consulterArticleSoldes;

          console.log('foundItems .. ArticleSolde!!! \n' + JSON.stringify(data.json()) + '\n\n');

        } catch (error) {
          Utils.showAlert(this.alertCtrl, '', "Service indisponible !");
        }

        this.dismissLoading();

      },
      err => {
        this.dismissLoading();
        console.error("\n err===", err);
        if (err.status == 504) {
          Utils.showAlert(this.alertCtrl, '', "504 Gateway Timeout .");
        } else if (err.status == 401) {

          this.logout(() => {
            Utils.showAlert(this.alertCtrl, '',
              (JSON.parse(err["_body"]).error && JSON.parse(err["_body"]).error.message) ? JSON.parse(err["_body"]).error.message : 'Erreur 401!');
          });
        } else {

          // Utils.showAlert(this.alertCtrl, '', "vérifier les entrées ou la connexion internet.");
          Utils.showAlert(this.alertCtrl, '', "Service indisponible !");
        }
        // Check status Error {401,404 ....} and show an alert box.
        // this.checkErrorStatus(err);
        Utils.checkErrorStatus(this.alertCtrl, err);
        // this.showAlert('Ooops', 'Oooops !!, We can not connect to the server "AgencyByRib" !!');
      }

    );
    return this.foundItems;
  }//getArticleSoldeByCodeContribuable


  browser: InAppBrowser;
  // url: string;
  /**
   * Confirmation Alert box for paiement url.
   */
  showAlertConfirmToPay(title, url) {
    // message = "<a href='" + message + "'  target='_blank'>Confirmer le paiement</a>";
    // let alert = this.alertCtrl.create({
    //   title: title,
    //   subTitle: message,
    //   buttons: ['OK']
    // });
    // alert.present();
    ////////////////////////////////////////////////////////////////////////////
    // const browser = this.iab.create('https://ionicframework.com/');

    let options: InAppBrowserOptions = {
      // location: 'no',
      // zoom: 'no',
      // hidden: 'yes'

      location: 'yes',//Or 'no' 
      hidden: 'no', //Or  'yes'
      clearcache: 'yes',
      clearsessioncache: 'yes',
      zoom: 'yes',//Android only ,shows browser zoom controls 
      hardwareback: 'yes',
      mediaPlaybackRequiresUserAction: 'no',
      shouldPauseOnSuspend: 'no', //Android only 
      closebuttoncaption: 'Close', //iOS only
      disallowoverscroll: 'no', //iOS only 
      toolbar: 'yes', //iOS only 
      enableViewportScale: 'no', //iOS only 
      allowInlineMediaPlayback: 'no',//iOS only 
      presentationstyle: 'pagesheet',//iOS only 
      fullscreen: 'yes',//Windows only  
    };

    // const browser2 = this.iab.create(url, '_system');
    // const browser3 = this.iab.create(url, '_self',options);//cordova webkit browser
    const browser = this.inAppBrowser.create(url, '_blank', options);//inap browser


    browser.show();

    // this.browser = new InAppBrowser(this.url);
    // this.browser.show();
    // browser.executeScript(...);
    // browser.insertCSS(...);
    // browser.close();
  }//END Alert

  /**
   * Confirmation Alert box for paiement url.
   */
  showAlertConfirmToPayExternalBrowser(title, url) {
    // message = "<a href='" + message + "'  target='_blank'>Confirmer le paiement</a>";
    // let alert = this.alertCtrl.create({
    //   title: title,
    //   subTitle: message,
    //   buttons: ['OK']
    // });
    // alert.present();
    ////////////////////////////////////////////////////////////////////////////
    // const browser = this.iab.create('https://ionicframework.com/');

    let options: InAppBrowserOptions = {
      // location: 'no',
      // zoom: 'no',
      // hidden: 'yes'

      location: 'yes',//Or 'no' 
      hidden: 'no', //Or  'yes'
      clearcache: 'yes',
      clearsessioncache: 'yes',
      zoom: 'yes',//Android only ,shows browser zoom controls 
      hardwareback: 'yes',
      mediaPlaybackRequiresUserAction: 'no',
      shouldPauseOnSuspend: 'no', //Android only 
      closebuttoncaption: 'Close', //iOS only
      disallowoverscroll: 'no', //iOS only 
      toolbar: 'yes', //iOS only 
      enableViewportScale: 'no', //iOS only 
      allowInlineMediaPlayback: 'no',//iOS only 
      presentationstyle: 'pagesheet',//iOS only 
      fullscreen: 'yes',//Windows only  
    };

    // const browser2 = this.iab.create(url, '_system');
    // const browser3 = this.iab.create(url, '_self',options);//cordova webkit browser
    const browser = this.inAppBrowser.create(url, '_system', options);//inap browser


    browser.show();

    // this.browser = new InAppBrowser(this.url);
    // this.browser.show();
    // browser.executeScript(...);
    // browser.insertCSS(...);
    // browser.close();
  }//END Alert
  ///////////////////////////////// Utils ////////////////////////////////////
  /**
    * Check status Error {401,404 ....} and show an alert box.
    * @param err
  */
  _checkErrorStatus(err): void {
    // if (err.status == 401) {
    //   this.showAlert('', 'Probleme d\'authentification.');
    // }
  }

  ///////////////////////////////////// Alert box ////////////////////////////////


  ///////////////////////////////////// loader; ////////////////////////////////
  /**
   * Show loading
   */
  dismissed = false
  presentLoading() {
    this.loader = this.loading.create({
      content: "Chargement des données...",
      showBackdrop: true,
      enableBackdropDismiss: true,
      //dismissOnPageChange: true
    });

    this.loader.onDidDismiss(() => {
      console.log('Dismissed loading');
    });

    this.loader.present();

    this.dismissed = false;

    // setTimeout(() => {
    //   this.loader.dismiss();
    // }, 15000);
  }

  /**
   * Dismiss loading
   */
  dismissLoading() {
    console.log('--Dismissed loading--');
    if (!this.dismissed) {
      this.dismissed = true;
      this.loader.dismiss();
    }

  }//////////////////////// END loader ///////////////////////////////////////


  mapNature(nature: string): string{
    var res = nature;
    switch (nature) {
      case "TP": res =  "TAXE PROFESSIONNELLE"; break;
      case "TSC": res = "TAXE HABITATION/SCES COMMUNAUX"; break;
      case "TH": res =  "TAXE HABITATION/SCES COMMUNAUX"; break;
      case "IS": res =  "IMPOT GENERAL SUR LES REVENUS"; break;
      case "IP": res =  "IMPOT DES PATENTES"; break;
      default:
        break;
    }
    return res;
  }





  ////////////////////////////////////////////////////////////////////////////////////////
  // /**
  //  * liste des codecontribuable d'un utilisateur {id}
  //  * https://api.tgr.gov.ma/api/AppUsers/{id}/contribuables?access_token={access_tocken}
  //  */
  // getCodeContribuableByUserId(userId, token) {
  //   this.contribuableService.getCodeContribuableByUserId(userId, token).subscribe(
  //     data => {
  //       //  this.staticCodeContribuables = data.json;
  //       //this.staticCodeContribuables = data.json;
  //       this.foundItems = data.json();
  //       console.log('\n foundItems .. Code contribuable!!! ' + JSON.stringify(data.json()) + '\n\n');
  //       console.log('\n foundItems .. Code contribuable!!! ' + this.codeContribuables + '\n\n');

  //       this.dismissLoading();
  //       // //Call Article Solder et non Solder for each codeContribuable
  //       // for (let item of this.staticCodeContribuable) {
  //       //   console.log('\n item \n' + JSON.stringify(item));
  //       //   //  console.log(this.staticCodeContribuable[i])
  //       //   //Call Article Solder et non Solder
  //       //   this.getArticleSoldeByCodeContribuable(this.staticUserId, this.staticToken, item);
  //       //   this.getArticlesNonSolderByCodeContribuable(this.staticUserId, this.staticToken, item);
  //       // }//For
  //       //this.loadAccount();
  //     },
  //     err => {
  //       console.error(err);
  //       // this.showAlert('Ooops', 'Oooops !!, We can not connect to the server "AgencyByRib" !!');
  //     },
  //     () => {
  //       console.log('getCodeContribuableByUserId completed');
  //     }
  //   );
  //   return this.codeContribuables;
  // }//getCodeContribuableByUserId

  // /**
  //  * liste des articles soldés
  //  * ex: https://api.tgr.gov.ma/api/Contribuables/getResultS/9758663
  //  */
  // _getArticleSoldeByCodeContribuable(userId, token, code_contribuable) {
  //   this.presentLoading();
  //   this.contribuableService.getArticlesSolder(userId, token, code_contribuable).subscribe(
  //     data => {
  //       this.foundItemsArticleSolder = data.json();

  //       console.log('foundItems .. ArticleSolde!!! \n' + JSON.stringify(data.json()) + '\n\n');
  //     },
  //     err => {
  //       console.error(err);
  //       // this.showAlert('Ooops', 'Oooops !!, We can not connect to the server "AgencyByRib" !!');
  //     },
  //     () => {
  //       console.log('getArticleSoldeByCodeContribuable completed');
  //       this.dismissLoading();
  //     }

  //   );

  //   return this.foundItems;
  // }//getArticleSoldeByCodeContribuable


  // /**
  //  * liste des articles non soldés
  //  * ex: https://api.tgr.gov.ma/api/Contribuables/getResultNS/9758663
  //  */
  // _getArticlesNonSolderByCodeContribuable(userId, token, code_contribuable) {
  //   this.contribuableService.getArticlesNonSolder(userId, token, code_contribuable).subscribe(
  //     data => {
  //       // let foundItems =Object.assign( data.json );
  //       this.foundItemsArticleNonSolder = data.json();
  //       console.log('\n foundItems .. ArticleNonSolde!!! \n' + JSON.stringify(this.foundItemsArticleNonSolder) + '\n\n');
  //     },
  //     err => {
  //       console.error(err);
  //       // this.showAlert('Ooops', 'Oooops !!, We can not connect to the server "AgencyByRib" !!');
  //     },
  //     () => {
  //       console.log('getArticleNonSoldeByCodeContribuable completed');
  //     }
  //   );
  //   return this.foundItems;
  // }//getArticleNonSoldeByCodeContribuable

}//END class



////////////////////////////////////////////////
export interface ConsulterArticlesNonSoldes {
  adresse: string;
  annees: string[];
  articles: string[];
  codepostes: string[];
  ids: string[];
  libelle: string;
  libellepostes: string[];
  libelles: string[];
  montantsfraisrecouvrement: string[];
  montantsmajoration: string[];
  montantsprincipal: string[];
  montantstotalligne: string[];
  natures: string[];
  nom: string;
  secteurs: string[];
}
export interface ArticleNonSolderRoot {
  consulterArticlesNonSoldes: ConsulterArticlesNonSoldes;
}
