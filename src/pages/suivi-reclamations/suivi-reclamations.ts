

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';

import { MyUtilsProvider } from '../../providers/my-utils/my-utils';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';


import { SyncDataProvider } from "../../providers/sync-data/sync-data";
/**
 * Generated class for the SuiviReclamationsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
//@IonicPage()
@Component({
  selector: 'page-suivi-reclamations',
  templateUrl: 'suivi-reclamations.html',
})
export class SuiviReclamationsPage {

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private auth: AuthServiceProvider,
    private myUtils: MyUtilsProvider) {
  }

  reclamationRef = "";
  storageRecsKey = "reclamations_unknow_user"
  storageRefsKey = "references_unknow_user"
  ionViewDidLoad() {
    console.log('ionViewDidLoad SuiviReclamationsPage');


    if (this.auth.currentUser) {
      this.storageRecsKey = "reclamations_" + this.auth.currentUser.LDAPData.id; //reclamations_userid
      this.storageRefsKey = "references_" + this.auth.currentUser.LDAPData.id; //references_userid

    }



    // get Reclamations From Storage for an user 'authenticated or unknown'
    
    //this.getReclamationsFromStorage();

  }
  /**
   * 
   *  get Reclamations From Storage for an user 'authenticated or unknown'
  */
  getReclamationsFromStorage() {
    try {
      this.myUtils.doGetPrefs(this.storageRecsKey).then(recs => {
        if (recs) {
          recs.forEach((rec) => {
            var found = false;
            for (let i = 0; i < this.reclamations.length; i++) {
              const r = this.reclamations[i];
              if (r.reference == rec.reference) {
                found = true;
                break;
              }
            }
            if (!found) {
              this.reclamations.push(rec);
              this.reponses.push(rec);
            }
          });
        }
        console.log("recs " + this.storageRecsKey, recs);
      })
  
      this.myUtils.doGetPrefs(this.storageRefsKey).then(recs => {
        if (recs) this.doSearchFor(true, recs)
        console.log("recs " + this.storageRefsKey, recs);
      })
    } catch (error) {
      console.warn(error);
      
    }
    
  }

  reclamations = [];
  suivi(inputTxt: string) {

    if (inputTxt.trim().length > 0) {

      if (this.reponses && this.reponses.length > 0) {
        this.reclamations = [];
        for (let i = 0; i < this.reponses.length; i++) {
          const ref: string = this.reponses[i].reference;
          if (ref.indexOf(inputTxt) !== -1) {
            this.reclamations.push(this.reponses[i])
          }
        }
      }
    } else {
      if (this.reponses && this.reponses.length > 0) {
        this.reclamations = [];
        for (let i = 0; i < this.reponses.length; i++) {
          this.reclamations.push(this.reponses[i])

        }
      }
    }
  }

  doSearchFor(showLoading, refs: Array<string>) {
    if (refs) {
      this.presentLoading();
      refs.forEach((ref, idx) => {
        this.doSearch(false, ref);
        if (idx === refs.length - 1) {
          this.dismissLoading();
        }
      })
    }

  }


  reponses = [];
  doSearch(showLoading, reclamationRef?) {
    if (!reclamationRef) reclamationRef = this.reclamationRef;

    console.log("doSearch>>", reclamationRef);
    if (showLoading) this.presentLoading();
    this.myUtils
      .doGet("https://api.tgr.gov.ma/api/ReponseReclamations/consulterReponseReclamation?ref=" + /*encodeURI*/(reclamationRef))
      .then(res => {
        if (showLoading) this.dismissLoading();
        console.warn(res);
        try {
          let results = JSON.parse(res['_body']);
          console.log(results);
          if (results.reponseReclamation) {
            console.log("OK !!");

            var found = false;
            for (var i = 0; i < this.reponses.length; i++) {
              if (this.reponses[i].reference == results.reponseReclamation.reference) {
                found = true;
                break;
              }
            }
            for (let i = 0; i < this.reclamations.length; i++) {
              const r = this.reclamations[i];
              if (r.reference == results.reponseReclamation.reference) {
                found = true;
                break;
              }
            }
            if (!found) {
              console.log("ref  insertion!!");
              this.reponses.unshift(results.reponseReclamation);
              this.reclamations.unshift(results.reponseReclamation);
              this.myUtils.doSetPrefs(this.storageRecsKey, this.reponses)
            } else {

              console.log("ref found !!");

            }

          } else
            if (results.reponseReclamation == null) {
              if (showLoading) this.showAlert("Aucune réclamation avec la référence renseignée !!")
            } else {
              if (showLoading) this.showAlert("Service indisponible..")
            }

        } catch (error) {
          if (showLoading) this.showAlert("Service indisponible..")
        }
      })
      .catch(err => {
        if (showLoading) this.dismissLoading();
        console.error(err);
        if (showLoading) this.showAlert("Service indisponible..")
      });
  }


  private loader;
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Chargement des données...",
      showBackdrop: true,
      enableBackdropDismiss: true,
      dismissOnPageChange: true
    });

    this.loader.onDidDismiss(() => {
      console.log('Dismissed loading');
    });

    this.loader.present();
  }

  dismissLoading() {
    console.log('--Dismissed loading--');
    if (!this.loader.Dismissed) this.loader.dismiss();

  }


  public showAlert(message) {
    let alert = this.alertCtrl.create({
      title: 'TGR Mobile',
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }//END Alert

}
