import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';

import { Utils } from '../../MyUtils/Utils';
import { UsersService } from '../../providers/users.service/users.service';
import { LoadingController } from 'ionic-angular';
/*

*/
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html'
})
export class ForgotPasswordPage {
  private data: any;
  private login;
  private email;
  private myUtils;
  private foundItems;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private usersService: UsersService,
    private loadingCtrl: LoadingController,

    public alertCtrl: AlertController
  ) {
    this.data = {};
    // this.data.password = '';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPasswordPage');
  }

  resetPassword() {
    Utils.presentLoading(this.loadingCtrl);
    this.usersService.resetPassword(this.login, this.email).subscribe(
      data => {
        this.foundItems = data.json();
        console.log("---> FoundItems!!", this.foundItems);


        if (this.foundItems.pass == 'ERROR') {
          // Utils.showAlert(this.alertCtrl, 'Réinitialisation du mot de passe', 'Veuillez verifier votre login et mot de passe ');
          Utils.showAlert(this.alertCtrl, 'Réinitialisation du mot de passe', 'Veuillez verifier votre login et mot de passe ');
        } else {
          //Utils.showAlert(this.alertCtrl, 'Réinitialisation du mot de passe', 'Votre nouveau  mot de passe est généré automatiquement  <br/> <strong>Mot de passe</strong> : '+this.foundItems.pass);
          Utils.showAlert(this.alertCtrl, 'Réinitialisation du mot de passe', 'Un nouveau mot de passe a été envoyé dans votre boite de réception');
        }
        Utils.dismissLoading();
      },
      err => {
        Utils.dismissLoading();
        console.error('reset pass error', err)
        Utils.showAlert(this.alertCtrl, 'Réinitialisation du mot de passe', 'Veuillez verifier votre login et mot de passe ');
        // if (err.status == 503) {

        //   Utils.showAlert(this.alertCtrl, 'Ooops', '503 !!');
        // }
      },
      () => {

        console.log('reset password completed')
      }
    );
  }

}
