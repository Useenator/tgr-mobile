//Najim

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events } from 'ionic-angular';

import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { MyUtilsProvider } from '../../providers/my-utils/my-utils';
import { Config } from '../../MyUtils/Config' ;

import { AccountDetailPage } from '../account-detail/account-detail';

/**
* Generated class for the AccountsPage page.
*
* See http://ionicframework.com/docs/components/#navigation for more info
* on Ionic pages and navigation.
*/
//@IonicPage()
@Component({
  selector: 'page-accounts',
  templateUrl: 'accounts.html',
})
export class AccountsPage {

  accounts = [];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private loadingCtrl: LoadingController,
    private auth: AuthServiceProvider,
    private myUtils: MyUtilsProvider, public events: Events) {
    }

    ionViewDidLoad() {
      console.log('ionViewDidLoad AccountsPage');
      this.presentLoading();

      this.auth.getLDAPData()
      .then(
        data => {
          console.log(data)
          if(typeof data != 'undefined' && data != null){
            let refs = data.norib;
            this.auth.getUserAccess()
            .then(
              data => {
                let userid = data.userid;
                this.auth.getTokenData()
                .then(
                  data => {
                    let token = data.id;
                    refs.forEach((ref)=>{
                      this.myUtils.doGet(Config.getSoldeBancaireApi(userid ,ref, token))
                      .then((responce)=>{
                        console.log("responce getSoldeBancaireApi ", responce);
                        
                        try {
                          if(responce["_body"] && JSON.parse(responce["_body"]).soldeBancaire){
                            let balance = "";
                            if(JSON.parse(responce["_body"]).soldeBancaire)if(JSON.parse(responce["_body"]).soldeBancaire.solde)if(JSON.parse(responce["_body"]).soldeBancaire.solde.solde.disponibleNet) balance = JSON.parse(responce["_body"]).soldeBancaire.solde.solde.disponibleNet;
                            this.accounts.push({rib: ref, balance: balance, identite: JSON.parse(responce["_body"]).soldeBancaire.identite});
                          }
                        } catch (error) { }
                        //this.dismissLoading();
                      })
                      .catch((err)=>{
                        console.log("retun err::", err);
                        
                      });
                    });
                    this.dismissLoading();
                  }
                )
              }
            );
          }else this.dismissLoading();
        }
      );
    }

    formatedFLoat(str){
      let res =  String(parseFloat(str).toFixed(2)).replace('.', ',');
      if(res == 'NaN') res="";
      return res;
    }

    private loader;
    presentLoading() {
      this.loader = this.loadingCtrl.create({
        // content: "Chargement des données..."
        content: "Chargement des données...",
        showBackdrop: true,
        enableBackdropDismiss: true,
        dismissOnPageChange: true
      });

      this.loader.onDidDismiss(() => {
        console.log('Dismissed loading');
      });

      this.loader.present();
    }

    dismissLoading() {
      console.log('--Dismissed loading--');
      this.loader.dismiss();

    }

    accountSelected(account){
      this.navCtrl.setRoot(AccountDetailPage, {selectedAccount: account});
    }

    logout(){
      let loading = this.loadingCtrl.create({
        content: 'Traitement en cours...',
        dismissOnPageChange: true
      });
      loading.present();

      this.events.publish('user:doLogout', Date.now());
    }

  }
