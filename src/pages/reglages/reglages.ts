import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, ModalController, NavParams, LoadingController, AlertController, Events, reorderArray } from 'ionic-angular';

import { AuthServiceProvider } from '../../providers/auth-service/auth-service';


import { ForgotPasswordPage } from '../forgot-password/forgot-password';
import { ModifyPwdPage } from "../modify-pwd/modify-pwd";
import { EditionProfilPage } from "../edition-profil/edition-profil";

import { MyUtilsProvider } from "../../providers/my-utils/my-utils";

import { TranslationServiceProvider } from "../../providers/translation-service/translation-service";
import { OnInit } from '@angular/core';

import { Config } from "../../MyUtils/Config";


/**
 * Generated class for the ReglagesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
//@IonicPage()
@Component({
  selector: 'page-reglages',
  templateUrl: 'reglages.html',
})
export class ReglagesPage {

  currentTheme = '0';


  notification: boolean = true;

  private selectedTranslation: string;
  private availableLanguages: any[];
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private loadingCtrl: LoadingController,
    private auth: AuthServiceProvider,
    private myUtils: MyUtilsProvider,
    public modalCtrl: ModalController,
    private events: Events,
    private translationService: TranslationServiceProvider,
  ) {
    this.connectedUser = this.auth.currentUser;

    this.selectedTranslation = this.translationService.getCurrentLanguage();

    this.availableLanguages = JSON.parse(localStorage.getItem('availableLanguages'));

    this.myUtils.doGetPrefs('notification')
      .then((val) => {
        console.log('--> notif: ', (val != null) ? val : true);
        if (val != null) {
          this.notification = val;
        } else {
          this.notification = true;
        }
      });


  }

  updateNotifCongf() {
    this.myUtils.doSetPrefs("notification", this.notification);
  }
  //de
  connectedUser;
  logout() {
    let loading = this.loadingCtrl.create({
      content: 'Traitement en cours...',
      dismissOnPageChange: true
    });
    loading.present();

    this.events.publish('user:doLogout', Date.now());
  }

  ionViewDidLoad() {

    this.myUtils.doGetPrefs("themes")
      .then((themes) => {
        console.log("themes: ", themes);

        let jThemes: Array<any> = themes;
        if (jThemes) {
          jThemes.forEach((theme, idx) => {
            if (theme.isActive) {
              this.currentTheme = String(idx + 1)
              console.log("this.currentTheme ", this.currentTheme);

            }
          })
          //console.log("this.currentTheme ", this.currentTheme);
        } else {
          this.getThemes();
        }
      })


  }



  selectLanguage(e) {
    this.translationService.setCurrentLanguage(this.selectedTranslation);
    this.navCtrl.setRoot(this.navCtrl.getActive().component);

  }
  openModifyPWD() {
    this.navCtrl.push(ModifyPwdPage);
  }

  openEditProfil() {
    this.navCtrl.push(EditionProfilPage);

  }

  openReinitPwd() {
    this.navCtrl.push(ForgotPasswordPage);
  }

  getThemes() {
    this.myUtils.doGet("https://api.tgr.gov.ma/api/Themes?filter=%7B%22where%22%3A%7B%22systemDomainId%22%3A%22mobile%22%20%7D%20%7D")
      .then(result => {
        console.warn("api themes result", result);

        try {
          let themes = JSON.parse(result["_body"]);
          console.log("api>> themes: ", themes);
          if (Array.isArray(themes)) {
            this.myUtils.doSetPrefs("themes", themes);
            themes.forEach((theme, idx) => {
              if (theme.isActive) {
                this.currentTheme = String(idx + 1)
                console.log("api>> this.currentTheme ", this.currentTheme);

              }
            })
          }
        } catch (error) {
          console.log(error);

        }

      })
  }

  toNumber(str: string) {
    return Number(str)
  }

  openModifyProfil() {
    let modal = this.modalCtrl.create(ProfilPage);
    modal.present();
  }


}


import { UsersService } from '../../providers/users.service/users.service';

@Component({
  selector: 'page-profil',
  template: `
    <ion-header>
      <ion-toolbar color="infos">
        <ion-buttons end>
          <button ion-button (click)='dismiss()'  icon-only>
            <ion-icon name="close"></ion-icon>
          </button>
        </ion-buttons>
        <ion-title>Edition de profil</ion-title>
      </ion-toolbar>

    </ion-header>

    <ion-content class="modal-content" scroll="false" >
    
        <img class="bgModalImg" src="assets/img/bg_menu.jpg" style="
        position: absolute ;
            width: 100%;
            height: 100%;
            z-index: -10;">
    
            <ion-scroll scrollY="true" style="height: 100%;" padding>
            <ion-list *ngIf="connectedUser" class="themes-block"
            style="background-color: transparent !important; margin-bottom: 1rem !important;">
        
              <ion-item style="background-color: rgba(18, 119, 89, 0.68) !important; color: white; border-radius: 1rem 1rem 0 0;">
                <ion-label>Mes infos: </ion-label>
                <button ion-button color="light" clear icon-only item-right (click)="showEdit()">
                  <ion-icon name="build"></ion-icon>
                </button>
              </ion-item>
              <ion-item style="background-color:rgba(255, 255, 255, 0.72) !important;">
                <p>
                  <b>User name: </b>{{connectedUser.LDAPData.username}}</p>
                <p>
                  <b>Nom: </b>{{(connectedUser.LDAPData.lastName | uppercase)}}</p>
                <p>
                  <b>Prénom: </b>{{connectedUser.LDAPData.firstName}}</p>
                <p>
                  <b>Email: </b>{{connectedUser.LDAPData.email}}</p>
                <p>
                  <b>Rib: </b> {{(connectedUser.LDAPData.norib && connectedUser.LDAPData.norib.length>0) ? '' : 'NONE' }}</p>
                <div *ngIf="(connectedUser.LDAPData.norib && connectedUser.LDAPData.norib.length>0)">
                  <p *ngFor="let r of connectedUser.LDAPData.norib">{{r}}</p>
                </div>
              </ion-item>
            </ion-list>
            
            </ion-scroll>
    
        </ion-content>
  `,
})
export class ProfilPage {


  connectedUser;
  constructor(public viewCtrl: ViewController, public navCtrl: NavController,
    public navParams: NavParams,
    private myUtils: MyUtilsProvider,
    public modalCtrl: ModalController,
    private auth: AuthServiceProvider,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController
    //, private syncService: SyncDataProvider,
  ) {
    this.connectedUser = this.auth.currentUser; 
  }


  ionViewDidLoad() {

  }

  showEdit() {
    let modal = this.modalCtrl.create(EditionPage);
    modal.present();
  
  /*
    let prompt = this.alertCtrl.create({
      title: 'Edition de profil',
      message: "Les champs que vous ne souhaitez pas modifier, laissez-les vides.",
      inputs: [
        {
          name: "lastName",
          label: "Le nom",
          placeholder: "Votre nom: "+this.connectedUser.LDAPData.lastName
        },
        {
          name: "firstName",
          label: "Le prénom",
          placeholder: "Votre prénom: "+this.connectedUser.LDAPData.firstName
        },
        {
          name: "email",
          label: "L'email",
          placeholder: "Votre email: "+this.connectedUser.LDAPData.email
        },
      ],
      buttons: [
        {
          text: 'Annuler',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Enregistrer',
          handler: data => {
            this.presentLoading()
            console.log('Saved clicked', data);
            this.myUtils.doGet(Config.getEditionProfiAlpi(this.connectedUser.LDAPData.username, data.lastName, data.firstName, data.email, this.connectedUser.tokenData.id))
            .then(data=>{
              this.dismissLoading()
              console.warn(data)

              try {

                let userUpdated = JSON.parse(data["_body"]).userUpdated
                this.auth.currentUser.LDAPData = userUpdated
                this.connectedUser = this.auth.currentUser; 

              } catch (error) {
                console.error(error)
                this.showAlert("Service indisponible");

              }

            })
            .catch(err=>{
              this.dismissLoading()
              console.error(err)
              this.showAlert("Service indisponible");

            })
          }
        }
      ]
    });
    prompt.present();
    */
  }

  private loader;
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Chargement des données...",
      showBackdrop: true,
      enableBackdropDismiss: true
    });
    /*
    this.loader.onDidDismiss(() => {
      console.log('Dismissed loading');
    });*/

    this.dismissed = false;
    this.loader.present();
  }

  dismissed = false;
  dismissLoading() {
    console.log('--Dismissed loading--');
    if (!this.dismissed) this.loader.dismiss();
    this.dismissed = true;

  }

  public showAlert(message) {
    let alert = this.alertCtrl.create({
      title: 'TGR Mobile',
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }//END Alert

  dismiss() {
    this.viewCtrl.dismiss();
  }

}


@Component({
  selector: 'edition-profil',
  template: `
    <ion-header>
      <ion-toolbar color="infos">
        <ion-buttons end>
          <button ion-button (click)='dismiss()' icon-only>
            <ion-icon name="close"></ion-icon>
          </button>
        </ion-buttons>
        <ion-title>Edition de profil</ion-title>
      </ion-toolbar>

    </ion-header>

    <ion-content class="modal-content edition" scroll="false" padding>
    
            <ion-list *ngIf="connectedUser" class="themes-block" style="background-color: transparent !important; margin-bottom: 1rem !important;">

              <form (ngSubmit)="editeForm()">
                <ion-item>
                  <ion-label>Nom: </ion-label>
                  <ion-input type="text" [(ngModel)]="data.lastName" placeholder="{{connectedUser.LDAPData.lastName}}" name="lastName"></ion-input>
                </ion-item>
                <ion-item>
                  <ion-label>Prénom: </ion-label>
                  <ion-input type="text" [(ngModel)]="data.firstName" placeholder="{{connectedUser.LDAPData.firstName}}" name="firstName"></ion-input>
                </ion-item>

                <ion-list *ngIf="connectedUser.LDAPData.codecontribuable && connectedUser.LDAPData.codecontribuable.length>0">
                  <ion-item style="margin-top: 1rem;"><ion-label >Code contribuable: </ion-label></ion-item>
                  <ion-item *ngFor="let item of connectedUser.LDAPData.codecontribuable">
                    <label>{{ item }}</label>
                  </ion-item>
                </ion-list>

                <button
                 style="height: 2.25em !important;" ion-button outline block icon-left class="submit-btn" 
                 center color="adhesion" (click)="addCodeContribuable()">
                  <ion-icon name="add-circle"></ion-icon>
                  <label>Ajouter un autre code contribuable</label> 
                </button>

                <ion-list>
                  <ion-item *ngFor="let item of newCodesContrib">
                    <label> <strong>code contribuable :</strong> {{ item }}</label>
                  </ion-item>
                </ion-list>

                <ion-item>
                  <ion-label stacked >Nouveau Code contribuable</ion-label>
                  <ion-input type="text"  placeholder="ex: 123456789|1234567" name="reference_avis" [(ngModel)]="data.reference_avis"></ion-input>
                </ion-item>
                <ion-item>
                  <ion-label stacked >Montant d'avis</ion-label>
                  <ion-input type="number"  placeholder="ex: 334.4" name="montant_avis" [(ngModel)]="data.montant_avis"></ion-input>
                </ion-item>
                <button ion-button type="submit" block>Enregistrer</button>
              </form>
              
            </ion-list>
    
        </ion-content>
  `,
  styles : [
    `
    ion-content.edition{
      background: url(../assets/img/bg_menu.jpg) !important;
      background-size: cover !important;
      background-repeat: no-repeat !important;
    }
    `
  ]
})
export class EditionPage {


  connectedUser;
  constructor(public viewCtrl: ViewController, public navCtrl: NavController,
    public navParams: NavParams,
    private myUtils: MyUtilsProvider,
    public modalCtrl: ModalController,
    private auth: AuthServiceProvider,
    private alertCtrl: AlertController,
    private userService: UsersService,
    private loadingCtrl: LoadingController
    //, private syncService: SyncDataProvider,
  ) {
    this.connectedUser = this.auth.currentUser; 
    
  }


  data = {lastName: "", firstName: ""};

  newCodesContrib = [];

  ionViewDidLoad() {

  }

  addCodeContribuable(){
    let inp = this.data['reference_avis'];
    let ref = (this.data['reference_avis'] as string).split("|")[0]
    let code = (this.data['reference_avis'] as string).split("|")[1]
    let montant_avis = this.data['montant_avis'];

    this.verifierContribuable(ref, montant_avis, ()=>{
      console.log("verifierContribuable finish");
      
    })

  }


  verifierContribuable(reference_avis, montant_avis, callback) {
    if (montant_avis.trim() != "" && reference_avis.trim() != "") {
      this.presentLoading();
      console.log('verifierContribuable begin');
      this.userService.calculateCodeContribuable(reference_avis, montant_avis).subscribe(
        data => {
          try {
            let foundContribuable = data.json();
            console.log('\n foundContribuable .. calculateCodeContribuable!!! ', JSON.stringify(data.json()));

            // if (foundContribuable.result as boolean == true) {
            //   console.log('\n calculateCodeContribuable.result !!! ', foundContribuable.result);
            //   //TODO: 

            // // userModel.codeContribuables = [this.codeContribuable];

            //-- verify if the entred code contrib is allready added
            if (!this.newCodesContrib.some(x => x == String(foundContribuable))) {
              this.newCodesContrib.push(String(foundContribuable));
            }

            console.log("userModel verifierContribuable", this.newCodesContrib);
            console.log("foundContribuable verifierContribuable", foundContribuable);

            callback();
          } catch (err) {
            this.showAlert("Service de verification du contribuable indisponible .. !");
          }
          //   // return true;
          // } else {

          //   this.showAlert("", "CodeContribuable n'existe pas ..!");
          // }

          // callback();
          // this.dismissLoading();
        },
        err => {
          this.dismissLoading();
          // errr(err);
          console.error(err);
          // if (err.status == 500) {
          //   this.showAlert(" ", "500 - Service Contribuable indisponible veuillez réessayer ultérieurement");
          // } else
          // if (err.status == 404) {
          //   this.showAlert(" ", "404 - Service Contribuable indisponible veuillez réessayer ultérieurement");
          // } else {
          //   this.showAlert(" ", "Service Contribuable indisponible veuillez réessayer ultérieurement");
          // }
          this.showAlert("Merci de vérifier les informations saisies !");
          
          //show a nice error message to the user

        }

      );//subscrib
      return true;
    } else {
      this.showAlert("Merci de vérifier les informations saisies !");
    }//END IF check data
  }

  editeForm(){
   

    this.presentLoading()
            console.log('Saved clicked', this.data);
            this.myUtils.doPost(Config.getEditionProfiAlpi(this.connectedUser.LDAPData.username, this.connectedUser.tokenData.id), 
            {
              "lastName": this.data['lastName'],
              "firstName": this.data['firstName'],
              "codecontribuable": this.newCodesContrib
          }
          )
            .then(data=>{
              this.dismissLoading()
              console.warn(data)

              try {

                let userUpdated = JSON.parse(data["_body"]).userUpdated
                this.auth.currentUser.LDAPData = userUpdated
                this.connectedUser = this.auth.currentUser; 

              } catch (error) {
                console.error(error)
                this.showAlert("Service indisponible");

              }

            })
            .catch(err=>{
              this.dismissLoading()
              console.error(err)
              this.showAlert("Service indisponible");

            })

  }

  showPrompt() {
    let prompt = this.alertCtrl.create({
      title: 'Edition de profil',
      message: "Les champs que vous ne souhaitez pas modifier, laissez-les vides.",
      inputs: [
        /*{
          name: "username",
          label: "Identifiant d'utilisateur",
          placeholder: "Votre Identifiant: "+this.connectedUser.LDAPData.username
        },*/
        {
          name: "lastName",
          label: "Le nom",
          placeholder: "Votre nom: "+this.connectedUser.LDAPData.lastName
        },
        {
          name: "firstName",
          label: "Le prénom",
          placeholder: "Votre prénom: "+this.connectedUser.LDAPData.firstName
        },
        {
          name: "email",
          label: "L'email",
          placeholder: "Votre email: "+this.connectedUser.LDAPData.email
        },
      ],
      buttons: [
        {
          text: 'Annuler',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Enregistrer',
          handler: data => {
            
          }
        }
      ]
    });
    prompt.present();
  }

  private loader;
  presentLoading() { 
    this.loader = this.loadingCtrl.create({
      content: "Chargement des données...",
      showBackdrop: true,
      enableBackdropDismiss: true
    });
    /*
    this.loader.onDidDismiss(() => {
      console.log('Dismissed loading');
    });*/

    this.dismissed = false;
    this.loader.present();
  }

  dismissed = false;
  dismissLoading() {
    console.log('--Dismissed loading--');
    if (!this.dismissed) this.loader.dismiss();
    this.dismissed = true;

  }

  public showAlert(message) {
    let alert = this.alertCtrl.create({
      title: 'TGR Mobile',
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }//END Alert

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
