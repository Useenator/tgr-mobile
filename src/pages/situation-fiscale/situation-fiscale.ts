import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ContribuableService } from '../../providers/contribuable-service/contribuable-service';
import { AlertController, Events } from 'ionic-angular';

import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { MyUtilsProvider } from '../../providers/my-utils/my-utils';
import { Config } from '../../MyUtils/Config';
import { Http } from '@angular/http';

import { ContribuablePage } from '../contribuable/contribuable';
//loader
import { LoadingController } from 'ionic-angular';


import { ArticlesNonPayesPage } from '../articles-non-payes/articles-non-payes';
import { AvisImpositionNonRecuPage } from '../avis-imposition-non-recu/avis-imposition-non-recu';


/*
   class for the Contribuable page.
*/

import { ContribuableObserverProvider } from '../../providers/contribuable-observer/contribuable-observer';



@Component({
  selector: 'page-situation-fiscale',
  templateUrl: 'situation-fiscale.html'
})
export class SituationFiscalePage {

  /////////////////////////// these parametres will from the session /////////////////////////////
  private token = '0rFRTPrdTMoyrPpqyFxE2BzX8FdEFLEy8nTAzZJsKbAiJ7bFsnG2c08w6mBX1DQX';
  private userId = 'CompteDemo';
  private codeContribuables = ["7350145", "4000", "7350145", "7350145"];// : Array<string> ;
  ///////////////////////////////////////////////////////////////////////////////////////////////

  ///////////////////////////END these parametres will from the session //////////////////////////

  private foundItems;//={"contribuables":["7350145"]};
  private foundItemsArticleNonSolder: ArticleNonSolderRoot = {
    consulterArticlesNonSoldes: {
      adresse: "",
      annees: [

      ],
      articles: [

      ],
      codepostes: [

      ],
      ids: [
      ],
      libelle: "",
      libellepostes: [
      ],
      libelles: [
      ],
      montantsfraisrecouvrement: [
      ],
      montantsmajoration: [
      ],
      montantsprincipal: [
      ],
      montantstotalligne: [
      ],
      natures: [
      ],
      nom: "",
      secteurs: [
      ]
    }
  };//ArticlesNonSoldes
  private foundItemsArticleNonSolder_offligne: ArticleNonSolderRoot = {
    consulterArticlesNonSoldes: {
      adresse: "BD MED V B MELLAL",
      annees: [
        "1997/1997",
        "1998/1998",
        "1998/1998",
        "1997/1997",
        "1997/1997",
        "2015/2015",
        "2017/2017",
        "2017/2017",
        "2014/2014",
        "2000/2000",
        "2000/2000",
        "1999/1999",
        "1998/1998",
        "1999/1999",
        "1999/1999",
        "2001/2001",
        "2001/2001",
        "2000/2000",
        "2013/2013",
        "2013/2014",
        "2014/2014",
        "2015/2015",
        "2016/2016",
        "2014/2014",
        "2015/2015",
        "2016/2016",
        "2012/2015",
        "2013/2013",
        "2013/2013",
        "2010/2011",
        "2011/2011",
        "2012/2012",
        "2014/2014",
        "2014/2014",
        "2015/2015",
        "2016/2016",
        "2003/2003",
        "2002/2002",
        "2002/2002",
        "2001/2001",
        "2002/2002",
        "2003/2003",
        "2003/2003",
        "2003/2005",
        "2003/2005",
        "2005/2005",
        "2004/2004",
        "2005/2008",
        "2004/2004",
        "2004/2004",
        "2006/2006",
        "2005/2005",
        "2005/2005",
        "2006/2006",
        "2008/2008",
        "2007/2007",
        "2006/2006",
        "2007/2007",
        "2007/2007",
        "2009/2009",
        "2010/2011",
        "2009/2009",
        "2008/2008",
        "2011/2013",
        "2011/2013",
        "2010/2010",
        "2011/2011",
        "2010/2010",
        "2012/2012",
        "2012/2012",
        "2011/2011",
        "2012/2012"
      ],
      articles: [
        "50560",
        "50560",
        "13090",
        "50560",
        "13090",
        "74388",
        "50560",
        "74388",
        "74388",
        "50560",
        "50560",
        "13090",
        "50560",
        "50560",
        "50560",
        "50560",
        "50560",
        "13090",
        "13090",
        "18304",
        "15948",
        "15948",
        "15948",
        "13090",
        "13090",
        "13090",
        "18304",
        "50560",
        "74388",
        "15948",
        "15948",
        "15948",
        "50560",
        "74388",
        "50560",
        "50560",
        "13090",
        "50560",
        "50560",
        "13090",
        "13090",
        "50560",
        "50560",
        "50560",
        "50560",
        "13090",
        "50560",
        "50560",
        "13090",
        "50560",
        "13090",
        "50560",
        "50560",
        "50560",
        "50560",
        "13090",
        "50560",
        "50560",
        "50560",
        "13090",
        "18304",
        "50560",
        "13090",
        "18304",
        "74388",
        "13090",
        "13090",
        "50560",
        "74388",
        "13090",
        "50560",
        "50560"
      ],
      codepostes: [
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111",
        "1111"
      ],
      ids: [
        "12818781",
        "12648801",
        "12387726",
        "12531086",
        "12263641",
        "94429159",
        "112342837",
        "112347439",
        "70751251",
        "12766905",
        "12441304",
        "12347318",
        "12365129",
        "12718699",
        "12593303",
        "12407862",
        "12600081",
        "12690020",
        "16907710",
        "56862485",
        "74913364",
        "96823926",
        "106088440",
        "74907871",
        "96817562",
        "106082210",
        "98408104",
        "16557873",
        "16565380",
        "12838001",
        "12625421",
        "12625151",
        "70743822",
        "70751252",
        "94425085",
        "102568138",
        "12391001",
        "12285820",
        "12247212",
        "12737655",
        "12740322",
        "12695820",
        "12517754",
        "12531550",
        "12599490",
        "12742167",
        "12248027",
        "12412828",
        "12791731",
        "12308156",
        "12683960",
        "12775628",
        "12828380",
        "12460995",
        "12542725",
        "12347916",
        "12829159",
        "12409714",
        "12556344",
        "12730839",
        "12786336",
        "12672639",
        "12794049",
        "12570909",
        "12576356",
        "12783604",
        "12495262",
        "12380288",
        "12501054",
        "12682560",
        "12501616",
        "12642219"
      ],
      libelle: "PERCEPTION BNI MELLAL - AM",
      libellepostes: [
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM"
      ],
      libelles: [
        "TAXE EDILITE AVANT 10/2000 ",
        "TAXE URBAINE AVANT 10/2000 ",
        "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
        "TAXE URBAINE AVANT 10/2000 ",
        "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
        "TAXE DE SERVICES COMMUNAUX ",
        "TAXE DE SERVICES COMMUNAUX ",
        "TAXE DE SERVICES COMMUNAUX ",
        "TAXE HABITATION ",
        "TAXE EDILITE AVANT 10/2000 ",
        "TAXE URBAINE AVANT 10/2000 ",
        "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
        "TAXE EDILITE AVANT 10/2000 ",
        "TAXE EDILITE AVANT 10/2000 ",
        "TAXE URBAINE AVANT 10/2000 ",
        "TAXE URBAINE APRES 10/2000 ",
        "TAXE EDILITE APRES 10/2000 ",
        "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
        "TAXE PROFESSIONNELLE ",
        "TAXE PROFESSIONNELLE ",
        "TAXE PROFESSIONNELLE ",
        "TAXE PROFESSIONNELLE ",
        "TAXE PROFESSIONNELLE ",
        "TAXE PROFESSIONNELLE ",
        "TAXE PROFESSIONNELLE ",
        "TAXE PROFESSIONNELLE ",
        "TAXE PROFESSIONNELLE ",
        "TAXE DE SERVICES COMMUNAUX ",
        "TAXE DE SERVICES COMMUNAUX ",
        "TAXE PROFESSIONNELLE ",
        "TAXE PROFESSIONNELLE ",
        "TAXE PROFESSIONNELLE ",
        "TAXE DE SERVICES COMMUNAUX ",
        "TAXE DE SERVICES COMMUNAUX ",
        "TAXE DE SERVICES COMMUNAUX ",
        "TAXE DE SERVICES COMMUNAUX ",
        "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
        "TAXE EDILITE APRES 10/2000 ",
        "TAXE URBAINE APRES 10/2000 ",
        "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
        "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
        "TAXE URBAINE APRES 10/2000 ",
        "TAXE EDILITE APRES 10/2000 ",
        "TAXE URBAINE APRES 10/2000 ",
        "TAXE EDILITE APRES 10/2000 ",
        "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
        "TAXE URBAINE APRES 10/2000 ",
        "TAXE URBAINE APRES 10/2000 ",
        "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
        "TAXE EDILITE APRES 10/2000 ",
        "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
        "TAXE EDILITE APRES 10/2000 ",
        "TAXE URBAINE APRES 10/2000 ",
        "TAXE URBAINE APRES 10/2000 ",
        "TAXE DE SERVICES COMMUNAUX ",
        "PATENTE EMISE APRES 1996 ET IMPOSE APRES 1996 ",
        "TAXE EDILITE APRES 10/2000 ",
        "TAXE URBAINE APRES 10/2000 ",
        "TAXE EDILITE APRES 10/2000 ",
        "TAXE PROFESSIONNELLE ",
        "TAXE PROFESSIONNELLE ",
        "TAXE DE SERVICES COMMUNAUX ",
        "TAXE PROFESSIONNELLE ",
        "TAXE PROFESSIONNELLE ",
        "TAXE DE SERVICES COMMUNAUX ",
        "TAXE PROFESSIONNELLE ",
        "TAXE PROFESSIONNELLE ",
        "TAXE DE SERVICES COMMUNAUX ",
        "TAXE DE SERVICES COMMUNAUX ",
        "TAXE PROFESSIONNELLE ",
        "TAXE DE SERVICES COMMUNAUX ",
        "TAXE DE SERVICES COMMUNAUX "
      ],
      montantsfraisrecouvrement: [
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0"
      ],
      montantsmajoration: [
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "6615.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "6615.0",
        "0.0",
        "0.0",
        "1842.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0",
        "0.0"
      ],
      montantsprincipal: [
        "4230.0",
        "5076.0",
        "7905.6",
        "4230.0",
        "6588.0",
        "42226.6",
        "10449.3",
        "888.7",
        "888.79",
        "5184.0",
        "5184.0",
        "7905.6",
        "5076.0",
        "5184.0",
        "5184.0",
        "5184.0",
        "5184.0",
        "7905.6",
        "38754.0",
        "41337.9",
        "11880.0",
        "11880.0",
        "11880.0",
        "38754.0",
        "38754.0",
        "38754.0",
        "42226.6",
        "10449.3",
        "888.7",
        "11880.0",
        "11880.0",
        "11880.0",
        "10449.3",
        "888.79",
        "10449.3",
        "10449.3",
        "21960.0",
        "5184.0",
        "5184.0",
        "7905.6",
        "7905.6",
        "5184.0",
        "5184.0",
        "12906.0",
        "12906.0",
        "21960.0",
        "5184.0",
        "12906.0",
        "21960.0",
        "5184.0",
        "21960.0",
        "5184.0",
        "5184.0",
        "18090.0",
        "10449.3",
        "21960.0",
        "18090.0",
        "18090.0",
        "18090.0",
        "38754.0",
        "41337.9",
        "10449.3",
        "38754.0",
        "41337.9",
        "888.7",
        "38754.0",
        "38754.0",
        "10449.3",
        "888.7",
        "38754.0",
        "10449.3",
        "10449.3"
      ],
      montantstotalligne: [
        "4230.0",
        "5076.0",
        "7905.6",
        "4230.0",
        "6588.0",
        "48841.6",
        "10449.3",
        "888.7",
        "888.79",
        "5184.0",
        "5184.0",
        "7905.6",
        "5076.0",
        "5184.0",
        "5184.0",
        "5184.0",
        "5184.0",
        "7905.6",
        "38754.0",
        "41337.9",
        "11880.0",
        "11880.0",
        "11880.0",
        "38754.0",
        "38754.0",
        "38754.0",
        "48841.6",
        "10449.3",
        "888.7",
        "13722.0",
        "11880.0",
        "11880.0",
        "10449.3",
        "888.79",
        "10449.3",
        "10449.3",
        "21960.0",
        "5184.0",
        "5184.0",
        "7905.6",
        "7905.6",
        "5184.0",
        "5184.0",
        "12906.0",
        "12906.0",
        "21960.0",
        "5184.0",
        "12906.0",
        "21960.0",
        "5184.0",
        "21960.0",
        "5184.0",
        "5184.0",
        "18090.0",
        "10449.3",
        "21960.0",
        "18090.0",
        "18090.0",
        "18090.0",
        "38754.0",
        "41337.9",
        "10449.3",
        "38754.0",
        "41337.9",
        "888.7",
        "38754.0",
        "38754.0",
        "10449.3",
        "888.7",
        "38754.0",
        "10449.3",
        "10449.3"
      ],
      natures: [
        "TE av2000",
        "TU av2000",
        "PAT",
        "TU av2000",
        "PAT",
        "TSC",
        "TSC",
        "TSC",
        "TH",
        "TE av2000",
        "TU av2000",
        "PAT",
        "TE av2000",
        "TE av2000",
        "TU av2000",
        "TU",
        "TE ap2000",
        "PAT",
        "TP",
        "TP",
        "TP",
        "TP",
        "TP",
        "TP",
        "TP",
        "TP",
        "TP",
        "TSC",
        "TSC",
        "TP",
        "TP",
        "TP",
        "TSC",
        "TSC",
        "TSC",
        "TSC",
        "PAT",
        "TE ap2000",
        "TU",
        "PAT",
        "PAT",
        "TU",
        "TE ap2000",
        "TU",
        "TE ap2000",
        "PAT",
        "TU",
        "TU",
        "PAT",
        "TE ap2000",
        "PAT",
        "TE ap2000",
        "TU",
        "TU",
        "TSC",
        "PAT",
        "TE ap2000",
        "TU",
        "TE ap2000",
        "TP",
        "TP",
        "TSC",
        "TP",
        "TP",
        "TSC",
        "TP",
        "TP",
        "TSC",
        "TSC",
        "TP",
        "TSC",
        "TSC"
      ],
      nom: "CREDIT DU MAROC",
      secteurs: [
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "419",
        "419",
        "419",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "419",
        "419",
        "419",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413",
        "413"
      ]
    }
  };
  private foundItemsArticleNon_Solder = {
    consulterArticlesNonSoldes: {
      adresse: "ND 20 AOUT LOT KHALIFA O HAMDA",
      annees: [
        "2014/2014",
        "2016/2016"
      ],
      articles: [
        "50560",
        "74388"
      ],
      codepostes: [
        "1111",
        "1111"
      ],
      ids: [
        "70743821",
        "102572516"
      ],
      libelle: "PERCEPTION BNI MELLAL - AM",
      libellepostes: [
        "PERCEPTION BNI MELLAL - AM",
        "PERCEPTION BNI MELLAL - AM"
      ],
      libelles: [
        "TAXE HABITATION ",
        "TAXE DE SERVICES COMMUNAUX "
      ],
      montantsfraisrecouvrement: [
        "0.0",
        "0.0"
      ],
      montantsmajoration: [
        "0.0",
        "0.0"
      ],
      montantsprincipal: [
        "0.02",
        "888.7"
      ],
      montantstotalligne: [
        "0.02",
        "888.7"
      ],
      natures: [
        "TH",
        "TSC"
      ],
      nom: "CREDIT DU MAROC",
      secteurs: [
        "413",
        "413"
      ]
    }
  };
  private selectedCodeContribuable = null;
  private selectedItems: string[] = [];
  private selectedItemsToPay: string[] = [];
  private myUtils;
  private loader;
  private checkedItems: boolean[];
  //TODO delete situation fiscal detail
  private itemDetails = {
    adresse: "ND 20 AOUT LOT KHALIFA O HAMDA",
    annees: [
      ""
    ],
    articles: [

      ""
    ],
    codepostes: [

      ""
    ],
    ids: [

      ""
    ],
    libelle: "",
    libellepostes: [
      ""
    ],
    libelles: [

      ""
    ],
    montantsfraisrecouvrement: [

      "0.0"
    ],
    montantsmajoration: [

      "0.0"
    ],
    montantsprincipal: [

      "0.0"
    ],
    montantstotalligne: [

      "0.0"
    ],
    natures: [

      ""
    ],
    nom: "",
    secteurs: [

      "0"
    ]
  };
  private index = null;

  childsShowHeader = false;

  constructor(public navCtrl: NavController,
    public events: Events,
    // public contribuableService: ContribuableService,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loading: LoadingController,
    private nav: NavController,
    public http: Http,
    public auth: AuthServiceProvider,
    public contribuableobserver: ContribuableObserverProvider,
  ) {
    this.connectedUser = this.auth.currentUser;



    ////////////////////// or whereever `items` is initialized ///////////////////////
    this.selectedItems = this.foundItemsArticleNonSolder.consulterArticlesNonSoldes.ids;

  }

  connectedUser;
  logout() {
    let loading = this.loading.create({
      content: 'Traitement en cours...',
      dismissOnPageChange: true
    });
    loading.present();

    this.events.publish('user:doLogout', Date.now());
  }

  selectedTab(arg) {
    console.log(arg);
  }

  hasAccess = false;

  ionViewDidLoad() {

    console.log("CONNECTED_USER", this.connectedUser);

    if (this.connectedUser) {
      if (this.connectedUser['roleData']) {
        console.warn("USER ROLES", this.connectedUser['roleData'])
        this.presentLoading();
        this.connectedUser['roleData'].forEach(role => {
          console.log('role: ', role);
          if (role.id == 'tsDossierFiscalContrib' || 
          role.id == 'grpContribMobile ,grpContribMobile' || 
          role.id == 'tsClientBqConsultSolde' || 
          role.id == 'tsClientBqEditionRelevesParDate') {
            this.hasAccess = true;
            

            //TODO: Delete //"codeContribuables": ["16624667"]
            // if (!this.connectedUser.LDAPData.codecontribuable) this.connectedUser.LDAPData.codecontribuable = [];
            // this.connectedUser.LDAPData.codecontribuable.push("16624667");
            // this.connectedUser.LDAPData.codecontribuable.push("00000000");
            //END TODO: Delete //"codeContribuables": ["16624667"]

            if (this.connectedUser.LDAPData && this.connectedUser.LDAPData.codecontribuable) {
              // Split code_contribuable array
              this.codeContribuables = this.splitAndSetCodeContribualArray(this.connectedUser.LDAPData.codecontribuable);
              console.log("LDAPData.codecontribuable", this.connectedUser.LDAPData.codecontribuable);
              //END split code_contribuable array
              this.userId = this.connectedUser["userAccess"].userid;
              this.token = this.connectedUser["tokenData"].id;

            }
            
          }
        });
        this.dismissLoading();
      }
    }
    console.log('ionViewDidLoad SituationFiscaleArticlesNonSoldesPage');


    ///////////////////////////////////////////End loader
    setTimeout(() => {
      this.contribuableobserver.observer.next({ key: "showChildHeader", data: false });
    }, 2000)



  }//END ionViewDidLoad /////////////////////////////////////////////////////////////////////////////////////////////////
  /**
   * Split code_contribuable array by "|" and return a new array with the second value of the first one.
   * @param codeContribuables
   */
  splitAndSetCodeContribualArray(codeContribuables: Array<any>): Array<any> {
    // split code_contribuable array
    var newCodeContribuable = [];
    console.warn('codeContribuables', codeContribuables)
    codeContribuables.forEach((code, idx) => {
      //['60000323|7577849'] we need just the second element of the array.
      // var tmpCode = { code: code.split("|")[1], selected: idx == 0 };
      /////////////////////////// in case codeContribuable doesn't contain "|" //////////////////////////////////////
      var tmpCode;
      if (code.indexOf("|") > -1) {
        tmpCode = { code: code.split("|")[1], label: code.split("|")[0], selected: idx == 0 };
      } else {
        tmpCode = { code: code, label: code, selected: idx == 0 };
      }
      ////////////////////////////////////////////////////////////////////////
      if (idx == 0) this.selectedCodeContribuable = tmpCode.code;
      newCodeContribuable.push(tmpCode);
    })
    console.log("newCodeContribuable", newCodeContribuable);
    return newCodeContribuable;
  }
  /**
   *
   */
  loadAccount() {
    this.contribuableobserver.observer.next({ key: "selectedCode", data: { userId: this.userId, token: this.token, selectedCodeContribuable: this.selectedCodeContribuable } });

    this.selectedItemsToPay = [];

    // this.foundItemsArticleNonSolder =

  }

  /**
    * Go to article non payes page
   */
  goToArticleNonPayes() {
    if (this.selectedCodeContribuable != null) {
      this.getArticlesNonSolderByCodeContribuable(this.userId, this.token, this.selectedCodeContribuable);
    } else {
      this.showAlert("", "Veuillez sélectionner un compte.");
    }
  }

  /**
   *Go to article payes page.
   */
  goToArticlePayes() {
    if (this.selectedCodeContribuable != null) {
      this.getArticleSoldeByCodeContribuable(this.userId, this.token, this.selectedCodeContribuable);
    } else {
      this.showAlert("", "veuillez sélectionner un compte.");
    }
  }


  /**
   * liste des codecontribuable d'un utilisateur {id}
   * https://api.tgr.gov.ma/api/AppUsers/{id}/contribuables?access_token={access_tocken}
   */
  getCodeContribuableByUserId(userId, token) {
    //   this.contribuableService.getCodeContribuableByUserId(userId, token).subscribe(
    //     data => {
    //       //  this.staticCodeContribuables = data.json;
    //       //this.staticCodeContribuables = data.json;
    //       this.foundItems = data.json;
    //       console.log('\n foundItems .. Code contribuable!!! ' + JSON.stringify(data.json()) + '\n\n');
    //       console.log('\n foundItems .. Code contribuable!!! ' + this.codeContribuables + '\n\n');

    //       this.dismissLoading();
    //       // //Call Article Solder et non Solder for each codeContribuable
    //       // for (let item of this.staticCodeContribuable) {
    //       //   console.log('\n item \n' + JSON.stringify(item));
    //       //   //  console.log(this.staticCodeContribuable[i])
    //       //   //Call Article Solder et non Solder
    //       //   this.getArticleSoldeByCodeContribuable(this.staticUserId, this.staticToken, item);
    //       //   this.getArticlesNonSolderByCodeContribuable(this.staticUserId, this.staticToken, item);
    //       // }//For
    //       //this.loadAccount();
    //     },
    //     err => {
    //       console.error(err);
    //       // this.showAlert('Ooops', 'Oooops !!, We can not connect to the server "AgencyByRib" !!');
    //     },
    //     () => {
    //       console.log('getCodeContribuableByUserId completed');
    //     }
    //   );
    //   return this.codeContribuables;
  }//getCodeContribuableByUserId


  /**
   * liste des articles soldés
   * ex: https://api.tgr.gov.ma/api/Contribuables/getResultS/9758663
   */
  getArticleSoldeByCodeContribuable(userId, token, code_contribuable) {
    this.nav.push(ContribuablePage, {
      page: 'article-soldes',
      title: 'Articles soldés',
      userId: userId,
      code_contribuable: code_contribuable,
      token: token,
      email: this.connectedUser.LDAPData.email
    });
  }//getArticleSoldeByCodeContribuable

  /**
   * liste des articles non soldés
   * ex: https://api.tgr.gov.ma/api/Contribuables/getResultNS/9758663
   */
  getArticlesNonSolderByCodeContribuable(userId, token, code_contribuable) {
    this.nav.push(ContribuablePage, {
      page: 'article-non-soldes',
      title: 'Articles non soldés',
      userId: userId,
      code_contribuable: code_contribuable,
      token: token,
      email: this.connectedUser.LDAPData.email
    });
  }//getArticleNonSoldeByCodeContribuable

  ///////////////////////////////////// Alert box ////////////////////////////////
  /**
   * reuseable Alert box.
   */
  showAlert(title, message) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }//END Alert

  ///////////////////////////////////// loader; ////////////////////////////////
  /**
   * Show loading
   */
  presentLoading() {
    this.loader = this.loading.create({
      content: "Chargement des données..."
    });

    this.loader.onDidDismiss(() => {
      console.log('Dismissed loading');
    });

    this.loader.present();

    setTimeout(() => {
      this.loader.dismiss();
    }, 5000);
  }

  openNonPaye() {
    this.navCtrl.push(ArticlesNonPayesPage);
  }

  openNonRecu() {
    this.navCtrl.push(AvisImpositionNonRecuPage);
  }

  /**
   * Dismiss loading
   */
  dismissLoading() {
    console.log('--Dismissed loading--');
    if (!this.loader.Dismissed) this.loader.dismiss();

  }//////////////////////// END loader ///////////////////////////////////////
}


////////////////////////////////////////////////
export interface ConsulterArticlesNonSoldes {
  adresse: string;
  annees: string[];
  articles: string[];
  codepostes: string[];
  ids: string[];
  libelle: string;
  libellepostes: string[];
  libelles: string[];
  montantsfraisrecouvrement: string[];
  montantsmajoration: string[];
  montantsprincipal: string[];
  montantstotalligne: string[];
  natures: string[];
  nom: string;
  secteurs: string[];
}
export interface ArticleNonSolderRoot {
  consulterArticlesNonSoldes: ConsulterArticlesNonSoldes;
}
