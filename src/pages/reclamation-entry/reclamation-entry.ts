import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events } from 'ionic-angular';

import { MyUtilsProvider } from '../../providers/my-utils/my-utils';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

import { ReclamationServiceProvider } from '../../providers/reclamation-service/reclamation-service';

import { ReclamationPage } from "../reclamation/reclamation";
import { SuiviReclamationsPage } from "../suivi-reclamations/suivi-reclamations";

/**
 * Generated class for the ReclamationEntryPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
//@IonicPage()
@Component({
  selector: 'page-reclamation-entry',
  templateUrl: 'reclamation-entry.html',
})
export class ReclamationEntryPage {
  lastName = "";
  firstName = "";
  email = "";

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private loadingCtrl: LoadingController,
    private auth: AuthServiceProvider,
    public events: Events,
    private reclamationService: ReclamationServiceProvider, ) {

    this.connectedUser = this.auth.currentUser;
  }

  connectedUser;
  logout() {
    let loading = this.loadingCtrl.create({
      content: 'Traitement en cours...',
      dismissOnPageChange: true
    });
    loading.present();

    this.events.publish('user:doLogout', Date.now());
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReclamationEntryPage');

    let user = this.auth.currentUser;
    if (user) {
      this.lastName = user.LDAPData.lastName;
      this.firstName = user.LDAPData.firstName;
      this.email = user.LDAPData.email;
    }

    this.getTypeReclamants();

  }

  private loader;
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Chargement des données..."
    });

    this.loader.onDidDismiss(() => {
      console.log('Dismissed loading');
    });

    this.loader.present();
  }

  dismissLoading() {
    console.log('--Dismissed loading--');
    if (!this.loader.Dismissed) this.loader.dismiss();

  }

  public foundReclamations = [];
  reclamantsCash = [
    { "code": "CLIENT_BANQUE", "libelle": "Client de la banque", "id": 1, "typeActeurId": null }, 
    { "code": "REDEVABLE_CONTTRIBUABLE", "libelle": "Redevable/Contribuable", "id": 2, "typeActeurId": null }, 
    { "code": "TTULAIRE_COMMANDE_PUBLIQUE", "libelle": "Titulaire d'une commande publique", "id": 3, "typeActeurId": null }, 
    { "code": "PENSIONNE", "libelle": "Pensionne", "id": 5, "typeActeurId": null }, 
    { "code": "ETUDIANT", "libelle": "Etudiant", "id": 6, "typeActeurId": null }, 
    { "code": "ASSOCIATION", "libelle": "Association", "id": 7, "typeActeurId": null }, 
    { "code": "ORDONNATEUR_SOUS_ORDONNATEUR", "libelle": "Ordonnateur/Sous Ordonnateur", "id": 8, "typeActeurId": 2 }, 
    { "code": "FONCTIONNAIRE", "libelle": "Fonctionnaire", "id": 9, "typeActeurId": 2 }, 
    { "code": "COLLECTIVITE_LOCALE", "libelle": "Collectivite locale", "id": 10, "typeActeurId": 6 },
    { "code": "AUTRES", "libelle": "Autres", "id": 4, "typeActeurId": null }
  ];

  getTypeReclamants() {
    this.presentLoading();
    this.reclamationService.getTypeReclamants().subscribe(
      data => {
        this.dismissLoading();
        console.info(typeof data, data);
        this.foundReclamations = data.json();
        this.foundReclamations.sort((x, y) => { return x.code == "AUTRES" ? 1 : y == "AUTRES" ? -1 : 0; });
        console.log('\n foundItems .. getTypeReclamants!!! ', data.json());
      },
      err => {
        this.dismissLoading();

        this.foundReclamations = this.reclamantsCash;
        this.foundReclamations.sort((x, y) => { return x.code == "AUTRES" ? 1 : y == "AUTRES" ? -1 : 0; });
        

        console.error(err)
      }
      ,
      () => {

        console.log('getTypeReclamants completed')
      }
    );
  }

  openReclamation(index) { 
    this.navCtrl.push(ReclamationPage, { reclamations: this.foundReclamations, selectedIdx: index });
  }

  openSuivi() {
    this.navCtrl.push(SuiviReclamationsPage);
  }

  getImgSrcOf(r) {
    let src = 'assets/img/ic-hank-large.png';
    if (r) {
      src = 'assets/img/r_' + r.code + '.png';
    }

    return src;
  }

}
