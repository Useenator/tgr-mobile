import { Component } from '@angular/core';
// import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { IonicPage, NavController, Events, NavParams, AlertController, LoadingController, reorderArray } from 'ionic-angular';

import { AnnuaireServiceProvider } from '../../providers/annuaire-service/annuaire-service';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

import { AnnuaireDetailPage } from '../annuaire-detail/annuaire-detail';

/**
 * Generated class for the AnnuaireParNomPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
//@IonicPage()
@Component({
  selector: 'page-annuaire-par-nom',
  templateUrl: 'annuaire-par-nom.html',
})
export class AnnuaireParNomPage {

  public foundResponsibles_ = {

    "responsables": [
      {
        "poste": {
          "code": "1132",
          "libelle": "PERCEPTION DE CHEFCHAOUEN",
          "adresse": "Perception Chefchaouen Sis, RUE  FES,  Chefchaouen",
          "fax": "0537574957",
          "secretariat": "0539986253",
          "position": null,
          "ordre": null,
          "typePosteId": 4,
          "posteRattachement": "1029",
          "villeId": 2601
        },
        "responsable": {
          "id": "318579",
          "username": "318579",
          "title": "PERCEPTEUR DE 1ERE CATEGORIE",
          "cin": "A560097",
          "norib": [
            "310810100011100341840193"
          ],
          "userOU": "1132",
          "userRAT": "1029",
          "lastName": "AHMED",
          "firstName": "YOUSFI",
          "completeName": "YOUSFI AHMED",
          "codeadministration": "29",
          "codeservice": "00000374",
          "sexe": "M",
          "sn": "AHMED",
          "titlecode": "50",
          "titlehierarchy": "2",
          "employeenumber": "318579",
          "preferredlanguage": "FR",
          "fixe": "No",
          "dn": "uid=318579,ou=collaborateurs,ou=users,DC=TGR,DC=MA"
        }
      }
    ]
  };
  public foundResponsibles = [
    {
      "poste": {
        "code": "1132",
        "libelle": "PERCEPTION DE CHEFCHAOUEN",
        "adresse": "Perception Chefchaouen Sis, RUE  FES,  Chefchaouen",
        "fax": "0537574957",
        "secretariat": "0539986253",
        "position": null,
        "ordre": null,
        "typePosteId": 4,
        "posteRattachement": "1029",
        "villeId": 2601
      },
      "responsable": {
        "id": "318579",
        "username": "318579",
        "title": "PERCEPTEUR DE 1ERE CATEGORIE",
        "cin": "A560097",
        "norib": [
          "310810100011100341840193"
        ],
        "userOU": "1132",
        "userRAT": "1029",
        "lastName": "AHMED",
        "firstName": "YOUSFI",
        "completeName": "YOUSFI AHMED",
        "codeadministration": "29",
        "codeservice": "00000374",
        "sexe": "M",
        "sn": "AHMED",
        "titlecode": "50",
        "titlehierarchy": "2",
        "employeenumber": "318579",
        "preferredlanguage": "FR",
        "fixe": "No",
        "dn": "uid=318579,ou=collaborateurs,ou=users,DC=TGR,DC=MA"
      }
    }
  ];
  private loader;

  public queryText = "";
  public position = {
    "lng": 0,
    "lat": 0
  };
  constructor(
    private annuaireService: AnnuaireServiceProvider,
    public alertCtrl: AlertController,
    private nav: NavController,
    private auth: AuthServiceProvider,
    public loading: LoadingController,
    private loadingCtrl: LoadingController,
    public navCtrl: NavController, public navParams: NavParams
  ) {
    // this.getReponsableByResponsibleName("ali");
    // this.getReponsableByResponsibleName(this.queryText);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AnnuaireParNomPage');
  }

  findResponsableByName() {
    this.getReponsableByResponsibleName(this.queryText);
  }

  /**
 * Push the details page to the navigation stack .. show Annnuaire details.
 */
  goToDetails(user) {
    this.nav.push(AnnuaireDetailPage, { user: user, position: this.position });
  }

  /**
 * get Reponsable By Responsible Name 
 * @param responsibleName 
 */
  getReponsableByResponsibleName(responsibleName) {
    this.annuaireService.getReponsableByResponsibleName(responsibleName).subscribe(
      data => {
        let obj = data.json();
        this.foundResponsibles = obj.responsables; //[0].responsable;
        // this.foundResponsiblesAll = this.foundResponsibles;
        console.log('\n Reeeeeesps_ByResponsibleName \n' + JSON.stringify(this.foundResponsibles));
      },
      err => {
        console.error("getReponsableByResponsibleName Error!! " + err);
        this.showAlert('', 'Nous ne pouvons pas nous connecter au serveur !!');
        // loader.dismiss();
      },
      () => {
        // console.log('QueryText ' + this.queryText);
        // loader.dismiss();
      }
    );//End service
  }
  ///////////////////////////////////// loader; ////////////////////////////////

  /**
 * Show loading
 */
  presentLoading() {
    this.loader = this.loading.create({
      content: "Chargement des données..."
    });

    this.loader.onDidDismiss(() => {
      console.log('Dismissed loading');
    });

    this.loader.present();

    // setTimeout(() => {
    //   this.loader.dismiss();
    // }, 15000);
  }

  /**
   * Dismiss loading
   */
  dismissLoading() {
    console.log('--Dismissed loading--');
    if (!this.loader.Dismissed) this.loader.dismiss();

  }
  /**
 * reuseable Alert box.
 */
  showAlert(title, message) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  } I
}
