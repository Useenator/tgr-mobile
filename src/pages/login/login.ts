//Najim

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

import { AccountDetailPage } from '../account-detail/account-detail';

import { RegisterPage } from '../register/register';

import { ForgotPasswordPage } from '../forgot-password/forgot-password';

import { LocalisationAndThemesProvider } from '../../providers/localisation-and-themes/localisation-and-themes';

import { RegisterValidatePage } from '../register-validate/register-validate';

/**
* Generated class for the LoginPage page.
*
* See http://ionicframework.com/docs/components/#navigation for more info
* on Ionic pages and navigation.
*/
//@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {
  xuser = {userid : "", password: "", saveSess: false};

  private isValid = false;

  private homeTranslation = "....";

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private auth: AuthServiceProvider,
    private loadingCtrl: LoadingController,
    private localisationAndThemesProvider: LocalisationAndThemesProvider,
    private alertCtrl: AlertController,
    public events: Events
  ) {

    this.translationAPI("Français");

    console.log("constructor LoginPage");

    this.auth.getUserSaveSess()
      .then(sess => {
        console.log("SESSSSSSSS", sess);

        if (sess) {
          console.log("login id");
          this.xuser['userid'] = sess;
          this.xuser['saveSess'] = true;
        }
      })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  startDemo(){
    var user = {userid : "CompteDemo", password: "CompteDemo1", saveSess: false};
    this.doLogin(user, ()=>{
      this.events.publish("showDemo", true);
    })
  }

  doLogin(userToLogin = undefined, cbFinish?) {
    this.isValid = false;
    if(!userToLogin){
      userToLogin = this.xuser
    }
    this.auth.login(userToLogin).subscribe(allowed => {
      console.log("subscribe allowed=" + allowed);
      this.dismissLoading();
      if (allowed) {
        this.isValid = true;
        if(cbFinish) cbFinish();
      } else {
        this.isValid = false;
        this.showAlert("Veuillez vérifier votre login, mot passe ou la connexion internet");
      }

      //  this.dismissLoading();
    },
      error => {

        this.dismissLoading();

        console.log(error);
      });

    this.presentLoading();
  }

  private loader;
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Chargement des données",
      showBackdrop: true,
      enableBackdropDismiss: true,
      dismissOnPageChange: true
    });

    this.loader.onDidDismiss(() => {
      console.log('Dismissed loading');
      //duplicated : app event
      //if(this.isValid) this.navCtrl.setRoot(AccountDetailPage);
    });

    this.loader.present();
  }

  dismissLoading() {
    console.log('--Dismissed loading--');
    this.loader.dismiss();
  }

  openRegister() {
    this.navCtrl.setRoot(RegisterPage);
  }

  public showAlert(message) {
    let alert = this.alertCtrl.create({
      title: 'TGR Mobile',
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }//END Alert

  openForgot() {
    this.navCtrl.push(ForgotPasswordPage);
  }

  /**
 * //get text value from the translation service
 * @param language 
 */
  translationAPI(language) {
    this.localisationAndThemesProvider.translationAPI(language).subscribe(
      data => {
        console.log("Server translation response DATA! ", data);

        try {
          let jdata = JSON.parse(data["_body"])[0].translation.Home;
          console.log(jdata);

          this.homeTranslation = jdata;
        } catch (error) {
          console.warn(error);
        }

        // if (data.json()[0].translation.home)
        //   this.homeTranslation = data.json()[0].translation.home;
        // else
        //   this.homeTranslation = "Home";
        // return true;
      },
      error => {
        console.log("Service indisponible veuillez réessayer ultérieurement");

      },
      () => {
        // this.dismissLoading();
        console.log("translationAPI Complete");
      }
    );
  }


  ////////////////////////
  /**
   * 
   */
  goToValidateAccount() {
    this.navCtrl.push(RegisterValidatePage);

  }
}
