import { Component } from '@angular/core';

import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';

import { ReclamationServiceProvider } from '../../providers/reclamation-service/reclamation-service';

//import { Camera, CameraOptions } from '@ionic-native/camera';

import { Events } from 'ionic-angular';

import { MyUtilsProvider } from '../../providers/my-utils/my-utils';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

import { SyncDataProvider } from "../../providers/sync-data/sync-data";

import { Observable } from 'rxjs/Rx';
import { File as iFile, FileEntry } from '@ionic-native/file';

import { ConnectivityProvider } from "../../providers/connectivity/connectivity";
import { Push } from 'ionic-native';

import * as EmailValidator from 'email-validator';
import { Utils } from '../../MyUtils/Utils';

/**
* Generated class for the ReclamationPage page.
*
* See http://ionicframework.com/docs/components/#navigation for more info
* on Ionic pages and navigation.
*/

//@IonicPage()
@Component({
  selector: 'page-reclamation',
  templateUrl: 'reclamation.html'
})
export class ReclamationPage {
  reclamantID: any = '';

  lastName = "";
  firstName = "";
  email = "";
  adresse = "";
  tel = "";

  rib = "";
  natureID: any = "";
  villeID: any = '';
  CNE = "";
  N_PPR = "";
  CIN = "";
  departementID: any = '';
  N_pension = "";
  Id_Fiscal = "";
  N_nature_article = "";
  postID: any = '';
  Raison_sociale = "";
  message = "";

  sendNotActive = false;

  reclamantsCash = [{ "code": "CLIENT_BANQUE", "libelle": "Client de la banque", "id": 1, "typeActeurId": null },
  { "code": "REDEVABLE_CONTTRIBUABLE", "libelle": "Redevable/Contribuable", "id": 2, "typeActeurId": null },
  { "code": "TTULAIRE_COMMANDE_PUBLIQUE", "libelle": "Titulaire d'une commande publique", "id": 3, "typeActeurId": null },
  { "code": "AUTRES", "libelle": "Autres", "id": 4, "typeActeurId": null },
  { "code": "PENSIONNE", "libelle": "Pensionne", "id": 5, "typeActeurId": null },
  { "code": "ETUDIANT", "libelle": "Etudiant", "id": 6, "typeActeurId": null },
  { "code": "ASSOCIATION", "libelle": "Association", "id": 7, "typeActeurId": null },
  { "code": "ORDONNATEUR_SOUS_ORDONNATEUR", "libelle": "Ordonnateur/Sous Ordonnateur", "id": 8, "typeActeurId": 2 },
  { "code": "FONCTIONNAIRE", "libelle": "Fonctionnaire", "id": 9, "typeActeurId": 2 },
  { "code": "COLLECTIVITE_LOCALE", "libelle": "Collectivite locale", "id": 10, "typeActeurId": 6 }
  ];

  public foundReclamations = [];
  public foundVilles = [];
  public foundDepartements: any = [];
  public foundVillePosts: any = [];
  public foundPosts: any = [];
  public foundNatures: any = [];
  public data;

  isPicking = false;
  togglePicker() {
    this.selectFileActive = false;
    this.isPicking = !this.isPicking;
  }
  doPick() {
    this.togglePicker();
  }

  selectFileActive = false;
  uploadAttach(srcInt = 0) {
    //wait 800ms for button animation click and trigger action
    //without this(setTimeout) UI stuck after choosing/taking a picture
    setTimeout(() => {
      /*
      if (srcInt == 0 || srcInt == 1) {
        
        let srcType;
        if (srcInt == 1) {
          srcType = this.camera.PictureSourceType.PHOTOLIBRARY;
          console.log("BIBLIO SELECTED");
        } else {
          srcType = this.camera.PictureSourceType.CAMERA;
          console.log("CAMERA SELECTED");
        }
        const options: CameraOptions = {
          quality: 100,
          destinationType: this.camera.DestinationType.FILE_URI,
          encodingType: this.camera.EncodingType.JPEG,
          mediaType: this.camera.MediaType.PICTURE,
          sourceType: srcType,
        }

        this.camera.getPicture(options).then(data => {
          // imageData is either a base64 encoded string or a file URI
          // If it's base64:
          //let base64Image = 'data:image/jpeg;base64,' + data;
          console.warn("IMG: ", data);
          let filename = data.replace(/^.*[\\\/]/, '');
          console.log("filename", filename);

          
          //var newFile = new File([data], "imagefile" + makeid);
          //console.log("newFile", newFile);
          //this.files.push(newFile)
         
          //*
          this.file.resolveLocalFilesystemUrl(data)
            .then(entry => (<FileEntry>entry).file(filedata => {

              setTimeout(() => {
                console.log("togglePicker");
                this.togglePicker();
              }, 500);

              //var newFile = new File([filedata], filename, { type: 'image/jpeg', lastModified: new Date().getTime() })
              //console.log("newFile", newFile);

              this.files.push(filedata);
            }))
            .catch(err => {
              this.togglePicker();
              console.log(err)
            });
          //*

        }, (err) => {
          console.error("err>> ", err);
        });
  } else */
  if (srcInt == 2) {
        this.selectFileActive = true;
      }

    }, 800);

    function makeid() {
      var text = "";
      var possible = "abcdefghijklmnopqrstuvwxyz0123456789";

      for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

      return text;
    }

  }


  currentReclamant;
  currentCodeReclamant;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private reclamationService: ReclamationServiceProvider,
    private loadingCtrl: LoadingController,
    //private camera: Camera,
    private myUtils: MyUtilsProvider,
    private auth: AuthServiceProvider,
    public events: Events,
    private syncService: SyncDataProvider,
    private alertCtrl: AlertController,
    private file: iFile,
    private connectivity: ConnectivityProvider
  ) {

    this.connectedUser = this.auth.currentUser;
    this.foundReclamations = this.navParams.get("reclamations");
    this.currentReclamant = this.foundReclamations[this.navParams.get("selectedIdx")]
    this.currentCodeReclamant = this.currentReclamant.code;
    console.log("this.currentReclamant ", this.currentReclamant);

    this.reclamantID = this.currentReclamant.id;


    console.info('--> FoundReclamation from reclamation entry  : ', this.foundReclamations, ' \n--> reclamatiuonID : ' + this.reclamantID);

    this.data = {};
  }

  connectedUser;
  logout() {
    let loading = this.loadingCtrl.create({
      content: 'Traitement en cours...',
      dismissOnPageChange: true
    });
    loading.present();

    this.events.publish('user:doLogout', Date.now());
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReclamationPage');

    this.presentLoading();
    //this.getTypeReclamants();

    this.reclamantSelect(String(this.reclamantID));




    let user = this.auth.currentUser;
    console.log("user>>", user);
    
    if (user) {
      this.lastName = user.LDAPData.lastName;
      this.firstName = user.LDAPData.firstName;
      this.email = user.LDAPData.email;
      this.CIN = user.LDAPData.cin;
    }else{
      this.lastName = ""
      this.firstName = ""
      this.email = ""
      this.CIN = ""
    }
    /*
    this.myUtils.doGetPrefs('lastRecalamationData')
      .then(data => {
        //console.warn('reclamation saved data: ', data);
        if (typeof data != 'undefined' && data != null) {

          //this.villeID = data.villeId;
          this.lastName = data._reclamant.nom;
          this.firstName = data._reclamant.prenom;
          this.adresse = data._reclamant.adresse;
          this.email = data._reclamant.mail;
          this.tel = data._reclamant.tel;
          this.CNE = data._reclamant.cne;
          this.CIN = data._reclamant.cin;

        }
      })
      */

  }

  /**
  *
  * ex: https://api.tgr.gov.ma/api/TypeReclamants
  */
  /*
  [
  {
  "code": "CLIENT_BANQUE",
  "libelle": "Client de la banque",
  "id": 12,
  "typeActeurId": null
}
]
*/
  getTypeReclamants(finishCB) {
    this.reclamationService.getTypeReclamants().subscribe(
      data => {
        finishCB();
        console.info(typeof data, data);
        this.foundReclamations = data.json();
        console.log('\n foundReclamations .. getTypeReclamants!!! ', data.json());
      },
      err => {
        finishCB();
        this.foundReclamations = this.reclamantsCash;
        console.error(err)
      }

      ,
      () => {

        console.log('getTypeReclamants completed')
      }
    );
  }

  /**
  * http://api.tgr.gov.ma/api/TypeReclamants/{idTypeReclamant}/natureReclamations
  * ex: http://api.tgr.gov.ma/api/TypeReclamants/12/natureReclamations
  */
  /* json
  [
  {
  "code": "Qualité de la relation clients en agence",
  "libelle": "Qualité de la relation clients en agence",
  "id": 95,
  "typeReclamantId": 12
  }
  ]
  */
  getNatureReclamations(finishCB?) {
    console.log("getNatureReclamations, code=======>", this.currentCodeReclamant);

    //*
    this.syncService.syncData("NatureReclamations_" + this.currentCodeReclamant,
      (res => {
        console.log("NatureReclamations res >>>>", res);

        console.log("--> this.foundNatures - NatureReclamations  res>>>>", res);
        finishCB();
        this.foundNatures = res;

      }),
      () => {
        //Online http ...
        //this.annuaire.getListFromHttp()
        ///... rest of code !!!
        return new Promise((resolve, reject) => {
          this.reclamationService.getNatureReclamations(this.currentCodeReclamant).subscribe(
            data => {
              console.info(typeof data, data);
              if (JSON.parse(data['_body']).natures && JSON.parse(data['_body']).natures instanceof Array && JSON.parse(data['_body']).natures.length > 0) {
                resolve(JSON.parse(data['_body']).natures);
                console.log(">>natures instanceof Array")



              } else if (JSON.parse(data['_body']).natures && JSON.parse(data['_body']).natures instanceof String) {
                this.showAlert(JSON.parse(data['_body']).natures);
                this.navCtrl.pop();
                reject(JSON.parse(data['_body']).natures);
                console.log(">>natures instanceof string")
              } else {
                this.showAlert("Vérifier votre connexion, et réessayer..");
                this.navCtrl.pop();
                reject("Vérifier votre connexion, et réessayer..");
                console.log(">>natures error")
              }

            },
            err => {
              console.error(err);
              this.showAlert("Vérifier votre connexion, et réessayer..");
              this.navCtrl.pop();
              reject(err);
            },
            () => console.log('getNatureReclamations completed')
          );
        });
      }
    );//*/
  }

  getPosts(codeville, finishCB?) {
    console.log("getPosts, params=======>", codeville);
    let reclamantParam = this.currentCodeReclamant;
    //*
    this.syncService.syncData("ReclamationsPosts__" + reclamantParam + "__" + codeville,
      (res => {
        console.log("ReclamationsPosts res>>>>", res);
        finishCB();
        this.foundPosts = res;

      }),
      () => {
        return new Promise((resolve, reject) => {

          this.reclamationService.getPosts(reclamantParam, codeville).subscribe(
            data => {
              console.info(typeof data, data);
              try {
                resolve(data.json().postes);
              } catch (error) {
                console.error(error);
                reject(error);
              }

            },
            err => {
              console.error(err);
              this.showAlert("Vérifier votre connexion, et réessayer..");
              this.navCtrl.pop();
              reject(err);

            },
            () => console.log('getPosts completed')
          );
        });
      }
    );//*/
  }

  getActeursCollectiviteLocale(codeVille, reclamantParam, finishCB?) {


    //*
    this.syncService.syncData("ActeursCollectiviteLocale_" + codeVille + "_" + reclamantParam,
      (res => {
        console.log("ActeursCollectiviteLocale res>>>>", res);
        finishCB();
        this.foundDepartements = res;

      }),
      () => {
        //Online http ...
        //this.annuaire.getListFromHttp()
        ///... rest of code !!!
        return new Promise((resolve, reject) => {
          this.reclamationService.getActeursCollectiviteLocale(codeVille, reclamantParam).subscribe(
            data => {

              try {
                let res = data.json().acteurs;
                console.info(res);
                //this.foundDepartements = res;
                resolve(res);
              } catch (e) {
                console.error(e);
                this.showAlert("Vérifier votre connexion, et réessayer..");
                this.navCtrl.pop();
                reject(e);
              }

            },
            err => {

              console.error(err);
              this.showAlert("Vérifier votre connexion, et réessayer..");
              this.navCtrl.pop();
              reject(err);
            },
            () => console.log('getActeursCollectiviteLocale completed')
          );
        });
      });
  }

  getActeurs(reclamantID, finishCB?) {

    let reclamantParam = this.currentCodeReclamant;

    //*
    this.syncService.syncData("Departements_" + reclamantParam,
      (res => {
        console.log("foundDepartements res>>>>", res);
        finishCB();
        this.foundDepartements = res;

      }),
      () => {
        //Online http ...
        //this.annuaire.getListFromHttp()
        ///... rest of code !!!
        return new Promise((resolve, reject) => {
          this.reclamationService.getActeurs(reclamantParam).subscribe(
            data => {

              try {
                let res = data.json().acteurs;
                console.info(res);
                //this.foundDepartements = res;
                resolve(res);
              } catch (e) {
                console.error(e);
                this.showAlert("Vérifier votre connexion, et réessayer..");
                this.navCtrl.pop();
                reject(e);
              }

            },
            err => {

              console.error(err);
              this.showAlert("Vérifier votre connexion, et réessayer..");
              this.navCtrl.pop();
              reject(err);
            },
            () => console.log('getActeurs completed')
          );
        });
      }
    );//*/

    /*
    this.reclamationService.getTypeActeursBytypeActeurID(id).subscribe(
      data => {
        this.dismissLoading();
        try {
          let res = JSON.parse(data['_body']);
          console.info(res);
          this.foundDepartements = res;
          this.events.unsubscribe('getNatureReclamations:finish');
        } catch (e) { }

      },
      err => {
        this.dismissLoading();
        console.error(err);
        this.events.unsubscribe('getNatureReclamations:finish');
      },
      () => console.log('getActeurs completed')
    );
    //*/
  }
  /*
  getVillesPostes(idVille, reclamantID, finishCB?) {
    console.log("idVille=======>", idVille, "reclamantID--->", reclamantID);
    let codetypeacteurParam = "";
    switch (String(reclamantID)) {//this methode used just for id = '18': "COLLECTIVITE-LOCALE"
      case '12': codetypeacteurParam = "CLIENT-BANQUE"; break;
      case '13': codetypeacteurParam = "REDEVABLE-CONTTRIBUABLE"; break;
      case '17': codetypeacteurParam = "PENSIONNE"; break;
      case '18': codetypeacteurParam = "COLLECTIVITE-LOCALE"; break;
      case '19': codetypeacteurParam = "ORDONNATEUR-SOUS-ORDONNATEUR"; break;
    }
    console.log("codetypeacteurParam>>>", codetypeacteurParam);

    //*
    this.syncService.syncData("getVillesPostes_" + idVille + "_" + reclamantID,
      (res => {
        console.log("getVillesPostes res>>>>", res);
        finishCB();
        this.foundVillePosts = res;

      }),
      () => {
        //Online http ...
        //this.annuaire.getListFromHttp()
        ///... rest of code !!!
        return new Promise((resolve, reject) => {
          this.reclamationService.getVillesPostesByIdVille(idVille, codetypeacteurParam).subscribe(
            data => {
              try {
                let res = JSON.parse(data['_body']).acteurs;
                console.info(res);

                resolve(res);
              } catch (e) {
                console.error(e);
                this.showAlert("Vérifier votre connexion, et réessayer..");
                this.navCtrl.pop();
                reject(e);
              }

            },
            err => {
              console.error(err);
              this.showAlert("Vérifier votre connexion, et réessayer..");
              this.navCtrl.pop();
              reject(err);

            },
            () => console.log('getVillesPostes completed')
          );
        });
      }
    );*/
  //}

  files: Array<any> = [];
  fileSelected($event) {
    let file: File = $event.target.files[0];

    console.log(file);

    this.togglePicker();

    if (file && file['name']) {
      this.files.push(file);
    }


  }

  cancelFile(index: number) {

    console.log("cancel file at: ", index);

    this.files.splice(index, 1);
  }

  sendFiles(files) {
    return this.makeFileRequest("https://api.tgr.gov.ma/api/FilePoolers/repoPJ/upload",
      [], files);
  }

  private makeFileRequest(url: string, params: string[], files: any[]): Observable<any> {
    return Observable.create(observer => {
      if (files && files.length > 0) {
        let formData: FormData = new FormData(),
          xhr: XMLHttpRequest = new XMLHttpRequest();

        formData.set('accept-charset', 'iso-8859-1,UTF-8');

        for (let i = 0; i < files.length; i++) {
          formData.append("uploads[]", files[i], files[i]['name']);
        }

        xhr.onreadystatechange = () => {
          if (xhr.readyState === 4) {
            if (xhr.status === 200) {
              observer.next(JSON.parse(xhr.response));
              observer.complete();
            } else {
              observer.error(xhr.response);
            }
          }
        };

        xhr.upload.onprogress = (event) => {
          //this.progress = Math.round(event.loaded / event.total * 100);
          //this.progressObserver.next(this.progress);
        };

        xhr.open('POST', url, true);
        xhr.send(formData);
      } else {
        observer.next();
        observer.complete();
      }
    });
  }

  /**
  *
  * ex: https://api.tgr.gov.ma/api/Villes
  */
  /* json
  [
  {
  "code": "1",
  "libelle": "CASABLANCA",
  "id": 2237
  }
  ]
  */
  isVillesFully = false;
  getVilles(finishCB) {
    //*
    this.syncService.syncData("ReclamationVilles",
      (res => {
        finishCB();
        console.log("res>>>>", res);
        this.foundVilles = res;
        if (this.foundVilles instanceof Array) {
          this.isVillesFully = true;

          this.foundVilles.sort(function (a, b) {
            return (a.libelle > b.libelle) ? 1 : ((b.libelle > a.libelle) ? -1 : 0);
          }
          );

        }
      }),
      () => {
        //Online http ...
        //this.annuaire.getListFromHttp()
        ///... rest of code !!!
        return new Promise((resolve, reject) => {
          this.reclamationService.getVilles().subscribe(
            data => {
              console.info("getVilles", typeof data, data);

              try {

                let villes = data.json().villes;



                resolve(villes);


              } catch (e) {
                console.error(e);
                this.showAlert("Vérifier votre connexion, et réessayer..");
                this.navCtrl.pop();
                reject(e);
              }
            },
            err => {
              console.error(err);
              this.showAlert("Vérifier votre connexion, et réessayer..");
              this.navCtrl.pop();
              reject(err);


            },
            () => {
              console.log('getVilles completed')
            }
          );
        });
      }
    );//*/
    /*
    this.reclamationService.getVilles().subscribe(
      data => {
        this.wsCallCount++;
        //if (this.wsCallCount == 2) 
        this.dismissLoading();
        cbFinish();

        console.info(typeof data, data);
        this.foundVilles = data.json();
        console.log('\n foundItems .. getVilles!!! ', data.json());
      },
      err => {
        //this.wsCallCount++;
        //if (this.wsCallCount == 2) 

        this.dismissLoading();
        cbFinish();

        console.error(err)
      },
      () => {
        console.log('getVilles completed')
      });
    //*/
  }

  /**
  * http://api.tgr.gov.ma/api/Villes/{idVille}/postes
  * ex: http://api.tgr.gov.ma/api/Villes/2237/postes
  */
  /* json
  [
  {
  "code": "1003",
  "libelle": "TRESORERIE PREFECTORALE DE CASABLANCA CENTRE OUEST",
  "adresse": "Trésorerie Provinciale  Casablanca, Place Med V, Casablanca",
  "fax": "0537574721",
  "secretariat": "0522221793",
  "position": null,
  "ordre": "30",
  "typePosteId": 4,
  "posteRattachement": "1506",
  "villeId": 2237
  }
  ]
  */
  getVillesPostesByIdVille(idVille, codetypeacteur) {
    this.reclamationService.getVillesPostesByIdVille(idVille, codetypeacteur).subscribe(
      data => {
        console.info(typeof data, data);
        this.foundReclamations = data.json();
        console.log('\n foundItems .. getVillesPostesByIdVille!!! ', data.json());
      },
      err => console.error(err),
      () => console.log('getVillesPostesByIdVille completed')
    );
  }

  /**
  * https://api.tgr.gov.ma/api/TypeReclamants/{typeReclamntID}/typeActeur
  * ex: https://api.tgr.gov.ma/api/TypeReclamants/18/typeActeur
  */
  /* json
  {
  "code": "COLLECTIVITE-LOCALE",
  "libelle": "COLLECTIVITE LOCALE",
  "id": 6
  }
  */
  getTypeReclamantsBytypeReclamntID(typeReclamntID) {
    this.reclamationService.getTypeReclamantsBytypeReclamntID(typeReclamntID).subscribe(
      data => {
        console.info(typeof data, data);
        this.foundReclamations = data.json();
        console.log('\n foundItems .. getTypeReclamantsBytypeReclamntID!!! ', data.json());
      },
      err => console.error(err),
      () => console.log('getTypeReclamantsBytypeReclamntID completed')
    );
  }

  /**
  * http://api.tgr.gov.ma/api/TypeActeurs/{typeActeurID}/acteurs
  * ex: http://api.tgr.gov.ma/api/TypeActeurs/12/acteurs
  */
  /* json
  [
  {
  "code": "1102",
  "libelle": "PERCEPTION D'AGADIR HAY NAHDA",
  "adresse": "CITE PRINCE HERITIER .ANGLE ZELLAKA ET ABDELLAH BEN YASSINE",
  "id": 28113,
  "villeId": 394,
  "posteId": null,
  "typeActeurId": 12
  }
  ]
  */
  /*
  getTypeActeursBytypeActeurID() {
    this.reclamationService.getTypeActeursBytypeActeurID().subscribe(
      data => {
        console.info(typeof data, data);
        this.foundReclamations = data.json();
        console.log('\n foundItems .. getTypeActeursBytypeActeurID!!! ', data.json());
      },
      err => console.error(err),
      () => console.log('getTypeActeursBytypeActeurID completed')
    );
  }
  */


  ///////////////////////////////////// Utils ///////////////////////////////////////

  private loader;
  presentLoading(message = "Chargement des données...") {
    this.loader = this.loadingCtrl.create({
      content: message,//"Chargement des données..."
      showBackdrop: true,
      enableBackdropDismiss: true,
      dismissOnPageChange: true
    });

    this.loader.onDidDismiss(() => {
      console.log('Dismissed loading');
    });

    this.loader.present();
  }

  dismissLoading() {
    console.log('--Dismissed loading--');
    if (!this.loader.Dismissed) this.loader.dismiss();

  }

  getReclamantByID(idReclamant) {
    let reclamant;
    this.foundReclamations.filter(item => {
      if (item.id == idReclamant) reclamant = item;
    })
    return reclamant;
  }

  postResult;

  doPostReclamation(body) {

    this.presentLoading("Envoie des données...");
    this.reclamationService.postReclamation(body).subscribe(
      data => {
        console.info(typeof data, data);

        if (data.reference) {

          let reclamations = [];

          if (this.auth.currentUser) {
            let key = "references_" + this.auth.currentUser.LDAPData.id; //reclamations_userid

            this.myUtils.doGetPrefs(key).then(recs => {
              if (recs) reclamations = recs;

              reclamations.unshift(data.reference);
              this.myUtils.doSetPrefs(key, reclamations);
            })
          } else {
            let key = "references_unknow_user";
            this.myUtils.doGetPrefs(key).then(recs => {
              if (recs) reclamations = recs;

              reclamations.unshift(data.reference);
              this.myUtils.doSetPrefs(key, reclamations);
            })

          }
          this.presentPrompt("Veuillez enregistrer la référence de votre réclamation:", data.reference);

          this.navCtrl.pop();

          this.dismissLoading();
        }
        //console.log('\n foundItems .. postReclamation!!! ', data.json());
      },
      err => {
        console.error(err);
        this.dismissLoading();
      }
    );
  }

  presentPrompt(msg, ref) {
    const alert = this.alertCtrl.create({
      title: 'Tgr Mobile',
      message: msg,
      inputs: [
        {
          value: ref
        }
      ],
      buttons: ['Ok']
    });
    alert.present();
  }

  reclamantSelect(reclamantID) {

    console.warn(typeof reclamantID, reclamantID);
    let reclamant = this.getReclamantByID(reclamantID);

    console.log('reclamant:', reclamant);
    if (typeof reclamant != 'undefined' && reclamant != null) {
      console.info("reclamant", reclamant, "typeActeurId", reclamant["typeActeurId"], "reclamantId", reclamant['id']);
      let idForActeurApi;
      switch (this.currentCodeReclamant) {
        case "CLIENT_BANQUE": //Client de la banque
          this.getVilles(() => {
            this.getNatureReclamations(() => {
              this.dismissLoading();
            });
          });
          break;
          ;
        case "COLLECTIVITE_LOCALE": //Collective locale 
          this.getVilles(() => {
            this.getNatureReclamations(() => {
              this.dismissLoading();
            });
          });
          break;
          ;
        case "FONCTIONNAIRE": //Fonctionnaire
          this.getNatureReclamations(() => {
            this.getActeurs(this.reclamantID, () => {
              this.dismissLoading();
            })
          });

          break;
          ;
        case "ORDONNATEUR_SOUS_ORDONNATEUR": //Ordonnateur/Sous Ordonnateur
          this.getVilles(() => {
            this.getNatureReclamations(() => {
              this.getActeurs(this.reclamantID, () => {
                this.dismissLoading();
              })
            });
          });
          break;
          ;
        case "PENSIONNE": //Pensionne
          this.getVilles(() => {
            this.getNatureReclamations(() => {
              this.dismissLoading();
            });
          });

          break;
          ;
        case "REDEVABLE_CONTTRIBUABLE": //Redevable/Contribuable
          this.getVilles(() => {
            this.getNatureReclamations(() => {
              this.dismissLoading();
            });
          });

          break;
          ;
        case "TTULAIRE_COMMANDE_PUBLIQUE": //Titulaire d'une commande publique
          this.getVilles(() => {
            this.getNatureReclamations(() => {
              this.dismissLoading();
            });
          });
          break;
          ;

      }//end switch !
    }//end if!

  }

  villeSelected(evt?) {
    if (this.isVillesFully) {
      console.log("villeSelected", evt, "ville", this.villeID);
      this.presentLoading();
      let villeId;
      if (evt) villeId = evt; else villeId = this.villeID;
      // acteurs/deparetements
      if (this.currentCodeReclamant == "COLLECTIVITE_LOCALE") {
        this.getActeursCollectiviteLocale(villeId, "COLLECTIVITE-LOCALE", () => {
          this.getPosts(villeId, () => {
            this.dismissLoading();
          });
        })
      } else {
        this.getPosts(villeId, () => {
          this.dismissLoading();
        });
      }
    }
  }

  postReclamation() {

    let inputsMsg = this.verifyInputs();
    if (inputsMsg.length <= 0) {
      //TODO:  bug natureReclation = Nan in postReclamation OBJ
      console.info('--> this.natureID in post reclamation: ', this.natureID, ' typeOf ' + typeof (this.natureID));
      //END bug natureReclation = Nan in postReclamation OBJ

      var toPost = {
        reference: "",
        date: new Date().toISOString(),
        description: this.message,
        postId: (this.postID == '') ? 0 : Number(this.postID),
        villeId: (this.villeID == '') ? 0 : Number(this.villeID),
        natureReclamationId: (this.natureID == '') ? 0 : Number(this.natureID),
        typeEnvoiReclamationId: 0,
        reclamantId: (this.reclamantID == '') ? 0 : Number(this.reclamantID),
        etatReclamationId: 0,
        _reclamant: {
          nom: this.lastName,
          prenom: this.firstName,
          adresse: this.adresse,
          mail: this.email,
          identifiantFiscal: this.Id_Fiscal,
          ppr: this.N_PPR,
          raisonSocia1e: this.Raison_sociale,
          natureArticle: (this.N_nature_article == '') ? 0 : Number(this.N_nature_article),
          tel: this.tel,
          rib: this.rib,
          numeroPension: (this.N_pension == '') ? 0 : Number(this.N_pension),
          cin: this.CIN,
          cne: this.CNE,
          acteurId: (this.departementID == '') ? 0 : Number(this.departementID),
          typeReclamantId: this.reclamantID//0  
          //BUG .. typeReclamantId:0 was static .. check with rajae
        },
        _etatReclamation: {
          date: new Date().toISOString(),
          typeEtatReclamationId: 1//Etat en instance !
        },
        _piecesJointes: []
      }//en toPost arg!
      /*
      "_piecesJointes":[

      {"container":"repoPJ","name":"1168600.jpeg"},

      {"container":"repoPJ ","name":"1168600.jpeg"}

      ]
      */
      if (this.connectivity.isOnline()) {
        this.presentLoading('Envoie des données...');

        this.sendFiles(this.files)
          .subscribe(res => {

            console.warn(' --> SendFile response obj' + JSON.stringify(res), res);
            


            var postedFiles = [];
            if (res && res.result && res.result.files && res.result.files["uploads[]"]) postedFiles = res.result.files["uploads[]"];
            if (this.files.length == 0 || postedFiles) {
              try {
                console.log('--> postedFiles', postedFiles);
                toPost._piecesJointes = res.result.files['uploads[]'];
              } catch (error) {
                console.warn(' --> toPost._piecesJointes error or empty: ' + error);
              }



              //!!! static test !!!!!
              //toPost._piecesJointes[0] = { "container": "repoPJ", "name": "icon.png", "type": "image/png", "size": 278651 };//.uploads ;//= postedFiles;
              console.warn(' --> toPost._piecesJointes Array: ' + JSON.stringify(toPost._piecesJointes), toPost._piecesJointes);
              /* {"result":{"files":{"uploads[]":
                                      [
                                        {"container":"repoPJ","name":"icon.png","type":"image/png","field":"uploads[]","size":278651}
                                      ]
                                    },"fields":{}}}
*/
              //*


              console.info(toPost._etatReclamation.date)

              this.myUtils.doSetPrefs('lastRecalamationData', toPost);

              this.doPostReclamation(toPost);

              //*/
            }//end if
            else {
              this.dismissLoading();
              this.showAlert("Service indisponible\nErreur d'envoi des fichiers");
            }
          },//data
          err => {
            this.dismissLoading();
            console.error(err);
            this.showAlert("Service indisponible");
          });
        //*
      } else {
        //no network !
        var reclamationToSave = {
          files: this.files,
          postObject: toPost,
          sent: false
        }

        let key = "reclamationPosts";

        this.myUtils.doGetPrefs(key)
          .then((data: Array<any>) => {
            if (data) {
              data.unshift(reclamationToSave);
              this.myUtils.doSetPrefs(key, data);
            } else {
              let data = [reclamationToSave];
              this.myUtils.doSetPrefs(key, data);
            }
          })

        this.showAlert("Vous êtes en mode déconnecté, la requette sera envoyé une fois la connexion est établie")
        this.navCtrl.pop();
      }

    } else {//if not (this.verifyInputs() 
      this.showAlert(inputsMsg);

      //this.showAlert("Veuillez remplir tous les champs obligatoires avant d'envoyer");
    }



  }

  verifyInputs(): string {
    var result = false;
    var msg = "Vous devez vérifier ces champs: \n";

    switch (this.currentCodeReclamant) {
      case 'CLIENT_BANQUE':
        if (this.rib && this.rib.trim().length > 0 && this.natureID && typeof this.villeID !== 'undefined' 
        && this.villeID != null && this.postID) result = true;
        else {
          if (this.rib.trim().length <= 0) {
            msg += "RIB, "
          }

          if (!this.natureID) {
            msg += "la nature, "
          }

          if (!this.villeID) {
            msg += "la ville, "
          }

          if (!this.postID) {
            msg += "le poste, "
          }

        }

        break;
      case 'REDEVABLE_CONTTRIBUABLE':
        if (this.Id_Fiscal && this.natureID && typeof this.villeID !== 'undefined' && this.villeID != null && this.postID) result = true;
        else {
          if (!this.Id_Fiscal) {
            msg += "id fiscale, "
          }

          if (!this.natureID) {
            msg += "la nature, "
          }

          if (!this.villeID) {
            msg += "la ville, "
          }

          if (!this.postID) {
            msg += "le poste, "
          }

        }

        break;
      case 'FONCTIONNAIRE'://Fonctionnaire
        //if (this.N_PPR && this.CIN && this.departementID && this.villeID) result = true;
        if (this.N_PPR && this.CIN && this.natureID && this.departementID) result = true;
        else {
          if (!this.N_PPR) {
            msg += "num de PPR, "
          }

          if (!this.CIN) {
            msg += "CIN, "
          }

          if (!this.natureID) {
            msg += "la nature, "
          }

          if (!this.departementID) {
            msg += "département, "
          }

        }

        break;
      case 'TTULAIRE_COMMANDE_PUBLIQUE'://Titulaire d'une commande publique
        //if (this.Raison_sociale && this.natureID && this.villeID && this.postID) result = true;
        if (this.Raison_sociale && this.natureID && typeof this.villeID !== 'undefined' && this.villeID != null && this.postID) result = true;
        else {
          if (!this.Raison_sociale) {
            msg += "raison sociale, "
          }

          if (!this.natureID) {
            msg += "la nature, "
          }

          if (!this.villeID) {
            msg += "la ville, "
          }

          if (!this.postID) {
            msg += "le poste, "
          }

        }


        break;
      case 'ASSOCIATION':
        result = true;

        break;
      case 'PENSIONNE':
        if (this.N_PPR && this.N_pension && this.natureID && typeof this.villeID !== 'undefined' && this.villeID != null
          && this.postID) result = true;
        else {
          if (!this.N_PPR) {
            msg += "num. de PPR, "
          }

          if (!this.N_pension) {
            msg += "pensionne, "
          }

          if (!this.natureID) {
            msg += "la nature, "
          }

          if (!this.villeID) {
            msg += "la ville, "
          }

          if (!this.postID) {
            msg += "le poste, "
          }

        }


        break;

      case 'COLLECTIVITE_LOCALE':
        if (this.natureID && typeof this.villeID !== 'undefined' && this.villeID != null && this.postID) result = true;
        else {

          if (!this.natureID) {
            msg += "la nature, "
          }

          if (!this.villeID) {
            msg += "la ville, "
          }

          if (!this.postID) {
            msg += "le poste, "
          }

        }


        break;

      case 'ORDONNATEUR_SOUS_ORDONNATEUR':
        if (this.departementID && this.natureID && this.villeID && this.postID) result = true;
        else {

          if (!this.departementID) {
            msg += "département, "
          }

          if (!this.natureID) {
            msg += "la nature, "
          }

          if (!this.villeID) {
            msg += "la ville, "
          }

          if (!this.postID) {
            msg += "le poste, "
          }

        }

        break;

      case 'ETUDIANT':
        if (this.CNE) result = true;
        else {

          msg += "CNE, "

        }

        break;
      case 'AUTRES':
        result = true;

        break;
    }

    if (!this.lastName || this.lastName.trim().length <= 0) {
      msg += "le nom, ";
      result = false;
    }
    if (!this.firstName || this.firstName.trim().length <= 0) {
      msg += "le prénom, ";
      result = false;
    }
    if (!this.adresse || this.adresse.trim().length <= 2) {
      msg += "l'adresse, ";
      result = false;
    }


    //Verify email
    if (!EmailValidator.validate(this.email)) {
      // Utils.showAlert(this.alertCtrl,"","Merci de vérifier votre Email SVP")
      result = false;
      msg += "l'email, "
    }


    if (this.message.trim().length <= 0) {
      result = false;
      msg += "l'objet de votre reclamation, "
    }

    if(result){
      return ""
    }else{
      return msg.substring(0, msg.length-2);
    }
  }

  public showAlert(message) {
    let alert = this.alertCtrl.create({
      title: 'TGR Mobile',
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }//END Alert

}
