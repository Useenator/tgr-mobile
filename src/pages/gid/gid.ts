import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { AlertController, LoadingController } from 'ionic-angular';
import { GidServiceProvider } from '../../providers/gid-service/gid-service';

import { GidDetailPage } from '../gid-detail/gid-detail'
import { Utils } from '../../MyUtils/Utils';

import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

/**
 * Generated class for the GidPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
//@IonicPage()
@Component({
  selector: 'page-gid',
  templateUrl: 'gid.html',
})
export class GidPage {
  private foundMds = [];
  private foundMps = [];
  private foundGIDAll = [];
  private foundGID = [];
  private queryText;
  private tier ;//= "165361";
  private token ;//= "qF5chaK7FWYB8AWLvRauxLEdsnZBfDeMqq0wedQub9Hr5Es6qZYSa4ytHj2MQSRR";

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private gidServiceProvider: GidServiceProvider,
    public alertCtrl: AlertController,
    private nav: NavController,
    public loadingCtrl: LoadingController,
    private auth: AuthServiceProvider,
    private events: Events
  ) {
    this.connectedUser = this.auth.currentUser;
    console.log(this.connectedUser);
    if (this.connectedUser) {
      this.tier = this.connectedUser.userAccess.userid;
      this.token = this.connectedUser.tokenData.id;
    }
  }
  connectedUser;
  logout() {
    let loading = this.loadingCtrl.create({
      content: 'Traitement en cours...',
      dismissOnPageChange: true
    });
    loading.present();

    this.events.publish('user:doLogout', Date.now());
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GidPage');
    //init test
    // this.getMds();
    // this.getMps();
    this.getGID(this.tier, this.token);//tier, token
  }


  /**
 * filter the responsible list and show it in the Ui listView
 */
  updateItems() {

    this.foundGID = this.filterItems(this.queryText);
    console.log('\n this.foundItems \n' + JSON.stringify(this.foundMds));
  }

  filterItems(searchTerm) {

    try {
      ///////// test concat array ////////////
      // this.foundMds.concat(this.foundMps);
      // this.foundMdsAll = this.foundMds;
      ///////// END test concat array ////////////

      this.foundGID = this.foundGIDAll;

      // console.log('\n RESPS foundMds \n', this.foundMds);
      // console.log('\n RESPS foundMds \n', this.foundMds);
      //******************************* Filter by type dépense ou type d'acte *********************************

      return this.foundGID.filter((item) => {
        let conditionObjetActe = false;
        if (typeof item.objetActe != 'undefined' && item.objetActe != null && item.objetActe != "") {
          conditionObjetActe = item.objetActe.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        }


        if (
          item.LibelleTypeDepense.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
          || item.libelleTypeActe.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
          || item.numeroActe.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
           || item.codeLoiFinance.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
          || item.libelleTypeEtat.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
          || conditionObjetActe
          || item.objetPrestation.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
        ) {
          return true;
        }

        // return item.LibelleTypeDepense.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
        //   || item.libelleTypeActe.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
        //   || item.numeroActe.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
        //   || item.numeroPrestation.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) > -1

        //   || item.libelleTypeEtat.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
        //   // || item.conditionObjetActe.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
        //   || conditionObjetActe
        //   || item.objetPrestation.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
        ;



      });

    } catch (err) {
      console.error("filterItems Error!! ", err);
    }
  }

  /**
   * Push the details page to the navigation stack .. show Annnuaire details.
   */
  goToDetails(item) {
    this.nav.push(GidDetailPage, { item: item, mds: this.foundGID });
  }

  /**

  /**
  *
  *https://api.tgr.gov.ma/api/Tiers/126/mPs
 */
  getGID(tier, token) {
    // this.presentLoading();
    this.presentLoading();
    // let foundMps;
    this.gidServiceProvider.getGid(tier, token).subscribe(
      data => {
        //this.foundMds = 
        this.foundMds = [];
        this.foundMds = [];
        (data.json()).tiersActesFournisseurs.forEach(el => {
          if(el.mDs && el.mDs.length>0){
            console.log("element mds found>>", el.mDs);
            
            el.mDs.forEach(mds => {
              this.foundMds.push(mds)
            });
            console.log("total::", this.foundMds.length);
            
          }

          if(el.mPs && el.mPs.length>0){
            console.log("element mPs found>>", el.mPs);
            
            el.mPs.forEach(mps => {
              this.foundMps.push(mps)
            });
            console.log("total::", this.foundMps.length);
            
          }

          
        });
        //this.foundMps = (data.json()).tiersActesFournisseurs.mPs;
        console.log("FoundMDS!!", this.foundMds, "FoundMPS !!", this.foundMps);

        if (this.foundMds && this.foundMds.length>0)
          this.foundGID = this.foundMds.concat(this.foundMps);

        this.foundGIDAll = this.foundGID;
        console.log("CONCAT foundGID", this.foundGID);
        this.dismissLoading();
      },
      err => {
        // this.dismissLoading();
        this.dismissLoading();
        console.error(err)
        if (err.status == 503) {

          Utils.showAlert(this.alertCtrl, '', '503 !!');
        }
      },
      () => {
        // this.dismissLoading();
        //this.dismissLoading();
        console.log('getGID list completed')
      }
    );
    return this.foundGID;
  }


  ///////////////////////////////////// loader; ////////////////////////////////
  /**
   * Show loading
   */
  private loader;
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Chargement des données...",
      showBackdrop: true,
      enableBackdropDismiss: true,
      //dismissOnPageChange: true
    });

    this.loader.onDidDismiss(() => {
      console.log('Dismissed loading');
    });

    this.loader.present();

    // setTimeout(() => {
    //   this.loader.dismiss();
    // }, 15000);
  }

  /**
   * Dismiss loading
   */
  dismissLoading() {
    console.log('--Dismissed loading--');
    if (!this.loader.Dismissed) this.loader.dismiss();

  }//////////////////////// END loader ///////////////////////////////////////

}
