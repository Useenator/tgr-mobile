
import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';

import { ContribuableService } from '../../providers/contribuable-service/contribuable-service';
import { AlertController } from 'ionic-angular';

import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { MyUtilsProvider } from '../../providers/my-utils/my-utils';
import { Config } from '../../MyUtils/Config';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
// import { SituationFiscaleDetailPage } from '../situation-fiscale-detail/situation-fiscale-detail';
//loader
import { LoadingController } from 'ionic-angular';
//import { AvisImpositionNonRecuDetailPage } from '../avis-imposition-non-recu-detail/avis-imposition-non-recu-detail';
import { ContribuablePage } from '../contribuable/contribuable';
import { Utils } from '../../MyUtils/Utils';
import { SyncDataProvider } from "../../providers/sync-data/sync-data";


/*
  Generated class for the AvisImpositionNonRecu page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-avis-imposition-non-recu',
  templateUrl: 'avis-imposition-non-recu.html',
  providers: [ContribuableService]
})
export class AvisImpositionNonRecuPage {

  private codenature ;//= 60;
  private reference ;//= "85595613";
  private montant ;//= 162.9;
  private annee ;//= "2016";
  private email ;//= "test-TGR@test.com";
  
  ////Testing data 
  // private codenature = 60;
  // private reference = "85595613";
  // private montant = 162.9;
  // private annee = "2016";
  // private email = "test-TGR@test.com";
  
  // private foundItems;
  private years;

  constructor(public navParams: NavParams,
    // public contribuableService: ContribuableService,
    // public authServiceProvider: AuthServiceProvider,
    public alertCtrl: AlertController,
    public loading: LoadingController,
    private nav: NavController,
    public http: Http,
    // private syncDataProvider: SyncDataProvider,
    private syncService: SyncDataProvider,
    private loadingCtrl: LoadingController,
    private auth: AuthServiceProvider,
    private contribuableService: ContribuableService,
    private events: Events
  ) {
    this.connectedUser = this.auth.currentUser;

    //fill the natureCreance  from the api.
    this.getNatureCreance();

  }

  connectedUser;
  logout() {
    let loading = this.loadingCtrl.create({
      content: 'Traitement en cours...',
      dismissOnPageChange: true
    });
    loading.present();

    this.events.publish('user:doLogout', Date.now());
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AvisImpositionNonRecuPage');

    this.years = this.getTenYearsFromNow();
  }

  /**
   * get Ten Years From Now.
   */
  getTenYearsFromNow(): number[] {
    var year: number = new Date().getUTCFullYear();
    var years: number[] = [];

    for (var i = 0; i < 10; i++) {

      years[i] = --year;
    }

    return years;
  }

  /**
   *
   */
  goToArticles() {
    this.getSituationFiscaleAvisImposition(this.codenature, this.reference, this.montant, this.annee, this.email)

  }

  private natureCreance: any = [];
  /**
 * get Nature Creance list 
 */
  getNatureCreance() {
    // var foundItem;
    //*
    this.syncService.syncData("NatureCreanceList",
      (res => {
        console.log("res>>>>", res);
        // foundItem = res;
        this.natureCreance = res;
      }),
      () => {
        //Online http ...
        ///... rest of code !!!
        return new Promise((resolve, reject) => {
          //service
          this.contribuableService.getNatureCreance().subscribe(
            data => {

              // foundItem = data.json();
              this.natureCreance = data.json();
              resolve(this.natureCreance);
            },
            err => {
              this.dismissLoading();
              console.error(err);
              reject(err);
              // Utils.showAlert(this.alertCtrl, 'Confirmation pour le paiement onligne ', "Service indisponible !");

            },
            () => {
              console.log('getNatureCreanceList completed');

              this.dismissLoading();
            }
          );
          //END service
        });
      }
    );//
  }


  /**
 * get Nature Creance list 
 */
  _getNatureCreance() {
    // let private syncService: SyncDataProvider,;
    let foundItem;
    Utils.presentLoading(this.loading);

    this.contribuableService.getNatureCreance().subscribe(
      data => {

        foundItem = data.json();
        this.natureCreance = data.json();
        // console.log('FOUND_ITEMS>>>', foundItem);
        // console.log('\n ..foundItems.message= ', data.json().code);
        // if (foundItem.code == "500") {
        //   Utils.showAlert(this.alertCtrl, 'Paiement en ligne', foundItem.message);
        //   console.log('\n ..foundItems.message ', foundItem.message);
        // } else {
        //   // this.openBrowser(String(foundItem.url));
        //   this.showAlertConfirmToPay('Confirmation pour le paiement onligne', foundItem.url);
        //   // this.showAlertConfirmToPayExternalBrowser('Confirmation pour le paiement onligne', foundItem.url);
        //   console.log('URL for paiement>>>', foundItem.url);
        // }

      },
      err => {
        this.dismissLoading();
        console.error(err);
        // Utils.showAlert(this.alertCtrl, 'Confirmation pour le paiement onligne ', "Service indisponible !");

      },
      () => {
        console.log('openPaiementURL completed');

        this.dismissLoading();
      }
    );
    // return this.foundItems;
  }


  /**
   * consulter situation fiscale par avis d'imposition
   * http://api.tgr.gov.ma/api/Contribuables/consulterArticlesARegler?nature={codenature}&reference={reference}&montant={montant]&annee={annee}
   */
  getSituationFiscaleAvisImposition(codenature, reference, montant, annee, email) {

    this.nav.push(ContribuablePage, {
      page: 'avi-imposition-non-recu',
      title: "Avis d'imposition non reçu",
      annee: annee,
      reference: reference,
      montant: montant,
      email: email,
      codenature: codenature
    });

  }//getSituationFiscaleAvisImposition

   ///////////////////////////////////// loader; ////////////////////////////////
  loader; 
  /**
   * Show loading
   */
  dismissed = false;
  presentLoading() {
    this.loader = this.loading.create({
      content: "Chargement des données...",
      showBackdrop: true,
      enableBackdropDismiss: true,
      dismissOnPageChange: true
    });

    this.loader.onDidDismiss(() => {
      console.log('Dismissed loading');
    });

    this.dismissed = false;
    this.loader.present();

    // setTimeout(() => {
    //   this.loader.dismiss();
    // }, 15000);
  }

  /**
   * Dismiss loading
   */
  dismissLoading() {
    console.log('--Dismissed loading--');
    if (!this.dismissed){
      this.dismissed = true;
      this.loader.dismiss();
    }

  }//////////////////////// END loader ///////////////////////////////////////

}
