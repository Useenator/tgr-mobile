//Najim

import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';

import { Chart } from 'chart.js';

/**
* Generated class for the GainPage page.
*
* See http://ionicframework.com/docs/components/#navigation for more info
* on Ionic pages and navigation.
*/
//@IonicPage()
@Component({
  selector: 'page-gain',
  templateUrl: 'gain.html',
})
export class GainPage {

  @ViewChild('chartCanvas') chartCanvas;

  chart: any;
  accountData;
  gLabels = [];
  dataP = [];
  dataN = [];
  mounths = {"1": "janv", "2": "févr", "3": "mars", 
  "4": "avr", "5": "mai", "6": "juin", "7": "juill", 
  "8": "août", "9": "sept", "10": "oct", "11": "nov", "12": "déc"}

  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events) {
    this.accountData = this.navParams.data; 
    console.info("GainPage--> ", this.accountData);

    let size = 0;
    var sortedGraph = this.accountData.graphData;

    //do sorting..
    /* 
    for (let i = 1; i < sortedGraph.length; i++) {
      let iGraph = sortedGraph[i];
      //01-2018
      var iCompare = new Date(Number(iGraph.date.substring(3)) , Number(iGraph.date.substring(0, 2))-1, 1);
      var j = i - 1
      let jGraph = sortedGraph[j];
      var jCompare = new Date(Number(jGraph.date.substring(3)) , Number(jGraph.date.substring(0, 2))-1, 1);
      while (j >= 0 && jCompare > iCompare){
        sortedGraph[j+1] = sortedGraph[j]
          j = j - 1
      }
      sortedGraph[j+1] = iGraph
      
    }//end sorting ! 
    // */

    sortedGraph = sortedGraph.sort(function(a,b){
      // Turn your strings into dates, and then subtract them
      // to get a value that is either negative, positive, or zero.
      let date1 = new Date(Number(a.date.substring(3)) , Number(a.date.substring(0, 2))-1, 1);
      let date2 = new Date(Number(b.date.substring(3)) , Number(b.date.substring(0, 2))-1, 1);
      
      return date1.getTime() - date2.getTime();
    });
    console.log("DATES SORT DONE>> ", sortedGraph);
    

    sortedGraph.forEach((gData) => {
      console.log(gData);
      

      let currentMounth = Number(gData.date.substring(0, 2))
      if(!(this.gLabels.indexOf(this.mounths[currentMounth]+" "+gData.date.substring(3)) > -1)){
        this.gLabels.push(this.mounths[currentMounth]+" "+gData.date.substring(3));
        size++;
      }
      if(gData.sensOperation.code == "CREDIT") this.dataP[size-1] = Number(gData.montant).toFixed(2);
      else this.dataN[size-1] = Number(-1*gData.montant).toFixed(2);
    });
    console.log(this.gLabels, this.dataP, this.dataN);
    
  }

  showIntro = false;

  ionViewDidLoad() {
    console.log('ionViewDidLoad GainPage');
    let ctx = this;
    this.chart = new Chart(this.chartCanvas.nativeElement, {
      type: 'bar',
      options: {
        cutoutPercentage: 90,
        animation: {
            duration: 1000,
            onProgress: (animation)=> {
                /*$progress.attr({
                    value: animation.animationObject.currentStep / animation.animationObject.numSteps,
                });
                  */
            },
            onComplete: (animation)=> {
                console.log('onAnimationComplete ', animation);
                ctx.events.publish('graph:loadingFinish', Date.now());
               //* 
                //*/
            }
        },
        legend: {
          labels: {
                fontColor: "white",
            },
          display: false
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: function(tooltipItem) {
              return tooltipItem.yLabel;
            }
          }
        },
        responsive: true,
        scales: {
          xAxes: [{
            ticks: {
                  fontColor: "#2f2f2f"
              },
            stacked: true,
            barThickness : 20
          }],
          yAxes: [{
            ticks: {
                    fontColor: "#2f2f2f"
                },
            stacked: true
          }]
        }
      },
      data: {
        labels: this.gLabels,
        datasets: [
          {
            backgroundColor: 'rgba(202, 162, 92, 0.65)',
            data: this.dataP
          }, {
            backgroundColor: 'rgba(136, 136, 136, 0.65)',
            data: this.dataN
          }]

        }

      });//end chart one !


    }

  }
