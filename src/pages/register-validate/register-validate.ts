import { Component } from '@angular/core';
// import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { UsersService } from '../../providers/users.service/users.service';

import { LoginPage } from '../login/login';
import { Utils } from '../../MyUtils/Utils';

// import 'rxjs/add/operator/map'
import { Observable } from 'rxjs/Rx';
/**
 * Generated class for the RegisterValidatePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
//@IonicPage()
@Component({
  selector: 'page-register-validate',
  templateUrl: 'register-validate.html',
  providers: [UsersService]
})
export class RegisterValidatePage {

  private codeValidation = "";
  private login = "";
  private password = "";
  // private myUtils;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    //private myUtils: MyUtilsProvider,

    public loading: LoadingController,
    private usersService: UsersService,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterValidatePage');
  }

  validateAccount() {
    // this.isValid = false;
    let postOBJValidation = {
      "login": this.login.trim(),
      "passwd": this.password.trim(),
      "codeActivation": this.codeValidation.trim()
    };
    console.log("validateAccount() ", postOBJValidation.passwd);
    ///////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
    var foundData;
         this.presentLoading();
    this.usersService.validateAccount(postOBJValidation).subscribe(
      data => {
        console.log('\n validate - postOBJValidation .. BEFORE !!! ', JSON.stringify(postOBJValidation));
        console.log('DATA ', data);
        foundData = data;//.json();
        // this.showAlert("", "Compte activé");
        if (foundData.user.id) {
          console.log('\n validate - postOBJValidation ..!!! ', postOBJValidation);
          console.log('\n validateAccount ..!!! ', data);
          this.showAlert("", "Compte activé");
          // this.dismissLoading();

          //redirect to login page.
          this.navCtrl.setRoot(LoginPage);
        }// else 
        if (foundData.user as boolean == true) {
          console.log('\n validate - postOBJValidation ..!!! ', postOBJValidation);
          console.log('\n validateAccount ..!!! ', data);
          this.showAlert("", "Compte activé");
          // this.dismissLoading();

          //redirect to login page.
          this.navCtrl.setRoot(LoginPage);
        }// else 
        if (foundData.user as boolean == false) {
          console.log('\n validateAccount ERROR ..!!! ', data);
          this.showAlert("", "Veuillez vérifier vos identifiants et/ou code de validation.");
        }//END if

      },
      err => {
        console.error(err);
        if (err.status == 400) {
          this.showAlert("Nous sommes désolés!", "Merci de vérifier les informations saisies !");
        }
        if (err.status == 422) {
          this.showAlert("", "le compte est déja validé !");
        }
        else {
          //this.showAlert("Nous sommes désolés!", "Service  indisponible veuillez réessayer ultérieurement");
        }
        // errr(err);
        //show a nice error message to the user
        // this.showAlert("Nous sommes désolés!", "Service  indisponible veuillez réessayer ultérieurement");
        // this.myUtils.dismissLoading();
        this.dismissLoading();
      },
      () => {
        console.log('validateAccountData completed')
        this.dismissLoading();
        // return true;
      });//subscrib
    // this.presentLoading();
  }

  // private loader;
 ///////////////////////////////////// loader; ////////////////////////////////
  private loader;
  /**
   * Show loading
   */
  presentLoading() {
    this.loader = this.loading.create({
      content: "Chargement des données...",
      showBackdrop: true,
      enableBackdropDismiss: true,
      dismissOnPageChange: true
    });

    this.loader.onDidDismiss(() => {
      console.log('Dismissed loading');
    });

    this.loader.present();

    // setTimeout(() => {
    //   this.loader.dismiss();
    // }, 15000);
  }

  /**
   * Dismiss loading
   */
  dismissLoading() {
    console.log('--Dismissed loading--');
    if (!this.loader.Dismissed) this.loader.dismiss();

  }//////////////////////// END loader ///////////////////////////////////////
  // presentLoading() {
  //   this.loader = this.loadingCtrl.create({
  //     content: "Chargement des données",
  //     showBackdrop: true,
  //     enableBackdropDismiss: true,
  //     dismissOnPageChange: true
  //   });

  //   this.loader.onDidDismiss(() => {
  //     console.log('Dismissed loading');
  //     //duplicated : app event
  //     //if(this.isValid) this.navCtrl.setRoot(AccountDetailPage);
  //   });

  //   this.loader.present();
  // }

  // dismissLoading() {
  //   console.log('--Dismissed loading--');
  //   this.loader.dismiss();
  // }
  /**
   * reuseable Alert box.
   */
  showAlert(title, message) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

}
