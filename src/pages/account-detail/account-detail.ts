//Najim

import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, Slides, Events } from 'ionic-angular';

import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { MyUtilsProvider } from '../../providers/my-utils/my-utils';
import { Config } from '../../MyUtils/Config';

import { GainPage } from '../gain/gain';
import { BalancePage } from '../balance/balance';
import { AccountsPage } from '../accounts/accounts';
import { HistoryPage } from '../history/history';
import { HistoryByDatePage } from '../history-by-date/history-by-date';

/**
* Generated class for the AccountDetailPage page.
*
* See http://ionicframework.com/docs/components/#navigation for more info
* on Ionic pages and navigation.
*/
//@IonicPage()
@Component({
  selector: 'page-account-detail',
  templateUrl: 'account-detail.html',
})
export class AccountDetailPage {

  selectedAccount: any = { rib: "", balance: "", identite: "" };
  accountsData = [];


  @ViewChild(Slides) slides: Slides;
  goToSlide(idx: number) {
    console.log("go to slide>>", idx);
    this.slides.slideTo(idx, 700);
  }
  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();
    console.log('Current index is', currentIndex, "selectedAccount: ", this.accountsData[currentIndex]);
    if (this.myUtils.checker(this.accountsData[currentIndex])) {
      this.selectedAccount = this.accountsData[currentIndex];
    }
  }

  tab_gain = GainPage;
  tab_balance = BalancePage;

  pageTitle: string = "Gain/dépenses";

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private auth: AuthServiceProvider,
    private myUtils: MyUtilsProvider,
    public events: Events) {

    //check if theres param passed 

    if (this.navParams.get('selectedAccount')) {
      this.selectedAccount = this.navParams.get('selectedAccount');
      console.log("selectedAccount", this.selectedAccount)
    }

    events.subscribe('graph:loadingFinish', (date) => {
      if (this.firstTime) {
        this.goToSlide(this.slideToGoIdx);
        this.firstTime = false;
      }
    });

  }

  ionViewDidLoad() {
    this.fillData();
  }

  showIntro = false;
  firstTime = true;
  slideToGoIdx: number = 0;

  //get data(name, rib, montant..) to display on UI !
  fillData() {
    let context = this;
    var isTokenExpired = false;

    this.presentLoading();
    console.log('ionViewDidLoad AccountDetailPage');
    //you can use derictly this.auth.currentUser
    this.auth.getLDAPData().then(LDAPData => {
      console.log(LDAPData);
      if (LDAPData) {
        let refs = LDAPData.norib;
        console.log(refs);
        this.auth.getUserAccess()
          .then(
          userData => {
            let userid = userData.userid;
            this.auth.getTokenData()
              .then(
              tokenData => {
                let token = tokenData["id"];
                if (refs) {
                  let ribsSize = refs.length;
                  let i = 0;
                  for (let idx = 0; idx < refs.length; idx++) {
                    const rib = refs[idx];

                    i++;
                    if(!isTokenExpired)
                    this.myUtils.doGet(Config.getSoldeBancaireApi(userid, rib, token))
                      .then((responce) => {
                        console.log("getSoldeBancaireApi", responce);

                        this.firstTime = true;
                        try {
                          if (responce["_body"] && JSON.parse(responce["_body"]).soldeBancaire) {
                            console.log("responce OK !");


                            let balance = "";
                            if (JSON.parse(responce["_body"]).soldeBancaire) if (JSON.parse(responce["_body"]).soldeBancaire.solde)
                              if (JSON.parse(responce["_body"]).soldeBancaire.solde.solde.disponibleNet)
                                balance = JSON.parse(responce["_body"]).soldeBancaire.solde.solde.disponibleNet;
                            let toPush: any = {
                              rib: rib,
                              balance: balance,
                              identite: JSON.parse(responce["_body"]).soldeBancaire.identite
                            };


                            if(!isTokenExpired)
                            this.myUtils.doGet(Config.getGraphGainsDepensesAPI(userid, rib, token))
                              .then(res => {
                                console.warn(res);
                                if (res['_body']) {
                                  let gData = JSON.parse(res['_body']);
                                  toPush.graphData = (gData.graphes) ? ((gData.graphes.graphes) ? gData.graphes.graphes : []) : [];

                                }
                                console.info("info toPush", toPush);
                                checkAndPush((i == ribsSize), toPush);
                              },
                              err => {
                                context.dismissLoading();
                                console.error(err);
                              })
                          }
                        } catch (error) {
                          console.error(error);
                        }

                      })
                      .catch((err) => {
                        console.log(err);
                        if (err.status == 401) {

                          this.logout(() => {
                            this.showAlert('',
                              (JSON.parse(err["_body"]).error && JSON.parse(err["_body"]).error.message) ? JSON.parse(err["_body"]).error.message : 'Erreur 401!');
                          });
                          isTokenExpired = true;
                        }

                        console.log("dismiss ************");

                        context.dismissLoading();

                      });
                      if(isTokenExpired) break;
                  }
                } else this.dismissLoading();
              })
          }
          );
      } else this.dismissLoading();
    });//end get accounts (rib & balance!)


    function checkAndPush(isFinish: boolean, toPush: any) {
      console.log("PUSH ACCOUNT");

      context.accountsData.push(toPush);
      if (isFinish) {
        console.log("IS FINISH");
        context.dismissLoading();
        if (context.selectedAccount && context.selectedAccount.rib) {
          for (var idx = 0; idx < context.accountsData.length; idx++) {
            if (context.accountsData[idx].rib == context.selectedAccount.rib) {
              console.log("is selectedAccount>>", context.selectedAccount);
              //context.goToSlide(idx);
              context.slideToGoIdx = idx;
              break;
            }
          }
          if (context.accountsData && context.accountsData.length > 1) {
            console.log("show intro !");

            context.showIntro = true;
            setTimeout(() => {
              context.showIntro = false;
            }, 2200);
          }
        } else {


          context.selectedAccount = context.accountsData[0];
          console.log("no selectedAccount>>", context.selectedAccount);
          //context.goToSlide(0);
          context.slideToGoIdx = 0;
        }
      }
    }
  }

  showAlert(title, message) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }
  private loader;
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Chargement des données...",
      showBackdrop: true,
      enableBackdropDismiss: true
    });
    /*
    this.loader.onDidDismiss(() => {
      console.log('Dismissed loading');
    });*/

    this.dismissed = false;
    this.loader.present();
  }

  dismissed = false;
  dismissLoading() {
    console.log('--Dismissed loading--');
    if (!this.dismissed) this.loader.dismiss();
    this.dismissed = true;

  }

  selectedTab(tabIdx) {
    console.log(">>", tabIdx);
    if (tabIdx == 1) {
      this.pageTitle = "Gain/dépenses";
    } else {
      this.pageTitle = "Solde";
    }
  }

  logout(cbFinish?) {
    if (this.auth.currentUser) {
      let loading = this.loadingCtrl.create({
        content: 'Traitement en cours...',
        dismissOnPageChange: true,
        showBackdrop: true,
        enableBackdropDismiss: true
      });
      loading.present();
      var dismissed = false;

      this.events.publish('user:doLogout', Date.now());
      var isCbExec = false;
      if (cbFinish) this.events.subscribe('user:logout', (xdate) => {
        if (!dismissed) {
          loading.dismiss();
          dismissed = true;
        }
        if (!isCbExec) {
          isCbExec = true;
          setTimeout(() => {
            cbFinish()
          }, 1000);
          
        };

      });
    }
  }

  openHistory() {
    this.navCtrl.push(HistoryPage, { selectedAccount: this.selectedAccount });
  }

  openBetween() {
    this.navCtrl.push(HistoryByDatePage, { selectedAccount: this.selectedAccount });
  }

  openAccounts() {
    this.navCtrl.push(AccountsPage);
  }

  formatedFLoat(str) {
    let res = String(parseFloat(str).toFixed(2)).replace('.', ',');
    if (res == 'NaN') res = "";
    return res;
  }

}
