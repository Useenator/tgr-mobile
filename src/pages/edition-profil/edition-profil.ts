import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AlertController, LoadingController } from 'ionic-angular';

import { Utils } from '../../MyUtils/Utils';

import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

import { UsersService } from '../../providers/users.service/users.service';

/**
 * Generated class for the EditionProfilPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
//@IonicPage()
@Component({
  selector: 'page-edition-profil',
  templateUrl: 'edition-profil.html'
  // ,providers: [UsersService, AuthServiceProvider]
})
export class EditionProfilPage {
  private data;
  private userID;
  private password;
  private email;
  private lastName;
  private firstName;
  // constructor(public navCtrl: NavController, public navParams: NavParams) {
  // }
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private usersService: UsersService,
    public alertCtrl: AlertController,
    private nav: NavController,
    public loadingCtrl: LoadingController,
    private auth: AuthServiceProvider,
    public loading: LoadingController,
  ) {
    this.connectedUser = this.auth.currentUser;
    console.log("CONNECTED_USER",this.connectedUser);
    if (this.connectedUser) {
      this.userID = this.connectedUser.userAccess.userid;
      this.password = this.connectedUser.userAccess.password;
      // this.token = this.connectedUser.tokenData.id;
      this.email = this.connectedUser.LDAPData.email;

      this.firstName = this.connectedUser.LDAPData.firstName;
      this.lastName = this.connectedUser.LDAPData.lastName;
    }
    /*
      {id: "CompteDemo", username: "CompteDemo", title: "No", cin: "1111111", norib: Array(2), …}
    */
    // this.connectedUser = this.auth.currentUser;

    this.data = {};
    this.data.nom = '';
    this.data.prenom = '';
    this.data.raisonSociale = '';
    this.data.cin = '';
    this.data.rc = '';
    this.data.mail = '';
    this.data.mobile = '';
    this.data.fixe = '';
    this.data.activite = '';
    this.data.adresse = '';
    this.data.login = '';
    this.data.password = '';
    this.data.ppr = '157814';
    this.data.codeContribuables = [];
    this.data.ribs = [];
    this.data._etatAdherent = {
      "dateEtat": "2017-05-25T14:28:05.862Z",
      "typeEtatId": 1
    };
    this.data.cin = '';

    this.data.password = '';
    //Contribuable
    this.data.reference_avis = '85595613';
    this.data.montant_avis = '162.9';

    //Fonctionnaire
    // this.data.ppr = '157814';
    this.data.net_mensuel = '3468';
    this.data.derniere_entree = '1981-01-02';

    this.data.response = '';
    // this._http = _http;

    console.log(" JSON DATA ", JSON.stringify(this.data));
  }
  private connectedUser;

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditionProfilPage');
  }

  /**
   * 
   * @param adherant 
   */
  modifyAdherent(adherant) {
    Utils.presentLoading(this.loading);
    this.usersService.modifyAdherentData(adherant).subscribe(
      data => {
        console.log("ADHERANT", data.json());
      },
      err => {
        console.log("ADHERANT ERROR !!", err);
        Utils.dismissLoading();
      },
      () => {
        Utils.dismissLoading();
      });
  }

}
/*
        import { Utils } from '../../MyUtils/Utils';

        private usersService: UsersService,
        public alertCtrl: AlertController,
        private nav: NavController,
        public loadingCtrl: LoadingController,
        private auth: AuthServiceProvider,
        public loading: LoadingController,

        Utils.dismissLoading();

        Utils.showAlert(this.alertCtrl, 'Confirmation pour le paiement onligne ', "Service indisponible !");
        
        Utils.dismissLoading();
*/