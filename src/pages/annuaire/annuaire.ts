///Ahmed

import { Component } from '@angular/core';
import { IonicPage, NavController, Events, NavParams, AlertController, LoadingController, reorderArray } from 'ionic-angular';

import { AnnuaireServiceProvider } from '../../providers/annuaire-service/annuaire-service';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { MyUtilsProvider } from '../../providers/my-utils/my-utils';
import { Config } from '../../MyUtils/Config';

import { SyncDataProvider } from "../../providers/sync-data/sync-data";

import { Utils } from '../../MyUtils/Utils';
import { AnnuaireParNomPage } from '../annuaire-par-nom/annuaire-par-nom';
import { AnnuaireDetailPage } from '../annuaire-detail/annuaire-detail';

// syncOFFLIGNE
// import {loopbacks} from '../../lbclient/lbclient';
// / <reference path="node_modules/xml2jse/xml2js.d.ts" />
// import lbclient from "../../lbclient/lbclient";
//import * as vvv from "../../lbclient/lbclient";
// import model = require('../../lbclient/lbclient');
// import model = require('../../lbclient/lbclient');
/**
 * Generated class for the AnnuairePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

// ( new vvv)
// (new loopbacks)


//@IonicPage()
@Component({
  selector: 'page-annuaire',
  templateUrl: 'annuaire.html',
})
export class AnnuairePage {

  public foundResponsibles = [];

  public foundResponsiblesAll = []
    ;

  public position = {
    "lng": 0,
    "lat": 0
  };
  public queryText = "";
  public CENTRAL = 'central';
  public REGIONAL = 'regional';
  public segment = this.CENTRAL;//regionaux centraux

  private myUtils;
  public loader;
  public showThis = true;
  public foundCentralDirections;
  public foundRegionalDirections;
  //OffLine
  NUMBER_OF_DAYS_OFFLINE = 30;

  //Will hold selected Direction ; Bound to the select in view
  public selectedRegionalDirections;
  public selectedCentralDirection;

  constructor(private annuaireService: AnnuaireServiceProvider,
    public alertCtrl: AlertController,
    private nav: NavController,
    private loadingCtrl: LoadingController,
    private auth: AuthServiceProvider,
    private syncService: SyncDataProvider,

    //    public loading: LoadingController,
    private events: Events
  ) {


    //Will hold found Directions ; Bound to the select in the view
    //this.foundCentralDirections = this.getCentralDirectionList();
    this.getCentralDirectionList();
    this.foundRegionalDirections = this.getRegionalDirectionList();

    // this.getReponsableByResponsibleName("mohamed");
    //  this.selectedRegionalDirections.code='1063';
    //  this.selectedCentralDirection.code='N500';

  }

  logout() {
    let loading2 = this.loadingCtrl.create({
      content: 'Traitement en cours...',
      dismissOnPageChange: true
    });
    loading2.present();

    this.events.publish('user:doLogout', Date.now());
  }

  /**
   * Reorder a given list.
   */
  reorderItem(indexes) {
    this.foundResponsibles = reorderArray(this.foundResponsibles, indexes);
  }

  /**
   * specific to angular 2 .. trigger on the init of the component.
   */
  ngOnInit() { }//

  /**
   * available before and after the page in question becomes active
   */
  ionViewWillEnter() { // THERE IT IS!!!
    //return this.service.getComments().then(data => this.comments = data);
  }

  ionViewDidLoad() {
    this.updateResponsables();
  }

  /**
   * Fetch central direction list
   
  getCentralDirectionListWithSync(foundCentralDirections) {
    // var foundCentralDirections;

    // this.annuaireService.getCentralDirectionsList().subscribe(
    //   data => {
    //     this.foundCentralDirections = data.json().directionsCentrales[0].postes;
    //     foundCentralDirections = data.json().directionsCentrales[0].postes;
    //   },
    //   err => console.error(err),
    //   () => console.log('getCentral list completed')
    // );

    this.syncList("CentralDirectionList", this.foundCentralDirections, (ctx, foundCentralDirections) => {
      return new Promise((resolve, reject) => {
        // varfoundCentralDirections;
        ctx.annuaireService.getCentralDirectionsList().subscribe(
          data => {
            ctx.foundCentralDirections = data.json().directionsCentrales[0].postes;
            resolve(ctx.foundCentralDirections);
            // foundCentralDirections = data.json().directionsCentrales[0].postes;
          },
          err => {
            console.error(err);
            reject(err);
          },
          () => console.log('getCentral list completed')
        );
        //return foundCentralDirections;
        // }//

      });//Promise

    });//END Sync
  }//END getCentralDirectionList
  */

  /**
 * Fetch central direction list
 */
  getCentralDirectionList() {
    var foundCentralDirections;
    //*
    this.syncService.syncData("CentralDirectionList",
      (res => {
        console.log("res>>>>", res);
        this.foundCentralDirections = res;
      }),
      () => {
        //Online http ...
        //this.annuaire.getListFromHttp()
        ///... rest of code !!!
        return new Promise((resolve, reject) => {
          this.annuaireService.getCentralDirectionsList().subscribe(
            data => {
              //this.foundCentralDirections = data.json().directionsCentrales[0].postes;
              //foundCentralDirections = data.json().directionsCentrales[0].postes;
              resolve(data.json().directionsCentrales[0].postes);
            },
            err => {
              console.error(err)
              reject(err);
            },
            () => console.log('getCentral list completed')
          );
        });
      }
    );//*/
    /*this.annuaireService.getCentralDirectionsList().subscribe(
      data => {
        this.foundCentralDirections = data.json().directionsCentrales[0].postes;
        foundCentralDirections = data.json().directionsCentrales[0].postes;
      },
      err => console.error(err),
      () => console.log('getCentral list completed')
    );*/
    //return foundCentralDirections;

  }//END getCentralDirectionList


  /**
   * Fetch regional direction list
   */
  getRegionalDirectionList() {
    var foundRegionalDirections;
    this.syncService.syncData("RegionalDirectionList",
      (res => {
        console.log("res>>>>", res);
        this.foundRegionalDirections = res;
      }),
      () => {
        //Online http ...
        //this.annuaire.getListFromHttp()
        ///... rest of code !!!
        return new Promise((resolve, reject) => {

          this.annuaireService.getRegionalDirectionsList().subscribe(
            data => {
              // this.foundRegionalDirections = data.json().directionsRegionales[0].postes;
              // foundRegionalDirections = data.json().directionsRegionales[0].postes;
              resolve(data.json().directionsRegionales[0].postes);
            },
            err => {
              console.error(err)
              reject(err)
            },
            () => console.log('getRegional list completed')

          );
        });
      }
      /*Number of days to cash data*/ //this.NUMBER_OF_DAYS_OFFLINE
    );//*/
  }//End getRegionalDirectionList
  /**
   * Fetch regional direction list
   */
  getRegionalDirectionList_deprecated() {
    var foundRegionalDirections;
    this.annuaireService.getRegionalDirectionsList().subscribe(
      data => {
        this.foundRegionalDirections = data.json().directionsRegionales[0].postes;
        foundRegionalDirections = data.json().directionsRegionales[0].postes;
      },
      err => console.error(err),
      () => console.log('getRegional list completed')
    );
    return foundRegionalDirections;
  }//End getRegionalDirectionList


  /**
   * Fetch central direction list
   */
  _foundCentralDirections;

  //////
  // syncList("CentralDirectionList",this.foundCentralDirections,callback );

  syncList(CentralDirectionList, foundCentralDirections, callback) {

  };

  //END getCentralDirectionList




  /**
   * Update the selected value in the view when a user select an other option
   */
  updateSelectedValue() {
    console.log("DR Regional " + this.selectedRegionalDirections);
    console.log("DR Central " + this.selectedCentralDirection);

    var directionCode = 'N500';
    var directionObj = {};
    if (this.segment === this.CENTRAL) {
      directionCode = this.selectedCentralDirection;
      console.log("this.selectedCentralDirection.code " + this.selectedCentralDirection);
    }
    if (this.segment === this.REGIONAL) {

      directionCode = this.selectedRegionalDirections;
      console.log("POSITION= " + JSON.stringify(this.getPosition(directionCode)[0].position));
    }
    //get the search word
    let searchWord = this.queryText.toString();
    //fill the listView with the convenient responsables
    console.log("fillListViewWithResponsables(searchWord= " + searchWord + " directionCode= " + directionCode);
    this.getReponsableByDirection(searchWord, directionCode);

  }//updateSelectedValue

  /**
   *
   */
  getPosition(directionCode) {
    //let code = this.foundRegionalDirections;

    return this.foundRegionalDirections.filter((item) => {
      return item.code.toLowerCase().indexOf(directionCode.toLowerCase()) > -1;
    });
  }

  /**
   * fill the listView with the convenient responsables by direction
   * @param searchWord
   * @param directionCode
   * @returns {any[]}
   */
  getReponsableByDirection_deprecated(searchWord, directionCode) {
    let resps;
    let loader = this.loadingCtrl.create({
      content: 'Chargement des données ...'//,
      , duration: 10000//,
      , dismissOnPageChange: true
      , enableBackdropDismiss: true
    });

    loader.present().then(() => {

      this.annuaireService.getReponsableByDirection(directionCode).subscribe(
        data => {
          let obj = data.json();
          this.foundResponsibles = obj.responsables; //[0].responsable;
          this.foundResponsiblesAll = this.foundResponsibles;
          console.log('\n Reeeeeesps \n' + JSON.stringify(this.foundResponsibles));
          loader.dismiss();
        },
        err => {
          console.error("fillListViewWithResponsables Error!! " + err);
          this.showAlert('Tgr Mobile', 'Service indisponible lié à la connexion internet');
          loader.dismiss();
        }
      );//End service
      //loader.dismiss();
    });//END loader
    ///////////////////////////////////////////End loader
    return [];
  }

  /**
 * fill the listView with the convenient responsables by direction
 * @param searchWord
 * @param directionCode
 * @returns {any[]}
 */
  getReponsableByDirection(searchWord, directionCode) {
    //let resps;
    //------ offLine------
    this.syncService.syncData(directionCode,
      (res => {
        console.log("res>>>>", res);
        this.foundResponsibles = res;
      }),
      () => {
        //Online http ...
        //this.annuaire.getListFromHttp()
        ///... rest of code !!!
        return new Promise((resolve, reject) => {
          //-----------offLine------*/
          Utils.presentLoading(this.loadingCtrl);
          console.log('\n directionCode \n' + directionCode);
          this.annuaireService.getReponsableByDirection(directionCode).subscribe(
            data => {
             try {
              let obj = data.json();
              this.foundResponsibles = obj.responsables; //[0].responsable;
              this.foundResponsiblesAll = this.foundResponsibles;
              //offLine
              resolve(this.foundResponsibles);
              //END offLine
              this.foundResponsiblesAll = this.foundResponsibles;
              console.log('\n Reeeeeesps \n' + JSON.stringify(this.foundResponsibles));
              Utils.dismissLoading();
             } catch (error) {
              Utils.dismissLoading();
              reject(error);
             }
            },
            err => {
              console.error("fillListViewWithResponsables Error!! " + err);
              this.showAlert('Tgr Mobile', 'Service indisponible lié à la connexion internet');
              //---------offLine ------
              reject(err);

              Utils.dismissLoading();
            }
          );//End service
          //------END offLine------
        });
      }
      /*Number of days to cash data*/, this.NUMBER_OF_DAYS_OFFLINE);//----END offLine------*/
    //loader.dismiss();
    //  }
    // );//END loader
    ///////////////////////////////////////////End loader
    return [];
  }

  /**
   * 
   * ex: https://api.tgr.gov.ma/api/Postes/chercherPostes?poste=taza
   * @param searchWord 
   * @param directionCode 
   */
  getReponsableByPoste(posteName, finishCB, isFound?) {
    var thisPosteName = posteName.trim();
    if (typeof thisPosteName != "undefined" && thisPosteName != "") {

      this.annuaireService.getReponsableByPosteName(thisPosteName).subscribe(
        data => {
          finishCB()
          try {
            let obj = data.json();
            var foundResponsiblesByPoste = obj.responsables; //[0].responsable;

            if (this.foundResponsibles && typeof this.foundResponsibles !== 'string' && foundResponsiblesByPoste && typeof foundResponsiblesByPoste !== 'string') {

              this.foundResponsibles = this.foundResponsibles.concat(foundResponsiblesByPoste);
              // this.foundResponsiblesAll = this.foundResponsibles;

            } else {
              console.warn('\n if (this.foundResponsibles && foundResponsiblesByPoste && typeof foundResponsiblesByPoste != string \n' + JSON.stringify(this.foundResponsibles));

              if (typeof foundResponsiblesByPoste === 'string') {
                console.warn('==>foundResponsiblesByPoste=="postes non trouvés!" ');
                if(!isFound)
                    Utils.showAlert(this.alertCtrl,"","aucun résultat trouvé!");

              }
            }
            console.log('\n Reeeeeesps_ByPoste + byName \n' + JSON.stringify(this.foundResponsibles));
          } catch (error) {
            console.error(error)
          }
        },
        err => {
          console.error("getReponsableByPosteName Error!! ", err);
          finishCB()
          if(!isFound)
              this.showAlert('', 'Poste non trouvé.');//TODO: Nous ne pouvons pas nous connecter au serveur !!');
        }
      );//End service
    }//END if
  }

  /**
  * get Reponsable By Responsible Name 
  * @param responsibleName 
  */
  getReponsableByName(responsibleName, finishCB) {
    responsibleName = responsibleName.trim();
    if (typeof responsibleName != "undefined" && responsibleName != "") {

      this.annuaireService.getReponsableByResponsibleName(responsibleName).subscribe(
        data => {
          try {
            let obj = data.json();
            var isFound = false
            if (typeof obj.responsables === 'string') {
              console.warn('==>this.foundResponsibles=="responsables non trouves!" ');
              //Utils.showAlert(this.alertCtrl, "", "aucun résultat trouvé");
              //return;
            } else {
              isFound = true
              this.foundResponsibles = obj.responsables; //[0].responsable;
              // this.foundResponsiblesAll = this.foundResponsibles;
              console.log('\n Reeeeeesps_ByResponsibleName \n' + JSON.stringify(this.foundResponsibles));

            }
            
            //then call
            this.getReponsableByPoste(this.queryText, () => {
              finishCB()
            }, isFound);///////////////////////////////////////////////////////////////

          } catch (error) {
            console.error(error)
            //then call
            this.getReponsableByPoste(this.queryText, () => {
              finishCB()
            }, false);//////////////////////////////////////////////////////////////////
          }
        },
        err => {
          console.error("getReponsableByResponsibleName Error!! " + err);
          //this.showAlert('', 'Responsable non trouvé.');//TODO: Nous ne pouvons pas nous connecter au serveur !!');
          //then call
          this.getReponsableByPoste(this.queryText, () => {
            finishCB()
          }, false);///////////////////////////////////////////////////////////////////////
        }
      );//End service
    }//END if
  }
  /**
   * Push annuaire-par-nom page to the navigation stack.
   */
  goToResponsibleByName() {
    // this.nav.push(AnnuaireParNomPage, { user: "user", position: this.position });


    Utils.presentLoading(this.loadingCtrl);
    this.getReponsableByName(this.queryText, () => {
      Utils.dismissLoading();
    });
    //this.getReponsableByPoste(this.queryText);
  }


  /**
   * filter the responsables list and show it il the UI listView
   */

  title = "Annuaire - Services centraux";
  segmentUpdateResponsables() {
    console.log(this.segment);
    if (this.segment === "central") {
      this.title = "Annuaire - Services centraux";
    } else {
      this.title = "Annuaire - Services déconcentrés";
    }

    this.foundResponsiblesAll = this.foundResponsibles = [];
    //this.selectedCentralDirection = "";

    //this.foundResponsibles = this.dataService.filterItems(this.searchTerm);
    this.updateResponsables();
    this.showDirectionsList();

    //update the listview when the segment change
    if (typeof this.selectedCentralDirection != 'undefined' &&
      typeof this.selectedRegionalDirections != 'undefined') {
      this.updateSelectedValue();//typeof x === 'undefined')
    }
    //*/
  }

  /**
   * filter the responsible list and show it in the Ui listView
   */
  updateResponsables() {
    //TODO this.foundResponsibles = this.annuaireService.filterItems(this.queryText, this.segment);
    this.foundResponsibles = this.filterItems(this.queryText, this.segment);
    console.log('\n this.foundResponsibles \n' + JSON.stringify(this.foundResponsibles));
  }

  /**
   * Set the {ShowThis} flag for the UI segment to show .
   */
  private showDirectionsList() {
    if (this.showThis && this.REGIONAL === this.segment) {
      this.showThis = false;
    }
    if (!this.showThis && this.CENTRAL === this.segment) {
      this.showThis = true;
    }
  }

  /**
   * Filter the listView with items .. by search term
   */
  filterItems(searchTerm, segment) {
    /*
    try {
      this.foundResponsibles = this.foundResponsiblesAll;
  
      console.log('\n RESPS foundResponsibles \n' + JSON.stringify(this.foundResponsibles));
      console.log('\n RESPS foundResponsiblesAll \n' + JSON.stringify(this.foundResponsiblesAll));
      //******************************* Filter by name or entity name *********************************
      if (segment === this.REGIONAL) {
        return this.foundResponsibles.filter((item) => {
          return item.libelle.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
             || item.responsable.lastName.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
             //|| item.responsable[0].lastName.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
            ;
  
        });
      }
  
      if (segment === this.CENTRAL) {
        return this.foundResponsibles.filter((item) => {
          return item.libelle.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
            // || item.responsable.firstName.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
            // || item.responsable.lastName.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
            ;
        });
      }
  
    } catch (err) {
      console.error("filterItems Error!! " + err);
    }*/

    //try{
    this.foundResponsibles = this.foundResponsiblesAll;

    if (segment === this.REGIONAL) {
      return this.foundResponsibles.filter((item) => {
        console.log("iem>>", item);
        console.log(item.responsable);
        console.log(item.responsable.lastName);

        let conditionLastName = false;
        if (typeof item.responsable != 'undefined' && item.responsable != null && item.responsable != "") {
          conditionLastName = item.responsable.lastName.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        }

        let conditionFirstName = false;
        if (typeof item.responsable != 'undefined' && item.responsable != null && item.responsable != "") {
          conditionFirstName = item.responsable.firstName.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        }

        let conditionTitle = false;
        if (typeof item.responsable != 'undefined' && item.responsable != null && item.responsable != "") {
          conditionTitle = item.responsable.title.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        }


        if (item.libelle.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
          || conditionLastName || conditionFirstName || conditionTitle) {
          return true;
        }
        ;
      });
    }

    if (segment === this.CENTRAL) {
      return this.foundResponsibles.filter((item) => {
        console.log("iem>>", item);
        console.log(item.responsable);
        console.log(item.responsable.lastName);

        let conditionLastName = false;
        if (typeof item.responsable != 'undefined' && item.responsable != null && item.responsable != "") {
          conditionLastName = item.responsable.lastName.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        }

        let conditionFirstName = false;
        if (typeof item.responsable != 'undefined' && item.responsable != null && item.responsable != "") {
          conditionFirstName = item.responsable.firstName.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        }

        let conditionTitle = false;
        if (typeof item.responsable != 'undefined' && item.responsable != null && item.responsable != "") {
          conditionTitle = item.responsable.title.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        }


        if (item.libelle.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
          || conditionLastName || conditionFirstName || conditionTitle) {
          return true;
        }
        ;
      });
    }
    /*
  } catch (err) {
    console.error("filterItems Error!! " + err);
  }
  */

  }//End updateResponsables

  /**
   * Push the details page to the navigation stack .. show Annnuaire details.
   */
  goToDetails(user) {
    this.nav.push(AnnuaireDetailPage, { user: user, position: this.position });
  }



  //private loader;
  /**
 * Show loading
 */
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Chargement des données...",
      showBackdrop: true,
      enableBackdropDismiss: true,
      // dismissOnPageChange: true
    });

    this.loader.onDidDismiss(() => {
      console.log('Dismissed loading');
    });

    this.loader.present();

    // setTimeout(() => {
    //   this.loader.dismiss();
    // }, 15000);
  }

  /**
   * Dismiss loading
   */
  dismissLoading() {
    console.log('--Dismissed loading--');
    if (!this.loader.Dismissed) this.loader.dismiss();

  }
  /**
   * reuseable Alert box.
   */
  showAlert(title, message) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

  doMail(email) {
    window.open(`mailto:${email}`, '_system');
  }

  doCall(phoneNumber) {
    window.open(`tel:${phoneNumber}`, '_system');
  }

  doSMS(phoneNumber) {
    window.open(`sms:${phoneNumber}`, '_system');
  }
}
