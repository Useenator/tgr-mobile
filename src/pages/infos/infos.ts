import { Component } from '@angular/core';
import {
  IonicPage, NavController, NavParams, ModalController, ViewController,
  LoadingController, Events
} from 'ionic-angular';

import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

import { MyUtilsProvider } from "../../providers/my-utils/my-utils";

/**
 * Generated class for the InfosPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
//@IonicPage()
@Component({
  selector: 'page-infos',
  templateUrl: 'infos.html',
})
export class InfosPage {

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    private auth: AuthServiceProvider,
    private loadingCtrl: LoadingController,
    public events: Events) {

    this.connectedUser = this.auth.currentUser;
  }

  connectedUser;
  logout() {
    let loading = this.loadingCtrl.create({
      content: 'Traitement en cours...',
      dismissOnPageChange: true
    });
    loading.present();

    this.events.publish('user:doLogout', Date.now());
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InfosPage');
  }

  presentFAQModal() {
    let modal = this.modalCtrl.create(FAQPage);
    modal.present();
  }

  presentSecuriteModal() {
    let modal = this.modalCtrl.create(SecuritePage);
    modal.present();
  }

  presentMentionslegalesModal() {
    let modal = this.modalCtrl.create(MentionslegalesPage);
    modal.present();
  }

  presentConditionsGeneralesModal() {
    let modal = this.modalCtrl.create(ConditionsGeneralesPage);
    modal.present();

  }

}
import { SyncDataProvider } from "../../providers/sync-data/sync-data";
import { retry } from 'ionic-native/node_modules/rxjs/operator/retry';

//FAQ MODAL
@Component({
  selector: 'page-faq',
  template: `
    <ion-header>
      <ion-toolbar color="infos">
        <ion-buttons end>
          <button ion-button (click)='dismiss()'  icon-only>
            <ion-icon name="close"></ion-icon>
          </button>
        </ion-buttons>
        <ion-title>{{libelle}}</ion-title>
      </ion-toolbar>

    </ion-header>

    <ion-content class="modal-content" scroll="false">
    
        <img class="bgModalImg" src="assets/img/bg_menu.jpg" style="
        position: absolute ;
            width: 100%;
            height: 100%;
            z-index: -10;">
    
            <ion-scroll scrollY="true" style="height: 100%;">
              <ion-list *ngIf="documents" radio-group style="background-color: transparent !important;">
                <ion-item style="background-color:rgba(255, 255, 255, 0.72) !important;" *ngFor="let d of documents;" (click)="openChildModal(d)">
                  <ion-icon style="opacity: 0.72;" color="infos" name="arrow-forward" item-start></ion-icon>
                  <ion-label>{{d.libelle}}</ion-label>
                </ion-item>
              </ion-list>
        
            <div *ngIf="paragraphes && paragraphes.length > 0">
              <ion-card *ngFor="let p of paragraphes; let idx = index" padding  style="background: rgba(255, 255, 255, 0.58) !important;">
                <p [innerHtml]="p.titre" class='makeItBold'> </p>
                <p [innerHtml]="p.contenu"> </p>
              </ion-card>
            </div>
          </ion-scroll>
    
        </ion-content>
  `,
  styles: [`
  ion-content.modal-content{
    background: url(../assets/img/bg_menu.jpg) !important;
    -webkit-background: url(../assets/img/bg_menu.jpg) !important;
    background-size: cover !important;
    background-repeat: no-repeat !important; 
  }

  .makeItBold{
    margin-top: 1rem;
    font-weight: bold;
  }
  `]
})
export class FAQPage {

  constructor(public viewCtrl: ViewController, public navCtrl: NavController,
    public navParams: NavParams,
    private myUtils: MyUtilsProvider,
    public modalCtrl: ModalController,
    private syncService: SyncDataProvider,
  ) {

    console.log("initialize NewItemModal")
    console.log(this.navParams.get("document"))
    if (this.navParams.get("document")) {
      this.currentDoc = this.navParams.get("document")
      this.code = this.currentDoc.code;
      this.libelle = this.currentDoc.libelle
    }

  }

  paragraphes;
  documents;
  currentDoc;
  code = "FAQ";
  libelle = "FAQ"
  ionViewDidLoad() {
    console.log('ionViewDidLoad FAQPage--');

    if (!this.paragraphes) {
      this.syncService.syncData("FAQList_" + this.code,
        (res => {
          console.log("res>>>>", res);
          if (res.document[0].documents) {
            this.documents = res.document[0].documents
          }

          if (res.document[0].paragraphes) {
            this.paragraphes = res.document[0].paragraphes
          }

        }),
        () => {
          //Online http ...
          return new Promise((resolve, reject) => {

            this.myUtils.doGet("https://api.tgr.gov.ma/api/Documents/editerDocument?code=" + this.code)
              .then(
              res => {
                try {

                  let jParagraphes = JSON.parse(res['_body']).document[0].paragraphes;
                  let jDocuments = JSON.parse(res['_body']).document[0].documents;

                  console.log("response faq !!");
                  console.log(jParagraphes);
                  console.log(jDocuments);

                  if (jParagraphes) {
                    jParagraphes.sort((a, b) => {
                      return a.order - b.order;
                    });
                  }

                  resolve(JSON.parse(res['_body']));

                } catch (e) {
                  console.error(e);
                  reject(e);
                }
              },
              err => {
                console.error(err);
                reject(err);
              });
          });
        }
      );
    }

  }

  openChildModal(document) {
    let modal = this.modalCtrl.create(FAQPage, { "document": document });
    modal.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}

//
@Component({
  selector: 'page-securite',
  template: `
  <ion-header>
  <ion-toolbar color="infos">
    <ion-buttons end>
      <button ion-button (click)='dismiss()'  icon-only>
        <ion-icon name="close"></ion-icon>
      </button>
    </ion-buttons>
    <ion-title>{{libelle}}</ion-title>
  </ion-toolbar>

</ion-header>

<ion-content class="modal-content" scroll="false">

    <img class="bgModalImg" src="assets/img/bg_menu.jpg" style="
    position: absolute ;
        width: 100%;
        height: 100%;
        z-index: -10;">

        <ion-scroll scrollY="true" style="height: 100%;">
          <ion-list *ngIf="documents" radio-group style="background-color: transparent !important;">
          <ion-item style="background-color:rgba(255, 255, 255, 0.72) !important;" *ngFor="let d of documents;" (click)="openChildModal(d)">
            <ion-icon style="opacity: 0.72;" color="infos" name="arrow-forward" item-start></ion-icon>
            <ion-label>{{d.libelle}}</ion-label>
          </ion-item>
        </ion-list>
    
          <div *ngIf="paragraphes && paragraphes.length > 0">
            <ion-card *ngFor="let p of paragraphes; let idx = index" padding  style="background: rgba(255, 255, 255, 0.58) !important;">
              <p [innerHtml]="p.titre" class='makeItBold'> </p>
              <p [innerHtml]="p.contenu"> </p>
            </ion-card>
          </div>
        </ion-scroll>

    </ion-content>
  `,
  styles: [`
  ion-content.modal-content{
    background: url(../assets/img/bg_menu.jpg) !important;
    -webkit-background: url(../assets/img/bg_menu.jpg) !important;
    background-size: cover !important;
    background-repeat: no-repeat !important; 
  }

  .makeItBold{
    margin-top: 1rem;
    font-weight: bold;
  }
  `]
})
export class SecuritePage {

  constructor(public viewCtrl: ViewController, public navCtrl: NavController,
    public navParams: NavParams,
    private myUtils: MyUtilsProvider,
    public modalCtrl: ModalController,
    private syncService: SyncDataProvider,
  ) {

    console.log("initialize NewItemModal")
    console.log(this.navParams.get("document"))
    if (this.navParams.get("document")) {
      this.currentDoc = this.navParams.get("document")
      this.code = this.currentDoc.code;
      this.libelle = this.currentDoc.libelle
    }

  }

  paragraphes;
  documents;
  currentDoc;
  code = "SECURITE";
  libelle = "Sécurité"
  ionViewDidLoad() {
    console.log('ionViewDidLoad SECURITEPage--');

    if (!this.paragraphes) {
      this.syncService.syncData("SECURITEList_" + this.code,
        (res => {
          console.log("res>>>>", res);
          if (res.document[0].documents) {
            this.documents = res.document[0].documents
          }

          if (res.document[0].paragraphes) {
            this.paragraphes = res.document[0].paragraphes
          }

        }),
        () => {
          //Online http ...
          return new Promise((resolve, reject) => {

            this.myUtils.doGet("https://api.tgr.gov.ma/api/Documents/editerDocument?code=" + this.code)
              .then(
              res => {
                try {

                  let jParagraphes = JSON.parse(res['_body']).document[0].paragraphes;
                  let jDocuments = JSON.parse(res['_body']).document[0].documents;

                  console.log("response SECURITE !!");
                  console.log(jParagraphes);
                  console.log(jDocuments);

                  if (jParagraphes) {
                    jParagraphes.sort((a, b) => {
                      return a.order - b.order;
                    });
                  }

                  resolve(JSON.parse(res['_body']));

                } catch (e) {
                  console.error(e);
                  reject(e);
                }
              },
              err => {
                console.error(err);
                reject(err);
              });
          });
        }
      );
    }

  }

  openChildModal(document) {
    let modal = this.modalCtrl.create(SecuritePage, { "document": document });
    modal.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}

//
@Component({
  selector: 'page-mentionslegales',
  template: `
  <ion-header>
  <ion-toolbar color="infos">
    <ion-buttons end>
      <button ion-button (click)='dismiss()'  icon-only>
        <ion-icon name="close"></ion-icon>
      </button>
    </ion-buttons>
    <ion-title>{{libelle}}</ion-title>
  </ion-toolbar>

</ion-header>

<ion-content class="modal-content" scroll="false">

    <img class="bgModalImg" src="assets/img/bg_menu.jpg" style="
    position: absolute ;
        width: 100%;
        height: 100%;
        z-index: -10;">

        <ion-scroll scrollY="true" style="height: 100%;">
          <ion-list *ngIf="documents" radio-group style="background-color: transparent !important;">
            <ion-item style="background-color:rgba(255, 255, 255, 0.72) !important;" *ngFor="let d of documents;" (click)="openChildModal(d)">
              <ion-icon style="opacity: 0.72;" color="infos" name="arrow-forward" item-start></ion-icon>
              <ion-label>{{d.libelle}}</ion-label>
            </ion-item>
          </ion-list>
    
          <div *ngIf="paragraphes && paragraphes.length > 0">
            <ion-card *ngFor="let p of paragraphes; let idx = index" padding  style="background: rgba(255, 255, 255, 0.58) !important;">
              <p [innerHtml]="p.titre" class='makeItBold'> </p>
              <p [innerHtml]="p.contenu"> </p>
            </ion-card>
          </div>
        </ion-scroll>

    </ion-content>
  `,
  styles: [`
  ion-content.modal-content{
    background: url(../assets/img/bg_menu.jpg) !important;
    -webkit-background: url(../assets/img/bg_menu.jpg) !important;
    background-size: cover !important;
    background-repeat: no-repeat !important; 
  }

  .makeItBold{
    margin-top: 1rem;
    font-weight: bold;
  }
  `]
})
export class MentionslegalesPage {


  constructor(public viewCtrl: ViewController, public navCtrl: NavController,
    public navParams: NavParams,
    private myUtils: MyUtilsProvider,
    public modalCtrl: ModalController,
    private syncService: SyncDataProvider,
  ) {

    console.log("initialize NewItemModal")
    console.log(this.navParams.get("document"))
    if (this.navParams.get("document")) {
      this.currentDoc = this.navParams.get("document")
      this.code = this.currentDoc.code;
      this.libelle = this.currentDoc.libelle
    }

  }

  paragraphes;
  documents;
  currentDoc;
  code = "MENTIONS";
  libelle = "Mentions légales"
  ionViewDidLoad() {
    console.log('ionViewDidLoad MentionslegalesPage--');

    if (!this.paragraphes) {
      this.syncService.syncData("MENTIONSList_" + this.code,
        (res => {
          console.log("res>>>>", res);
          if (res.document[0].documents) {
            this.documents = res.document[0].documents
          }

          if (res.document[0].paragraphes) {
            this.paragraphes = res.document[0].paragraphes
          }

        }),
        () => {
          //Online http ...
          return new Promise((resolve, reject) => {

            this.myUtils.doGet("https://api.tgr.gov.ma/api/Documents/editerDocument?code=" + this.code)
              .then(
              res => {
                try {

                  let jParagraphes = JSON.parse(res['_body']).document[0].paragraphes;
                  let jDocuments = JSON.parse(res['_body']).document[0].documents;

                  console.log("response MENTIONS !!");
                  console.log(jParagraphes);
                  console.log(jDocuments);

                  if (jParagraphes) {
                    jParagraphes.sort((a, b) => {
                      return a.order - b.order;
                    });
                  }

                  resolve(JSON.parse(res['_body']));

                } catch (e) {
                  console.error(e);
                  reject(e);
                }
              },
              err => {
                console.error(err);
                reject(err);
              });
          });
        }
      );
    }

  }

  openChildModal(document) {
    let modal = this.modalCtrl.create(MentionslegalesPage, { "document": document });
    modal.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}

//
@Component({
  selector: 'page-conditionsgenerales',
  template: `
    <ion-header>
      <ion-toolbar color="infos">
        <ion-buttons end>
          <button ion-button (click)='dismiss()'  icon-only>
            <ion-icon name="close"></ion-icon>
          </button>
        </ion-buttons>
        <ion-title>Conditions générales d'utilisation</ion-title>
      </ion-toolbar>

    </ion-header>

    <ion-content class="modal-content" scroll="false">
    
        <img class="bgModalImg" src="assets/img/bg_menu.jpg" style="
        position: absolute ;
            width: 100%;
            height: 100%;
            z-index: -10;">
    
            <ion-scroll scrollY="true" style="height: 100%;">
              <ion-list *ngIf="documents" radio-group style="background-color: transparent !important;">
              <ion-item style="background-color:rgba(255, 255, 255, 0.72) !important;" *ngFor="let d of documents;" (click)="openChildModal(d)">
                <ion-icon style="opacity: 0.72;" color="infos" name="arrow-forward" item-start></ion-icon>
                <ion-label>{{d.libelle}}</ion-label>
              </ion-item>
            </ion-list>
        
              <div *ngIf="paragraphes && paragraphes.length > 0">
                <ion-card padding  *ngFor="let p of paragraphes;" style="background: rgba(255, 255, 255, 0.47) !important;">
                  <ion-card-header 
                  style="font-weight: bold;
                  white-space: normal !important;">
                    {{p.titre}}
                  </ion-card-header>
                  <ion-card-content [innerHtml]="p.contenu" >
                  
                  </ion-card-content>
                </ion-card>
              </div>
            
            </ion-scroll>
    
        </ion-content>
  `,
})
export class ConditionsGeneralesPage {


  constructor(public viewCtrl: ViewController, public navCtrl: NavController,
    public navParams: NavParams,
    private myUtils: MyUtilsProvider,
    public modalCtrl: ModalController,
    private syncService: SyncDataProvider,
  ) {

  }

  paragraphes;

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConditionsGeneralesPage');

    if (!this.paragraphes) {
      this.syncService.syncData("CONDITIONSList",
        (res => {
          console.log("res>>>>", res);

          if (res.document[0].paragraphes) {
            this.paragraphes = res.document[0].paragraphes
            //*
            if (this.paragraphes) {
              this.paragraphes.sort((a, b) => { 
                //return a.ordre - b.ordre

                if (a.ordre < b.ordre)
                  return -1;
                if (a.ordre > b.ordre)
                  return 1;

                return 0;

              });
            }//*/
          }

        }),
        () => {
          //Online http ...
          //this.annuaire.getListFromHttp()
          ///... rest of code !!!
          return new Promise((resolve, reject) => {

            this.myUtils.doGet("https://api.tgr.gov.ma/api/Documents/editerDocument?code=CONDITIONS")
              .then(
              res => {
                try {

                  let jParagraphes = JSON.parse(res['_body']).document[0].paragraphes;

                  console.log("response terms !!");
                  console.log(jParagraphes);

                  resolve(JSON.parse(res['_body']));

                } catch (e) {
                  console.error(e);
                  reject(e);
                }
              },
              err => {
                console.error(err);
                reject(err);
              });
          });
        }
      );
    }

  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
