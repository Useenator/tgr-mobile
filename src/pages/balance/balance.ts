//Najim

import { Component, ElementRef, ViewChild } from '@angular/core';

import { DOCUMENT } from '@angular/platform-browser';

import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';

import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { BankServiceProvider } from '../../providers/bank-service/bank-service';

import { Observable } from 'rxjs/Rx';

import { Chart } from 'chart.js';

import { Slides } from 'ionic-angular';


/**
 * Generated class for the BalancePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */


interface GraphLine {
  id: number, montant: number, date: Date
}
//@IonicPage()
@Component({
  selector: 'page-balance',
  templateUrl: 'balance.html',
})
export class BalancePage {
  @ViewChild('m1')
  canvasM1: ElementRef;
  @ViewChild('m2')
  canvasM2: ElementRef;
  @ViewChild('m3')
  canvasM3: ElementRef;

  foundItems: any = [];
  grouped: any = [];

  chart1; chart2; chart3;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private bankService: BankServiceProvider,
    private auth: AuthServiceProvider,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController) {
    this.rib = this.navParams.data.rib;
    this.balance = this.navParams.data.balance;

  }

  @ViewChild(Slides) slides: Slides;

  goToSlide(idx: number, speed?: number) {
    this.slides.slideTo(idx, (speed) ? speed : 500);
  }

  isLeftBtnEnabled = false;
  isRightBtnEnabled = false;

  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();
    console.log('Current index is', currentIndex);

    this.isLeftBtnEnabled = true;
    this.isRightBtnEnabled = true;
    if (this.slides.isBeginning()) {
      this.isLeftBtnEnabled = false;
    }
    if (this.slides.isEnd()) {
      this.isRightBtnEnabled = false;
    }
    if (this.slides.length() == 0) {
      this.isLeftBtnEnabled = false;
      this.isRightBtnEnabled = false;
    }
  }

  prev() {
    this.slides.slidePrev();
  }

  next() {
    this.slides.slideNext()
  }

  rib: string;
  balance: number = 0;
  graphs: Array<GraphLine> = [];
  nbrOfMonths = 4;

  ionViewDidLoad() {
    let token = this.auth.currentUser.tokenData.id;
    let rib = this.rib;//selected rib from bank view(accountsDataPage)
    let user_id = this.auth.currentUser.LDAPData.id;

    this.presentLoading();
    this.bankService.getGraphSolde(user_id, rib, token)
      .subscribe(data => {
        console.log("getGraphSolde", data);

        try {
          let graphsData = JSON.parse(data["_body"]).graphes.graphes;

          if (graphsData && graphsData.length > 0) {

            var sortedGraph: Array<any> = graphsData;

            //do sorting..
            /*
            for (let i = 1; i < sortedGraph.length; i++) {
              let iGraph = sortedGraph[i]; //ex: .date = 16-12-2017
              var iDateParts = iGraph.date.split('-');
              var iCompare = new Date(iDateParts[2], iDateParts[1] - 1, iDateParts[0]);
              var j = i - 1
              let jGraph = sortedGraph[j];
              var jDateParts = jGraph.date.split('-');
              var jCompare = new Date(jDateParts[2], jDateParts[1] - 1, jDateParts[0]);
              while (j >= 0 && jCompare > iCompare) {
                sortedGraph[j + 1] = sortedGraph[j]
                j = j - 1
              }
              sortedGraph[j + 1] = iGraph

            }//end sorting !
            // */

            sortedGraph = sortedGraph.sort(function (a, b) {
              // Turn your strings into dates, and then subtract them
              // to get a value that is either negative, positive, or zero.
              let dateParts1 = a.date.split('-');
              let date1 = new Date(dateParts1[2], dateParts1[1] - 1, dateParts1[0]);

              let dateParts2 = b.date.split('-');
              let date2 = new Date(dateParts2[2], dateParts2[1] - 1, dateParts2[0]);

              return date1.getTime() - date2.getTime();
            });
            console.log("sortedGraph", sortedGraph);

            //emit each element
            const source = Observable.from(sortedGraph);
            //group by date
            const example = source
              .groupBy(item => item.date.substring(3, 5))
              //return as array of each group
              .flatMap(group => group.reduce((acc, curr) => [...acc, curr], []))


            const subscribe = example.subscribe(val => {

              this.grouped.push(val);
            });
            console.log("grouped", this.grouped);

          }

          this.traceGraphe()


        } catch (error) {
          this.dismissLoading();
          console.log(error);
        }

      },
      err => {
        this.dismissLoading();
        console.error(err);
        //this.showAlert('Ooops', 'Oooops !!, We can not connect to the server "OperationBetweenTwoDatesByRib" !!');
        //this.dismissLoading();
      }
      );//End service

  }

  initialIdx = -1;


  montantsDatasets1: Array<{
    label: string,
    steppedLine: boolean,
    fill: boolean,
    pointStyle: string,
    backgroundColor: string,
    borderColor: string,
    pointBackgroundColor: string,
    data: Array<number>
  }> = [];
  montantsDatasets2: Array<{
    label: string,
    steppedLine: boolean,
    fill: boolean,
    pointStyle: string,
    backgroundColor: string,
    borderColor: string,
    pointBackgroundColor: string,
    data: Array<number>
  }> = [];
  montantsDatasets3: Array<{
    label: string,
    steppedLine: boolean,
    fill: boolean,
    pointStyle: string,
    backgroundColor: string,
    borderColor: string,
    pointBackgroundColor: string,
    data: Array<number>
  }> = [];
  traceGraphe() {


    function random_rgba() {
      var o = Math.round, r = Math.random, s = 255;
      var alpha = "0.7"; // will make alpha fixed with 70%;
      //let color =  'rgba(' + o(r() * s) + ',' + o(r() * s) + ',' + o(r() * s) + ',' + r().toFixed(1) + ')'; //genrated color with random alpha!
      let color = 'rgba(' + o(r() * s) + ',' + o(r() * s) + ',' + o(r() * s) + ',' + alpha + ')';
      return color;
    }

    var daysLabel1 = [];
    var daysLabel2 = [];
    var daysLabel3 = [];

    if (this.grouped && this.grouped.length > 0) {

      this.grouped.forEach((group, idx) => {
        if (group && group.length > 0) {

          var avgOfCurrentGrp: number = 0;
          group.forEach(data => {
            if (idx == this.grouped.length - 3) {
              if (daysLabel1.indexOf(Number(data.date.substring(0, 2))) == -1) daysLabel1.push(Number(data.date.substring(0, 2)))
              let sensOp = (data.sensOperation.code == 'CREDIT') ? 1 : -1;
              avgOfCurrentGrp += data.montant * sensOp
            } else if (idx == this.grouped.length - 2) {
              if (daysLabel2.indexOf(Number(data.date.substring(0, 2))) == -1) daysLabel2.push(Number(data.date.substring(0, 2)))
              let sensOp = (data.sensOperation.code == 'CREDIT') ? 1 : -1;
              avgOfCurrentGrp += data.montant * sensOp
            } else if (idx == this.grouped.length - 1) {
              if (daysLabel3.indexOf(Number(data.date.substring(0, 2))) == -1) daysLabel3.push(Number(data.date.substring(0, 2)))
              let sensOp = (data.sensOperation.code == 'CREDIT') ? 1 : -1;
              avgOfCurrentGrp += data.montant * sensOp
            }
          });
          avgOfCurrentGrp = avgOfCurrentGrp / group.length;
        }
      })
    }

    daysLabel1 = daysLabel1.sort((a, b) => {
      return a - b;
    });
    daysLabel2 = daysLabel2.sort((a, b) => {
      return a - b;
    });
    daysLabel3 = daysLabel3.sort((a, b) => {
      return a - b;
    });


    if (this.grouped && this.grouped.length > 0) {
      for (let idx = this.grouped.length - 3; idx <= this.grouped.length - 1; idx++) {
        var group = this.grouped[idx];

        var montants = [];

        if (group && group.length > 0) {

          group.forEach(data => {
            if (idx == this.grouped.length - 3) {
              let sensOp = (data.sensOperation.code == 'CREDIT') ? 1 : -1;
              let idx = daysLabel1.indexOf(Number(data.date.substring(0, 2)))
              montants[idx] = data.montant * sensOp;
            } else if (idx == this.grouped.length - 2) {
              let sensOp = (data.sensOperation.code == 'CREDIT') ? 1 : -1;
              let idx = daysLabel2.indexOf(Number(data.date.substring(0, 2)))
              montants[idx] = data.montant * sensOp;
            } else if (idx == this.grouped.length - 1) {
              let sensOp = (data.sensOperation.code == 'CREDIT') ? 1 : -1;
              let idx = daysLabel3.indexOf(Number(data.date.substring(0, 2)))
              montants[idx] = data.montant * sensOp;
            }
          });

        }

        let mounths = {
          "1": "janv", "2": "févr", "3": "mars",
          "4": "avr", "5": "mai", "6": "juin", "7": "juill",
          "8": "août", "9": "sept", "10": "oct", "11": "nov", "12": "déc"
        }

        console.log("group ", idx, "data: ", group);

        if (idx == this.grouped.length - 3) {
          this.montantsDatasets1.push({
            //"15-01-2018"
            label: mounths[String(Number(group[0].date.substring(3, 5)))] + " " + group[0].date.substring(6),
            steppedLine: false,
            fill: true,
            pointStyle: 'circle',
            backgroundColor: "rgba(168, 14, 14, 0.7)",//random_rgba(),
            borderColor: "rgba(101, 10, 10, 1)",//random_rgba(),
            pointBackgroundColor: "rgba(101, 10, 10, 1)",//random_rgba(),
            data: montants
          });

          this.initialIdx++;
        } else if (idx == this.grouped.length - 2) {
          this.montantsDatasets2.push({
            //"15-01-2018"
            label: mounths[String(Number(group[0].date.substring(3, 5)))] + " " + group[0].date.substring(6),
            steppedLine: false,
            fill: true,
            pointStyle: 'circle',
            backgroundColor: "rgba(234, 134, 20, 0.7)",//random_rgba(),
            borderColor: "rgba(142, 80, 10, 1)",//random_rgba(),
            pointBackgroundColor: "rgba(142, 80, 10, 1)",//random_rgba(),
            data: montants
          });
          this.initialIdx++;
        } else if (idx == this.grouped.length - 1) {
          this.montantsDatasets3.push({
            //"15-01-2018"
            label: mounths[String(Number(group[0].date.substring(3, 5)))] + " " + group[0].date.substring(6),
            steppedLine: false,
            fill: true,
            pointStyle: 'circle',
            backgroundColor: "rgba(113, 168, 171, 0.7)",//random_rgba(),
            borderColor: "rgba(38, 90, 94, 1)",//random_rgba(),
            pointBackgroundColor: "rgba(38, 90, 94, 1)",//random_rgba(),
            data: montants
          });
          this.initialIdx++;
        }

      }
    }


    Chart.defaults.global.legend.display = false;

    var chartCanvas = this.canvasM1.nativeElement;
    console.log(chartCanvas);
    //var ctx = chartCanvas.getContext("2d");
    this.chart1 = new Chart(chartCanvas, {
      type: 'line',
      options: {
        legend: {
          display: true
        },
        showDatasetLabels: true,
        scales: {
          xAxes: [{
            ticks: {
              fontColor: "#2f2f2f"
            }
          }],
          yAxes: [{
            ticks: {
              fontColor: "#2f2f2f"
            }
          }]
        }
      },
      data: {
        legend: {
          display: false
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem) {
              return tooltipItem.yLabel;
            }
          }
        },
        labels: daysLabel1,
        datasets: this.montantsDatasets1
        /*[
      {
        steppedLine: false,
        fill: true,
        pointStyle: 'circle',
        backgroundColor: 'rgba(224, 187, 114, 0.65)',
        borderColor: "rgb(179, 141, 67)",
        pointBackgroundColor: "#f4f4f4",
        data: [null, 1000, 14000, 100, 900, 13800, 320]
      },
      {
        steppedLine: false,
        fill: true,
        pointStyle: 'circle',
        backgroundColor: 'rgba(234, 198, 170, 0.65)',
        borderColor: "#dc9963",
        pointBackgroundColor: "#f4f4f4",
        data: [200, 9300, 12000, 9100, 14900, 5800, 1320]
      }
    ]*/
      }

    });//end chart !  


    var chartCanvas2 = this.canvasM2.nativeElement;
    console.log(chartCanvas2);
    //var ctx = chartCanvas.getContext("2d");
    this.chart2 = new Chart(chartCanvas2, {
      type: 'line',
      options: {
        legend: {
          display: true
        },
        showDatasetLabels: true,
        scales: {
          xAxes: [{
            ticks: {
              fontColor: "#2f2f2f"
            }
          }],
          yAxes: [{
            ticks: {
              fontColor: "#2f2f2f"
            }
          }]
        }
      },
      data: {
        legend: {
          display: false
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem) {
              return tooltipItem.yLabel;
            }
          }
        },
        labels: daysLabel2,
        datasets: this.montantsDatasets2
        /*[
      {
        steppedLine: false,
        fill: true,
        pointStyle: 'circle',
        backgroundColor: 'rgba(224, 187, 114, 0.65)',
        borderColor: "rgb(179, 141, 67)",
        pointBackgroundColor: "#f4f4f4",
        data: [null, 1000, 14000, 100, 900, 13800, 320]
      },
      {
        steppedLine: false,
        fill: true,
        pointStyle: 'circle',
        backgroundColor: 'rgba(234, 198, 170, 0.65)',
        borderColor: "#dc9963",
        pointBackgroundColor: "#f4f4f4",
        data: [200, 9300, 12000, 9100, 14900, 5800, 1320]
      }
    ]*/
      }

    });//end chart !  

    var chartCanvas3 = this.canvasM3.nativeElement;
    console.log(chartCanvas3);
    //var ctx = chartCanvas.getContext("2d");
    this.chart3 = new Chart(chartCanvas3, {
      type: 'line',
      options: {
        legend: {
          display: true
        },
        showDatasetLabels: true,
        scales: {
          xAxes: [{
            ticks: {
              fontColor: "#2f2f2f"
            }
          }],
          yAxes: [{
            ticks: {
              fontColor: "#2f2f2f"
            }
          }]
        }
      },
      data: {
        legend: {
          display: true
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem) {
              return tooltipItem.yLabel;
            }
          }
        },
        labels: daysLabel3,
        datasets: this.montantsDatasets3
        /*[
      {
        steppedLine: false,
        fill: true,
        pointStyle: 'circle',
        backgroundColor: 'rgba(224, 187, 114, 0.65)',
        borderColor: "rgb(179, 141, 67)",
        pointBackgroundColor: "#f4f4f4",
        data: [null, 1000, 14000, 100, 900, 13800, 320]
      },
      {
        steppedLine: false,
        fill: true,
        pointStyle: 'circle',
        backgroundColor: 'rgba(234, 198, 170, 0.65)',
        borderColor: "#dc9963",
        pointBackgroundColor: "#f4f4f4",
        data: [200, 9300, 12000, 9100, 14900, 5800, 1320]
      }
    ]*/
      }

    });//end chart !  

    this.goToSlide((this.initialIdx < 0) ? 0 : this.initialIdx, 300);

    this.dismissLoading();
  }

  showAlert(title, message) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

  private loader;
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Chargement des données...",
      showBackdrop: true,
      enableBackdropDismiss: true
    });
    /*
    this.loader.onDidDismiss(() => {
      console.log('Dismissed loading');
    });*/

    this.loader.present();
  }

  dismissLoading() {
    console.log('--Dismissed loading--');
    if (this.loader.isOverlay) this.loader.dismiss();

  }

}
