import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

////////////////////////////////////////////////////////////////////
import { Pipe, PipeTransform } from '@angular/core';
//our root app component
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser'
import { Observable } from 'rxjs/Rx';
////////////////////////////////////////////////////////////////////
/**
 * Generated class for the GidDetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
//@IonicPage()
@Component({
  selector: 'page-gid-detail',
  templateUrl: 'gid-detail.html',

})
export class GidDetailPage {

  private item;
  private mds = [];
  private mdsByTypeActeGroups = [];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.item = navParams.get('item');
    this.mds = navParams.get('mds');
    this.mds = this.getMdsByID(this.item.idPrestation);
    //test
    this.mds = this.groupByTypeActe();
  }

  capitalizeFirstLetter(str) {
    if (typeof str == 'undefined' || str == null) return '';
    else return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
  }

  formatedFLoat(str) {
    let res = String(parseFloat(str).toFixed(2)).replace('.', ',');
    if (res == 'NaN') res = "";
    return res;
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad GidDetailPage');
  }

  getMdsByID(id) {
    return this.mds.filter(item => {
      if (item.idPrestation == id) return true; else return false;
    })
  }

  /**
   * groupByTypeActe
   * 
   */
  groupByTypeActe() {

    /* const item =[
        {"libelleTypeacte": "Ordonnance de paiement","numeroActe": 1},
        {libelleTypeacte": "Ordonnance de paiement","numeroActe": 1},
        {"libelleTypeacte": "Engagement de dépenses","numeroActe": 1},
        {libelleTypeacte": "Report des crédits","numeroActe": 1},
      ];
    */
    const items = this.mds;
    //emit each element
    const source = Observable.from(items);
    //group by typrActe
    const example = source
      .groupBy(item => item.libelleTypeActe)
      //return as array of each group
      .flatMap(group => group.reduce((acc, curr) => [...acc, curr], []))
    /*
      output:
      [{"libelleTypeacte": "Ordonnance de paiement","numeroActe": 1},{libelleTypeacte": "Ordonnance de paiement","numeroActe": 1}]
      [{"libelleTypeacte": "Engagement de dépenses","numeroActe": 1}]
      [{libelleTypeacte": "Report des crédits","numeroActe": 1}]
    */

    const subscribe = example.subscribe(val => {

      this.mdsByTypeActeGroups.push(val);
    });
    console.log("GROUPED LIST!! mdsByTypeActeGroups", this.mdsByTypeActeGroups);
    return this.mdsByTypeActeGroups;
  }
}//class


