
import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';

import { ContribuableService } from '../../providers/contribuable-service/contribuable-service';
import { AlertController } from 'ionic-angular';

import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { MyUtilsProvider } from '../../providers/my-utils/my-utils';
import { Config } from '../../MyUtils/Config';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ContribuablePage } from '../contribuable/contribuable';
//loader
import { LoadingController } from 'ionic-angular';
/*
  Generated class for the ArticlesNonPayes page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-articles-non-payes',
  templateUrl: 'articles-non-payes.html',
  providers: [ContribuableService]
})
export class ArticlesNonPayesPage {

  ////// ngModels
  private years;
  private reference_avis;// = 602728021067;//"605654067251";
  private montant ;//= 451.7;//"306.9";
  private email = "";
  private email_confirm = "";
  ///// END ngModels
  private loader;
  private foundItems;

  constructor(public navCtrl: NavController,
    private nav: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loading: LoadingController,
    public contribuableService: ContribuableService,
    private loadingCtrl: LoadingController,
    private auth: AuthServiceProvider,
    private events: Events
  ) {

  }

  logout() {
    let loading = this.loadingCtrl.create({
      content: 'Traitement en cours...',
      dismissOnPageChange: true
    });
    loading.present();

    this.events.publish('user:doLogout', Date.now());
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ArticlesNonPayesPage');

    this.years = this.getTenYearsFromNow();
  }

  /**
   *
   */
  goToArticles() {
    // this.getResultPEL(this.reference_avis, this.montant);
    if (this.email != this.email_confirm || this.email == "") {
      console.log("Veuillez verifier votre email ..");
      this.showAlert("TGR Mobile", "Veuillez verifier votre email ..");
    } else {
      this.nav.push(ContribuablePage, {
        // items: items,
        page: 'articles_non_payes',
        title: 'Articles à régler',
        reference: this.reference_avis,
        montant: this.montant,
        email: this.email
      });
    }

  }

  showAlert(title, message) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

  /**
   * Liste des articles non payés.
   * ex : https://api.tgr.gov.ma/api/Contribuables/getResultPEL/605654067251/306.9
   */
  getResultPEL(reference, montant) {
    this.presentLoading();
    this.contribuableService.getResultPEL(reference, montant).subscribe(
      data => {
        this.foundItems = data.json;
        console.log('\n foundItems .. getResultPEL!!! ' + JSON.stringify(data.json()) + '\n\n');
        this.goToDetails(this.foundItems);
      },
      err => {
        console.error(err);
        // this.showAlert('Ooops', 'Oooops !!, We can not connect to the server "AgencyByRib" !!');
      },
      () => {
        console.log('getResultPEL completed');
        this.dismissLoading();
      }
    );
  }


  goToDetails(items) {

    this.nav.push(ContribuablePage, {
      items: items,
      page: 'articles_non_payes',
      title: 'Articles à régler',
      reference: this.reference_avis,
      montant: this.montant,
      email: this.email
    });
    ///////////////////////////////// TEST /////////////////////////////////////////

  }
  /**
   * get Ten Years From Now.
   */
  getTenYearsFromNow(): number[] {
    var year: number = new Date().getUTCFullYear();
    var years: number[] = [];
    // console.log('\n this Year \n'+year);
    for (var i = 0; i < 10; i++) {
      // years.push(year+1);
      years[i] = year++;
    }
    // console.log('\n Years array \n'+years);
    return years;
  }


  ///////////////////////////////////// loader; ////////////////////////////////
  /**
   * Show loading
   */
  presentLoading() {
    this.loader = this.loading.create({
      content: "Chargement des données..."
    });

    this.loader.onDidDismiss(() => {
      console.log('Dismissed loading');
    });

    this.loader.present();

    setTimeout(() => {
      this.loader.dismiss();
    }, 5000);
  }

  /**
   * Dismiss loading
   */
  dismissLoading() {
    console.log('--Dismissed loading--');
    if (!this.loader.Dismissed) this.loader.dismiss();

  }//////////////////////// END loader ///////////////////////////////////////
}//END class
