import { Component } from '@angular/core';
import { IonicPage, LoadingController, NavController, NavParams, Events } from 'ionic-angular';

import { AvisImpositionNonRecuPage } from '../avis-imposition-non-recu/avis-imposition-non-recu';
// import { AvisImpositionNonRecuDetailPage } from '../pages/avis-imposition-non-recu-detail/avis-imposition-non-recu-detail';
import { ArticlesNonPayesPage } from '../articles-non-payes/articles-non-payes';
import { SituationFiscalePage } from '../situation-fiscale/situation-fiscale';


import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { MyUtilsProvider } from '../../providers/my-utils/my-utils';


/**
 * Generated class for the ContribuableEntryPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
//@IonicPage()
@Component({
  selector: 'page-contribuable-entry',
  templateUrl: 'contribuable-entry.html',
})
export class ContribuableEntryPage {

  hasAccess = false;//has access to situation fiscal view !

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public auth: AuthServiceProvider,
    public utils: MyUtilsProvider,
    public loadingCtrl: LoadingController,
    private events: Events
  ) {
    this.connectedUser = this.auth.currentUser; 
    this.auth.getUser()
      .then(user => {
        if (user['roleData']) {
          console.warn("USER ROLES", user['roleData'])
          user['roleData'].forEach(role => {
            console.log('role: ', role);
            if (role.id == "tsDossierFiscalTGR" || 
              role.id == 'tsDossierFiscalContrib' || 
              role.id == 'tsClientBqConsultSolde' || 
              role.id == 'tsClientBqEditionRelevesParDate') {
              this.hasAccess = true;
            }
          })
        }
      })

  }

  connectedUser;
  logout() {
    let loading = this.loadingCtrl.create({
      content: 'Traitement en cours...',
      dismissOnPageChange: true
    });
    loading.present();

    this.events.publish('user:doLogout', Date.now());
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContribuableEntryPage');
  }

  openNonPaye() {
    this.navCtrl.push(ArticlesNonPayesPage);
  }

  openNonRecu() {
    this.navCtrl.push(AvisImpositionNonRecuPage);
  }

  openSFiscale() {
    this.navCtrl.push(SituationFiscalePage);
  }

}
