import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Events } from 'ionic-angular';


import { MyUtilsProvider } from "../../providers/my-utils/my-utils";

import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { LoginPage } from '../login/login';

/**
 * Generated class for the ModifyPwdPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
//@IonicPage()
@Component({
  selector: 'page-modify-pwd',
  templateUrl: 'modify-pwd.html',
})
export class ModifyPwdPage {

  oldPwd: string = ""
  newPwd: string = ""
  confPwd: string = ""

  constructor(public navCtrl: NavController, public navParams: NavParams, public myUtils: MyUtilsProvider, 
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,public auth: AuthServiceProvider,
    private events: Events) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModifyPwdPage');
    
  }

  validerClick(){
    this.presentLoading();
    if(this.newPwd == this.confPwd){
      this.myUtils.doPostForm("https://api.tgr.gov.ma/api/AppUsers/change-password?access_token="+this.auth.currentUser.tokenData.id, 
      [{key: "oldPassword", value: this.oldPwd}, {key: "newPassword", value: this.newPwd}])
      .then(
        res => {
          console.log("validerClick", res);
          
          this.dismissLoading();
          if(res["status"] == 204){
            this.showAlert("TGR Mobile", "Le mot passe est bien modifié!")

            this.loader = this.loadingCtrl.create({
                content: "Chargement des données...",
                showBackdrop: true,
                enableBackdropDismiss: true,
                dismissOnPageChange: true
            });

            this.loader.onDidDismiss(() => {
                console.log('Dismissed loading');
            });

            this.loader.present();


            setTimeout(() => {
                this.loader.dismiss();
            }, 3000);
          
              this.events.publish('user:doLogout', Date.now());
            
          }else{
            this.showAlert("TGR Mobile", "Ancien mot de passe est incorrect!")
          }
        }
      )
      .catch((error)=>{
        this.dismissLoading();
        console.log(error);

        var msg = ""
        try {
          msg = JSON.parse(error['_body']).error.message;
        } catch (error) { }
        
        this.showAlert("TGR Mobile", msg)
      })
    }else{
      this.dismissLoading();
      this.showAlert("TGR Mobile", "La confirmation du mot de passe est incorrect!")
      
    }
    
  }

  private loader;
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Chargement des données",
      dismissOnPageChange: true,
      enableBackdropDismiss: true,
      showBackdrop: true
    });

    this.loader.onDidDismiss(() => {
      console.log('Dismissed loading');
    });

    this.loader.present();
  }

  dismissLoading() {
    console.log('--Dismissed loading--');
    if (!this.loader.Dismissed) this.loader.dismiss();

  }

  /**
  * reuseable Alert box.
  */
  showAlert(title, message) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

}
