import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

// import { Utils } from '../../MyUtils/Utils';

/*
  Generated class for the SituationFiscaleDetail page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-situation-fiscale-detail',
  templateUrl: 'situation-fiscale-detail.html'
})
export class SituationFiscaleDetailPage {

  fromSFiscal = false;

  private itemDetails= {
      adresse: "",
      annees: [
        ""
      ],
      articles: [
        ""
      ],
      codepostes: [
        ""
      ],
      ids: [
        ""
      ],
      libelle: "",
      libellepostes: [
        ""
      ],
      libelles: [
        ""
      ],
      montantsfraisrecouvrement: [
        "0.0"
      ],
      montantsmajoration: [
        "0.0"
      ],
      montantsprincipal: [
        "0.0"
      ],
      montantstotalligne: [
        "0.0"
      ],
      natures: [
        ""
      ],
      nom: "",
      secteurs: [
        "0"
      ]
    };
  private index;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
        this.itemDetails = navParams.get('item');
        this.index = navParams.get('index');
        this.fromSFiscal = navParams.get('FromSFiscal');
    }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SituationFiscaleDetailPage');
  }
  formatedFLoat(str) {
    let res = String(parseFloat(str).toFixed(2)).replace('.', ',');
    if (res == 'NaN') res = "";
    return res;
  }

}
