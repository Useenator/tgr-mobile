//Ahmed

import { Component } from '@angular/core';
import { ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AnnuaireDetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
declare var google: any;
//@IonicPage()
@Component({
    selector: 'page-annuaire-detail',
    templateUrl: 'annuaire-detail.html',
})
export class AnnuaireDetailPage {
    public user;
    map: any;

    @ViewChild('mapCanvas') mapElement: ElementRef;
    constructor(
        private nav: NavController,
        private navParams: NavParams) {

        this.user = navParams.get('user');
    }

    ionViewDidLoad() {
        this.loadMap();
    }

    /**
     * Load the map with the Responsable position and informations.
     */
    loadMap() {
        let lat = '33.954960';
        let lng = '-6.873216';
        var latLng = new google.maps.LatLng(lat, lng);//Rabat;
        if (this.user.typePoste)
            if (this.user.typePoste.code === 'REGIONAL') {
                latLng = new google.maps.LatLng(this.user.position.lat, this.user.position.lng);
                console.log('\nREGIONAL ' + this.user.position.lat);
            }

        let mapEle = this.mapElement.nativeElement;

        let mapOptions = {
            center: latLng,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        this.map = new google.maps.Map(mapEle, mapOptions);

        //Add mareker.
        this.addMarker();
    }

    /**
     * Add marker to the map
     */
    addMarker() {
        let marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: this.map.getCenter()
        });

        let content = '<h4>' + this.user.responsable.firstName + ' ' + this.user.responsable.lastName + '</h4>';

        this.addInfoWindow(marker, content);

    }
    /**
     * Add infos to the marker.
     * @param marker
     * @param content
     */
    addInfoWindow(marker, content) {
        let infoWindow = new google.maps.InfoWindow({
            content: content
        });

        google.maps.event.addListener(marker, 'click', () => {
            infoWindow.open(this.map, marker);
        });

    }

    doMail(email) {
        window.open(`mailto:${email}`, '_system');
    }

    doCall(phoneNumber) {
        window.open(`tel:${phoneNumber}`, '_system');
    }

}
