// import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Component, Directive } from '@angular/core';
// import { NavController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { Response, RequestOptions } from '@angular/http';
import { Http, Headers } from '@angular/http';

import { Observable } from 'rxjs/Rx';

import { AlertController } from 'ionic-angular';

import { UsersService } from '../../providers/users.service/users.service';

// import { LoginPage } from '../login/login';
import { RegisterValidatePage } from '../register-validate/register-validate';

import { Storage } from '@ionic/storage';
/**
 * Generated class for the RegisterStep2Page page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
//@IonicPage()
@Component({
  selector: 'page-register-step2',
  templateUrl: 'register-step2.html',
})
export class RegisterStep2Page {
  private contribuableFlag = false;
  private fonctionnaireFlag = false;
  private foundUser;
  private data;
  private _http;
  private userModel;
  private validationResponseObj;
  private VALIDATION_RESPONSE_OBJ = "validationResponseObj";





  constructor(
    private storage: Storage,
    private userService: UsersService,
    private nav: NavController,
    _http: Http,
    private navParams: NavParams,
    public loading: LoadingController,
    public alertCtrl: AlertController) {
    this.data = {};
    // //Contribuable
    // this.data.reference_avis = '85595613';
    // this.data.montant_avis = '162.9';

    // //Fonctionnaire
    // this.data.ppr = '157814';
    // this.data.net_mensuel = '3468';
    // this.data.derniere_entree = '1981-01-02';

    // this.data.response = '';
    this._http = _http;

    console.log(" JSON DATA ", JSON.stringify(this.data));

    this.userModel = navParams.get('user');
    // //test
    // this.userModel.codeContribuables.push("55555");
    // this.userModel.codeContribuables.push("66666");

    //Contribuable 60565406725/306.5
    this.data.reference_avis ;//= '60565406725';
    this.data.montant_avis ;//= '306.5';

    // //Fonctionnaire .. BD Calif
    // this.data.ppr = '157814';
    // this.data.net_mensuel = '3468';
    // this.data.dateEntree = '1981-01-02';

    //Fonctionnaire .. BD PROD
    this.data.ppr ;//= '1168305';
    this.data.net_mensuel ;//= '9821.21';
    this.data.dateEntree ;//= '1999-07-01';
  }
  /* codeValidation
  {
      "nom": "8888",
      "prenom": "8888",
      "raisonSociale": "string",
      "cin": "string",
      "rc": "string",
      "mail": "test@gmail.com",
      "fixe": "string",
      "mobile": "string",
      "activite": "string",
      "adresse": "string",
      "login": "loginoougouk",
      "password": "88888888",
      "ppr": "8888",
      "codeValidation": "igcycdqpzzop94cunofn9k1501189034588",
      "codeContribuables": [],
      "ribs": [],
      "id": 141,
      "etatActuelId": 175
  }*/

  /**
  *
  * @param e
  */
  fonctionnaireFlagShow(e) {
    if (e.checked) {
      this.fonctionnaireFlag = true;
    } else {
      this.fonctionnaireFlag = false;
      //bug,after adding contrib code then disactivate contribe switch => the contrib codes still added to the sended OBJ ... reset contribuable Array to []
      //this.userModel.ppr = '';
      //delete this.userModel.ppr;
      delete this.userModel['ppr'];
      console.warn('==> obj response adhesion', this.userModel);
    }
  }

  /**
  *
  * @param e
  */
  contribuableFlagShow(e) {
    if (e.checked) {
      this.contribuableFlag = true;

    } else {
      //bug,after adding contrib code then disactivate contribe switch => the contrib codes still added to the sended OBJ ... reset contribuable Array to []
      this.userModel.codeContribuables = [];
      this.contribuableFlag = false;
    }

  }

  /**
   * Check if the employee exist or not
   * ex:https://api.tgr.gov.ma/api/Adherents/verifierFonctionnaire?ppr=157814&amp;dateEntree=1981-01-02&amp;netMensuel=3468
   * @param ppr 
   * @param dateEntree 
   * @param netMensuel 
   */
  verifierFonctionnaire(ppr, dateEntree, netMensuel, userModel, callback) {
    this.presentLoading();
    console.log('verifierFonctionnaire begin');
    this.userService.checkFonctionnaireData(ppr, dateEntree, netMensuel).subscribe(
      data => {

        console.log('\n foundFonctionnaire .. checkFonctionnaireData!!! ', JSON.stringify(data));

        let foundFonctionnaire = data;

        if (foundFonctionnaire.result as boolean == true) {
          // if (foundFonctionnaire.result as boolean == false) {
          console.log('\n foundFonctionnaire.result !!! ', foundFonctionnaire.result);
          //TODO: 
          ///////// callback();//////////////
          //test if the login == ppt in fonctionnaire case.
          if (this.fonctionnaireFlag) {
            //bug ... userModel.login=this.data.ppr;
          }
          userModel.ppr = this.data.ppr;
          console.log("userModel verifierFonctionnaire", userModel);
          this.callService(userModel);
          ///////////////////////////////////
          // return true;
        } else {

          this.showAlert("", "Le fonctionnaire rechercher, n'existe pas ..!");
          // this.dismissLoading();

        }
        // return false;
        //let's assume the login is dispo
        callback();
      },
      err => {
        console.error(err);
        if (err.status == 400) {
          // this.showAlert(" ", "Verifier les valeur entrées !");
        } else {
          this.showAlert(" ", "Service Fonctionnaire indisponible veuillez réessayer ultérieurement");
        }
        // errr(err);
        //show a nice error message to the user
        // this.showAlert(" ", "Service Fonctionnaire indisponible veuillez réessayer ultérieurement");
        this.dismissLoading();
      },
      () => {
        console.log('checkFonctionnaireData completed')
        this.dismissLoading();
        // return true;
      }

    );//subscrib
    return true;
  }

  /**
   * Check if the employee exist or not
   * ex:https://api.tgr.gov.ma/api/Adherents/verifierFonctionnaire?ppr=157814&amp;dateEntree=1981-01-02&amp;netMensuel=3468
   * @param ppr 
   * @param dateEntree 
   * @param netMensuel 
   */
  validerFonctionnaire(ppr, dateEntree, netMensuel, userModel) {
    this.presentLoading();
    console.log('verifierFonctionnaire begin');
    this.userService.checkFonctionnaireData(ppr, dateEntree, netMensuel).subscribe(
      data => {

        console.log('\n foundFonctionnaire .. checkFonctionnaireData!!! ', JSON.stringify(data));

        let foundFonctionnaire = data;

        if (foundFonctionnaire.result as boolean == true) {
          // if (foundFonctionnaire.result as boolean == false) {
          console.log('\n foundFonctionnaire.result !!! ', foundFonctionnaire.result);
          //TODO: 
          ///////// callback();//////////////
          //userModel.ppr = this.data.ppr;
          console.log("userModel verifierFonctionnaire", userModel);
          this.showAlert("", "les donnees sont valides, vous pouvez continuer l'enregistrement  ");
          // this.callService(userModel);
          ///////////////////////////////////
          // return true;
        } else {

          this.showAlert("", "Fonctionnaire n'existe pas ..!");
          // this.dismissLoading();

        }
        // return false;
        //let's assume the login is dispo
        // callback();
      },
      err => {
        console.error(err);
        if (err.status == 400) {
          this.showAlert(" ", "Merci de vérifier les informations saisies !");
        } else {
          this.showAlert(" ", "Service Fonctionnaire indisponible veuillez réessayer ultérieurement");
        }
        // errr(err);
        //show a nice error message to the user
        //  this.showAlert(" ", "Service Fonctionnaire indisponible veuillez réessayer ultérieurement");
        this.dismissLoading();
      },
      () => {
        console.log('checkFonctionnaireData completed')
        this.dismissLoading();
        // return true;
      }

    );//subscrib
    return true;
  }
  /**
   * 
   * @param reference_avis 
   * @param montant_avis 
   */
  verifierContribuable(reference_avis = "85595613", montant_avis = "162.9", userModel, callback) {
    if (montant_avis.trim() != "" && reference_avis.trim() != "") {
      this.presentLoading();
      console.log('verifierContribuable begin');
      this.userService.calculateCodeContribuable(reference_avis, montant_avis).subscribe(
        data => {
          try {
            let foundContribuable = data.json();
            console.log('\n foundContribuable .. calculateCodeContribuable!!! ', JSON.stringify(data.json()));

            // if (foundContribuable.result as boolean == true) {
            //   console.log('\n calculateCodeContribuable.result !!! ', foundContribuable.result);
            //   //TODO: 

            // // userModel.codeContribuables = [this.codeContribuable];

            //-- verify if the entred code contrib is allready added
            if (!userModel.codeContribuables.some(x => x == String(foundContribuable))) {
              userModel.codeContribuables.push(String(foundContribuable));
            }

            console.log("userModel verifierContribuable", userModel);
            console.log("foundContribuable verifierContribuable", foundContribuable);

            callback();
          } catch (err) {
            this.showAlert("", "Service de verification du contribuable indisponible .. !");
          }
          //   // return true;
          // } else {

          //   this.showAlert("", "CodeContribuable n'existe pas ..!");
          // }

          // callback();
          // this.dismissLoading();
        },
        err => {
          // errr(err);
          console.error(err);
          // if (err.status == 500) {
          //   this.showAlert(" ", "500 - Service Contribuable indisponible veuillez réessayer ultérieurement");
          // } else
          // if (err.status == 404) {
          //   this.showAlert(" ", "404 - Service Contribuable indisponible veuillez réessayer ultérieurement");
          // } else {
          //   this.showAlert(" ", "Service Contribuable indisponible veuillez réessayer ultérieurement");
          // }
          this.showAlert("", "Merci de vérifier les informations saisies !");
          this.dismissLoading();
          //show a nice error message to the user

        },
        () => {
          console.log('CodeContribuable completed')
          this.dismissLoading();
          // return true;
        }

      );//subscrib
      return true;
    } else {
      this.showAlert("", "Merci de vérifier les informations saisies !");
    }//END IF check data
  }

  /**
  *
  *
  *
 */
  isLoginAvailable(userModel) {


  }//END isLoginAvailable


  /**
   * 
   */
  newUser() {
    let dateNow = new Date().toISOString();
    console.log('DATE_NOW', dateNow);

    var codeContribuable;

    console.log('checkLoginAvailability begin')


    /////////////////////////////////////
    if (!this.contribuableFlag && !this.fonctionnaireFlag) {
      this.callService(this.userModel);
    }
    ////// Check if the adherant want to subscribe to fonctionnaire module
    else if (!this.contribuableFlag && this.fonctionnaireFlag) {

      //**Call verifierFonctionnaireCallback 
      this.verifierFonctionnaire(this.data.ppr, this.data.dateEntree, this.data.net_mensuel, this.userModel, () => {
        this.dismissLoading();
      });

    }
    //////////Check if the adherant want to subscribe to contribuable module ///////// 
    else if (this.contribuableFlag && !this.fonctionnaireFlag) {//this.showAlert("Error", "Le contribuable n'existe pas!");

      //**Call verifierContribuableCallback
      this.verifierContribuable(this.data.reference_avis, this.data.montant_avis, this.userModel, () => {
        this.callService(this.userModel);
        this.dismissLoading();
      });

    }
    //////////Check if the adherant want to subscribe to contribuable and fonctionnaire modules ///////// 
    else if (this.contribuableFlag && this.fonctionnaireFlag) {

      //**Call verifierContribuableCallback
      this.verifierContribuable(this.data.reference_avis, this.data.montant_avis, this.userModel, () => {
        //**Call verifierFonctionnaireCallback 
        this.verifierFonctionnaire(this.data.ppr, this.data.dateEntree, this.data.net_mensuel, this.userModel, () => {
          // this.callService(userModel);
          this.dismissLoading();
        });
        // this.dismissLoading();
      });//END callback

    }//END if FLAGS
    /////////////////////////////////////

  }//END newUser


  addCodeContribuable() {
    // Verify and push the calculated code Contribuable to the userModel.
    this.verifierContribuable(this.data.reference_avis, this.data.montant_avis, this.userModel, () => {
      // userModel.codeContribuables = [this.codeContribuable];
      // userModel.codeContribuables.push(String(foundContribuable));
    });
  }

  goTovalidateFonctionnaire() {
    this.validerFonctionnaire(this.data.ppr, this.data.dateEntree, this.data.net_mensuel, this.userModel);
  }

  /**
   * Launch the post request to add the adherent.
   * @param userModel 
   */
  callService(userModel) {
    this.presentLoading();
    console.log("callService begin ", userModel);
    this.userService.sendAdherentData(userModel).subscribe(
      data => {
        this.showAlert("", "Adhérent ajouté avec succes : le code est envoyé par email");
        console.log("Server response DATA! ", data);
        //redirect to login page.
        // this.nav.setRoot(LoginPage);
        this.nav.setRoot(RegisterValidatePage);

        this.validationResponseObj = data;

        // set a key/value pair
        // this.storage.set(this.VALIDATION_RESPONSE_OBJ, this.validationResponseObj);

        return true;
      },
      error => {
        console.error("Error saving user! " + error);
        //show a nice error message to the user
        if (error.status == 502) {
          // this.showAlert(" ", "Login indisponible, veuillez choisir un autre !");
          this.showAlert(" ", "502 - Erreur d'enregistrement de l'adherent  ");
        }
        if (error.status == 422) {
          // this.showAlert(" ", "Login indisponible, veuillez choisir un autre !");
          this.showAlert(" ", "422 - Erreur d'enregistrement de l'adherent !! ");
          // console.log("422 : " + JSON.stringify(error));
          // this.showAlert(" ", "422 : " + JSON.stringify(error.error.error.message));// + error.error.message);

        } else
          if (error.status == 500) {
            this.showAlert(" ", "500 - Service  d'enregistrement des adherents indisponible veuillez réessayer ultérieurement");
          } else {
            this.showAlert(" ", "Service  d'enregistrement des adherents indisponible veuillez réessayer ultérieurement");
          }

        this.dismissLoading();
        return Observable.throw(error);
      },
      () => {
        this.dismissLoading();
        console.log("callService begin Complete");
      }
    );
    ////////////////////////////////// END service call ////////////////////////////////////////

  }// END callservice()




















  ////////////////////////////////////// Alert ///////////////////////////////////////
  showAlert(title, message) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }


  ///////////////////////////////////// loader; ////////////////////////////////
  private loader;
  /**
   * Show loading
   */
  presentLoading() {
    this.loader = this.loading.create({
      // content: "Patienter svp ...",
      content: "Chargement des données...",
      showBackdrop: true,
      enableBackdropDismiss: true,
      // dismissOnPageChange: true
    });

    this.loader.onDidDismiss(() => {
      console.log('Dismissed loading');
    });

    this.loader.present();

    setTimeout(() => {
      this.loader.dismiss();
    }, 15000);
  }

  /**
   * Dismiss loading
   */
  dismissLoading() {
    console.log('--Dismissed loading--');
    if (!this.loader.Dismissed) this.loader.dismissAll();

  }

}
