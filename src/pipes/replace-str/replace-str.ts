import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the ReplaceStrPipe pipe.
 *
 * See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
 * Angular Pipes.
 */
@Pipe({
  name: 'replaceStr',
})
export class ReplaceStrPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {
    if(!value) return value;
    return value.replace(args[0], args[1]);
  }
}
