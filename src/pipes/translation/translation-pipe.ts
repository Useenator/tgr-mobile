import { Pipe, PipeTransform } from '@angular/core';
import { TranslationServiceProvider } from '../../providers/translation-service/translation-service';


@Pipe({name: 'translate'})
export class TranslatePipe implements PipeTransform {

    private translations : any[];

    constructor(private translationService : TranslationServiceProvider){
        this.translations = JSON.parse(localStorage.getItem('availableTranslations'));
    }

    transform(value: string, args: string[]): any {
        
        console.log("val-->>", value);
        
        var resolvedTranslation = value;

        var currentLanguage = this.translationService.getCurrentLanguage();
        console.warn(this.translations);

        if(this.translations && this.translations != {}&&  typeof this.translations != 'undefined'){
            console.warn("Translating with",this.translations);
            
            this.translations.forEach(element => {
                  
                if(element['language'] === currentLanguage 
                    && element['translation'] != null
                    ){
                    

                        if (typeof element['translation'][value] === 'undefined') {
                            console.log("--- undefined",element['translation'][value]);
                            console.warn("return 1----------");                            
                            resolvedTranslation = value;
                        }else{
                            console.log("--- defined",element['translation'][value])
                            console.warn("return 2----------");                            
                            
                            resolvedTranslation = element['translation'][value];
                        }

                   
                    //return element['translation'][value];
                }
            });
            console.warn("return 3 ----------");
            return resolvedTranslation;
        }else{        
            return value;
        }
        
        
    }
}