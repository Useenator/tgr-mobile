import { Component, ViewChild, trigger, keyframes, animate, transition, style, NgZone } from '@angular/core';
import { Nav, Platform, Events, MenuController, LoadingController, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { ActualitesPage } from '../pages/actualites/actualites';

import { AccountsPage } from '../pages/accounts/accounts';
import { AccountDetailPage } from '../pages/account-detail/account-detail';
import { LoginPage } from '../pages/login/login';
import { HistoryPage } from '../pages/history/history';
import { AnnuairePage } from '../pages/annuaire/annuaire';
import { FonctionnairesPage } from '../pages/fonctionnaires/fonctionnaires';
import { ReclamationPage } from '../pages/reclamation/reclamation';
import { ReclamationEntryPage } from '../pages/reclamation-entry/reclamation-entry';
import { SituationFiscalePage } from '../pages/situation-fiscale/situation-fiscale';
import { ContribuablePage } from '../pages/contribuable/contribuable';
import { ContribuableEntryPage } from '../pages/contribuable-entry/contribuable-entry';
import { GidPage } from '../pages/gid/gid';
import { RegisterPage } from '../pages/register/register';
import { InfosPage } from '../pages/infos/infos';
import { ReglagesPage } from '../pages/reglages/reglages';

import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { MyUtilsProvider } from '../providers/my-utils/my-utils';

import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { Observable } from 'rxjs/Rx';
import { PushServiceProvider } from '../providers/push-service/push-service';
import { TranslationServiceProvider } from '../providers/translation-service/translation-service';

import { LocalisationAndThemesProvider } from '../providers/localisation-and-themes/localisation-and-themes';

import { Storage } from '@ionic/storage';
import { AppMinimize } from '@ionic-native/app-minimize';


import { ConnectivityProvider } from "../providers/connectivity/connectivity";

import { ReclamationServiceProvider } from '../providers/reclamation-service/reclamation-service';


import 'web-animations-js/web-animations.min';

@Component({
  templateUrl: 'app.html',
  animations: [
    trigger('flipInY', [
      transition('inactive => active', animate(1000, keyframes([
        style({ transform: 'perspective(400px) rotate3d(0, 1, 0, 90deg)', offset: .15 }),
        style({ transform: 'perspective(400px) rotate3d(0, 1, 0, 90deg)', offset: .30 }),
        style({ transform: 'perspective(400px) rotate3d(0, 1, 0, 90deg)', offset: .45 }),
        style({ transform: 'perspective(400px) rotate3d(0, 1, 0, 90deg)', offset: .60 }),
        style({ transform: 'perspective(400px) rotate3d(0, 1, 0, 90deg)', offset: .75 }),
        style({ transform: 'none', offset: 1 }),
      ]))),
    ])
  ]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;

  userFullName = "   ";

  pages = [];
  homeTranslation = "....";


  //allPages = [
 
  m_LoginPage = { id: "m-00", title: 'Accueil', component: LoginPage, picto: 'assets/img/ic-menu-accueil.png' };
  m_ActualitesPage = { id: "m-01", title: 'Actualités', component: ActualitesPage, picto: 'assets/img/ic-menu-actualite.png' };
  //{ id: "m-02", title: 'Mon compte', component: AccountsPage, picto: 'assets/img/ic-menu-moncompte.png'};
  m_AnnuairePage = { id: "m-03", title: 'Annuaire', component: AnnuairePage, picto: 'assets/img/ic-menu-annuaire.png' };
  m_AccountDetailPage = { id: "m-04", title: 'Banque', component: AccountDetailPage, picto: 'assets/img/ic-menu-banque.png' };
  m_FonctionnairesPage = { id: "m-05", title: 'Fonctionnaire', component: FonctionnairesPage, picto: 'assets/img/ic-menu-fonctionnaire.png' };
  m_ReclamationEntryPage = { id: "m-06", title: 'Réclamations', component: ReclamationEntryPage, picto: 'assets/img/ic-menu-reclamation.png' };
  m_GidPage = { id: "m-07", title: 'Fournisseur', component: GidPage, picto: 'assets/img/ic-menu-espaceGID.png' };
  m_SituationFiscalePage = { id: "m-08", title: 'Contribuable', component: SituationFiscalePage, picto: 'assets/img/ic-menu-contribuable.png' };
  m_ReglagesPage = { id: "m-09", title: 'Réglages', component: ReglagesPage, picto: 'assets/img/ic-menu-réglages.png' };
  m_RegisterPage = { id: "m-11", title: "Inscription", component: RegisterPage, picto: 'assets/img/ic-menu-adhesion.png' };
  m_InfosPage = { id: "m-12", title: "Info", component: InfosPage, picto: 'assets/img/ic-menu-infos.png' };
  //]



  setPublicPages() {
    this.pages = [];
    this.pages.push(this.m_LoginPage);
    this.pages.push(this.m_ActualitesPage);
    this.pages.push(this.m_AnnuairePage);
    this.pages.push(this.m_ReclamationEntryPage);
    this.pages.push(this.m_SituationFiscalePage);//Contribuable mean ..
    this.pages.push(this.m_ReglagesPage);
    this.pages.push(this.m_RegisterPage);
    this.pages.push(this.m_InfosPage);
  }

  showDemo: boolean = false;
  constructor(public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public events: Events,
    private auth: AuthServiceProvider,
    private myUtils: MyUtilsProvider,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private pushServiceProvider: PushServiceProvider,
    private reclamationService: ReclamationServiceProvider,
    private connectivity: ConnectivityProvider,
    private localisationAndThemesProvider: LocalisationAndThemesProvider,
    public push: Push,
    public zone: NgZone,
    private storage: Storage,
    private appMinimize: AppMinimize,
    private translationService: TranslationServiceProvider
  
    ) {

    this.initializeApp();

    this.setPublicPages();

    this.translationAPI("Français");

    this.events.subscribe('showDemo', (show) => {
      this.showDemo = show;
    });

    this.checkReclamationsAndSend();


    this.getThemes();

    // Load data for translations ...
    this.translationService.loadData();
  }

  checkReclamationsAndSend() {
    console.log("Start CHECK..");

    if (this.connectivity.isOnline()) {
      let key = "reclamationPosts";
      this.myUtils.doGetPrefs(key)
        .then((data: Array<any>) => {
          if (data) {
            /*
            data.forEach((rec, idx) => {
              if (!rec.sent) {
                
                //let reclamationToSave = {
                //  files: this.files,
                //  postObject: toPost,
                //  sent: false
                //}
                
                console.log("rec.postObject", rec.postObject);

                this.postReclamation(rec.files, rec.postObject, () => {
                  //rec.sent = true;
                  data[idx].sent = true;
                });
              }

              if (idx == data.length - 1) {
                this.myUtils.doSetPrefs(key, data);
              }
            });//end for
            */

            let asyncFunction = (rec, cb) => {
              setTimeout(() => {
                if (!data[idx].sent) {
                  /*
                  let reclamationToSave = {
                    files: this.files,
                    postObject: toPost,
                    sent: false
                  }
                  */
                  console.log("rec.postObject", rec.postObject);

                  this.postReclamation(rec.files, rec.postObject, () => {
                    //rec.sent = true;
                    data[idx].sent = true;
                    console.log('done with data[' + idx + ']', data[idx]);
                    idx++;

                  });
                } else {
                  console.log('done with', rec);
                  idx++;

                }

                cb();
              }, 100);
            }

            var idx = 0;
            let requests = data.map((rec) => {
              return new Promise((resolve) => {
                asyncFunction(rec, resolve);
              });
            })

            Promise.all(requests).then(() => {
              this.myUtils.doSetPrefs(key, data);
              console.log('done')
            });

          }
        })
    }
  }

  doPostReclamation(body, successCB) {
    this.reclamationService.postReclamation(body).subscribe(
      data => {
        console.info(typeof data, data);

        if (data.reference) {

          successCB();

          var reclamations = [];

          if (this.auth.currentUser) {
            let key = "reclamations_" + this.auth.currentUser.LDAPData.id; //reclamations_userid

            this.myUtils.doGetPrefs(key).then(recs => {
              if (recs) reclamations = recs;

              reclamations.unshift(data.reference);
              this.myUtils.doSetPrefs(key, reclamations);
            })
          } else {
            let key = "reclamations_unknow_user";
            this.myUtils.doGetPrefs(key).then(recs => {
              if (recs) reclamations = recs;

              reclamations.unshift(data.reference);
              this.myUtils.doSetPrefs(key, reclamations);
            })

          }
        }
        //console.log('\n foundItems .. postReclamation!!! ', data.json());
      },
      err => console.error(err),
      () => console.log('postReclamation completed')
    );
  }

  private makeFileRequest(url: string, params: string[], files: any[]): Observable<any> {
    return Observable.create(observer => {
      if (files && files.length > 0) {
        let formData: FormData = new FormData(),
          xhr: XMLHttpRequest = new XMLHttpRequest();

        for (let i = 0; i < files.length; i++) {
          formData.append("uploads[]", files[i], files[i]['name']);
        }

        xhr.onreadystatechange = () => {
          if (xhr.readyState === 4) {
            if (xhr.status === 200) {
              observer.next(JSON.parse(xhr.response));
              observer.complete();
            } else {
              observer.error(xhr.response);
            }
          }
        };

        xhr.upload.onprogress = (event) => {
          //this.progress = Math.round(event.loaded / event.total * 100);
          //this.progressObserver.next(this.progress);
        };

        xhr.open('POST', url, true);
        xhr.send(formData);
      } else {
        observer.next();
        observer.complete();
      }
    });
  }

  sendFiles(files) {
    return this.makeFileRequest("https://api.tgr.gov.ma/api/FilePoolers/repoPJ/upload",
      [], files);
  }

  postReclamation(filesToPost, objectToPost, successCB) {

    this.sendFiles(filesToPost)
      .subscribe(res => {

        console.warn(res);

        let postedFiles = [];
        if (res && res.result && res.result.files && res.result.files.uploads) postedFiles = res.result.files.uploads;
        if (filesToPost.length == 0 || postedFiles) {

          objectToPost._piecesJointes = postedFiles;

          //*

          try {
            console.info(objectToPost._etatReclamation.date)
          } catch (error) {

          }


          //this.myUtils.doSetPrefs('lastRecalamationData', filesToPost);

          this.doPostReclamation(objectToPost, successCB);

          //*/
        }//end if
      },
      err => {
        console.error(err);
      });
    //*

  }




  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.pushsetup();
      this.statusBar.styleDefault();

      this.splashScreen.hide();

      this.platform.registerBackButtonAction(() => {
        this.appMinimize.minimize();
      });

      this.auth.getUser()
        .then(userData => {
          console.log("userData =>", userData);
          //if(this.myUtils.checker(userData['tokenData'])){
          //  setUserPages(userData);
          //}else{
          this.nav.setRoot(LoginPage);
          //}
        })

      this.events.subscribe('user:login', (user) => {
        console.log("currentUser", this.auth.currentUser);

        setUserPages(user);
      });

      let ctx = this;
      function setUserPages(user) {

        ctx.userFullName = user.LDAPData.lastName + " " + user.LDAPData.firstName;

        if (user.roleData) {
          console.warn("USER ROLES", user.roleData)
          user.roleData.forEach(role => {
            console.log('role: ', role);
            if (
              role.id == 'tsClientBqConsultSolde' ||
              role.id == 'tsClientBqEditionRelevesParDate') {

              console.log('PUSH bank page!');

              let found = false;
              for (var i = 0; i < ctx.pages.length; i++) {
                if (ctx.pages[i] == ctx.m_AccountDetailPage) {
                  found = true;
                  break;
                }
              }
              if (!found) ctx.pages.push(ctx.m_AccountDetailPage);

            }
            if (role.id == 'tsFONCTPPR' || role.id == 'grpFonctMobile') {
              console.log('PUSH fonctionairePages');

              let found = false;
              for (var i = 0; i < ctx.pages.length; i++) {
                if (ctx.pages[i] == ctx.m_FonctionnairesPage) {
                  found = true;
                  break;
                }
              }
              if (!found) ctx.pages.push(ctx.m_FonctionnairesPage);
            }

            if (role.id == 'grpGidFournisseursMobile') {
              console.log('PUSH gidPage');

              let found = false;
              for (var i = 0; i < ctx.pages.length; i++) {
                if (ctx.pages[i] == ctx.m_GidPage) {
                  found = true;
                  break;
                }
              }
              if (!found) ctx.pages.push(ctx.m_GidPage);

            }
          });
        }

        ctx.pages.forEach((p, idx) => {
          if (p.id == "m-00" || p.id == "m-11" || p.id == "m-12") { //accueil or adhesion or infos
            ctx.pages.splice(idx, 1);
          }
        });

        console.log("pages==>", ctx.pages);
        ctx.pages.sort(function (a, b) {
          return a.id.localeCompare(b.id);
        });

        ctx.nav.setRoot(AnnuairePage);
        ctx.menuCtrl.open();

      }

      this.events.subscribe('user:doLogout', (xdate) => {
        console.log("DO LOGOUT");
        this.auth.logout();
        if(this.menuCtrl.isOpen()) this.menuCtrl.close();
      });

      this.events.subscribe('user:logout', (xdate) => {
        this.setPublicPages();
        this.nav.setRoot(LoginPage);
        
        this.userFullName = "";
      });

    });

  }

  /////////////////////////////////// Push /////////////////////////////////////////
  pushsetup() {
    const options: PushOptions = {
      android: {
        //  
        //senderID: '111670591426'
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'false'
      },
      windows: {}
    };

    const pushObject: PushObject = this.push.init(options);
    console.log("push init..");

    let isActiveNotif= this.myUtils.doGetPrefs('notification') ? Boolean(this.myUtils.doGetPrefs('notification')) : true;
    if(!isActiveNotif){
      pushObject.unregister()
    }

    pushObject.on('notification').subscribe((notification: any) => {
      
        if (notification.additionalData.foreground) {
          let youralert = this.alertCtrl.create({
            title: 'TGR notification',
            message: notification.message
          });
          youralert.present();
        }
      
    });

    pushObject.on('registration').subscribe((token: any) => {
      //do whatever you want with the registration ID
      console.log("!!!! registraction token", token);
      //this.showAlert("REGISTRACTION",JSON.stringify(registration));

      //TODO: get the appUserId from the session
      let registrationTokenObj = {
        "registrationToken": token.registrationId,
        "appUserId": "CompteDemo"
      }
      // this.postRegistrationToken(registrationTokenObj);
      this.postRegistrationTokenStorage(token.registrationId);

    });

    pushObject.on('error').subscribe(error =>
      // alert('Error with Push plugin' + error)
      console.log('Error with Push plugin' + error)
    );
  }
  /**
   *  
      {
          "registrationToken" : "xxxxx",
          "appUserId":"xxxxx"
      }
  
   *
   * https://api.tgr.gov.ma/api/devices 
   */

  postRegistrationToken(registrationToken) {
    this.pushServiceProvider.postPushRegistrationToken(registrationToken).subscribe(
      data => {
        // this.showAlert("", "Adhérent ajouté avec succes");
        console.info("Server response DATA! ", data);
        //redirect to login page.
        // this.nav.setRoot(LoginPage);

        let validationResponseObj = data;
        if (validationResponseObj.status == 200) {
          // set a key/value pair
          // this.storage.set(this.VALIDATION_RESPONSE_OBJ, this.validationResponseObj);
          console.log("registraction token sended successfully");

        }

        return true;
      },
      error => {
        console.error("Error saving token! " + error);
        //show a nice error message to the user
        if (error.status == 422) {
          // this.showAlert("Nous sommes désolés!", "Login indisponible, veuillez choisir un autre !");
          // this.showAlert("Nous sommes désolés!", "422 - Erreur d'enregistrement de l'adherent - Essayez un autre email !! ");
        } else
          if (error.status == 500) {
            // this.showAlert("Nous sommes désolés!", "500 - Service  d'enregistrement des adherents indisponible veuillez réessayer ultérieurement");
          } else {
            // this.showAlert("Nous sommes désolés!", "Service  d'enregistrement des device indisponible veuillez réessayer ultérieurement");
            console.log("Service  d'enregistrement des device indisponible veuillez réessayer ultérieurement");
          }

        // this.dismissLoading();
        return Observable.throw(error);
      },
      () => {
        // this.dismissLoading();
        console.log("postRegistrationToken Complete");
      }
    );
  }
  /**
   * //get text value from the translation service
   * @param language 
   */
  translationAPI(language) {
    this.localisationAndThemesProvider.translationAPI(language).subscribe(
      data => {
        console.info("Server translation response DATA! ", data);

        // if (data.json()[0].translation.home)
        // this.homeTranslation = data.json()[0].translation.home;
        // else
        // this.homeTranslation = "Home";
        // // return true;

        try {
          let jdata = JSON.parse(data["_body"])[0].translation.Home;
          console.log(jdata);

          this.homeTranslation = jdata;

          //get text value from the translation service
          this.m_LoginPage.title = this.homeTranslation;
        } catch (error) {
          console.warn(error);
        }

      },
      error => {
        console.log("Service indisponible veuillez réessayer ultérieurement");

      },
      () => {
        // this.dismissLoading();
        console.log("translationAPI Complete");
      }
    );
  }

  private PUSH_TOKEN = "push-token";
  postRegistrationTokenStorage(registrationToken) {
    this.storage.set(this.PUSH_TOKEN, registrationToken);
  }
  /////////////////////////////////// END Push ////////////////////////////////////


  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  doLogout() {
    let loading = this.loadingCtrl.create({
      content: 'Veuillez patientez svp...',
      dismissOnPageChange: true
    });
    loading.present();

    this.events.publish('user:doLogout', Date.now());
  }

  showAlert(title, message) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

  getThemes() {
    this.myUtils.doGet("https://api.tgr.gov.ma/api/Themes?filter=%7B%22where%22%3A%7B%22systemDomainId%22%3A%22mobile%22%20%7D%20%7D")
      .then(result => {
        console.warn(result);

        try {
          let themes = JSON.parse(result["_body"]);
          if (Array.isArray(themes)) {
            this.myUtils.doSetPrefs("themes", themes);
          }
        } catch (error) {
          console.log(error);

        }

      })
  }
}
