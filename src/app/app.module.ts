import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
//import { Camera, CameraOptions } from '@ionic-native/camera';

import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { TranslationServiceProvider } from '../providers/translation-service/translation-service';


import { GainPage } from '../pages/gain/gain';
import { BalancePage } from '../pages/balance/balance';

import { GidPage } from '../pages/gid/gid';
import { GidDetailPage } from '../pages/gid-detail/gid-detail';
import { InfosPage } from '../pages/infos/infos';
import { FAQPage } from '../pages/infos/infos';
import { MentionslegalesPage } from '../pages/infos/infos';
import { ConditionsGeneralesPage } from '../pages/infos/infos';
import { SecuritePage } from '../pages/infos/infos';



import { AccountsPage } from '../pages/accounts/accounts';
import { AccountDetailPage } from '../pages/account-detail/account-detail';
import { LoginPage } from '../pages/login/login';
import { HistoryPage } from '../pages/history/history';
import { HistoryByDatePage } from '../pages/history-by-date/history-by-date';
import { AnnuairePage } from '../pages/annuaire/annuaire';
import { AnnuaireParNomPage } from '../pages/annuaire-par-nom/annuaire-par-nom';

import { FonctionnairesPage } from '../pages/fonctionnaires/fonctionnaires';
import { OptionsList } from '../pages/fonctionnaires/fonctionnaires';

import { ActualitesPage } from '../pages/actualites/actualites';
import { AnnuaireDetailPage } from '../pages/annuaire-detail/annuaire-detail';
import { ReclamationPage } from '../pages/reclamation/reclamation';
import { ReclamationEntryPage } from '../pages/reclamation-entry/reclamation-entry';
import { SuiviReclamationsPage } from '../pages/suivi-reclamations/suivi-reclamations';

import { ReglagesPage } from '../pages/reglages/reglages';
import { ProfilPage } from "../pages/reglages/reglages";
import { EditionPage } from "../pages/reglages/reglages";

import { ModifyPwdPage } from "../pages/modify-pwd/modify-pwd";

import { ContribuableEntryPage } from '../pages/contribuable-entry/contribuable-entry';

import { ContribuablePage } from '../pages/contribuable/contribuable';
// import { SituationFiscaleArticlesSoldesPage } from '../pages/situation-fiscale-articles-soldes/situation-fiscale-articles-soldes';
import { SituationFiscalePage } from '../pages/situation-fiscale/situation-fiscale';
import { SituationFiscaleDetailPage } from '../pages/situation-fiscale-detail/situation-fiscale-detail';
import { AvisImpositionNonRecuPage } from '../pages/avis-imposition-non-recu/avis-imposition-non-recu';
// import { AvisImpositionNonRecuDetailPage } from '../pages/avis-imposition-non-recu-detail/avis-imposition-non-recu-detail';
import { ArticlesNonPayesPage } from '../pages/articles-non-payes/articles-non-payes';

import { RegisterStep2Page } from '../pages/register-step2/register-step2';
import { RegisterPage } from '../pages/register/register';
import { RegisterValidatePage } from '../pages/register-validate/register-validate';

import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password';
import { EditionProfilPage } from '../pages/edition-profil/edition-profil';

import { MyUtilsProvider } from '../providers/my-utils/my-utils';
import { BankServiceProvider } from '../providers/bank-service/bank-service';
import { AnnuaireServiceProvider } from '../providers/annuaire-service/annuaire-service';
import { FonctionnairesServiceProvider } from '../providers/fonctionnaires-service/fonctionnaires-service';
import { ReclamationServiceProvider } from '../providers/reclamation-service/reclamation-service';

import { GidServiceProvider } from '../providers/gid-service/gid-service';


import { UsersService } from '../providers/users.service/users.service';

import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';

import { AppMinimize } from '@ionic-native/app-minimize';
import { Network } from '@ionic-native/network';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CloudSettings, CloudModule } from '@ionic/cloud-angular';
import { ContribuableObserverProvider } from '../providers/contribuable-observer/contribuable-observer';

import { PushServiceProvider } from '../providers/push-service/push-service';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { ReplaceStrPipe } from '../pipes/replace-str/replace-str';
import { SyncDataProvider } from '../providers/sync-data/sync-data';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { LocalisationAndThemesProvider } from '../providers/localisation-and-themes/localisation-and-themes';
import { ConnectivityProvider } from '../providers/connectivity/connectivity';
import { TranslatePipe } from '../pipes/translation/translation-pipe'

import { IonicImageLoader } from 'ionic-image-loader';


const cloudSettings: CloudSettings = {
  'core': {
    'app_id': 'ma.gov.tgr.application'
  }
};
@NgModule({
  declarations: [
    MyApp,
    GainPage,
    BalancePage,
    TranslatePipe,
    LoginPage,
    AccountsPage,
    AccountDetailPage,
    HistoryPage,
    HistoryByDatePage,
    AnnuairePage,
    AnnuaireParNomPage,

    FonctionnairesPage,
    OptionsList,

    ActualitesPage,
    AnnuaireDetailPage,
    ReclamationPage,
    ReclamationEntryPage,
    SuiviReclamationsPage,
    ReglagesPage,
    ProfilPage,
    EditionPage,
    ModifyPwdPage,
    SituationFiscalePage,
    SituationFiscaleDetailPage,
    AvisImpositionNonRecuPage,
    ArticlesNonPayesPage,
    ContribuablePage,
    ContribuableEntryPage,
    RegisterPage,
    RegisterStep2Page,
    RegisterValidatePage,
    GidPage,
    GidDetailPage,

    InfosPage,
    SecuritePage,
    ConditionsGeneralesPage,
    MentionslegalesPage,
    FAQPage,

    ForgotPasswordPage,
    ReplaceStrPipe,
    EditionProfilPage

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: 'Retour'
    }),
    IonicStorageModule.forRoot(),
    IonicImageLoader.forRoot(),
    CloudModule.forRoot(cloudSettings),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    GainPage,
    BalancePage,

    LoginPage,
    AccountsPage,
    AccountDetailPage,
    HistoryPage,
    HistoryByDatePage,
    AnnuairePage,
    AnnuaireParNomPage,
    FonctionnairesPage,
    OptionsList,

    ActualitesPage,
    AnnuaireDetailPage,
    ReclamationPage,
    ReclamationEntryPage,
    SuiviReclamationsPage,
    ReglagesPage,
    ProfilPage, 
    EditionPage,
    ModifyPwdPage,
    SituationFiscalePage,
    SituationFiscaleDetailPage,
    AvisImpositionNonRecuPage,
    ArticlesNonPayesPage,
    ContribuablePage,
    ContribuableEntryPage,
    RegisterPage,
    RegisterStep2Page,
    RegisterValidatePage,
    GidPage,
    GidDetailPage,

    InfosPage,
    SecuritePage,
    ConditionsGeneralesPage,
    MentionslegalesPage,
    FAQPage,

    ForgotPasswordPage,
    EditionProfilPage

  ],

  providers: [
    Push,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthServiceProvider,
    TranslationServiceProvider,
    MyUtilsProvider,
    BankServiceProvider,
    AnnuaireServiceProvider,
    FonctionnairesServiceProvider,
    ReclamationServiceProvider,
    GidServiceProvider,
    //Camera,
    File,
    FilePath,
    Transfer,
    StatusBar,
    SplashScreen,
    AppMinimize,
    Network,
    UsersService,
    ContribuableObserverProvider,
    PushServiceProvider,
    SyncDataProvider,
    InAppBrowser,
    LocalisationAndThemesProvider,
    ConnectivityProvider

  ]
})
export class AppModule { }
