export class Config {

  public static mainUrlSsl : string = 'https://api.tgr.gov.ma/api';


  public static getGraphGainsDepensesAPI(userName, rib, token){
    return 'https://api.tgr.gov.ma/api/AppUsers/'+encodeURI(userName)+'/'+encodeURI(rib)+'/grapheGainsDepenses?access_token='+encodeURI(token)
  }
  public static getLoginApi(){
    return Config.mainUrlSsl+'/AppUsers/login';
  }

  public static getLogoutApi(token){
    return Config.mainUrlSsl+'/AppUsers/logout?access_token='+encodeURI(token);
  }

  public static getUserLDAP(UserID, token){
    return  Config.mainUrlSsl+'/AppUsers/'+encodeURI(UserID)+'?access_token='+encodeURI(token);
  }

  public static getNewsApi(){
    return Config.mainUrlSsl+'/Actualites/getNews';
  }

  public static getHabilitationsApi(uid, token){
    return Config.mainUrlSsl+'/AppUsers/'+encodeURI(uid)+'/appRoles?access_token='+encodeURI(token)
  }

  public static getAnnuaireDirectionsCentrales(){
    return Config.mainUrlSsl+'/TypesPoste/directionsCentrales'
  }

  public static getAnnuaireDirectionsRegionalesApi(){
    return Config.mainUrlSsl+'/TypesPoste/directionsRegionales'
  }

  public static getAnnuaireDirectionApi(direction){
    return Config.mainUrlSsl+'/Postes/listResponsables?direction='+encodeURI(direction);
  }

  public static getSoldeBancaireApi(uid, rib, token){
    return Config.mainUrlSsl+'/AppUsers/'+encodeURI(uid)+'/'+encodeURI(rib)+'/soldeBancaire?access_token='+encodeURI(token)
  }

  public static getDernieresOperationsApi(rib){
    return Config.mainUrlSsl+'/Comptes/dernieresOperations?rib='+encodeURI(rib);
  }

  public static getEntreDatesOperationsApi(rib, date1, date2){
    return Config.mainUrlSsl+'/Comptes/entreDatesOperations?rib='+encodeURI(rib)+'&date1='+encodeURI(date1)+'&date2='+encodeURI(date2)
  }

  public static getSituationAdminsitrativeApi(ppr){
    return Config.mainUrlSsl+'/SituationAdministratives/situationAdminsitrative?ppr='+encodeURI(ppr)
  }

  public static getAttestationSalaireApi(ppr){
    return Config.mainUrlSsl+'/SituationAdministratives/attestationSalaire?ppr='+encodeURI(ppr)
  }

  public static getSituationPrelevementsApi(ppr){
    return Config.mainUrlSsl+'/SituationAdministratives/situationPrelevements?ppr='+encodeURI(ppr)
  }

  public static getAdherentsVerifierLoginApi(ppr){
    return 'https://api.tgr.gov.ma/Adherents/verifierLogin?login='+encodeURI(ppr);
  }

  public static getIdentifiantApi(referenceAvis, montantAvis){
    return Config.mainUrlSsl+'/Contribuables/getIdentifiant/'+encodeURI(referenceAvis)+'/'+encodeURI(montantAvis)
  }

  public static getVerifierFonctionnaireApi(ppr, dateEntree, netMensuel){
    return Config.mainUrlSsl+'/Adherents/verifierFonctionnaire?ppr='+encodeURI(ppr)+'&dateEntree='+encodeURI(dateEntree)+'&netMensuel='+encodeURI(netMensuel)
  }

  public static getAdherentsApi(){
    return Config.mainUrlSsl+'/Adherents'
  }

  public static getValiderEtatAdherentApi(){
    return Config.mainUrlSsl+'/EtatAdherents/validerEtatAdherent'
  }

  public static getEnregistrerAdherentApi(){
    return Config.mainUrlSsl+'/EtatAdherents/enregistrerAdherent'
  }

  public static getTypeReclamantsApi(){
    return Config.mainUrlSsl+'/TypeReclamants'
  }

  public static getNatureReclamationsApi(idTypeReclamant){
    return Config.mainUrlSsl+'/TypeReclamants/'+encodeURI(idTypeReclamant)+'/natureReclamations'
  }

  public static getVillesApi(){
    return Config.mainUrlSsl+'/Villes'
  }

  public static getPostesApi(idVille){
    return Config.mainUrlSsl+'/Villes/'+encodeURI(idVille)+'/postes'
  }

  public static getTypeActeurApi(typeReclamntID){
    return Config.mainUrlSsl+'/TypeReclamants/'+encodeURI(typeReclamntID)+'/typeActeur'
  }

  public static getActeursApi(typeActeurID){
    return Config.mainUrlSsl+'/TypeActeurs/'+encodeURI(typeActeurID)+'/acteurs'
  }

  public static getReclamationsApi(){
    return Config.mainUrlSsl+'/Reclamations'
  }

  public static getUploadApi(){
    return Config.mainUrlSsl+'/FilePoolers/repoPJ/upload'
  }

  public static getReponseReclamationsApi(){
    return Config.mainUrlSsl+'/ReponseReclamations'
  }
  public static getConsulterArticlesAReglerApi(codenature, reference, montant, annee){
    return Config.mainUrlSsl+'/Contribuables/consulterArticlesARegler?nature='+encodeURI(codenature)+'&reference='+encodeURI(reference)+'&montant='+encodeURI(montant)+']&annee='+encodeURI(annee)
  }

  public static getContribuablesApi(code, cc, re, mon, email, total, id){
    return Config.mainUrlSsl+'/Contribuables/getURL/'+encodeURI(code)+'/'+encodeURI(cc)+'/'+encodeURI(re)+'/'+encodeURI(mon)+'/'+encodeURI(email)+'/'+encodeURI(total)+'/'+encodeURI(id)
  }

  public static getContribuablesResultSApi(codcontribuable){
    return Config.mainUrlSsl+'/Contribuables/getResultS/'+encodeURI(codcontribuable)
  }

  public static getContribuablesResultNSApi(codcontribuable){
    return Config.mainUrlSsl+'/Contribuables/getResultNS/'+encodeURI(codcontribuable)
  }

  public static getEditionProfiAlpi(userId: string, token: string){
    
    return Config.mainUrlSsl+'/AppUsers/CompteDemo/'+encodeURI(userId)+'/modifierProfile?access_token='+encodeURI(token)
  
  }


/*
  public static getApi(){
    return
  }
  */

}
