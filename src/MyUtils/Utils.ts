import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';


export class Utils {

    public static vvv: string = '';
    public static alertCtrl: AlertController;
    public static loading: LoadingController;

    constructor(
        // alertCtrl: AlertController,
        // loading: LoadingController
    ) { }


    /**
      * Check status Error {401,404 ....} and show an alert box.
      * @param err  
    */
    public static checkErrorStatus(alertCtrl, err): void {
        if (err.status == 401) {
            this.showAlert(alertCtrl, '', 'Probleme d\'authentification.');
        }
    }

    ///////////////////////////////////// Alert box ////////////////////////////////
    /**
     * reuseable Alert box.
     */
    public static showAlert(alertCtrl, title, message) {
        let alert = alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }//END Alert

    ///////////////////////////////////// loader; ////////////////////////////////
    public static loader;
    /**
     * Show loading
     */
    public static presentLoading(loading, message = "Chargement des données...") {

        if (!this.loading) {
            // =========================================
            this.loader = loading.create({
                content: message, // "Chargement des données...",
                showBackdrop: true,
                enableBackdropDismiss: true,
                dismissOnPageChange: true
            });

            this.loader.onDidDismiss(() => {
                console.log('Dismissed loading');
            });

            this.loader.present();


            setTimeout(() => {
                this.loader.dismiss();
            }, 60000);
        }


    }

    /**
     * Dismiss loading
     */
    public static dismissLoading() {
        try {
            console.log('--Dismissed loading--');
            if (!this.loader.Dismissed) this.loader.dismissAll();
            // if (this.loading) {
            //     this.loader.dismiss();
            //     this.loading = null;
            // }
        } catch (err) { }

    }//////////////////////// END loader ///////////////////////////////////////

    formatedFLoat(str) {
        let res = String(parseFloat(str).toFixed(2)).replace('.', ',');
        if (res == 'NaN') res = "";
        return res;
    }
}